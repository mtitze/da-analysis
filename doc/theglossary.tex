\newglossaryentry{precompute_da}
{
  name=\texttt{sixdesk.da.precompute\_da},
  description={\;\\\textit{Input}
               \begin{itemize}[topsep=-4pt]
               \item[] \texttt{filenames}: A list of strings of SQL SixDeskDB files.
               \item[] \texttt{instance}: An instance of type \texttt{sixdesk.da.pre\_da}.
               \item[] \texttt{verbose}: (boolean, default: \texttt{True}) verbose mode.
               \item[] \texttt{**kwargs}: Arguments passed to the \texttt{run} routine of \texttt{instance}.
               \end{itemize}
               \textit{Description}\\
               Child of \texttt{sixdesk.notebook\_tools.six\_handler}. General routine to determine the border between
               stable and unstable motion for a list of SixDesk SQL files, based on
               a user-given method. Calls the \texttt{run} routine of \texttt{instance} in a loop}
}

\newglossaryentry{pre_da}
{
  name=\texttt{sixdesk.da.pre\_da},
  description={\;\\\textit{Input}
               \begin{itemize}[topsep=-4pt]
               \item[] \texttt{filename}: The name of the SQL SixDeskDB file.
               \item[] \texttt{turns}: A list of turns.
               \item[] \texttt{with\_errors}: (boolean, default: \texttt{False}) Compute the errors in the DA and the turn.
               \end{itemize}
               \textit{Description}\\
               Master class to load the amplitudes from a SixDeskDB SQL file and loop over the given seeds and turns, computing
               the stable and unstable particles. This is done by its \texttt{run} method. Keyworded arguments of \texttt{run} are passed
               to the \texttt{calc} method of the child class. The results are stored into the SQL database}
}

\newglossaryentry{pre_da_stable_unstable}
{
  name=\texttt{sixdesk.da.pre\_da\_stable\_unstable},
  description={\;\\\textit{Input}
               \begin{itemize}[topsep=-4pt]
               \item[] \texttt{filename}: The name of the SQL SixDeskDB file.
               \item[] \texttt{turns}: A list of turns.
               \item[] \texttt{with\_errors}: (boolean, default: \texttt{False}) Compute the errors in the DA and the turn.
               \end{itemize}
               \textit{Description}\\
               Child of \texttt{sixdesk.da.pre\_da}. This class groups together certain similar methods and finds stable
               and unstable particles for a given turn number and angle. Its internal \texttt{calc} method passes keyworded
               arguments to the routine
               \texttt{find\_stable\_unstable} with the option
               \texttt{min\_wrt\_turns} (boolean, default: \texttt{False}).
               If \texttt{min\_wrt\_turns = False}, then the stable and unstable particles are determined by an index difference of 1. 
               If \texttt{min\_wrt\_turns = True}, then the stable particle for the given turn is determined by the
               particle which will become next unstable (and has a smaller amplitude).\\\\
               \underline{Example:}\\\\
                amplitude\\
        \hphantom{index:} \hspace{-2px} 5  |---------$\bullet$ \\
        \hphantom{index:} \hspace{-2px} 4  |---$\bullet$\\ % unstable for turn $t_0$\\
        \hphantom{index:} \hspace{-2px} 3  |-----------------$\bullet$\\
        \hphantom{index:} \hspace{-2px} 2  |------------$\bullet$\\ % stable for turn $t_0$\\
        \hphantom{index:} \hspace{-2px} 1  |--------------------------$\bullet$\\
        index:  0  |--------------------------------$\bullet$\\
        \hphantom{index:} \hspace{6px} |\\
        \hphantom{index:} \hspace{-2px} \; +------|--------------------------- turn\\
        \hphantom{index:} \hspace{-2px} \; \hspace{0.8cm} $t_0$\\\\
        If \texttt{min\_wrt\_turns = False}, then for turn $t_0$:\\
        \texttt{index\_stable = 3}\\
        \texttt{index\_unstable = 4}\\\\
        If \texttt{min\_wrt\_turns = True}, then for turn $t_0$:\\
        \texttt{index\_stable = 2}\\
        \texttt{index\_unstable = 4}\\\\
        Be aware if there are already initial holes in the amplitudes, as this function is only working with the indices}
}

\newglossaryentry{pre_da_interpolate}
{
  name=\texttt{sixdesk.da.pre\_da\_interpolate},
  description={\;\\\textit{Input}
               see \texttt{sixdesk.da.pre\_da\_stable\_unstable}.\\
               \textit{Description}\\
               Child of \texttt{sixdesk.da.pre\_da\_stable\_unstable}. Linear interpolation of the DA between the unstable 
               and the stable particle for intermediate turns}
}

\newglossaryentry{pre_da_default}
{
  name=\texttt{sixdesk.da.pre\_da\_default},
  description={\;\\\textit{Input}
               see \texttt{sixdesk.da.pre\_da\_stable\_unstable}.\\
               \textit{Description}\\
               Child of \texttt{sixdesk.da.pre\_da\_stable\_unstable}. DA is computed according to the parent \texttt{calc} method
               (see \texttt{sixdesk.da.pre\_da\_stable\_unstable})}
}

\newglossaryentry{pre_da_original}
{
  name=\texttt{sixdesk.da.pre\_da},
  description={\;\\\textit{Input}
               see \texttt{sixdesk.da.pre\_da}.\\
               \textit{Description}\\
               Child of \texttt{sixdesk.da.pre\_da}. For details see Sec. \ref{subsec:pre_da_original}}
}



\newglossaryentry{compute_da}
{
  name=\texttt{sixdesk.da.compute\_da},
  description={\;\\\textit{Input}
               \begin{itemize}[topsep=-4pt]
               \item[] \texttt{filenames}: A list of strings of SQL SixDeskDB files.
               \item[] \texttt{verbose}: (boolean) verbose mode (default: \texttt{True}).
               \item[] \texttt{**kwargs}: Arguments passed to \texttt{sixdesk.da.davst}.
               \end{itemize}
               \textit{Description}\\
               Child of \texttt{notebook\_tools.six\_handler} to compute the DA, based on one of the methods of Sec. \ref{subsec:da},
               for the given list of SQL files}
}

\newglossaryentry{davst}
{
  name=\texttt{sixdesk.da.davst},
  description={\;\\\textit{Input}
               \begin{itemize}[topsep=-4pt]
               \item[] \texttt{filename}: The name of the SQL SixDeskDB file.
               \item[] \texttt{with\_errors}: (boolean, default: \texttt{False}) also propagate the errors in the stable amplitude to the DA.
               \end{itemize}
               \textit{Description}\\
               Child of \texttt{sixdesk.da\_instance}. Class to compute the DA, based on one of the methods of Sec. \ref{subsec:da},
               for an individual SQL file. Its \texttt{run} method has the following input:\\
               \texttt{emittances}: A list of emittance pairs to be used in the DA calculation.\\
               \texttt{method} (str, default: \texttt{simpson}) the integration method to be used. Currently implemented:
               \begin{itemize}[topsep=-4pt]
               \item[-] \texttt{simpson} Simpson integration method for open boundaries, Eq. (4.1.18) in Ref. \cite{bib:press1997}, p.129.
               \item[-] \texttt{trapezoid} Integration based on the trapezoid formula, Eq. (4.1.15) in Ref. \cite{bib:press1997}, p.129.
               \item[-] \texttt{uniform} Integration with uniform weights.
               \end{itemize}
               \texttt{save} (boolean, default: \texttt{True}) store the results in the SQL database.\\
               \texttt{verbose} (boolean, default: \texttt{True}) verbose mode to print additional information.\\
               \texttt{start\_index} (int, default: \texttt{None}) The start index of the given turns to be considered.\\
               \texttt{stop\_index} (int, default: \texttt{None}) The stop index of the given turns to be considered.\\
               \texttt{mode} (str, default: \texttt{classic}) The modus of the DA, see Sec. \ref{subsec:da}. Possible options:
               \begin{itemize}[topsep=-4pt]
               \item[-] \texttt{classic} Eq. \eqref{eq:da_classic}.
               \item[-] \texttt{rms} Eq. \eqref{eq:da_rms}.
               \item[-] \texttt{original} Eq. \eqref{eq:da_original}.
               \end{itemize}
               }
}

\newglossaryentry{da_instance}
{
  name=\texttt{sixdesk.da.da\_instance},
  description={\;\\\textit{Input}
               \begin{itemize}[topsep=-4pt]
               \item[] \texttt{filename}: The name of the SQL SixDeskDB file.
               \item[] \texttt{with\_errors}: (boolean, default: \texttt{False}) also propagate the errors in the stable amplitude to the DA.
               \end{itemize}
               \textit{Description}\\
               Child of \texttt{notebook\_tools.six\_instance}. Contains the basic functionality to store and load the relevant data to and
               from an individual SQL file}
}



