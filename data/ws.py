# tools to load and compute the emittances coming from the raw wirescanner data

from .profile_tools import prepare_shot, bi_gaussian
import matplotlib.pyplot as plt
from matplotlib import cm

from misc.general import helpsort
from .database_tools import pytimber_tools, offline_hdf_database, lhc_intensity_params

import numpy as np
import pandas as pd

from pyarrow import parquet as papq
from misc.general import append_to_parquet_table

from itertools import product
from scipy.optimize import curve_fit, least_squares
from scipy.integrate import simps

import time as timelib


def correlate_groups(group1, group2, span, t0=None, t1=None):
    '''
    This function will generate a list of correlation indices of two data sets with e.g.
    different time stamps (we have
    horizontal and vertical wire scanner measurements in mind) together if they are closer
    than 'timespan' [s].
    
    t0, t1 are optional boundaries within which we want to obtain the pairs.
    
    Example:
    --------
    span = 300
    group1 = [1200, 1340, 4609, 4525, 12043]
    group2 = [1103, 1242, 4700, 9800, 12000]
    
    I)
    correlate_groups(g1, g2, 300)
    returns: [(0, 0), (0, 1), (1, 0), (1, 1), (2, 2), (3, 2), (4, 4)]
    so that: |group1[k] - group2[j]| < span iff (k, j) in result
    
    II)
    correlate_groups(group1, group2, 300, t0=1200, t1=11000)
    returns [(1, 1), (2, 2), (3, 2)]
    '''

    group1 = np.array(group1)
    group2 = np.array(group2)
    n1, n2 = len(group1), len(group2)
    if t0 != None:
        wg10 = np.where(group1 > t0)
        wg20 = np.where(group2 > t0)
        group1 = group1[wg10]
        group2 = group2[wg20]
    if t1 != None:
        wg11 = np.where(group1 < t1)
        wg21 = np.where(group2 < t1)       
        group1 = group1[wg11]
        group2 = group2[wg21]
        
    result = []
    for k1 in range(len(group1)):
        e1 = group1[k1]
        for k2 in range(len(group2)):
            e2 = group2[k2]
            if abs(e1 - e2) < span:
                result.append((k1, k2))
  
    # now we have to map the indices back to the original ones. Note that the order of t0 and t1 check must be reversed.
    if t1 != None:
        result_tmp = np.copy(result)
        result = []
        for pair in result_tmp:
            pair1 = wg11[0][pair[0]], wg21[0][pair[1]] 
            result.append(pair1)
    if t0 != None:
        result_tmp = np.copy(result)
        result = []
        for pair in result_tmp:
            pair1 = wg10[0][pair[0]], wg20[0][pair[1]] 
            result.append(pair1)
            
    return result


class pytimber_ws_tools(pytimber_tools):
    def __init__(self):
        pytimber_tools.__init__(self)

    def get_wsdata(self, fill_nr, fundamentals, phys_key='INJPHYS'):
        '''
        Obtain the PyTimber wire scanner data of a specific LHC fill and beam number.
        inout: if not 'IN' or 'OUT' we will get both values
        '''

        t1, t2 = self.get_fill_timespan(fill_nr, phys_key=phys_key)
        pt_wsdata = self.db.get(list(fundamentals.values()), t1, t2)
        return pt_wsdata

    def get_wsdata_all(self, fills, fundamentals, verbose=True):
        '''
        Load the raw ws data from the PyTimber database.
        '''
        if verbose:
            print ('getting raw wire scanner data ...')
        
        wsdata = {}
        n_fills = len(fills)
        for fill_index in range(n_fills):
            fill = int(fills[fill_index])
            if verbose:
                print ('\rFill {}, {}/{}'.format(fill, fill_index + 1, n_fills), end='')
            wsdata[fill] = self.get_wsdata(fill_nr=fill, fundamentals=fundamentals)
        return wsdata


class pytimber_lhc_nemittances(pytimber_tools):
    '''
    Class to load the LHC normalized emittances (based on the wire scanner profiles, provided by a fit tool).
    '''

    def __init__(self, beam_nr):
        pytimber_tools.__init__(self)
        self.beam_nr = beam_nr

        nemit_fundamentals = {}
        lr = {1: 'R', 2: 'L'}
        for hv in ['H', 'V']:
            for io in ['IN', 'OUT']:
                nemit_fundamentals[(hv, io)] = 'LHC.BWS.5{}4.B{}{}.APP.{}:EMITTANCE_NORM'.format(lr[beam_nr], beam_nr, hv, io)
        self.nemit_fundamentals = nemit_fundamentals

    def _get_nemit_data_fill(self, fill_nr, shot_nr=None, minfit=1e-1, maxfit=10, bunch_indices=[], check=False):
        '''
        For a given fill number and beam number, obtain LHC wire scanner data at injection. The data
        is returned in form of tuples, where the first tuple denotes the measurement time and the second one
        normalized emittances of the bunches from the tool in the control room.

        The tool used an automatic fitting procedure to obtain these emittances.
    
        minfit, maxfit: some basic bounds to prevent getting bad data (i.e. zeros or very large values)
        only if check == True.
        '''

        if not hasattr(self, 'nemit_fundamentals'):
            print ('ERROR: Run set_nemit_fundamentals first.')
            return

        lr = {1: 'R', 2: 'L'}
        ws_data = {}
        for direction, inout in self.nemit_fundamentals.keys():
            ws_string = self.nemit_fundamentals[(direction, inout)]
            raw_data = self.get_data(fill_nr, ws_string)

            if raw_data == None:
                continue

            data = raw_data[0][ws_string]
            if len(data[0]) == 0: # then no data; skip this case
                continue

            if shot_nr != None:
                times = [data[0][shot_nr]]
                nemittances = [data[1][shot_nr]]
            else:
                times = data[0]
                nemittances = data[1]

            if len(bunch_indices) > 0:
                nemittances_new = []
                for nemittance in nemittances:
                    nemittances_new.append(nemittance[(bunch_indices,)])
                nemittances = nemittances_new

            if check:
                nemittances_new = []
                for nemittance in nemittances:
                    if np.mean(nemittance) > minfit and np.mean(nemittance) < maxfit:
                        nemittances_new.append(nemittance)
                nemittances = nemittances_new

            ws_data[(direction, inout)] = [times, nemittances]
        return ws_data

    def get_nemit_data_all(self, fills, verbose=True):
        '''
        Loop get_nrmit_data over all fills of interest.
        '''
        ws_data_all = {}
        k = 1
        for fill in fills:
            if verbose:
                print ('\rFill {}, {}/{}'.format(fill, k, len(fills)), end='')
            ws_data = self.get_nemit_data(fill)
            if len(ws_data.keys()) > 0:
                ws_data_all[fill] = ws_data
            k += 1
        return ws_data_all


class lhc_ws_params(lhc_intensity_params):
    '''
    Stores the PyTimber LHC fundamentals related to wire scanner measurements.
    '''
    def __init__(self, **kwargs):
        lhc_intensity_params.__init__(self, **kwargs)
        self._set_wirescanner()

        # LHC wire scanner default variables to control .profile_tools.simple_noise_check
        self.noise_check_args = {'threshold': 5, 'window_length': 31, 'polyorder': 4, 'min_width': 3, 'offset_perc': 30}

    def _set_wirescanner(self, direction=['H', 'V'], inout=['IN', 'OUT']):
        '''
        Set the PyTimber fundamental variables for the LHC wire scanner.
        '''

        fundamentals = {}
        nemit_fundamentals = {}

        # beam_nr-dependent quantities:
        lr = {1: 'R', 2: 'L'}
        device_number = {1: {'H': 2, 'V': 2}, 2: {'H': 1, 'V': 2}} 
        # combinations of the form "1: (1, 2), (2, 1)" in H or others in V do not yield full data.

        for hv in direction:

            dv = device_number[self.beam_nr][hv]

            for io in inout:

                device_name = 'LHC.BWS.5{}4.B{}{}'.format(lr[self.beam_nr], self.beam_nr, hv)
                ws_device_name = '{}{}'.format(device_name, dv)

                fundamentals[(hv, 'position', io)] = '{}:PROF_POSITION_{}'.format(ws_device_name, io) #db.tree.LHC.Beam_Instrumentation.Beam_Profile.Wire_Scanners.B1H2.LHC_BWS_5R4_B1H2_PROF_POSITION_OUT
                fundamentals[(hv, 'value', io)] = '{}:PROF_DATA_{}'.format(ws_device_name, io) # db.tree.LHC.Beam_Instrumentation.Beam_Profile.Wire_Scanners.B1H2.LHC_BWS_5R4_B1H2_PROF_DATA_OUT
                fundamentals[(hv, 'nscans', io)] = '{}:NB_PTS_SCAN_{}'.format(ws_device_name, io) # db.tree.LHC.Beam_Instrumentation.Beam_Profile.Wire_Scanners.B1H2.LHC_BWS_5R4_B1H2_NB_PTS_SCAN_OUT
                nemit_fundamentals[(hv, io)] = '{}.APP.{}:EMITTANCE_NORM'.format(device_name, io)
            fundamentals[(hv, 'date')] = '{}:ACQ_TIME'.format(ws_device_name, hv) # db.tree.LHC.Beam_Instrumentation.Beam_Profile.Wire_Scanners.B1H2.LHC_BWS_5R4_B1H2_ACQ_TIME
        self.fundamentals = fundamentals
        self.nemit_fundamentals = nemit_fundamentals


class ws_database(offline_hdf_database):
    ''' 
    Class to load/store the pytimber wire scanner data from the PyTimber tools to HDF and to operate on this data.
    '''
    def __init__(self, filename, **kwargs):
        offline_hdf_database.__init__(self, filename=filename, **kwargs)

        # To denote wire scanner database tables
        self._wirescanner_table_prefix = 'ws'
        self._wirescanner_fills_info = 'ws_fills'

        self._nemittance_table_prefix = 'nemit'
        self._nemittances_fills_info = 'nemit_fills'

        self._processed_profiles_prefix = 'wsp'
        self._processed_profiles_info = 'stored_profiles'
        self._processed_profiles_min_itemsize = {'inout': 3, 'name': len(self._processed_profiles_info) + 16} # used for HDF append

    def store_nemit(self, data, verbose=True):
        '''
        Stores the normalized emittances from LHC EMITTANCE_NORM data (in table format).
        The data can be obtained via the class pytimber_lhc_nemittances from PyTimber.

        The tables will get the following form (e.g.):
            nemit_H_OUT_6052
        '''
        if verbose:
            print ('Storing BSR emittances into tables (e.g.) "{}_H_IN_<fill_nr>" ...'.format(self._nemittance_table_prefix))

        hdfstore = pd.HDFStore(self.filename, mode='a')

        fills = sorted(data.keys())
        n_fills = len(fills)
        fill_information = [] # to track the table names which will be stored in the database
        for fill_index in range(n_fills):
            fill = int(fills[fill_index])
            if verbose:
                print ('\rProcessing fill {}, {}/{}'.format(fill, fill_index + 1, n_fills), end='')
    
            fill_data = data[fill]
            for case in fill_data.keys():
                data_case = fill_data[case]
                try:
                    timestamps = data_case[0]
                except:
                    if verbose:
                        print ('Data of fill {}, case {} not available.'.format(fill, case))
                    continue
        
                emittances_all_bunches_all_times = data_case[1]
        
                # we concatenate the data of all respective times
                n_timestamps = len(timestamps)
                all_bunch_indices, all_nemittances = [], []
                all_times = []
                for time_index in range(n_timestamps):
                    time = timestamps[time_index]
                    emittances_all_bunches = emittances_all_bunches_all_times[time_index]
                    n_bunches = len(emittances_all_bunches)
                    bunch_indices = range(1, n_bunches + 1)
            
                    all_bunch_indices.append(bunch_indices)
                    all_nemittances.append(emittances_all_bunches)
                    all_times.append([time]*n_bunches)
            
                all_bunch_indices = np.hstack(all_bunch_indices)
                all_nemittances = np.hstack(all_nemittances)
                all_times = np.hstack(all_times)
                tohdf = pd.DataFrame(np.array([all_times, all_bunch_indices, all_nemittances]).transpose(), 
                             columns=['timestamp', 'bunch_index', 'emittance'])
                table_name = '{}_{}_{}'.format(self._nemittance_table_prefix, '_'.join(case), fill)
                hdfstore.put(value=tohdf, key=table_name, format='t', data_columns=True)
                fill_information.append([fill] + list(case) + [table_name])

        if len(fill_information) > 0:
            # also store the fills used:
            info_table_name = '{}'.format(self._nemittances_fills_info)
            if verbose:
                print ('\nStoring fill information into table "{}"'.format(info_table_name))
            tohdf_info = pd.DataFrame(fill_information, columns=['fill', 'direction', 'inout', 'repetition', 'name'])
            hdfstore.put(value=tohdf_info, key=info_table_name, format='t', data_columns=True)
        hdfstore.close()

    def store_wsdata(self, data, verbose=True, vverbose=False):
        '''
        Store wire scanner raw data to HDF file (in table format: For fast access without the need to load everything into memory).

        The data will be stored using a table name structure of the following form: (e.g.)
            H_position_IN_5970

        Note that self.set_beam must be called with a class having a similar functionality as lhc_ws_params.
        '''

        if not hasattr(self, 'beam'):
            print ('ERROR: set_beam has to be initiated first.')
            return

        if verbose:
            print ('\nStoring raw wire scanner data into tables (e.g.) "{}_H_position_IN_<index>_<fill_nr>" ...'.format(self._wirescanner_table_prefix))

        rel_fundamentals = {key: self.beam.fundamentals[key] for key in self.beam.fundamentals.keys() if key[1] not in ['date', 'nscans']}
        hdfstore = pd.HDFStore(self.filename, mode='a')

        fills = sorted(data.keys())
        n_fills = len(fills)
        fill_information = [] # to track the table names which will be stored in the database
        for fill_index in range(n_fills):
            fill = int(fills[fill_index])
            if verbose:
                print ('\rProcessing fill {}, {}/{} ...'.format(fill, fill_index + 1, n_fills), end='')
    
            for fkey in rel_fundamentals.keys():
                table_data = data[fill][rel_fundamentals[fkey]]
        
                timestamps = table_data[0]
                n_timestamps = len(timestamps)
        
                if n_timestamps == 0:
                    if vverbose:
                        print ('Fill {}, case {} has no data. Skipping.'.format(fill, fkey))
                    continue
            
                # The data from PyTimber is usually weird enough that data for different
                # time stamps can have different lengths, even if the instrument is the same.
                # On the other hand, the individual shots contain the information on the bunch indices.
                # That's why we do not concatenate everything together but store them in differently indexed tables.
                content = table_data[1]
        
                for time_index in range(n_timestamps):
                    content_t = content[time_index]
                    if len(content_t) == 0:
                        if vverbose:
                            print ('Fill {}, case {}, time_index {} has no data. Skipping.'.format(fill, fkey, time_index))
                        continue

                    # see e.g.
                    # https://stackoverflow.com/questions/44882211/read-specific-columns-from-hdf5-file-and-pass-conditions
                    tohdf = pd.DataFrame(np.array([content_t]).transpose(), columns=['value'])
                    table_name = '{}_{}_{}_{}'.format(self._wirescanner_table_prefix, '_'.join(fkey), time_index, fill)
                    hdfstore.put(value=tohdf, key=table_name, format='t', data_columns=True)
                    fill_information.append([fill] + list(fkey) + [time_index, timestamps[time_index], table_name])

        if len(fill_information) > 0:
            # also store the fills used:
            info_table_name = '{}'.format(self._wirescanner_fills_info)
            if verbose:
                print ('\nStoring fill information into table "{}"'.format(info_table_name))
            tohdf_info = pd.DataFrame(fill_information, columns=['fill', 'direction', 'type', 'inout', 
                                                                 'repetition', 'timestamp', 'name'])
            hdfstore.put(value=tohdf_info, key=info_table_name, format='t', data_columns=True)
        hdfstore.close()

    @staticmethod
    def _get_bunch_ws_profiles(xdata, ydata, non_empty_slots, slot_indices):
        '''
        Helper function to obtain the profiles of those bunches in which whe are interested. The raw wire scanner
        data contains a measurement of all profiles.
        '''
        fill_index = 0
        lp = len(xdata)
        y_all, bunch_indices = [], []
        for k in range(len(non_empty_slots)):

            if fill_index == len(slot_indices):
                break
            bunch_index = slot_indices[fill_index]

            if non_empty_slots[k] == bunch_index:
                fill_index += 1

                y = ydata[lp*k:lp*(k + 1)]
                # It can also happen that ydata does not contain the bunch data of the entire fill
                # I don't know why... but we have to catch this, so in this case we just do not get all selected bunches.
                # In such weird cases, ydata does not contain any data. Therefore we check this.
                if len(y) != lp:
                    continue

                y_all.append(y)
                bunch_indices.append(bunch_index)

        return y_all, bunch_indices

    def _process_profiles_fill(self, fill_nr, cases={'H': ['IN', 'OUT'], 'V': ['IN', 'OUT']}, hdfstore=None,
                               pilot_threshold=1e8, store_raw=False, bg_normalize=[0.8, 900.0, 1200.0, 0.0, 0.0, 1e7], 
                               verbose=False, **kwargs):
        '''
        Reads a raw wire scanner data table and determines, based on the filling time schemes, the respective bunch indices.

        cases: Specify which cases we want to consider. For example we could consider only the 'IN' scans of the
               horizontal scans. Default: {'H': ['IN', 'OUT'], 'V': ['IN', 'OUT']}

        store_raw: Also store the raw data in the tables.

        pilot_threshold: The wire scanner data usually also contains the pilot beam. In order to find its position in the filling
                         scheme, and to sort out the pilot beam, we have to lower the threshold accordingly.

        bg_normalize: list of 6 parameters: Attempt to normalize the curves by using a bi_gaussian fit to remove any offset beforehand.
        The list corresponds to the start parameters of the fitting procedure. If the list is empty, then this procedure is not executed.
        --> note that this procedure takes extra time.
        '''
        close_hdf = False
        if hdfstore == None:
            hdfstore = pd.HDFStore(self.filename, mode='a')
            close_hdf = True

        # information of the wire scanner data for this fill
        ws_keys_fill = hdfstore.select(key=self._wirescanner_fills_info, where='"fill" == {}'.format(fill_nr))

        # information of the selected fills for this fill. We will process only the wire scanner data of the selected bunches,
        # i.e. the fill number must be chosen from a subset of the valid fills of these selected fills.
        fill_information = hdfstore.select(key='{}_{}'.format(self._selected_fills_prefix, fill_nr))
        slot_indices = sorted(fill_information['slot_index'].unique()) # the slot numbers of the respective bunches

        # the times within the bunches of our selection are present in the machine (phase)
        bunch_time_start, bunch_time_end = min(fill_information['timestamp']), max(fill_information['timestamp'])

        if verbose:
            print ()

        wsdata_processed = {} # processed wire scanner data of specific fill
        for hv in cases.keys():
            for io in cases[hv]:
                case_info = ws_keys_fill.loc[(ws_keys_fill['direction'] == hv) & (ws_keys_fill['inout'] == io)]
                if len(case_info) == 0:
                    continue

                repetitions = case_info['repetition'].unique() # sometimes the operators have done several measurements

                for time_index in repetitions:
                    # check if both position and value entries for the respective repetition exists in the database.
                    position_label = case_info.loc[(case_info['repetition'] == time_index) & (case_info['type'] == 'position')]
                    value_label = case_info.loc[(case_info['repetition'] == time_index) & (case_info['type'] == 'value')]
                    if len(position_label) != 1 or len(value_label) != 1:
                        # in some cases it can happen that e.g. position data is stored, but no values for the specific shot.
                        # we will skip these weird cases.
                        if verbose:
                            print ('Error at repetition {}:\n  len(position_label) = {}\n  len(value_label) = {}'.format(time_index, len(position_label), len(value_label))) 
                        continue

                    # obtain the data from the database.
                    wirescanner_p = hdfstore.select(key=position_label['name'].values[0])
                    wirescanner_v = hdfstore.select(key=value_label['name'].values[0]) 

                    # for the wire scanners we stored identical times in the table, so the first time is sufficient
                    ws_measurement_time = position_label['timestamp'].values[0]
                     
                    if ws_measurement_time < bunch_time_start or bunch_time_end < ws_measurement_time:
                        # if the wire scanner measurement time does not fit to the bunch times for some reason, we skip this case
                        if verbose:
                            print ('Error at repetition {}:'.format(time_index))
                            print (' ws_measurement_time: {}'.format(ws_measurement_time))
                            print ('    bunch_time_start: {}'.format(bunch_time_start))
                            print ('      bunch_time_end: {}'.format(bunch_time_end))
                        continue

                    # now get the filling scheme at the time of the wire scanner measurement. 
                    # We lower the threshold to get the pilot beam as well, as this beam will be contained in the wire scanner profiles
                    scheme = self.get_filling_scheme(fill_nr, hdfstore=hdfstore,
                             where='"intensity" > "{}" and "timestamp" <= "{}"'.format(pilot_threshold, ws_measurement_time))

                    # the non-empty slot numbers at the time of the wire scanner measurement:
                    non_empty_slots = sorted(scheme['intensities']['slot_index'].unique())

                    # Now we are ready to extract the various bunch profiles from the raw data, taking into account the
                    # proper bunch indices
                    xdata = wirescanner_p['value'].values
                    ydata_all, bunch_indices = self._get_bunch_ws_profiles(xdata=xdata, 
                                                                           ydata=wirescanner_v['value'].values,
                                                                           non_empty_slots=non_empty_slots,
                                                                           slot_indices=slot_indices)

                    # prepare the profiles to be used for fitting etc.
                    xdata_all_prepared, ydata_all_prepared = [], []
                    bunch_indices_prepared = []
                    n_ydata = len(ydata_all)
                    for k in range(n_ydata):
                        x, y, ok, message = prepare_shot(xdata, ydata_all[k], bg_normalize=bg_normalize, 
                                                         verbose=False, **self.beam.noise_check_args)
                        if not ok:
                            if verbose:
                                print (message)
                            continue
                        xdata_all_prepared.append(x)
                        ydata_all_prepared.append(y)
                        bunch_indices_prepared.append(bunch_indices[k])

                    n_prepared = len(bunch_indices_prepared)
                    if n_prepared == 0:
                        if verbose:
                            print ('Error at repetition {}:'.format(time_index))
                            print ('No prepared profiles found.')
                        continue

                    # store profiles to HDF database ...
                    table_name = '{}_{}_{}_{}_{}'.format(self._processed_profiles_prefix, hv, io, time_index, fill_nr)
                    if verbose:
                        print ('\rWriting to table "{}" ...'.format(table_name), end='')

                    columns = ['x_{}'.format(int(bi)) for bi in bunch_indices_prepared] + \
                              ['y_{}'.format(int(bi)) for bi in bunch_indices_prepared]
                    tohdf = xdata_all_prepared + ydata_all_prepared
                    if store_raw:
                        columns += ['x_raw'] + ['y_raw_{}'.format(k + 1) for k in range(n_ydata)]
                        tohdf += [xdata] + ydata_all
                              
                    # sometimes the prepared x and y-data can have different lengths. The remaining values are added with NaNs.
                    df = pd.concat([pd.DataFrame({columns[k]: tohdf[k]}) for k in range(len(columns))], axis=1)
                    hdfstore.put(value=df, key=table_name, format='t', data_columns=True)

                    tohdf_info = pd.DataFrame([[fill_nr, hv, io, time_index, n_prepared, ws_measurement_time, table_name]],
                                              columns=['fill', 'direction', 'inout', 'repetition', 'n_profiles', 'timestamp', 'name'])
                    hdfstore.append(value=tohdf_info, key=self._processed_profiles_info, format='t', data_columns=True,
                                         min_itemsize=self._processed_profiles_min_itemsize)
        if close_hdf:
            hdfstore.close()

    def process_profiles(self, verbose=True, vverbose=False, **kwargs):
        '''
        Traverse all wire scanner data (their fills must be a subset of all selected_fills) and store the processed profiles
        into the database.
        '''

        if not hasattr(self, 'beam'):
            print ('ERROR: beam must be set first.')
            return

        # in this case information of the time spans within we consider the beam profiles can be extracted    
        hdfstore = pd.HDFStore(self.filename, mode='a')
        fills = sorted(hdfstore.select(self._wirescanner_fills_info)['fill'].unique())

        if verbose:
            print ('Resetting table "{}"'.format(self._processed_profiles_info))
        try:
            hdfstore.remove(key=self._processed_profiles_info)
        except KeyError:
            pass

        n_fills = len(fills)
        for fill_index in range(n_fills):
            fill = int(fills[fill_index])
            if verbose:
                print ('\rProcessing fill {}, {}/{} ...'.format(fill, fill_index + 1, n_fills), end='')
            self._process_profiles_fill(fill_nr=fill, verbose=vverbose, hdfstore=hdfstore, **kwargs)

        # The indices of the info file are all 0, we therefore reset the index
        try:
            info_db_old = hdfstore.select(self._processed_profiles_info)
            info_db = info_db_old.reset_index(drop=True)
            hdfstore.put(value=info_db, key=self._processed_profiles_info, format='t', data_columns=True)
        except:
            if verbose:
                print ('\nERROR: Check table "{}"'.format(self._processed_profiles_info))
            pass
        hdfstore.close()


def get_emittances(database, lookup_key, drop_nans=True, verbose=False):
    '''
    Obtain the emittances to be used for SixDB analysis from a given Pandas dataframe. If the number of unique keys
    is smaller than the length of the database, averaged values are returned (using the median).
    '''
    if verbose:
        print (' looking for column: {}'.format(lookup_key))

    if drop_nans:
        n_table = len(database)
        database = database.dropna() # we remove the NaNs
        if verbose:
            print (' ... dropped {} out of {} rows which contained NaNs.'.format(n_table - len(database), n_table))

    unique_keys = database[lookup_key].unique()
    if len(unique_keys) < len(database):
        sorted_keys = sorted(unique_keys)
        emittances = []
        for key in sorted_keys:
            data_key = database.loc[database[lookup_key] == key]
            ex = np.median(data_key['enx'].values)
            ey = np.median(data_key['eny'].values)
            emittances.append((ex, ey))
    else:
        ex = database['enx'].values
        ey = database['eny'].values
        emittances = [tpl for tpl in zip(ex, ey)]

    return emittances


class profile_model(ws_database):
    '''
    Generic class to compute and display the rms values of a given database of profiles.

    Furthermore it will contain methods to compute the beam losses from the database.
    '''
    def __init__(self, filename, **kwargs):
        ws_database.__init__(self, filename=filename, **kwargs)
        self.description = '<generic>'
        self._profile_model_table_name = 'profile_model'
        self._profile_emittance_table_name = 'emit_model'
        self._prepared_table_name = 'scaled_data'
        self.loss_model_description = 'generic'

    def generate_model_table(self, verbose=True, **kwargs):
        '''
        This function will generate a table containing the fit parameters of all horizontal/vertical profile pairs,
        which are considered as an individual bunch measurement, and can later be taken into consideration if
        calculating emittances and beam losses.

        Requirements: A table of processed profiles must be present in the database.

        **kwargs are passed to the respective _get_profile_fit_parameters_fill function of the individual method.
        '''

        if verbose:
            print ('Modus: {}'.format(self.description))
            print ('Generating table "{}" ...'.format(self._profile_model_table_name))

        hdfstore = pd.HDFStore(self.filename, mode='a')
        try:
            stored_profiles_info = hdfstore.select(key=self._processed_profiles_info)
        except Exception as ex:
            print (ex)
            hdfstore.close()
            return
        fills = sorted(stored_profiles_info['fill'].unique())
        n_fills = len(fills)
        tohdf = []
        for fill_index in range(n_fills):
            fill = int(fills[fill_index])
            if verbose:
                print ('\rFill {}, {}/{}'.format(fill, fill_index + 1, n_fills), end='')
            tohdf.append(self._get_profile_fit_parameters_fill(fill_nr=fill, verbose=verbose, hdfstore=hdfstore, **kwargs))

        tohdf = pd.concat(tohdf, ignore_index=True)

        if len(tohdf) == 0:
            print ('\nERROR: no data.')

        hdfstore.put(value=tohdf.astype({'fill': 'int', 'slot_index': 'int', 'bunch_index': 'int', 'bunch_count': 'int'}), 
                          key=self._profile_model_table_name, format='t', data_columns=True)
        hdfstore.close()


    def compute_emittances(self, energy_gev, dpp, store=True, verbose=True, **kwargs):
        '''
        Computes the emittances of a subset of fitted profiles. The selection can be specified
        by a 'where' statement:
 
        **kwargs are passed to hdfstore.select.

        store: True: Save the table to the given HDF database.
               False: The Pandas table will be returned

        xlim, ylim: optional tuples to prevent outliers in the data (i.e. outliers will be ignored).

        Example:
        ========
        On 2017 LHC data using Ioct = 40:
        self.compute_emittances(dpp=0, energy_gev=450, where='"rmsh" < 1200 and "rmsv" > 1160 and "rmsv" < 1600')
        '''

        if not hasattr(self, 'beam'):
            print ('ERROR: set_beam call required to provide beam optics information.')
            return

        if verbose:
            print ('Reading table:\n "{}"'.format(self._profile_model_table_name))
        selected_profiles = self.get_table(table_name=self._profile_model_table_name, **kwargs)
        ex = self.beam.sigma2emit(sigma=selected_profiles['rmsh'].values, hv='H', dpp=dpp, energy_gev=energy_gev)
        ey = self.beam.sigma2emit(sigma=selected_profiles['rmsv'].values, hv='V', dpp=dpp, energy_gev=energy_gev)

        # create the table with emittances
        # n.b. hdf does not provide good updating of existing tables, so we will just create a new one.
        selected_profiles['enx'] = ex
        selected_profiles['eny'] = ey

        if store:
            table_name = self._profile_emittance_table_name
            hdfstore = pd.HDFStore(self.filename, mode='a')
            hdfstore.put(value=selected_profiles, key=table_name, format='t', data_columns=True)
            hdfstore.close()
            if verbose:
                print ('Table "{}" created.'.format(table_name))
        else:
            return selected_profiles

    def plot_emittances(self, data='', title='', s=2, filename='', xlim=[], ylim=[],
                        show_medians=True, figsize=(8, 6), verbose=True):
        '''
        Plot the emittances 'enx' and 'eny' of the given data frame. The fit quality is indicated by
        the 'profile_chi2'-values using a color coding. If no data frame is provided, then the data
        is taken from the table self._profile_emittance_table_name.
        '''
        if len(data) == 0:
            data = self.get_table(self._profile_emittance_table_name)
 
        fig = plt.figure(figsize=figsize)
        ax = fig.add_axes([0.15, 0.15, 0.6, 0.7])

        if len(title) > 0:
            ax.set_title(title, loc='left')

        datax = data['enx'].values
        datay = data['eny'].values
        scat = ax.scatter(datax, datay, s=s, 
                          c=data['profile_chi2'].values, cmap=plt.cm.coolwarm)
        ax.set_xlabel(r'$\epsilon_{n, x}$ [mm mrad]')
        ax.set_ylabel(r'$\epsilon_{n, y}$ [mm mrad]')
        if len(xlim) > 0:
            ax.set_xlim(xlim)
        if len(ylim) > 0:
            ax.set_ylim(ylim)

        if show_medians:
            medx = np.median(datax)
            medy = np.median(datay)

            if verbose:
                print ('ex_med, ey_med:\n {}, {}'.format(medx, medy))

            x_limits = ax.get_xlim()
            y_limits = ax.get_ylim()

            ax.hlines([medy], xmin=x_limits[0], xmax=x_limits[1], linestyle='--', color='black')
            ax.vlines([medx], ymin=y_limits[0], ymax=y_limits[1], linestyle='--', color='black', 
                      label='med. ({:.3f}, {:.3f})'.format(medx, medy))
            ax.legend()

        cbaxes = fig.add_axes([0.78, 0.15, 0.04, 0.7]) 

        cbar = plt.colorbar(scat, cax=cbaxes)
        cbar.set_label(r'$\chi^2$', rotation=90)

        if len(filename) > 0:
            plt.savefig(filename, dpi=300, format='png')
        fig.canvas.draw()

    ############################################
    # Methods to prepare beam loss computation #
    ############################################

    def generate_profile_parameters_table(self, freq=None, profile_table=None, verbose=False):
        '''
        Obtain the profile fit parameter table from the HDF database and compute the turns, based on a given freuquency.

        For performance reasons we convert the strings denoting the IN/OUT wirescanner information to numbers
        a negative value means an IN-scan, a positive value an OUT scan. 
        The absolute value denotes the repetition number.

        The table itself is also returned for further analysis.
        '''

        if freq == None:
            freq = self.beam.freq

        if verbose:
            print ('    Frequency: {}'.format(freq))

        if len(profile_table) == 0:
            if verbose:
                print ('Reading profile fit parameters from table:\n "{}"'.format(self._prepared_table_name))
            profile_table = self.get_table(self._prepared_table_name)

        hv_to_int = lambda x: {'IN': -1, 'OUT': 1}[x.split('_')[2]]*(int(x.split('_')[3]) + 1) # we change the strings to integer here, because when we compute the beam losses we will deal with datablocks which should contain only floats.
        profile_table['nameh'] = profile_table['nameh'].apply(hv_to_int)
        profile_table['namev'] = profile_table['namev'].apply(hv_to_int)
        profile_table['turn'] = profile_table['duration'].apply(lambda x: x*freq)
        return profile_table.rename(columns={'nameh': 'wsh_inout', 'namev': 'wsv_inout'})

    def prepare_sim_loss_with_bsrt(self, duration_time=None, scale_to=1, reg_tol=[120, 800], 
        duration_window=[], drop_pos_losses=True, profile_chi2_perc=100, verbose=True, store=True):
        '''
        Prepare the table containing the profile fit parameters, based on the time-evolution of
        the BSRT emittances, with respect to an optional time window [s].

        duration_time (default: None): If a duration time is provided, then extrapolate all quantities to the given
                                       duration time.

        scale to (default: 1): If T0 is the time at which the bunch intensity measurement starts 
                               and T1 the time at the end (where we usually want to compute the beam loss) then, 
                               based on the considered scaling factors, scale all values to 
                                  scale_to*(T1 - T0) + T0.
                               Because this scaling depends on T0 and T1, it can be different from case to case.

                               This factor is not used if duration_time != None.
        
        duration_window: An optional window to consider only fills having a specific duration length 
                         within the given window.
        
        reg_tol: Tolerances within which the start and end values of the timestamps of the BSRT data agree to the
                 corresponding start and end times of the intensity data. This is required to prevent inclusion
                 of cases having small BSRT times, which would consequently lead to artifacts in the extrapolation
                 to large times.
        drop_pos_losses: If True, we do not record cases belonging to 'positive' losses.

        profile_chi2_perc: If <100, then only those profiles will be considered by which profile_chi2 is smaller
                           than the given percentile.

        store: If True, then store the table back to the database.

        In the final database, emit_index will be an index counting all the different bunches of all valid fills.
        '''
        if verbose:
            print ('Reading emittances from table:\n "{}"'.format(self._profile_emittance_table_name))
            print ('Reading losses from table:\n "{}"'.format(self._loss_table_name))
            print ('Reading BSRT data from table:\n "{}"'.format(self._bsrt_regression_info))

        loss_table_kwargs = {}
        if len(duration_window) > 0:
            loss_table_kwargs['where'] = '"duration" >= {} and "duration" <= {}'.format(duration_window[0], duration_window[1])

        emit_table, loss_table, bsrt_table = self.get_tables([self._profile_emittance_table_name,
                                                              self._loss_table_name,
                                                              self._bsrt_regression_info],
                                                             [{}, loss_table_kwargs, {}])

        # only consider profiles having a 'good' fit with respect to a percentile of all data.
        if profile_chi2_perc < 100:
            Q = np.percentile(emit_table['profile_chi2'].values, profile_chi2_perc)
            emit_table = emit_table.loc[emit_table['profile_chi2'] <= Q]

        # determine the common fills of the three tables
        fills_et = emit_table['fill'].unique()
        fills_lt = loss_table['fill'].unique()
        fills_rt = bsrt_table['fill'].unique()
        fills = sorted(set(fills_et).intersection(set(fills_lt)).intersection(set(fills_rt)))
        n_fills = len(fills)

        bunches = emit_table['bunch_count'].unique()

        if verbose:
            print ('Duration window for selecting fills:\n {}'.format(duration_window))
            print ('                  # fills: {}'.format(n_fills))
            print ('                # bunches: {}'.format(len(bunches))) 
            print ('          drop_pos_losses: {}'.format(drop_pos_losses))
            print ('BSRT regression tolerance: {} [s]'.format(reg_tol))   
            print ('                 scale_to: {}'.format(scale_to))
            print ('    (fixed) duration_time: {}'.format(duration_time))        
            start_time = timelib.time()

        # determine regression labels & order
        regcols_x = [c for c in bsrt_table.columns if 'enx_fit' in c]
        regcols_y = [c for c in bsrt_table.columns if 'eny_fit' in c]     
        fit_curve = lambda params, t: sum([params[k]*t**k for k in range(len(regcols_x))])

        data = []
        namesh, namesv = [], []
        fill_index = 0
        dropped1, dropped2, dropped3 = 0, 0, 0 # to display how many items have been dropped.
        for fill in fills:
            if verbose:
                print ('\rFill {}, {}/{}'.format(fill, fill_index + 1, n_fills), end='')
            fill_index += 1

            for bunch_count in bunches:

                db_ws_bunch = emit_table.loc[(emit_table['fill'] == fill) & (emit_table['bunch_count'] == bunch_count)] # database containing the fit parameters of all the wire scanner measurements for the respective bunch (H/V combinations) 
                db_bsrt_bunch = bsrt_table.loc[(bsrt_table['fill'] == fill) & (bsrt_table['bunch_count'] == bunch_count)] # database containing the BSRT fit parameters for the specific bunch (database length = 1)
                db_loss_bunch = loss_table.loc[(loss_table['fill'] == fill) & (loss_table['bunch_count'] == bunch_count)] # database containing the beam loss information from the intensity measurements (database length = 1)

                n_ws_data = len(db_ws_bunch)

                if n_ws_data == 0 or len(db_bsrt_bunch) != 1 or len(db_loss_bunch) != 1: # skip case
                    dropped1 += n_ws_data
                    continue

                if drop_pos_losses:
                    if db_loss_bunch.iloc[0]['loss'] > 0:
                        # skip this case
                        dropped2 += n_ws_data
                        continue

                ##### determine the timestamps ####
                int_duration = db_loss_bunch.iloc[0]['duration'] # the duration time, denoting how long intensity was measured for the individual bunch.
                int_timestamp_1 = db_loss_bunch.iloc[0]['timestamp'] # the timestamp at which the relative loss was computed (the final time)
                int_timestamp_0 = int_timestamp_1 - int_duration

                # define the timestamps of the wire scanner measurement. We will consider the average of the H and V times here:
                ws_timestamps = (db_ws_bunch['timestamp_h'].values + db_ws_bunch['timestamp_v'].values)/2

                bsrt_timestamp_0 = db_bsrt_bunch.iloc[0]['timestamp'] # the reference time at which enx_fit_0 and eny_fit_0 were taken
                bsrt_duration = db_bsrt_bunch.iloc[0]['duration'] # the duration (from time0_bsrt_fit onward) where BSRT data for the bunch was available
                bsrt_timestamp_1 = bsrt_timestamp_0 + bsrt_duration
                ###################################

                # check if the BSRT fit data is sufficient, i.e. if the BSRT tool provides data over the entire time span of interest, within given bounds.
                if abs(bsrt_timestamp_0 - int_timestamp_0) > reg_tol[0] or abs(bsrt_timestamp_1 - int_timestamp_1) > reg_tol[1]:
                    dropped3 += n_ws_data
                    continue

                # the parameters of the BSRT emittance fit for the specific bunch
                params_x = db_bsrt_bunch[regcols_x].values[0]
                params_y = db_bsrt_bunch[regcols_y].values[0]

                # In order to get the scaling factors for the wire scanner emittances, we compute the BSRT emittance at the times where
                # the wirescans have been taken -- and and the time of interest (e.g. the intensity start time or the end time).
                if duration_time != None:
                    time_of_interest = int_timestamp_0 + duration_time
                else:
                    time_of_interest = int_timestamp_0 + scale_to*(int_timestamp_1 - int_timestamp_0)

                bsrt_ex_at_wstime = fit_curve(params_x, ws_timestamps - bsrt_timestamp_0) # a list of emittances according to the various times of the H/V measurements.
                bsrt_ey_at_wstime = fit_curve(params_y, ws_timestamps - bsrt_timestamp_0) # a list of emittances according to the various times of the H/V measurements.

                bsrt_ex_at_time_of_interest = fit_curve(params_x, time_of_interest - bsrt_timestamp_0)
                bsrt_ey_at_time_of_interest = fit_curve(params_y, time_of_interest - bsrt_timestamp_0)

                # the scaling factors of the BSRT emittances from time0_fit to the time at which we want to extrapolate
                scale_x = bsrt_ex_at_time_of_interest/bsrt_ex_at_wstime
                scale_y = bsrt_ey_at_time_of_interest/bsrt_ey_at_wstime

                # scale all relevant wire scanner fit parameters
                scaled_x_ws_parameters = np.matmul(np.diag(scale_x), db_ws_bunch[self._columns_to_xscale].values)
                scaled_y_ws_parameters = np.matmul(np.diag(scale_y), db_ws_bunch[self._columns_to_yscale].values)

                # include the non-scaled (but necessary) parameters for beam loss calculation and other information
                not_scaled_ws_parameters = db_ws_bunch[self._columns_not_scaled].values
                en0x = db_ws_bunch['enx'].values
                en0y = db_ws_bunch['eny'].values

                # now create the table containing all of this information
                slot_indices = db_ws_bunch['slot_index'].values.transpose().tolist()
                bunch_indices = db_ws_bunch['bunch_index'].values.transpose().tolist() # should be identical to a specific number

                # get H/V IN/OUT information
                namesh += db_ws_bunch['nameh'].values.tolist()
                namesv += db_ws_bunch['namev'].values.tolist()

                # add profile chi2 values
                profile_chi2 = db_ws_bunch['profile_chi2'].values.transpose().tolist()

                datablock = [[fill]*n_ws_data, slot_indices, bunch_indices, [bunch_count]*n_ws_data, ws_timestamps, 
                                [int_timestamp_1]*n_ws_data, [int_duration]*n_ws_data, profile_chi2,
                                scale_x, scale_y, [-db_loss_bunch.iloc[0]['loss']]*n_ws_data, en0x, en0y] + \
                                scaled_x_ws_parameters.transpose().tolist() + scaled_y_ws_parameters.transpose().tolist() + \
                                not_scaled_ws_parameters.transpose().tolist()
                data.append(np.array(datablock).transpose())

        data = pd.DataFrame(np.vstack(data), columns=['fill', 'slot_index', 'bunch_index', 'bunch_count',
                                           'timestamp_ws', 'timestamp_loss', 'duration', 'profile_chi2', 'scale_x',
                                           'scale_y', 'exp_loss', 'ws_enx', 'ws_eny'] + self._columns_to_xscale + self._columns_to_yscale +
                            self._columns_not_scaled).astype({'fill': 'int', 'slot_index': 'int', 'bunch_index': 'int', 'bunch_count': 'int'})
        data['nameh'] = namesh
        data['namev'] = namesv
        data['emit_index'] = data.index

        if len(data) > 0 and store:
            table_name = self._prepared_table_name
            hdfstore = pd.HDFStore(self.filename, mode='a')
            hdfstore.put(value=data, key=table_name, format='t', data_columns=True)
            hdfstore.close()
            if verbose:
                print ('\nStored prepared data for beam loss calculations into table:\n "{}"'.format(table_name))

        if verbose:
            print ('\nTime elapsed: {:.3f} [s]'.format(timelib.time() - start_time))
            print ('\ndropped cases\n-------------')
            print ('invalid bunch loss raise: {}'.format(dropped1))
            print ('    no wire scanner data: {}'.format(dropped2))
            print ('    BSRT times > reg_tol: {}'.format(dropped3))
            print ('          => valid cases: {}/{}'.format(len(data), len(data) + dropped1 + dropped2 + dropped3))

        return data

    def prepare_sim_loss_no_bsrt(self, duration_window=[], drop_pos_losses=True, profile_chi2_perc=100, verbose=True, store=True):
        '''
        Prepare the table containing the profile fit parameters from the wire scanners with respect to an optional time window [s].
        This function has been taken from prepare_sim_loss_with_bsrt, but here without any check and scaling using the BSRT data.

        duration_window: An optional window to consider only fills having a specific duration length within the given window.
        drop_pos_losses: If True, we do not record cases belonging to 'positive' losses.

        profile_chi2_perc: If <100, then only those profiles will be considered by which profile_chi2 is smaller
                           than the given percentile.


        In the final database, emit_index will be an index counting all the different bunches of all valid fills.
        '''
        if verbose:
            print ('Reading emittances from table:\n "{}"'.format(self._profile_emittance_table_name))
            print ('Reading losses from table:\n "{}"'.format(self._loss_table_name))

        loss_table_kwargs = {}
        if len(duration_window) > 0:
            loss_table_kwargs['where'] = '"duration" >= {} and "duration" <= {}'.format(duration_window[0], duration_window[1])

        emit_table, loss_table = self.get_tables([self._profile_emittance_table_name,
                                                              self._loss_table_name],
                                                  [{}, loss_table_kwargs])

        # only consider profiles having a 'good' fit with respect to a percentile of all data.
        if profile_chi2_perc < 100:
            Q = np.percentile(emit_table['profile_chi2'].values, profile_chi2_perc)
            emit_table = emit_table.loc[emit_table['profile_chi2'] <= Q]
    
        # determine the common fills of the two tables
        fills_et = emit_table['fill'].unique()
        fills_lt = loss_table['fill'].unique()
        fills = sorted(set(fills_et).intersection(set(fills_lt)))
        n_fills = len(fills)

        bunches = emit_table['bunch_count'].unique()

        if verbose:
            print ('          duration window: {}'.format(duration_window))
            print ('                  # fills: {}'.format(n_fills))
            print ('                # bunches: {}'.format(len(bunches))) 
            print ('          drop_pos_losses: {}'.format(drop_pos_losses))        
            start_time = timelib.time()

        data = []
        namesh, namesv = [], []
        fill_index = 0
        dropped1, dropped2 = 0, 0 # to display how many items have been dropped.
        for fill in fills:
            if verbose:
                print ('\rFill {}, {}/{}'.format(fill, fill_index + 1, n_fills), end='')
            fill_index += 1

            for bunch_count in bunches:

                db_ws_bunch = emit_table.loc[(emit_table['fill'] == fill) & (emit_table['bunch_count'] == bunch_count)] # database containing the fit parameters of all the wire scanner measurements for the respective bunch (H/V combinations) 
                db_loss_bunch = loss_table.loc[(loss_table['fill'] == fill) & (loss_table['bunch_count'] == bunch_count)] # database containing the beam loss information from the intensity measurements (database length = 1)

                n_ws_data = len(db_ws_bunch)

                if n_ws_data == 0 or len(db_loss_bunch) != 1: # skip case
                    dropped1 += n_ws_data
                    continue

                if drop_pos_losses:
                    if db_loss_bunch.iloc[0]['loss'] > 0:
                        # skip this case
                        dropped2 += n_ws_data
                        continue

                ##### determine the timestamps ####
                int_duration = db_loss_bunch.iloc[0]['duration'] # the duration time, denoting how long intensity was measured for the individual bunch.
                int_timestamp_1 = db_loss_bunch.iloc[0]['timestamp'] # the timestamp at which the relative loss was computed (the final time)
                int_timestamp_0 = int_timestamp_1 - int_duration

                # define the timestamps of the wire scanner measurement. We will consider the average of the H and V times here:
                ws_timestamps = (db_ws_bunch['timestamp_h'].values + db_ws_bunch['timestamp_v'].values)/2
                ###################################

                # the relevant wire scanner fit parameters, here not scaled in comparison to prepare_sim_loss_with_bsrt
                # (so we will use directly the values from the wire scanner)
                scaled_x_ws_parameters = db_ws_bunch[self._columns_to_xscale].values
                scaled_y_ws_parameters = db_ws_bunch[self._columns_to_yscale].values

                # include the non-scaled (but necessary) parameters for beam loss calculation and other information
                not_scaled_ws_parameters = db_ws_bunch[self._columns_not_scaled].values
                en0x = db_ws_bunch['enx'].values
                en0y = db_ws_bunch['eny'].values

                # now create the table containing all of this information
                slot_indices = db_ws_bunch['slot_index'].values.transpose().tolist()
                bunch_indices = db_ws_bunch['bunch_index'].values.transpose().tolist() # should be identical to a specific number
                # get H/V IN/OUT information
                namesh += db_ws_bunch['nameh'].values.tolist()
                namesv += db_ws_bunch['namev'].values.tolist()

                # add profile chi2 values
                profile_chi2 = db_ws_bunch['profile_chi2'].values.transpose().tolist()

                datablock = [[fill]*n_ws_data, slot_indices, bunch_indices,
                             [bunch_count]*n_ws_data, ws_timestamps, 
                                [int_timestamp_1]*n_ws_data, [int_duration]*n_ws_data, profile_chi2,
                                [-db_loss_bunch.iloc[0]['loss']]*n_ws_data, en0x, en0y] + \
                                scaled_x_ws_parameters.transpose().tolist() + scaled_y_ws_parameters.transpose().tolist() + \
                                not_scaled_ws_parameters.transpose().tolist()
                data.append(np.array(datablock).transpose())

        data = pd.DataFrame(np.vstack(data), columns=['fill', 'slot_index', 'bunch_index', 'bunch_count',
                                           'timestamp_ws', 'timestamp_loss', 'duration', 'profile_chi2',
                                           'exp_loss', 'ws_enx', 'ws_eny'] + self._columns_to_xscale + self._columns_to_yscale +
                            self._columns_not_scaled).astype({'fill': 'int', 'slot_index': 'int', 'bunch_index': 'int', 'bunch_count': 'int'})
        data['nameh'] = namesh
        data['namev'] = namesv
        data['emit_index'] = data.index

        if len(data) > 0 and store:
            table_name = self._prepared_table_name
            hdfstore = pd.HDFStore(self.filename, mode='a')
            hdfstore.put(value=data, key=table_name, format='t', data_columns=True)
            hdfstore.close()
            if verbose:
                print ('\nStored prepared data for beam loss calculations into table:\n "{}"'.format(table_name))

        if verbose:
            print ('\nTime elapsed: {:.3f} [s]'.format(timelib.time() - start_time))
            print ('\ndropped cases\n-------------')
            print ('invalid bunch loss raise: {}'.format(dropped1))
            print ('    no wire scanner data: {}'.format(dropped2))
            print ('          => valid cases: {}/{}'.format(len(data), len(data) + dropped1 + dropped2))

        return data


    def prepare_sim_loss(self, scale_to=None, freq=None, **kwargs):
        '''
        For information on this routine see
            self.prepare_sim_loss_with_bsrt
        or
            self.prepare_sim_loss_no_bsrt

        freq: Revolution frequency to compute the turn numbers from the duration times (in [s]).
              If not provided, then a beam must be set.
        '''

        if freq == None and not hasattr(self, 'beam'):
            # A frequency is required to transform seconds into turn numbers
            raise NameError('Revolution frequency not provided but self.beam was not found.') 

        if scale_to != None:
            profile_table = self.prepare_sim_loss_with_bsrt(scale_to=scale_to, **kwargs)
        else:
            profile_table = self.prepare_sim_loss_no_bsrt(**kwargs)

        return self.generate_profile_parameters_table(profile_table=profile_table, freq=freq, verbose=False)

    def get_emittances(self, lookup_key, drop_nans=True, verbose=False):
        '''
        Obtain the emittances to be used for SixDB analysis from the HDF table 'self._prepared_table_name'.
        '''
        if verbose:
            print ('Obtaining emittances from table\n "{}"'.format(self._prepared_table_name))
        work_table = self.get_table(self._prepared_table_name, columns=[lookup_key, 'enx', 'eny'])

        return get_emittances(work_table, drop_nans=drop_nans, verbose=verbose)

    def compute_loss(self, da_model, fit_parameters, profile_fit_parameters, turn):
        '''
        Compute the losses of the profile model, based on a given DA model, its fit_parameters and a turn.

        This function utilizes the internal routine compute_simulated_losses, which depends only on the profile fit parameters,
        together with a given DA model.
        '''
        da2_series = da_model.run(fit_parameters[da_model.fit_parameter_keys].values.transpose(), turn)**2 # da of all seeds, tunes and SQL files at for this specific turn value.
        losses, da2_mult, profile_params = self.compute_simulated_losses(profile_fit_parameters, da2_series)
        da_mult = np.sqrt(da2_mult)
        return losses, da_mult, profile_params, da2_series

    @staticmethod
    def _init_cdict(da_model):
        '''
        Helper function which returns a dictionary mapping the covariance indices to their respective column names 
        '''
        n_parameters = len(da_model.fit_parameter_keys)
        cdict = {}
        for i, j in product(range(n_parameters), repeat=2):
            if i <= j:
                cdict[(i, j)] = 'cov_{}_{}'.format(i + 1, j + 1)
            else:
                cdict[(i, j)] = 'cov_{}_{}'.format(j + 1, i + 1)
        return cdict

    def compute_loss_errors(self, da_model, **kwargs):
        '''
        Compute the errors in the losses, based on a given Pandas Dataframe which must contain the entries of the covariance
        matrix.

        '''
        cdict = self._init_cdict(da_model=da_model)
        return self.compute_loss_errors_noinit(da_model=da_model, cdict=cdict, **kwargs)

    def compute_loss_errors_noinit(self, error_table, da_model, fit_parameters, profile_fit_parameters, 
                                      turn, sigma, da_series, cdict):
        '''
        Compute the errors in the losses, based on a given Pandas Dataframe which must contain the entries of the covariance
        matrix.

        sigma: The sigma-level of the fit-parameters.
        '''

        n_parameters = len(da_model.fit_parameter_keys)

        grad_dict = da_model.darun(fit_parameters[da_model.fit_parameter_keys].values.transpose(), turn)
        grad = np.array([grad_dict[p] for p in da_model.fit_parameter_keys]) # N.B. grad.shape = (n_parameters, len(xdata)*n_files)               
        da_series_errors = np.zeros(len(error_table))
        for j, w in product(range(n_parameters), range(n_parameters)): # the error in the da is given as da**2 = grad*fit_errors*grad.transpose() by first-order error propagation
            f1 = error_table[cdict[(j, w)]].values*grad[w, :]
            da_series_errors += grad[j, :]*f1
        da_series_errors = np.sqrt(da_series_errors)*sigma # now take the square root and multiply with the given sigma-value

        loss_errors_pos, da2_mult_pos, profile_params = self.compute_simulated_losses(profile_fit_parameters, 
                                                                    (da_series + da_series_errors)**2)
        loss_errors_neg, da2_mult_neg, profile_params = self.compute_simulated_losses(profile_fit_parameters, 
                                                                    (da_series - da_series_errors)**2)

        return loss_errors_pos, loss_errors_neg, da2_mult_pos, da2_mult_neg, profile_params


########################################################
# Objects related to the double-Gaussian profile model #
########################################################

def dg_residual(params, *args):
    '''
    This function is the residual used to fit the H and V wire scanner profiles (each represented in form of
    x and y data) to a double-Gaussian, using the *same* a1-normalizing factor (if possible, need to
    be checked), in order to examine the resulting pair of sigma-values used in the DA formula. 
    
    We further assume sigma1h/sigma1v = sigma2h/sigma2v (this asssumption will simplify the DA calculation
    with double-Gaussian 2D profiles significantly).

    For more details see the description in ws_rms_hvlink function.
    '''
    xh = args[0]
    yh = args[1]
    xv = args[2]
    yv = args[3]

    a1 = params[0]
    sigma1h = max([params[1], 1e-8]) # ensure that we do not divide by zero
    sigma2h = params[2]
    centerh = params[3]
    offseth = params[4]
    
    sigma1v = params[5]
    sigma2v = sigma2h/sigma1h*sigma1v # ensure that sigma1h/sigma2h = sigma1v/sigma2v
    centerv = params[6]
    offsetv = params[7]
    
    rh = yh - bi_gaussian(xh, a1=a1, sigma1=sigma1h, sigma2=sigma2h, center=centerh, offset=offseth)
    rv = yv - bi_gaussian(xv, a1=a1, sigma1=sigma1v, sigma2=sigma2v, center=centerv, offset=offsetv)

    return np.array(list(rh) + list(rv))


def ws_rms_hvlink(xh, yh, xv, yv, p0=[0.5, 600.0, 3200.0, 0.0, 0.0, 600.0, 0.0, 0.0], with_error=False, verbose=True):
    '''
    Fit two double-Gaussian profiles to given horizontal and vertical profile data, hereby maintaining the
    eccentricity of the two resulting emittances (see manual).

    Detailed description:

    If x denotes the horizontal direction and y the vertical direction, then the following profile is used:

    rho(x, y) = alpha1/(2*pi*sigma1x*sigma1y)*exp(-x**2/(2*sigma1x**2) - y**2/(2*sigma1y**2)) +
                (1 - alpha1)/(2*pi*sigma2x*sigma2y)*exp(-x**2/(2*sigma2x**2) - y**2/(2*sigma2y**2))
                
    If we assume further that sigma1x/sigma1y = sigma2x/sigma2y =: sqrt(kappa), then the 2D integral   
    T(S) := int_0^(2*pi) int_S^infty rho(x, y) dx dy
    will become
    T(S) = alpha1*exp(-S**2) + (1 - alpha1)/kappa*exp(- kappa*S**2)

    With respect to an individual direction, e.g. the horizontal direction, rho(x, y) projects to

    int rho(x, y) dy = alpha1/sqrt(2*pi)/sigma1x*exp(-x**2/(2*sigma1x**2)) +
                        (1 - alpha1)/sqrt(2*pi)/sigma2x*exp(-x**2/(2*sigma2x**2))

    For the vertical direction, a similar equation holds. This means that the factors alpha1 are the same
    for both directions.
    
    This function therefore attempts to fit given horizontal and vertical profiles in such a way that
    1. there is a common alpha1
    2. there is a common quotient kappa.

    We will compute the respective parameters and also the rms values of the resulting curves in both
    directions. In order to prepare the mutual fit, both curves will be normalized individually first, by
    using individual double-Gaussian fits. In all fits we also include a general offset with respect to
    the ordinates.

    INPUT
    -----
    xh, yh   horizontal profile
    xv, yv   vertical profile
    p0       initial parameter guess for fit
    '''

    lower_bounds = np.array([0, 0, 0, -np.inf, -np.inf, 0, -np.inf, -np.inf])
    upper_bounds = np.array([1, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf])

    xhn, yhn = xh, yh
    xvn, yvn = xv, yv
    result = least_squares(dg_residual, p0, args=(xhn, yhn, xvn, yvn), bounds=(lower_bounds, upper_bounds))

    # some elementary checks
    params = result['x']
    success = result['success'] and all(np.array(params)) > 0

    out = {}
    if success:
        if verbose:
            print ('Link dGauss fit successfully')
        out['a1'] = params[0]
        out['sigma1h'] = params[1]
        out['sigma2h'] = params[2]
        out['centerh'] = params[3]
        out['offseth'] = params[4]
        out['sigma1v'] = params[5]
        out['sigma2v'] = out['sigma2h']/out['sigma1h']*out['sigma1v']
        out['centerv'] = params[6]
        out['offsetv'] = params[7]

        out['xhn'] = xhn
        out['yhn'] = yhn
        out['xvn'] = xvn
        out['yvn'] = yvn

        # compute the rms values
        a1 = out['a1']
        out['rmsh'] = np.sqrt(a1*out['sigma1h']**2 + (1 - a1)*out['sigma2h']**2)
        out['rmsv'] = np.sqrt(a1*out['sigma1v']**2 + (1 - a1)*out['sigma2v']**2)

        out['kappa'] = (out['sigma1h']/out['sigma2h'])**2

        n_dof = max([1, len(result['fun']) - 8]) # 8: number of fit parameters
        out['profile_chi2'] = sum(result['fun']**2)/n_dof
    else:
        if verbose:
            print ('Link dGauss fit NOT successful')

    return out

class double_gaussian(profile_model):
    '''
    Generic class to collect common methods which are related to the double-Gaussian profile model.
    '''
    
    def __init__(self, filename, suffix='_dg-<gen>', description='dGauss-<gen>', **kwargs):
        profile_model.__init__(self, filename=filename, **kwargs)

        self.suffix = suffix
        self.description = description

        self._columns_to_xscale = ['rmsh', 'sigma1h', 'sigma2h', 'enx'] # used for self.prepare_sim_losses
        self._columns_to_yscale = ['rmsv', 'sigma1v', 'sigma2v', 'eny']
        self._columns_not_scaled = ['a1', 'kappa'] # used for self.prepare_sim; columns required for calculating beam losses

        self._profile_model_table_name += suffix
        self._profile_emittance_table_name += suffix
        self._prepared_table_name += suffix
        self.loss_model_description = 'dGauss'

    def plot_ws_data(self, fills, slot_indices, indices=[], logscale=False, differences=False,
        show_rms_values=True, title='', show_legend=True, show_relative_differences=False,
        xlim1=[], ylim1=[], xlim2=[], ylim2=[], figsize=(12, 4)):
        '''
        Plot wire scanner data & fit.
        Indices: If non-empty plot only those graphs of the given indices. The ordering is given by the underlying table.

        fills: A list of fills to be considered
        slot_indices: A list of slot indices to be considered.
        logscale: If true, use a logarithmic scale.
        differences: If true, show the differences between fit and raw data.
        show_rms_valuse: If true, show the rms values in form of vertical lines.
        show_legend: show the plot legend
        show_relative_differences: If True, show the relative differences instead. Only works if differences==True.
        
        Example:
        --------
        fills = [6060, 6060]
        slot_indices = [14, 42]
        will plot the profiles of in the 14th and 42th index in the given database.
        '''
        
        fig = plt.figure(figsize=figsize)
        ax1 = fig.add_subplot(1, 2, 1)
        ax2 = fig.add_subplot(1, 2, 2)
        
        fills_bc_dict = {fill_nr: [e[1] for e in zip(*(fills, slot_indices)) if e[0] == fill_nr] for fill_nr in np.unique(fills)}
        profile_table = self.get_table(self._profile_model_table_name)
        
        n_profiles = 0 # count for title
        cm_start, cm_stop = 0, 1
        unique_bunch_counts = list(profile_table.bunch_count.unique())
        cm_subsection = np.linspace(cm_start, cm_stop, len(unique_bunch_counts)) 
        colors = [ cm.jet(x) for x in cm_subsection ]
        
        difflabels = []

        # load the raw data & plot
        hdfstore = pd.HDFStore(self.filename, mode='r')
        print ("Reading profiles from database:\n '{}'".format(self.filename))
        
        for fill_nr in fills_bc_dict.keys():
            profiles_oi = profile_table.loc[(profile_table.fill == fill_nr) &
                                     (profile_table.slot_index.isin(fills_bc_dict[fill_nr]))]

            namesh_oi = profiles_oi.nameh.unique()
            namesv_oi = profiles_oi.namev.unique()

            if len(namesh_oi) == 0 or len(namesv_oi) == 0:
                continue
                
            # load the required raw data
            datah = {nameh: hdfstore.select(nameh).dropna() for nameh in namesh_oi}
            datav = {namev: hdfstore.select(namev).dropna() for namev in namesv_oi}
            
            if len(indices) > 0:
                profile_indices = sorted(set(indices).intersection(set(np.arange(len(profiles_oi)))))
            else:
                profile_indices = np.arange(len(profiles_oi))

            for k in profile_indices:
                data_k = profiles_oi.iloc[k]

                slot_index = data_k['slot_index']
                bunch_count = data_k['bunch_count']

                nameh = data_k['nameh']
                namev = data_k['namev']

                dh = datah[nameh]
                dv = datav[namev]
                hx = dh['x_{}'.format(slot_index)]
                hy = dh['y_{}'.format(slot_index)]
                vx = dv['x_{}'.format(slot_index)]
                vy = dv['y_{}'.format(slot_index)]

                timeh = data_k['timestamp_h']
                timev = data_k['timestamp_v']
                delta_t = timev - timeh

                a1, sigma1h, sigma2h = data_k['a1'], data_k['sigma1h'], data_k['sigma2h']
                centerh = data_k['centerh']
                rmsh = data_k['rmsh']
                
                sigma1v, sigma2v = data_k['sigma1v'], data_k['sigma2v']
                centerv = data_k['centerv']
                rmsv = data_k['rmsv']

                hy_fit = bi_gaussian(hx, a1=a1, sigma1=sigma1h, sigma2=sigma2h, center=centerh)
                vy_fit = bi_gaussian(vx, a1=a1, sigma1=sigma1v, sigma2=sigma2v, center=centerv)

                if not differences:
                    ax1.plot(hx, hy, label='raw data')
                    ax1.plot(hx, hy_fit, label='fit')
                    hlines_ymin = 0
                    hlines_ymax = max(hy_fit)
                    
                    ax2.plot(vx, vy, label='raw data')
                    ax2.plot(vx, vy_fit, label='fit')
                    vlines_ymin = 0
                    vlines_ymax = max(vy_fit)
                else:
                    hdiff = hy_fit - hy
                    vdiff = vy_fit - vy
                    if show_relative_differences:
                        hdiff = hdiff/hy 
                        vdiff = vdiff/vy
                        difflabel = '(fit - raw data (b{}))/raw'.format(bunch_count)
                    else:
                        difflabel = 'fit - raw data (b{})'.format(bunch_count)

                    hlines_ymin = min(hdiff)
                    hlines_ymax = max(hdiff)

                    vlines_ymin = min(vdiff)
                    vlines_ymax = max(vdiff)
                    
                    if difflabel not in difflabels:
                        ax1.plot(hx, hdiff, label=difflabel, color=colors[unique_bunch_counts.index(bunch_count)])
                        ax2.plot(vx, vdiff, label=difflabel, color=colors[unique_bunch_counts.index(bunch_count)])
                        difflabels.append(difflabel)
                    else:
                        ax1.plot(hx, hdiff, color=colors[unique_bunch_counts.index(bunch_count)])
                        ax2.plot(vx, vdiff, color=colors[unique_bunch_counts.index(bunch_count)])
                
                    
                if show_rms_values:
                    ax1.vlines([-rmsh + centerh, rmsh + centerh], ymin=hlines_ymin, ymax=hlines_ymax,
                               linestyle='--', color='black', label='rms: {:.3f}'.format(rmsh))

                    ax2.vlines([-rmsv + centerv, rmsv + centerv], ymin=vlines_ymin, ymax=vlines_ymax, 
                                linestyle='--', color='black', label='rms: {:.3f}'.format(rmsv))
                    
                n_profiles += 1


        hdfstore.close()
                    
        if n_profiles > 1:
            ax1.set_title('{} horizontal profiles'.format(n_profiles), loc='right')
        ax1.set_xlabel(r'$x \; [\mu m]$')

        if not differences:
            ylabel = r'$n$'
            ax1.set_ylabel(ylabel)
        elif not show_relative_differences:
            ylabel = r'$n_{sim} - n_{exp}$'
        else:
            ylabel = r'$(n_{sim} - n_{exp})/n_{exp}$'

        if logscale:
            ax1.set_yscale('log')
            ax2.set_yscale('log')
            ylabel = r'$\log({})$'.format(ylabel)

        ax1.set_ylabel(ylabel)
        ax2.set_ylabel(ylabel)

        if len(xlim1) > 0:
            ax1.set_xlim(xlim1)
        if len(xlim2) > 0:
            ax2.set_xlim(xlim2)
        if len(ylim1) > 0:
            ax1.set_ylim(ylim1)
        if len(ylim2) > 0:
            ax2.set_ylim(ylim2)

        if n_profiles > 1:
            title = '{}\n{} vertical profiles'.format(title, n_profiles)
        if len(title) > 0:
            ax2.set_title(title, loc='right')
        ax2.set_xlabel(r'$y \; [\mu m]$')

        if show_legend:
            ax1.legend()
            ax2.legend()

        plt.show()


    def plot_ws_fit_statistics(self, fills, slot_indices, fit_labels=[], indices=[], title='', figsize=(12, 4), 
        combined=False, show_medians=True, xlim=[], **kwargs):
        '''
        Plot wire scanner fit parameter statistics.
        Indices: If non-empty plot only those graphs of the given indices. The ordering is given by the underlying table.

        fills: A list of fills to be considered
        slot_indices: A list of slot indices to be considered.
        fit_labels: A list of fit parameter labels. An individual entry will produce a histogram; a tuple will
                    produce a scatter plot. If not specified, then self.loss_params will be used.
        combined: If True, plot combined statistics plots instead.

        
        Example:
        --------
        fills = [6060, 6060]
        slot_indices = [14, 42]
        will plot the profile fit parameter statistics of in the 14th and 42th index in the given database.
        '''

        if len(fit_labels) == 0:
            fit_labels = self.loss_params

        fig = plt.figure(figsize=figsize)
        axes = {}
        for k in range(len(fit_labels)):
            axes[k] = fig.add_subplot(1, len(fit_labels), k + 1)

        profile_table = self.get_table(self._profile_model_table_name)

        if not combined:
            fills_bc_dict = {fill_nr: [e[1] for e in zip(*(fills, slot_indices)) if e[0] == fill_nr] for fill_nr in np.unique(fills)}
            for fill_nr in fills_bc_dict.keys():
                profiles_oi = profile_table.loc[(profile_table.fill == fill_nr) &
                                         (profile_table.slot_index.isin(fills_bc_dict[fill_nr]))]
                
                if len(profiles_oi) == 0:
                    continue

                if len(indices) > 0:
                    profile_indices = sorted(set(indices).intersection(set(np.arange(len(profiles_oi)))))
                else:
                    profile_indices = np.arange(len(profiles_oi))

                for k in range(len(fit_labels)):
                    values_k = profiles_oi[fit_labels[k]].values
                    h = axes[k].hist(values_k, **kwargs)

                    axes[k].set_xlabel(fit_labels[k])
                    axes[k].set_ylabel(r'$n$')

                    if show_medians:
                        median = np.median(values_k)
                        axes[k].vlines([median], ymin=0, ymax=max(h[0]), linestyle='--', color='black')

                    if len(xlim) == len(fit_labels):
                        axes[k].set_xlim(xlim[k])

        else:
            for k in range(len(fit_labels)):
                values_k = profile_table[fit_labels[k]].values
                h = axes[k].hist(values_k, **kwargs)

                print ('n_datapoints: {}, n_bins: {}'.format(len(values_k), len(h[0])))

                axes[k].set_xlabel(fit_labels[k])
                axes[k].set_ylabel(r'$n$')
                if show_medians:
                    median = np.median(values_k)
                    axes[k].vlines([median], ymin=0, ymax=max(h[0]), linestyle='--', color='black', 
                        label='median: {:.3}'.format(median))
                    axes[k].legend()

                if len(xlim) == len(fit_labels):
                    axes[k].set_xlim(xlim[k])

        axes[len(fit_labels) - 1].set_title('{}'.format(title), loc='right')


        plt.show()


class ws_dgaussian_link(double_gaussian):
    '''
    Class related to fitting procedures of a 2D double Gaussian with certain assumptions (see ws_rms_hvlink for details).
    '''

    def __init__(self, filename, loss_model='4d', **kwargs):
        double_gaussian.__init__(self, filename=filename, suffix='_dgl', description='dGaussL', **kwargs)

        self._columns_hvlink = ['rmsh', 'rmsv', 'a1', 'sigma1h', 'sigma1v', 'centerh', 'sigma2h', 'sigma2v', 'centerv', 'kappa', 'profile_chi2']
        self.columns = ['fill', 'nameh', 'namev', 'timestamp_h', 'timestamp_v', 'slot_index', 'bunch_index', 'bunch_count'] + self._columns_hvlink
        self.loss_params = ['a1', 'kappa'] # the parameters for the loss equations

        self.set_loss_model(loss_model=loss_model)

    def set_loss_model(self, loss_model):
        if loss_model == '4d':
            self.loss_model = self.loss_model4d
        if loss_model == '6d':
            self.loss_model = self.loss_model6d
        self.loss_model_description = 'dGaussL-{}'.format(loss_model)


    def _get_profile_fit_parameters_fill(self, fill_nr, span=120, hdfstore=None, verbose=True):
        '''
        Compute the rms values of the profiles of a given fill. 

        span: Profiles taken within a given time span [s] are considered to be correlated.
        '''
        close_hdf = False
        if hdfstore == None:
            hdfstore = pd.HDFStore(self.filename, mode='r')
            close_hdf = True
        stored_profiles_fill_info = hdfstore.select(key=self._processed_profiles_info,
                                                    where='"fill" == "{}"'.format(fill_nr))

        hscans = stored_profiles_fill_info[(stored_profiles_fill_info['direction'] == 'H')]
        vscans = stored_profiles_fill_info[(stored_profiles_fill_info['direction'] == 'V')]

        cor = correlate_groups(hscans['timestamp'].values, vscans['timestamp'].values, span=span)

        # information of the selected fills for this fill. This serves to identify the correct bunch enumeration.
        fill_information = hdfstore.select(key='{}_{}'.format(self._selected_fills_prefix, fill_nr))

        slot_indices = fill_information['slot_index'].values
        bunch_indices = fill_information['bunch_index'].values
        bunch_counts = fill_information['bunch_count'].values
        slot2bunch_index = lambda x: dict(zip(slot_indices, bunch_indices))[x]
        slot2bunch_count = lambda x: dict(zip(slot_indices, bunch_counts))[x]
        unique_slot_indices = sorted(np.unique(slot_indices))

        out = []
        for tpl in cor:
            hinfo = hscans.iloc[tpl[0]]
            vinfo = vscans.iloc[tpl[1]]
            n_profiles_h = hinfo['n_profiles']
            n_profiles_v = vinfo['n_profiles']
            hname = hinfo['name']
            vname = vinfo['name']
            timestamp_h = hinfo['timestamp']
            timestamp_v = vinfo['timestamp']

            hdata = hdfstore.select(hname).dropna()
            vdata = hdfstore.select(vname).dropna()

            # hdata.columns etc. have the form
            # 'x_51', 'x_52', 'x_53', 'x_54', 'x_55', 'x_63', 'x_64', 'x_65', 'x_66', 
            # where the running index corresponds to the slot index
            slot_indices_h = [int(e.split('_')[1]) for e in hdata.columns[:n_profiles_h]]
            slot_indices_v = [int(e.split('_')[1]) for e in vdata.columns[:n_profiles_v]]

            # FIXME perhaps this could be optimized to avoid using sets & sorting ...
            common_slot_indices = sorted(set(slot_indices_h).intersection(set(slot_indices_v)))

            for k in range(len(common_slot_indices)):
                slot_index = common_slot_indices[k]
                xh = hdata['x_{}'.format(slot_index)].values
                yh = hdata['y_{}'.format(slot_index)].values
        
                xv = vdata['x_{}'.format(slot_index)].values
                yv = vdata['y_{}'.format(slot_index)].values
        
                try:
                    rmsdat = ws_rms_hvlink(xh, yh, xv, yv, verbose=False)
                except RuntimeError:
                    if verbose:
                        print ('RuntimeError encountered, continuing ...')
                    continue

                if len(rmsdat) == 0:
                    if verbose:
                        print ('ws_rms_hvlink did not return data. Continuing ...')
                    continue

                bunch_index = slot2bunch_index(slot_index)
                bunch_count = slot2bunch_count(slot_index)
                out.append([fill_nr, hname, vname, timestamp_h, timestamp_v, slot_index, bunch_index, bunch_count] + \
                           [rmsdat[c] for c in self._columns_hvlink])
        if close_hdf:
            hdfstore.close()
        return pd.DataFrame(out, columns=self.columns)

    @staticmethod
    def loss_model4d(a1, kappa, da2):
        w1_half_da2 = (a1 + (1 - a1)/kappa)/2*da2 # kappa = epsilon_{1, x}/epsilon_{2, x} = epsilon_{1, y}/epsilon_{2, y}
        w2_half_da2 = kappa*w1_half_da2
        # Details see https://www.overleaf.com/project/5d0b9f9c5005405666aaceb7
        # out = a1*np.exp(-w1_half_da2) + (1 - a1)*np.exp(-w2_half_da2) # loss formula in Floquet-space (outdated; it corresponds to cutting a 2D projected distribution)
        return a1*(w1_half_da2 + 1)*np.exp(-w1_half_da2) + (1 - a1)*(w2_half_da2 + 1)*np.exp(-w2_half_da2) # 4D loss formula in angle-action coordinates

    @staticmethod
    def loss_model6d(a1, kappa, da2):
        w1_half_da2 = (a1 + (1 - a1)/kappa)/2*da2 # kappa = epsilon_{1, x}/epsilon_{2, x} = epsilon_{1, y}/epsilon_{2, y} = epsilon_{1, z}/epsilon_{2, z}
        w2_half_da2 = kappa*w1_half_da2
        # Details see https://www.overleaf.com/project/5d0b9f9c5005405666aaceb7
        return a1*(w1_half_da2**2/2 + w1_half_da2 + 1)*np.exp(-w1_half_da2) + (1 - a1)*(w2_half_da2**2/2 + w2_half_da2 + 1)*np.exp(-w2_half_da2) # 6D loss formula in angle-action coordinates

    def compute_simulated_losses(self, profile_fit_parameters, da2_series):
        '''
        INPUT
        =====
        profile_fit_parameters: Pandas dataframe of length m containing the required parameters in its columns.
        da_series: numpy array of DA values of length n

        Note that for each of the given fit parameters, a da-series is constructed.

        OUTPUT
        ======
        losses: np.array of shape(n, m) for each of the given profile fit parameters and da values.
        '''
        # for each of the given fit parameters, a da-series is constructed
        da2_series_mult = np.hstack([[e]*len(profile_fit_parameters) for e in da2_series])
        data_mult = np.array(profile_fit_parameters.values.tolist()*len(da2_series))
        a1_index = profile_fit_parameters.columns.get_loc('a1')
        kappa_index = profile_fit_parameters.columns.get_loc('kappa')
        losses = self.loss_model(a1=data_mult[:, a1_index], kappa=data_mult[:, kappa_index], da2=da2_series_mult)
        return losses, da2_series_mult, data_mult


class ws_dgaussian(double_gaussian):
    '''
    Class related to fitting procedures of 1D double-Gaussians. The horizontal and vertical profiles are treated independently here.
    '''

    def __init__(self, filename, **kwargs):
        double_gaussian.__init__(self, filename=filename, suffix='_dg', description='dGauss', **kwargs)

        self._columns_hfit = ['a1_h', 'rmsh', 'sigma1h', 'sigma2h', 'centerh', 'profile_h_chi2']
        self._columns_vfit = ['a1_v', 'rmsv', 'sigma1v', 'sigma2v', 'centerv', 'profile_v_chi2']

        self.columns = ['fill', 'nameh', 'namev', 'timestamp_h', 'timestamp_v', 'slot_index', 'bunch_index', 'bunch_count'] + \
                       self._columns_hfit + self._columns_vfit
        self.loss_params = ['a1', 'q1', 'q2'] # the parameters for the loss equations

        self.key_translator = {'a1_h': 'a1', 'a1_v': 'a1',
                               'rmsh': 'rms', 'rmsv': 'rms',
                               'centerh': 'center', 'centerv': 'center',
                               'profile_h_chi2': 'profile_chi2', 'profile_v_chi2': 'profile_chi2',
                               'sigma1h': 'sigma1', 'sigma1v': 'sigma1',
                               'sigma2h': 'sigma2', 'sigma2v': 'sigma2'} # we use the same double-Gaussian routine for H and V

    @staticmethod
    def rms_via_bigaussian(x, y, p0=[0.5, 600.0, 3200.0, 0.0, 0.0]):
        '''
        Fit a bi-Gaussian curve to given data.
        Return the fitting parameters a1, sigma1 and sigma2 and the rms value.
        '''
        out = {}
        ff = curve_fit(bi_gaussian, x, y, p0=p0)
        out['a1'], out['sigma1'], out['sigma2'], out['center'], out['offset'] = np.abs(ff[0][0:5])

        n_dof = min([1, len(x) - len(p0)]) # min to prevent division by zero as precaution. 
        out['profile_chi2'] = sum((y - bi_gaussian(x, a1=out['a1'], sigma1=out['sigma1'],
                                     sigma2=out['sigma2'], center=out['center'], offset=out['offset']))**2)/n_dof
        a1 = out['a1']
        out['rms'] = np.sqrt(a1*out['sigma1']**2 + (1 - a1)*out['sigma2']**2)
        return out


    def _get_profile_fit_parameters_fill(self, fill_nr, span=120, hdfstore=None, verbose=True):
        '''
        Compute the rms values of the profiles of a given fill. 

        span: Profiles taken within a given time span [s] are considered to be correlated.
        '''

        close_hdf = False
        if hdfstore == None:
            hdfstore = pd.HDFStore(self.filename, mode='r')
            close_hdf = True
        stored_profiles_fill_info = hdfstore.select(key=self._processed_profiles_info,
                                                                   where='"fill" == "{}"'.format(fill_nr))

        hscans = stored_profiles_fill_info[(stored_profiles_fill_info['direction'] == 'H')]
        vscans = stored_profiles_fill_info[(stored_profiles_fill_info['direction'] == 'V')]

        cor = correlate_groups(hscans['timestamp'].values, vscans['timestamp'].values, span=span)

        # information of the selected fills for this fill. This serves to identify the correct bunch enumeration.
        fill_information = hdfstore.select(key='{}_{}'.format(self._selected_fills_prefix, fill_nr))

        slot_indices = fill_information['slot_index'].values
        bunch_indices = fill_information['bunch_index'].values
        bunch_counts = fill_information['bunch_count'].values
        slot2bunch_index = lambda x: dict(zip(slot_indices, bunch_indices))[x]
        slot2bunch_count = lambda x: dict(zip(slot_indices, bunch_counts))[x]
        unique_slot_indices = sorted(np.unique(slot_indices))

        out = []
        for tpl in cor:
            hinfo = hscans.iloc[tpl[0]]
            vinfo = vscans.iloc[tpl[1]]
            n_profiles_h = hinfo['n_profiles']
            n_profiles_v = vinfo['n_profiles']
            hname = hinfo['name']
            vname = vinfo['name']
            timestamp_h = hinfo['timestamp']
            timestamp_v = vinfo['timestamp']

            hdata = hdfstore.select(hname).dropna()
            vdata = hdfstore.select(vname).dropna()

            slot_indices_h = [int(e.split('_')[1]) for e in hdata.columns[:n_profiles_h]]
            slot_indices_v = [int(e.split('_')[1]) for e in vdata.columns[:n_profiles_v]]

            # FIXME perhaps this could be optimized to avoid using sets & sorting ...
            common_slot_indices = sorted(set(slot_indices_h).intersection(set(slot_indices_v)))
  
            for k in range(len(common_slot_indices)):
                slot_index = common_slot_indices[k]
                xh = hdata['x_{}'.format(slot_index)].values
                yh = hdata['y_{}'.format(slot_index)].values
        
                xv = vdata['x_{}'.format(slot_index)].values
                yv = vdata['y_{}'.format(slot_index)].values
        
                try:
                    rmsdat_h = rms_via_bigaussian(xh, yh)
                    rmsdat_v = rms_via_bigaussian(xv, yv)
                except RuntimeError:
                    if verbose:
                        print ('RuntimeError encountered, continuing ...')
                    continue

                if len(rmsdat_h) == 0 or len(rmsdat_v) == 0:
                    if verbose:
                        print ('rms_via_bigaussian did not return data. Continuing ...')
                    continue

                bunch_index = slot2bunch_index(slot_index)
                bunch_count = slot2bunch_count(slot_index)
                out.append([fill_nr, hname, vname, timestamp_h, timestamp_v, slot_index, bunch_count] + 
                            [rmsdat_h[self.key_translator[c]] for c in self._columns_hfit] + 
                            [rmsdat_v[self.key_translator[c]] for c in self._columns_vfit])
        if close_hdf:
            hdfstore.close()
        return pd.DataFrame(out, columns=self.columns)

    @staticmethod
    def loss_model(a1, q1, q2, da2):
        # qj = epsilon/epsilon_j 
        # Details see https://www.overleaf.com/project/5d0b9f9c5005405666aaceb7
        return a1*np.exp(-q1*da2/2) + (1 - a1)*np.exp(-q2*da2/2) # loss formula in angle-action coordinates

    def compute_simulated_losses(self, profile_fit_parameters, da2_series):
        '''
        INPUT
        =====
        profile_fit_parameters: Pandas dataframe of length m containing the required parameters in its columns.
        da_series: numpy array of DA values of length n

        Note that for each of the given fit parameters, a da-series is constructed.

        OUTPUT
        ======
        losses: np.array of shape(n, m) for each of the given profile fit parameters and da values.
        '''
        da2_series_mult = np.hstack([[e]*len(profile_fit_parameters) for e in da2_series])
        profile_fit_parameters['q1_h'] = profile_fit_parameters['rmsh'].values/profile_fit_parameters['sigma1h'].values
        profile_fit_parameters['q2_h'] = profile_fit_parameters['rmsh'].values/profile_fit_parameters['sigma2h'].values
        profile_fit_parameters['q1_v'] = profile_fit_parameters['rmsv'].values/profile_fit_parameters['sigma1v'].values
        profile_fit_parameters['q2_v'] = profile_fit_parameters['rmsv'].values/profile_fit_parameters['sigma2v'].values
        profile_fit_parameters_mult = pd.concat([profile_fit_parameters]*len(da2_series), ignore_index=True)

        losses_h = self.loss_model(a1=profile_fit_parameters_mult['a1_h'].values, q1=profile_fit_parameters_mult['q1_h'].values, 
                                   q2=profile_fit_parameters_mult['q2_h'].values, da2=da2_series_mult)
        losses_v = self.loss_model(a1=profile_fit_parameters_mult['a1_v'].values, q1=profile_fit_parameters_mult['q1_v'].values, 
                                   q2=profile_fit_parameters_mult['q2_v'].values, da2=da2_series_mult)

        profile_fit_parameters_mult = pd.concat([profile_fit_parameters_mult]*2, ignore_index=True)
        da2_series_mult = np.hstack([da2_series_mult.tolist()]*2)
        losses = np.hstack([losses_h, losses_v])
        return losses, da2_series_mult, profile_fit_parameters_mult


