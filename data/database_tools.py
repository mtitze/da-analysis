import numpy as np
import pandas as pd
import datetime
import time as timelib

from sixdesk.notebook_tools import six_instance
import scipy.constants as const
from misc import madx_tfs_reader

import matplotlib.pyplot as plt
import operator

####################### general tools to obtain data etc.

def cluster_successors(c):
    '''
    Function to group lists of successors together.
    Input must be an array-like.

    Maybe there is a more elegant built-in function but I have not found it yet.

    Example:
    Input: [32, 33, 34, 35, 36, 60, 61, 62, 90, 91]
    Output: [[32, 33, 34, 35, 36], [60, 61, 62], [90, 91]]
    '''

    w = np.where(np.diff(c) != 1)[0]
    if len(w) > 0:
        d = [c[w[k] + 1: w[k + 1] + 1] for k in range(len(w) - 1)]
        dfin = [c[0:w[0] + 1]]
        d.append(c[w[-1] + 1:])
        dfin += d
    else:
        dfin = [c]
    return dfin


class pytimber_tools:
    def __init__(self):
        import pytimber
        self.db = pytimber.LoggingDB()  # initialize py timber database
        #self.db = pytimber.LoggingDB(source='ldb') 

    def get_fill_timespan(self, fill_number, phys_key='INJPHYS', verbose=True):
        '''Get the time stamps of the beginning and end for a specific fill.'''
        data = self.db.getLHCFillData(int(fill_number))['beamModes']
        output = ()
        for j in data:
            if j['mode'] == phys_key:
                output = (j['startTime'], j['endTime'])
    
        if len(output) == 2:
            return output
        else:
            if verbose:
                print ('ERROR: Fill {} has no key {}'.format(fill_number, phys_key))

    def get_fills_timespan(self, fills):
        '''
        Displays the timespan of given set of fills.
        '''
        f0 = sorted(list(fills))
        fill0, fill1 = int(f0[0]), int(f0[-1])
        dat_test0 = self.db.getLHCFillData(fill0)
        dat_test1 = self.db.getLHCFillData(fill1)
        print ('timespan\n--------\nfill, time:')
        print('{}, {}'.format(fill0, datetime.datetime.fromtimestamp(dat_test0['startTime'])))
        print('{}, {}'.format(fill1, datetime.datetime.fromtimestamp(dat_test1['startTime'])))
    

    def has_phase(self, fill_nr, lookfor='STABLE', verbose=False):
        '''
        Basic test if a given fill contains e.g. the STABLE phase of an LHC fill at top energy in order
        to avoid taking e.g. certain MD fills into account.
        '''
        bm = self.db.getLHCFillData(int(fill_nr))['beamModes']
        criterium = lookfor in [bmdict['mode'] for bmdict in bm]
        if not criterium and verbose:
            print ('Fill {} does not contain {} phase.'.format(fill_nr, lookfor))
        return criterium

    def _check_stable(self, fill_nr):
        return self.has_phase(fill_nr, lookfor='STABLE', verbose=False)

    def get_data(self, fill_nr, data_key, phys_key='INJPHYS', verbose=False, check_stable=True):
        '''Get the data for the selected beam and fill.'''

        startend = self.get_fill_timespan(fill_nr, phys_key=phys_key, verbose=verbose)
        if startend != None:

            ok = False
            if check_stable:
                # check if data leads to stable beams at top energy
                ok = self._check_stable(fill_nr=fill_nr)
    
            if ok:
                start, end = startend
                data = self.db.get(data_key, start, end)
                return data, start, end

    def get_lhcfills(self, year1, month1, day1, year2, month2, day2):
        '''
        Returns the number of LHC fills within a certain time span.
        '''
        d1 = datetime.date(year1, month1, day1)
        d2 = datetime.date(year2, month2, day2)
        unixtime1 = timelib.mktime(d1.timetuple())
        unixtime2 = timelib.mktime(d2.timetuple())
        lhcfills_data = self.db.getLHCFillsByTime(unixtime1, unixtime2)
        return sorted([int(lhcfills_data[k]['fillNumber']) for k in range(len(lhcfills_data))])

    def get_intensity_data(self, fills, beam_parameters, phys_key='INJPHYS', verbose=True, **kwargs):
        '''
        Load PyTimber beam intensity, bunch-by-bunch intensities and the octupole currents from the given fills.
        '''

        data_dict = {}
        k = 0
        good_fills, bad_fills = [], []
        for fill in fills:
            fill = int(fill)
            print('\r{0}/{1} Fill number: {2}'.format(k + 1, len(fills), fill), end='')
            data = self.get_data(fill, [beam_parameters.beam_intensity, beam_parameters.bunch_intensity] 
                                             + beam_parameters.oct_f + beam_parameters.oct_d, phys_key=phys_key, **kwargs)
            if not data == None:
                fill_dict, start, end = data
                data_dict[fill] = fill_dict
                good_fills.append(fill)
            else:
                bad_fills.append(fill)

            k += 1

        if verbose:
            print ('\nfills containing {}: {} out of {} fills in total.'.format(phys_key, len(good_fills), len(fills)))

        return pd.DataFrame(data_dict)

    def get_bbq_tunes(self, fills, beam_parameters):

        data_dict = {}
        k = 0
        for fill in fills:
            fill = int(fill)
            print('\r{0}/{1} Fill number: {2}'.format(k + 1, len(fills), fill), end='')
            tune_data = self.get_data(fill, [beam_parameters.tune_h, beam_parameters.tune_v])
            k += 1

            if tune_data == None:
                continue

            th, qh = tune_data[0][beam_parameters.tune_h]
            tv, qv = tune_data[0][beam_parameters.tune_v]
            start, end = tune_data[1:3]

            if len(qh) > 0 and len(qv) > 0:
                data_dict[fill] = {'H': [th, qh], 'V': [tv, qv]}

        return data_dict


class beam_optics_tool:
    '''
    Generic class to contain machine optics related parameters and calculations.
    '''

    def __init__(self):
        self._cfac_mmmrad = 1e6 # conversion of emittances [m] to/from [mm] [mrad]
        self._cfac = 1e-6 # conversion factor from WS raw data to [m] (default)

    @staticmethod
    def relativistic_factors(gev):
        ''' 
        Basic function to obtain the beta and gamma factor of a proton at the
        given value in GeV.
        '''
        c = const.speed_of_light
        e = const.elementary_charge
        pmass_si = const.proton_mass    
        energy_si = gev*1e9*e
        total_energy = np.sqrt(energy_si**2 + pmass_si**2)
        rest_energy = pmass_si*c**2
        gamma0 = total_energy/rest_energy
        beta0 = np.sqrt(1 - 1/gamma0)
        return gamma0, beta0

    def sigma2emit(self, sigma, hv, dpp, energy_gev, verbose=False):
        '''
        Compute the emittances at a given position, based on:
             sigma: beam rms size [mu m]
               dpp: energy rms size
        energy_gev: kinetic beam energy in GeV
        '''
        gamma0, beta0 = self.relativistic_factors(energy_gev)

        sigma = np.array(sigma)*self._cfac
        betagamma = beta0*gamma0
        emit = (sigma**2 - self.dispersion[hv]**2*dpp**2)/self.beta[hv]*betagamma*self._cfac_mmmrad # conversion to [mm mrad]
    
        if verbose:
            print ('-- {}-emittance calculation --'.format(hv))
            print ('    energy = {} [GeV]'.format(energy_gev))
            print ('      dp/p = {}'.format(dpp))
            print ('beta*gamma = {}'.format(betagamma))

        return emit

    def emit2sigma(self, emittance, hv, dpp, energy_gev):
        gamma0, beta0 = self.relativistic_factors(energy_gev)
        betagamma = beta0*gamma0
        sigma = np.sqrt(emittance/self._cfac_mmmrad/betagamma*self.beta[hv] +  self.dispersion[hv]**2*dpp**2)
        return sigma


class lhc_beam_params(beam_optics_tool):
    '''
    Convenience class to manage the LHC beam parameters which are of most interest in our studies.
    E.g. beam_nr = 1

    LHC wirescanner beam optics from table. E.g. for 2016:
    http://abpdata.web.cern.ch/abpdata/lhc_optics_web/www/opt2016/inj/index.html
    (IR4)

    N.B.
    Energy at flat bottom = 450 GeV
    '''
    def __init__(self, beam_nr, madx_tfs_filename, ws_horizontal='', ws_vertical=''):
        beam_optics_tool.__init__(self)
        self._cfac = 1e-6 # conversion factor from WS raw data to [m] (LHC default)
        self._length = 26658.8832 # the length of the LHC in [m]

        self.beam_nr = beam_nr

        # default position of the LHC wirescanners for beam 1 and 2
        if len(ws_horizontal) == 0:
            if self.beam_nr == 1:
                ws_horizontal = "BWS.5R4.B1"
            if self.beam_nr == 2:
                ws_horizontal = "BWS.5L4.B2"
        if len(ws_vertical) == 0:
            if self.beam_nr == 1:
                ws_vertical = "BWS.5R4.B1"
            if self.beam_nr == 2:
                ws_vertical = "BWS.5L4.B2"

        self.ws_horizontal = ws_horizontal
        self.ws_vertical = ws_vertical

        # Set optics parameters at the location of the wirescanners
        self.load_optics_parameters(madx_tfs_filename=madx_tfs_filename, ws_horizontal=ws_horizontal, ws_vertical=ws_vertical)

        # set standard names for experimental parameters to look for
        self._set_bsrt_emit()
        self._set_chroma()
        self._set_energy()
        self._set_octupole()
        self._set_tune()

    def load_optics_parameters(self, madx_tfs_filename, ws_horizontal, ws_vertical):
        '''
        The optics beta-functions depend on the current lattice used. This function loads
        these files from a MAD-X tfs file.

        INPUT
        =====
        madx_tfs_filename: The filename of a MAD-X twiss output.

        ws_horizontal, ws_vertical: The names of the wirescanners to be used for emittance calculations etc.
        Example:
        ws_horizontal = "BWS.5R4.B1"
        ws_vertical = "BWS.5R4.B1"
        '''

        self.beta = {}
        self.dispersion = {}

        self.madx_optics_file = madx_tfs_filename
        self.madx_ws_horizontal = ws_horizontal
        self.madx_ws_vertical = ws_vertical
        madx_data, self.madx_inputs = madx_tfs_reader.get_inputs([madx_tfs_filename])
        self.freq = const.c*self.madx_inputs['beta']/self._length  # set the revolution frequency, e.g. 11245.47506686197

        # Assign the horizontal and vertical beta functions to the locations of the horizontal and vertical
        # wirescanners, respectively:
        ele_ind_h = np.where([name == '"{}"'.format(ws_horizontal) for name in madx_data['name']])[0]
        ele_ind_v = np.where([name == '"{}"'.format(ws_vertical) for name in madx_data['name']])[0]
        if len(ele_ind_h) > 0 and len(ele_ind_v) > 0:
            ele_index_h = ele_ind_h[0]
            ele_index_v = ele_ind_v[0]
        else:
            raise RuntimeError('At least one element {}, {} not found.'.format(ws_horizontal, ws_vertical))
        self.beta['H'] = madx_data['beta_x'][ele_index_h]
        self.beta['V'] = madx_data['beta_y'][ele_index_v]
        self.dispersion['H'] = madx_data['d_x'][ele_index_h]
        self.dispersion['V'] = madx_data['d_y'][ele_index_v]

    def _set_bsrt_emit(self):
        self.bsrt_emit_h = 'LHC.BQSH.B{}:NORM_EMITTANCE_BSRT'.format(self.beam_nr)
        self.bsrt_emit_v = 'LHC.BQSV.B{}:NORM_EMITTANCE_BSRT'.format(self.beam_nr)
        
    def _set_chroma(self):
        self.chroma = 'LHC.BOFSU:CHROMA_B{}_H'.format(self.beam_nr)

    def _set_energy(self):
        self.energy = 'LHC.BQSH.B{}:ENERGY'.format(self.beam_nr)

    def _set_octupole(self):
        '''Get the LHC octupole strings.'''

        self.oct_d = ['RPMBB.RR13.ROD.A81B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.RR17.ROD.A12B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.RR53.ROD.A45B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.RR57.ROD.A56B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.RR73.ROD.A67B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.RR77.ROD.A78B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.UJ33.ROD.A23B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.UJ33.ROD.A34B{}:I_MEAS'.format(self.beam_nr)]

        self.oct_f = ['RPMBB.RR13.ROF.A81B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.RR17.ROF.A12B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.RR53.ROF.A45B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.RR57.ROF.A56B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.RR73.ROF.A67B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.RR77.ROF.A78B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.UJ33.ROF.A23B{}:I_MEAS'.format(self.beam_nr),
                      'RPMBB.UJ33.ROF.A34B{}:I_MEAS'.format(self.beam_nr)]

        # code to obtain the above strings:
        #octd = db.search("%ROD%I_MEAS")  # many thanks to Xavier: R: name of power converter, o: octupole, f/d: focusing/defocusing
        # get a group of the octupole currents for each LHC arc
        #octf = db.search("%ROF%I_MEAS")

    def _set_tune(self):
        self.tune_h = 'LHC.BQBBQ.CONTINUOUS.B{}:TUNE_H'.format(self.beam_nr)
        self.tune_v = 'LHC.BQBBQ.CONTINUOUS.B{}:TUNE_V'.format(self.beam_nr)


class lhc_intensity_params(lhc_beam_params):
    '''
    E.g. beam_nr = 1, intensity_instrument = 'A'
    '''
    def __init__(self, intensity_instrument, **kwargs):
        lhc_beam_params.__init__(self, **kwargs)
        self.intensity_instrument = intensity_instrument
        self._set_beam()
        self._set_bunch()

    def _set_beam(self):
        self.beam_intensity = 'LHC.BCTDC.{}6R4.B{}:BEAM_INTENSITY'.format(self.intensity_instrument, self.beam_nr)

    def _set_bunch(self):
        self.bunch_intensity = 'LHC.BCTFR.{}6R4.B{}:BUNCH_INTENSITY'.format(self.intensity_instrument, self.beam_nr)


def get_oct(data, keys, t0=None, t1=None, warnthr=5, verbose=True):
    '''
    This function is intended to read the octupole currents of the various groups, 
    given by the list of names 'keys'.
    It will return the average and error within the given time span.

    The data must be produced by _load_octupole_currents of offline_hdf_database.
    Function deprecated.
    '''
    warn_out = False
    
    if t0 == None or t1 == None:
        within = [0.2, 0.8]
        try:
            timestamps = data[keys[0]]['timestamp'].values
            start = timestamps[0]
            end = timestamps[-1]
            time_span = end - start
            t0 = start + within[0]*time_span
            t1 = start + within[1]*time_span
        except:
            pass
        
    values, values_err = {}, {}
    for k in keys:
        timestamps_df = data[k]['timestamp']
        values_df = data[k]['current']
        values_k = values_df.loc[(timestamps_df >= t0) & (timestamps_df <= t1)].values # the values of interest

        value_k = np.mean(values_k)
        rms_k = np.sqrt(np.mean((values_k - value_k)**2))
        values[k] = value_k
        values_err[k] = rms_k

        if rms_k/value_k > warnthr/100.0:
            warn_out = True
        
    # combine all octupole currents
    values0, values_err0 = np.array(list(values.values())), np.array(list(values_err.values()))

    ioct = np.mean(values0)
    ioct_err = np.sqrt(np.mean((values0 - ioct)**2))

    if ioct_err/ioct > warnthr/100.0:
        if verbose:
            print ('ioct spread {:.3f}'.format(ioct_err/ioct))
            print ('values {}'.format(values0))
        warn_out = True

    if ((np.abs(values0) != values0).any() and max(values0) > 0):
        if verbose:
            print ('ioct powered differently')
        warn_out = True

    out = {}
    out['ioct'] = ioct
    out['ioct_err'] = ioct_err
    out['warn'] = warn_out
    out['ioct_all'] = values
    out['time_span'] = [t0, t1]
    out['ioct_all_err'] = values_err
    return out

def get_slot_indices(indices_oi, pattern_oi, scheme, occurrences=[], verbose=False):
    '''
    Takes bunch indices of interest and a filling pattern of interest, and returns a list of valid slot indices
    with respect to the given filling scheme. The input scheme is taken from the output of
    offline_hdf_database.get_filling_scheme.

    occurrences: A list defining the indices when we collect the bunch indices if a pattern has been found. For example,
                occurrences = [2] means that the first two valid patterns are ignored. 
                If occurrences is empty, we collect the indices of all valid patterns.

    OUTPUT
    ======
    valid_slot_indices, valid_indices
    - The list 'valid_slot_indices' contains the slot numbers of the filling scheme where the 
      bunches of interest have been found.
    - The list 'valid_indices' has the same length as 'valid_slot_indices' and so that it holds for every element:
      valid_indices[k] in indices_oi and valid_slot_indices[k] is the slot of that specific bunch.
      For example, if indices_oi = [1, 4, 5] and we are looking for a specific pattern with two occurrences,
      then e.g. valid_slot_indices = [121, 124, 125, 221, 224, 225] and valid_indices = [1, 4, 5, 1, 4, 5]. 

    Note that it is possible to pass over a specific slot several times, with different indices of interest, 
    if valid patterns overlap.
    
    Example
    =======
      pattern_oi = [(">=", 8, 0), ("=", 48, 1), ("=", 8, 0), ("=", 48, 1), ("=", 8, 0), ("=", 48, 1), (">=", 8, 0)]
    This means that we are looking in the filling scheme of a pattern which starts with 8 or more empty slots,
    then 48 filled slots, then 8 empty slots, then 48 filled slots, then 8 empty slots, then 48 filled slots and
    finally 8 or more empty slots.
      indices_oi = [0, 1, 2, 3, 4]
    Whenever a pattern of the above kind has been found in the filling scheme, we will return the slot indices of the
    0-th to 4th filled slots.
    '''
    ops = {">=": operator.ge, "<=": operator.le, ">": operator.gt, "<": operator.lt, "=": operator.eq}
    
    if verbose:
        print ('pattern of interest:\n {}'.format(pattern_oi))
        print ('indices of interest:\n {}'.format(indices_oi))
        print ('occurrences:\n {}'.format(occurrences))

    groups = scheme['scheme']
    
    # Step 1: 
    # Determine the lengths of the fill pattern, by
    # constructing a list 'fill_pattern' whose elements have the form (n, f, i0, i1),
    # where n denotes the number of slots, f \in {0, 1} means: these slots
    # are not filled (0) or filled (1) and i0 and i1 are the start and end slot indices.
    prev = 0
    fill_pattern = []
    for group in groups:
        if len(group) == 0:
            continue
        start, stop = group[0], group[-1]
        length = stop - start + 1
        diff = start - prev
        fill_pattern.append((diff, 0, prev, start))
        fill_pattern.append((length, 1, start, stop))
        prev = stop
        
    # Step 2:
    # Iterate over the given fill scheme, identify the valid patterns and obtain the valid slot indices:
    nfp = len(fill_pattern)
    nfp_oi = len(pattern_oi)
    valid_slot_indices = []
    valid_indices = []
    occurrence_count = 0
    for k in range(nfp - nfp_oi):
        fill_pattern_k = fill_pattern[k: k + nfp_oi] # e.g. = [(13, 0, 0, 13), (12, 1, 13, 24), (32, 0, 24, 56)]
        condition = True
        for j in range(nfp_oi):
            psj = fill_pattern_k[j]
            psj_oi = pattern_oi[j]
            condition = condition and psj[1] == psj_oi[2] and ops[psj_oi[0]](psj[0], psj_oi[1]) 
        if not condition:
            continue

        fill_pattern_filled = [e for e in fill_pattern_k if e[1] == 1]
        if len(occurrences) > 0:
            occurrence_count += 1
            if occurrence_count - 1 not in occurrences:
                continue

        if len(fill_pattern_filled) > 0:
            indices_with_bunches = np.hstack([range(e[2], e[3] + 1) for e in fill_pattern_filled])
            valid_slot_indices.append(indices_with_bunches[indices_oi])
            valid_indices.append(indices_oi)
       
    if len(valid_slot_indices) > 0:
        return np.hstack(valid_slot_indices), np.hstack(valid_indices)
    else:
        return [], []


class offline_hdf_database:
    '''
    Class to store and load PyTimber data in form of HDF5 files for offline use.
    '''
    def __init__(self, filename, verbose=True):
        if verbose:
            print ('Initializing HDF database ...')
            print ('Filename:\n {}'.format(filename))
        self.filename = filename

        ##############################################
        # default names of tables in the HDF databases
        self._bunch_table_name_prefix = 'bunch_intensities'
        self._bunch_fill_info = 'bunch_fill_information'

        self._beam_table_name = 'beam_intensities'
        self._loss_table_name = 'bunch_losses'

        self._octupole_group_prefix = 'oct'
        self._octupole_group_names = 'octupole_group_names'

        self._selected_fills_prefix = 'selected_fill'
        self._selected_fills_info = 'selected_fills'

        self._selected_fills_octupoles = 'selected_fills_oct'

        self._bsrt_regression_info = 'bsrt_regression' # Store all table names of the selected regression.
        self._bsrt_regression_table_prefix = 'bsrt_reg_' # Store the individual regression data.
        ##############################################

    def set_beam(self, machine_params):
        '''
        Sets PyTimber beam keys.
        machine_params must be an instance of a class having the functionality of lhc_intensity_params.
        '''
        self.beam = machine_params

    def get_table(self, table_name, hdfstore=None, close_hdf=True, **kwargs):
        '''
        Obtain a table from the HDF5 database.
        **kwargs is given to Pandas HDFStore routine.
        '''
        if hdfstore == None:
            hdfstore = pd.HDFStore(self.filename, mode='r')
        try:
            table = hdfstore.select(table_name, **kwargs)
        except Exception as e:
            print (str(e))
            table = []
        if close_hdf:
            hdfstore.close()
        return table

    def get_tables(self, table_name_list, kwargs_list):
        '''
        Obtain multiple tables from the HDF5 database.
        kwargs_list is an optional list to be given to the Pandas HDFStore routine.
        '''
        tables = []
        n_tables = len(table_name_list)
        if n_tables > 0: # ensure that we run the next loop at least once, and close the HDF database in any case.
            hdfstore = pd.HDFStore(self.filename, mode='r')

        close_hdf = False
        for k in range(n_tables):
            table_name = table_name_list[k]
            if len(kwargs_list) > k:
                kwargs = kwargs_list[k]
            if k == n_tables - 1:
                close_hdf = True
            tables.append(self.get_table(table_name=table_name, hdfstore=hdfstore, close_hdf=close_hdf, **kwargs))
        return tables

    def remove_table(self, table_name):
        hdfstore = pd.HDFStore(self.filename, mode='w')
        try:
            hdfstore.remove(table_name)
        except Exception as e: 
            print (str(e))
        hdfstore.close()        

    def store_raw_data(self, data, verbose=True):
        '''
        Store all data, generated via the pytimber_tools.get_intensity_data, to HDF database. 
        '''
        self._store_beam_intensities(data=data, verbose=verbose)
        self._store_octupole_currents(data=data, verbose=verbose)
        self._store_bunch_intensities(data=data, verbose=verbose)

    def _store_octupole_currents(self, data, verbose=True):
        '''
        Store octupole currents to HDF database. The data must be generated via the get_pytimber_data function.
        '''
        octupole_strings = self.beam.oct_d + self.beam.oct_f
        octupole_group_index = 1

        hdfstore = pd.HDFStore(self.filename, mode='a')

        fills = sorted(data.keys())
        for oct_string in octupole_strings:
            xvals, yvals = [], []
            fills_multiple = []
            for fill in fills:
                xvals_fill, yvals_fill = data[fill][oct_string]
                xvals.append(xvals_fill)
                yvals.append(yvals_fill)
                fills_multiple.append(len(xvals_fill)*[int(fill)])

            xvals = np.hstack(xvals)
            yvals = np.hstack(yvals)
            fills_multiple = np.hstack(fills_multiple)
            tohdf = pd.DataFrame(np.array([fills_multiple, xvals, yvals]).transpose(), columns=['fill', 'timestamp', 'current'])
            
            table_name = '{}_{}'.format(self._octupole_group_prefix, octupole_group_index)
            if verbose:
                print ('Storing {} into table "{}" ...'.format(oct_string, table_name))

            tohdf = tohdf.astype({'fill': 'int'})
            hdfstore.put(value=tohdf, key=table_name, format='t', data_columns=True)

            octupole_group_index += 1
        # we create tables of the form oct_1_XXX oct_2_XXX etc.. To relate the number to the respective PyTimber string, we keep track of the strings and store them also in the database
        octupole_names = pd.DataFrame(np.array([range(1, len(octupole_strings) + 1), octupole_strings]).transpose(), 
                                      columns=['octupole_index', 'name'])
        if verbose:
            print ('Storing octupole strings into table "{}" ...'.format(self._octupole_group_names))
        hdfstore.put(value=octupole_names, key=self._octupole_group_names, format='t', data_columns=True)
        hdfstore.close()

    def _store_beam_intensities(self, data, verbose=True):
        '''
        Store beam intensities to HDF database. The data must be generated via the pytimber_tools.get_intensity_data function.
        '''
        fills = sorted(data.keys())
        xvals, yvals = [], []
        fills_multiple = []
        for fill in fills:
            xvals_fill, yvals_fill = data[fill][self.beam.beam_intensity]
            xvals.append(xvals_fill)
            yvals.append(yvals_fill)
            fills_multiple.append(len(xvals_fill)*[int(fill)])

        xvals = np.hstack(xvals)
        yvals = np.hstack(yvals)
        fills_multiple = np.hstack(fills_multiple)
        tohdf = pd.DataFrame(np.array([fills_multiple, xvals, yvals]).transpose(), 
                             columns=['fill', 'timestamp', 'beam_intensity'])
        if verbose:
            print ('Storing beam intensities into table "{}" ...'.format(self._beam_table_name))
        hdfstore = pd.HDFStore(self.filename, mode='a')
        hdfstore.put(value=tohdf, key=self._beam_table_name, format='t', data_columns=True)
        hdfstore.close()

    def _store_bunch_intensities(self, data, verbose=True):
        '''
        Store bunch intensities to HDF database. The data must be generated via the pytimber_tools.get_intensity_data function.
        '''
        if verbose:
            print ('Storing bunch intensities into tables "{}_<fill nr>" ...'.format(self._bunch_table_name_prefix))

        fills = sorted(data.keys())
        n_fills = len(fills)
        valid_fills = []
        hdfstore = pd.HDFStore(self.filename, mode='a')
        for fill_index in range(n_fills):
            fill = int(fills[fill_index])
            xvals_fill, yvals_fill = data[fill][self.beam.bunch_intensity]

            if len(yvals_fill) == 0:
                print ('ERROR: No data found for fill {}. Skipping.'.format(fill))
                continue

            n_timestamps = len(yvals_fill)
            timestamps, intensities, slot_indices = [], [], []
            for k in range(n_timestamps):
                timestamp = xvals_fill[k]
                intensities_all_slots = yvals_fill[k]
                n_slots = len(intensities_all_slots)
                timestamps.append([timestamp]*n_slots)
                intensities.append(intensities_all_slots)
                slot_indices.append(range(1, n_slots + 1))

            timestamps = np.hstack(timestamps)
            intensities = np.hstack(intensities)
            slot_indices = np.hstack(slot_indices)

            tohdf = pd.DataFrame(np.array([timestamps, slot_indices, intensities]).transpose(), 
                                 columns=['timestamp', 'slot_index', 'intensity'])

            table_name = '{}_{}'.format(self._bunch_table_name_prefix, fill)
            if verbose:
                print ('\rStoring data of fill {}, {}/{}'.format(fill, fill_index + 1, n_fills), end='')
            tohdf = tohdf.astype({'slot_index': 'int'})
            hdfstore.put(value=tohdf, key=table_name, format='t', data_columns=True)

            timestamp_ref = min(timestamps)
            valid_fills.append([fill, timestamp_ref, table_name])

        if len(valid_fills) > 0:
            # also store a general information about the fills
            if verbose:
                print ('\nStoring fill information into table "{}"'.format(self._bunch_fill_info))
            fill_info = pd.DataFrame(valid_fills, columns=['fill', 'timestamp', 'name'])
            hdfstore.put(value=fill_info, key=self._bunch_fill_info, format='t', data_columns=True)
        hdfstore.close()

    def get_fill_numbers(self):
        '''
        Returns an array of fill numbers if they were created with _store_bunch_intensities.
        '''
        hdfstore = pd.HDFStore(self.filename, mode='r')
        fill_numbers = hdfstore.select(key=self._bunch_fill_info)['fill'].values
        hdfstore.close()
        return fill_numbers

    def _load_octupole_currents(self, octupole_names=[], hdfstore=None, **kwargs):
        '''
        Load octupole currents from HDF database.
        '''
        close_hdf = False
        if hdfstore == None:
            hdfstore = pd.HDFStore(self.filename, mode='r')
            close_hdf = True

        if len(octupole_names) == 0:
            octupole_names = self.beam.oct_d + self.beam.oct_f

        names2index = hdfstore.select(key=self._octupole_group_names)

        out = {}
        for k in range(len(names2index)):
            index = names2index['octupole_index'].iloc[k]
            octupole_group_name = names2index['name'].iloc[k]
            out[octupole_group_name] = hdfstore.select(key='{}_{}'.format(self._octupole_group_prefix, index), **kwargs).reset_index(drop=True)

        if close_hdf:
            hdfstore.close()
        return out

    def _load_octupole_currents_fill(self, fill_nr, hdfstore=None):
        '''
        Load octupole currents from HDF database for specific fill number.
        '''
        return self._load_octupole_currents(where="'fill' == '{}'".format(fill_nr), hdfstore=hdfstore)

    def _load_beam_intensities(self, **kwargs):
        '''
        Load beam intensities from HDF database.
        '''
        return self.get_table(self._beam_table_name, **kwargs)

    def _load_bunch_intensities(self, fill_nr, hdfstore=None, **kwargs):
        '''
        Load bunch intensities from HDF database for a given fill number.
        '''
        if hdfstore == None:
            hdfstore = pd.HDFStore(self.filename, mode='r')
            bunch_intensities = hdfstore.select(key='{}_{}'.format(self._bunch_table_name_prefix, fill_nr), **kwargs)
            hdfstore.close()
        else:
            bunch_intensities = hdfstore.select(key='{}_{}'.format(self._bunch_table_name_prefix, fill_nr), **kwargs)
        return bunch_intensities

    def get_filling_scheme(self, fill_nr, hdfstore=None, **kwargs):
        '''
        Determine the filling scheme based on the bunch intensities and a given threshold. Also returns
        the bunch indices, so that it is not necessary to read from the DB again.

        kwargs: passed to self._load_bunch_intensities. In particular, can be a condition of the form 
        where="'intensity' > 7e10" 
        etc.

        Returns a dictionary with the following values:

        'scheme':
        A list of arrays. Each array correspond to a group of bunches. The elements of an individual array
        correspond to the slot number.

        'intensities':
        A Pandas DataFrame object with three columns: 'timestamp', 'slot_index' and 'intensity'. This DataFrame 
        therefore contains the intensity evolution for every slot_index which has been selected
        according to the conditions by which this method was called (so every slot_index appearing in 'scheme').
        '''
        bunch_intensities = self._load_bunch_intensities(fill_nr=fill_nr, hdfstore=hdfstore, **kwargs)
        slots_filled = bunch_intensities['slot_index'].unique()
        scheme = cluster_successors(slots_filled)
        return {'scheme': scheme, 'intensities': bunch_intensities}


    def get_slot_intensities(self, indices_oi, pattern_oi, fills=[], occurrences=[],
                             verbose=True, **kwargs):
        '''
        Passes through a given set of fills and read the filling scheme. Then find those
        indices according to the pattern of interest 'pattern_oi' and indices of interest
        'indices_oi'. For occurrences and further details see routine get_bunch_indices.

        INPUT
        =====
        kwargs: passed to self.get_filling_scheme. In particular, can be a condition of the form 
        where="'intensity' > 7e10" 
        where="'slot_index' == 101"
        etc.
        '''
        if verbose:
            print ('pattern of interest:\n {}'.format(pattern_oi))
            print ('indices of interest:\n {}'.format(indices_oi))
            print ('occurrences:\n {}'.format(occurrences))
            if 'where' in kwargs.keys():
                print ('where: {}'.format(kwargs['where']))
            else:
                print ('WARNING: No further conditions specified!')
    
        if len(fills) == 0:
            fills = self.get_fill_numbers()
        
        hdfstore = pd.HDFStore(self.filename, mode='r')

        n_fills, fill_index = len(fills), 0
        matched_index = 1
        slot_intensities = {}
        total_bunch_numbers = {}
        for fill in fills:
            fill_index += 1
            if verbose:
                print ('\rreading fill {}, {}/{} ...'.format(fill, fill_index, n_fills), end='')
            scheme = self.get_filling_scheme(fill, hdfstore=hdfstore, **kwargs)
            valid_slot_indices, valid_indices = get_slot_indices(indices_oi=indices_oi, 
                                                                   pattern_oi=pattern_oi, 
                                                                   scheme=scheme, 
                                                                   occurrences=occurrences)
            if len(valid_slot_indices) == 0:
                continue

            all_intensities = scheme['intensities']
            locations = all_intensities['slot_index'].isin(valid_slot_indices)

            dfi = all_intensities[['timestamp', 'slot_index', 'intensity']].loc[locations].reset_index(drop=True)
            # also store the respective indices of interest
            dfi['bunch_index'] = dfi['slot_index'].apply(lambda x: dict(zip(valid_slot_indices, valid_indices))[x])
            # if a pattern is found several times, it can (and will) happen that different slot indices belong to
            # the same index of interest. Therefore we also introduce a general bunch counter.
            unique_slots = sorted(np.unique(dfi['slot_index'].values))
            dfi['bunch_count'] = dfi['slot_index'].apply(lambda x: unique_slots.index(x) + 1)

            n_total_bunches = len(np.hstack(scheme['scheme']))
            total_bunch_numbers[fill] = n_total_bunches

            slot_intensities[fill] = dfi
                
            matched_index += 1
        hdfstore.close()

        if verbose:
            print ('\n{}/{} fills matched conditions.'.format(matched_index - 1, n_fills))

        return slot_intensities, total_bunch_numbers


    def _store_selected_fills(self, slot_intensities, verbose=True):
        '''
        Stores the selected fills and data given by self.get_slot_intensities
        '''
        if verbose:
            print ('Storing selected fills in tables "{}_<fill_nr>" ...'.format(self._selected_fills_prefix))

        hdfstore = pd.HDFStore(self.filename, mode='a')

        fills = sorted(slot_intensities.keys())
        n_fills = len(fills)
        info_fills = []
        for fill_index in range(n_fills):
            fill = int(fills[fill_index])
            if verbose:
                print ('\rFill {}, {}/{}'.format(fill, fill_index + 1, n_fills), end='')

            table_name = '{}_{}'.format(self._selected_fills_prefix, fill)
            hdfstore.put(value=slot_intensities[fill], key=table_name, format='t', data_columns=True)
            times = slot_intensities[fill]['timestamp'].values
            time0, time1 = min(times), max(times)
            info_fills.append([fill, time0, time1, table_name])
        # also store the list of selected fills
        if verbose:
            print ('\nStoring selected fills information in table "{}"'.format(self._selected_fills_info))
        hdfstore.put(value=pd.DataFrame(info_fills, columns=['fill', 'time0', 'time1', 'name']), 
                          key=self._selected_fills_info, format='t', data_columns=True)
        hdfstore.close()

    def _store_selected_fills_octupoles(self, ioct_range, table_id, otype='F', verbose=True):
        '''
        Determine a subset of the selected fills in which the octupole currents are within a given range.
        N.B. the database must have the respective fields.

        otype: (str) type of octupole ('F', 'D') for which we want to check octupole current range against.

        table_id: string to identify the octupole table in the HDF file.

        '''
        hdfstore = pd.HDFStore(self.filename, mode='a')
        selected_fills_db = hdfstore.select(key=self._selected_fills_info)
        fills = selected_fills_db['fill'].values
        n_fills = len(fills)
        valid_fills, iocts, ioct_errs = [], [], []
        for fill_index in range(n_fills):
            fill = int(fills[fill_index])
            if verbose:
                print ('\rProcessing octupole currents of fill {}, {}/{} ...'.format(fill, fill_index + 1, n_fills), end='')

            # we need the times of the fills in question ...
            sdata_fill = selected_fills_db.iloc[fill_index]
            tmin, tmax = sdata_fill['time0'], sdata_fill['time1']
            try:
                octupoles_fill = self._load_octupole_currents_fill(fill_nr=fill, hdfstore=hdfstore)
            except:
                if verbose:
                    print ('No octupole current information available for fill {}'.format(fill))
                continue
            of = get_oct(octupoles_fill, keys=self.beam.oct_f, t0=tmin, t1=tmax)
            od = get_oct(octupoles_fill, keys=self.beam.oct_d, t0=tmin, t1=tmax)

            if of['warn'] or od['warn']:
                if verbose:
                    print (' skipping fill {}, which seems to have bad octupole currents.'.format(fill))
                continue

            if otype == 'F':
                condition = ioct_range[0] <= of['ioct'] and of['ioct'] <= ioct_range[1]

            if not condition:
                if verbose:
                    print ('invalid; octupole strength out of bounds: {}'.format(of['ioct']))
                continue

            valid_fills.append(fill)
            iocts.append(of['ioct'])
            ioct_errs.append(of['ioct_err'])

        tohdf = pd.DataFrame(np.array([valid_fills, iocts, ioct_errs]).transpose(), 
                columns=['fill', 'current', 'rms']).astype({'fill': 'int'})
        table_name = '{}_{}'.format(self._selected_fills_octupoles, table_id)
        if verbose:
            print ('\n{}/{} fills matched the condition.'.format(len(valid_fills), n_fills))
            print ('Storing fill for valid octupole currents in table:\n "{}"'.format(table_name))
        hdfstore.put(value=tohdf, key=table_name, format='t', data_columns=True)
        hdfstore.close()

    def get_selected_fills_octupoles(self, table_id, verbose=True):
        '''
        Returns the selected fills for a given octupole ID.
        '''
        table_name = '{}_{}'.format(self._selected_fills_octupoles, table_id)
        if verbose:
            print ('Reading from table:\n {}'.format(table_name))
        data = self.get_table(table_name)
        if len(data) > 0:
            return data['fill'].values
        else:
            return []

    def get_octupole_strengths(self, table_id=None, verbose=True):
        '''
        Get the strengths of the octupole currents for the fills in table self._selected_fills_info
        '''

        octupole_names = self.get_table(self._octupole_group_names)
        if table_id is not None:
            selected_fills_table = self.get_table(self._selected_fills_octupoles + '_{}'.format(table_id))
        else:
            selected_fills_table = self.get_table(self._selected_fills_info)

        octupole_data = {}

        fills = selected_fills_table['fill'].values
        n_fills = len(fills)

        for k in range(len(octupole_names)):
            octupole_info = octupole_names.iloc[k]
            octupole_index = octupole_info['octupole_index']
            group_name = octupole_info['name']
            octupole_table = self.get_table('{}_{}'.format(self._octupole_group_prefix, octupole_index))

            if verbose:
                print ('{} / {}'.format(k + 1, len(octupole_names)))

            for fill_index in range(n_fills):
                fill = int(fills[fill_index])

                if verbose:
                    print ('\rProcessing octupole currents of {}, fill {}, {}/{} ...'.format(group_name, fill, fill_index + 1, n_fills), end='')

                ofill = octupole_table.loc[(octupole_table['fill'] == fill)]
        
                idata_fill = self.get_table(table_name='{}_{}'.format(self._selected_fills_prefix, fill)) # need to improve this to speed up
                times_fill = idata_fill['timestamp'].values
                tmin, tmax = min(times_fill), max(times_fill)
        
                o1 = get_oct({group_name: ofill}, keys=[group_name], t0=tmin, t1=tmax)
                
                if fill not in octupole_data.keys():
                    octupole_data[fill] = {}
                octupole_data[fill][group_name] = [o1['ioct'], o1['ioct_err']]

            if verbose:
                print ()
        return octupole_data

    def _show_octupole_strengths_setup(self, octupole_data, title='', figsize=(12, 4), setup='focusing'):

        legend = 3  # horizontal space for legend
        # we create a plot with 2 subplots (in which the 2nd one is empty) in order to make space for the legend, which
        # we want to show on the right-hand side, outside of the plot.
        fig, axes = plt.subplots(figsize=(figsize[0] + legend, figsize[1]), 
                                    ncols=2, gridspec_kw={'width_ratios': [figsize[0], legend]})

        fills = sorted(octupole_data.keys())
        if setup == 'defocusing':
            oct_labels = self.beam.oct_d
        if setup == 'focusing':
            oct_labels = self.beam.oct_f

        for label in oct_labels:
            ydata = [octupole_data[f][label][0] for f in fills]
            ydata_err = [octupole_data[f][label][1] for f in fills]
    
            axes[0].scatter(x=fills, y=ydata, s=10, label='{}'.format(label))
    
        axes[0].set_xlabel('Fill number')
        axes[0].set_ylabel(r'$I_\mathrm{oct.}$')
        axes[1].axis('off')
        plt.title('{} beam {}, {} octupoles'.format(title, self.beam.beam_nr, setup), loc='right')
        lgnd = fig.legend(loc=7)
        for k in range(len(lgnd.legendHandles)):
            lgnd.legendHandles[k]._sizes = [40] 
        plt.show()

    def show_octupole_strengths(self, figsize=(12, 4), verbose=True, **kwargs):
        '''
        Show the strengths of the octupole currents for the fills in table self._selected_fills_info
        '''
        octupole_data = self.get_octupole_strengths(verbose=verbose, **kwargs)
        self._show_octupole_strengths_setup(octupole_data, setup='focusing')
        self._show_octupole_strengths_setup(octupole_data, setup='defocusing')

    def compute_measured_losses(self, start_time=0, fills=[], final_values=True, verbose=True):
        '''
        Determine the (measured) relative losses, based on specific fills and an optional duration window, as well as the duration times.

        If fills are not specified, then the function will read them from
        self._selected_fills_info

        start_time: optional time [s] from which we compute the relative losses, relative to the first time when data is available.

        final_values: If 'True' consider only the final values of the individual intensity data.
        '''

        # get the names of the intensity tables
        if verbose:
            print ('Reading intensity from table:\n "{}"'.format(self._selected_fills_info))
        hdfstore = pd.HDFStore(self.filename, mode='r+')
        intensity_tables = hdfstore.select(self._selected_fills_info)

        if len(fills) == 0:
            fills = intensity_tables['fill'].unique()

        columns = ['fill', 'slot_index', 'bunch_index', 'bunch_count', 'timestamp', 'duration', 'intensity0', 'loss']

        all_data = []
        skip_fill = False
        n_fills = len(fills)
        fill_index = 0
        for fill in fills:
            fill = int(fills[fill_index])
            if verbose:
                print ('\rFill {}, {}/{}'.format(fill, fill_index + 1, n_fills), end='')
            fill_index += 1

            intensity_table_info = intensity_tables.loc[intensity_tables['fill'] == fill]
            intensity_table_name = intensity_table_info['name'].values[0]
            timestamp_ref = intensity_table_info['time0'].values[0] # the time when first intensity has been measured.

            t0 = timestamp_ref + start_time
            intensities_df = hdfstore.select(key=intensity_table_name, where='"timestamp" >= {}'.format(t0))

            slot_indices = intensities_df['slot_index'].unique()          

            rel_intensities, init_intensities = [], []
            nz_bunch_indices, nz_slot_indices, nz_bunch_counts = [], [], []
            timestamps, durations = [], []
            for slot_index in slot_indices:
                data_bunch = intensities_df.loc[intensities_df['slot_index'] == slot_index]

                if len(data_bunch) == 0:
                    continue

                intensities_bunch = data_bunch['intensity'].values
                timestamps_bunch = data_bunch['timestamp'].values
                initial_intensity_bunch = intensities_bunch[0]
                durations_bunch = timestamps_bunch - timestamps_bunch[0]
                rel_intensities_bunch = (intensities_bunch - initial_intensity_bunch)/initial_intensity_bunch

                if final_values == True:
                    # only use the final value of the series
                    timestamps_bunch = np.array([timestamps_bunch[-1]])
                    durations_bunch = np.array([durations_bunch[-1]])
                    rel_intensities_bunch = np.array([rel_intensities_bunch[-1]])

                # if the intensities have only one datapoint, we do not store them
                non_zero_duration_indices = np.where(durations_bunch)
                
                timestamps_bunch = timestamps_bunch[non_zero_duration_indices]
                durations_bunch = durations_bunch[non_zero_duration_indices]
                rel_intensities_bunch = rel_intensities_bunch[non_zero_duration_indices]

                if len(timestamps_bunch) == 0:
                    # do not store empty data
                    continue

                nz_slot_indices.append(data_bunch['slot_index'].values[non_zero_duration_indices]) # = slot_index
                nz_bunch_indices.append(data_bunch['bunch_index'].values[non_zero_duration_indices])
                nz_bunch_counts.append(data_bunch['bunch_count'].values[non_zero_duration_indices])
                timestamps.append(timestamps_bunch)
                durations.append(durations_bunch)
                rel_intensities.append(rel_intensities_bunch)
                init_intensities.append(np.array([initial_intensity_bunch]*len(timestamps_bunch)))

            timestamps = np.hstack(timestamps)
            durations = np.hstack(durations)
            rel_intensities = np.hstack(rel_intensities)
            init_intensities = np.hstack(init_intensities)
            nz_slot_indices = np.hstack(nz_slot_indices)
            nz_bunch_indices = np.hstack(nz_bunch_indices)
            nz_bunch_counts = np.hstack(nz_bunch_counts)
            fill = [fill]*len(durations)
            datablock = np.array([fill, nz_slot_indices, nz_bunch_indices, nz_bunch_counts, timestamps, durations, init_intensities, rel_intensities]).transpose()
            all_data.append(pd.DataFrame(datablock, columns=columns).astype({'fill': 'int', 'slot_index': 'int', 
                                    'bunch_index': 'int', 'bunch_count': 'int'}))

        if len(all_data) > 0:
            all_data = pd.concat(all_data, ignore_index=True)
            if verbose:
                print ('\nStoring bunch losses into table "{}" ...'.format(self._loss_table_name))
            hdfstore.put(value=all_data, key=self._loss_table_name, format='t', data_columns=True)
        else:
            print ('\nERROR: no data.')
        hdfstore.close()

    def get_loss_statistics(self, bunches=[], drop_pos_losses=True, verbose=True, **kwargs):
        '''
        Get statistics of the bunch losses from the database table self._loss_table_name. 
        Keyworded arguments are passed to hdfstore.select.

        INPUT
        =====
        bunches: An optional list of bunches to be considered.
        drop_pos_losses: Cases which admit 'positive' losses are not considered.
        '''
        loss_data = self.get_table(self._loss_table_name, **kwargs)
        if verbose:
            print ('drop_pos_losses: {}'.format(drop_pos_losses))
            print ('Reading losses from table:\n {}'.format(self._loss_table_name))

        if len(bunches) > 0:
            loss_data = loss_data.loc[loss_data['bunch_index'].isin(bunches)]

        if drop_pos_losses:
            loss_data_new = loss_data.loc[(loss_data['loss'] < 0)]
            if verbose:
                print ('dropping {}/{} cases because of invalid beam loss raise.'.format(len(loss_data) - len(loss_data_new), len(loss_data)))
            loss_data = loss_data_new

        return loss_data

    def show_loss_statistics(self, bunches=[], duration_window=[], bins='auto', title='', figsize=(6, 4), drop_pos_losses=True, 
                             verbose=True, **kwargs):
        '''
        Show statistics of the bunch losses in the database. Keyworded arguments are passed to hdfstore.select.

        INPUT
        =====
        see self.get_loss_statistics.
        '''

        loss_db = self.get_loss_statistics(bunches=bunches, drop_pos_losses=drop_pos_losses, verbose=verbose, **kwargs)

        if len(duration_window) > 0:
            loss_db = loss_db.loc[(loss_db['duration'] >= duration_window[0]) &
                                  (loss_db['duration'] <= duration_window[1])]

            if verbose:
                print ('losses shown with respect to a duration window of:\n {} [s]'.format(duration_window))
                print ('# of windowed data points: {}'.format(len(loss_db)))

        plt.figure(figsize=figsize)
        plt.hist(-loss_db['loss'].values, histtype='step', bins=bins)
        plt.xlabel(r'$\Delta I/I_0$')
        plt.ylabel(r'$N$')
        plt.title(title)

        plt.figure(figsize=figsize)
        plt.hist(loss_db['duration'].values, histtype='step', bins=bins)
        plt.xlabel('Duration [s]')
        plt.ylabel(r'$N$')
        plt.title(title)

        plt.show()


class offline_sql_database(six_instance):
    '''
    Class to store and load PyTimber data via SQL files for offline use (experimental class).
    '''
    def __init__(self, filename):
        six_instance.__init__(self, filename=filename)

    def set_beam(self, **kwargs):
        '''
        Sets PyTimber beam keys.
        '''
        self.beam = lhc_beam_params(**kwargs)

    def _store_octupole_currents(self, data):
        '''
        Store octupole currents to SQL database. The data must be generated via the pytimber_tools.get_intensity_data function.
        '''

        fills = sorted(data.keys())
        for oct_string in self.beam.oct_d + self.beam.oct_f:
            xvals, yvals = [], []
            fills_multiple = []
            for fill in fills:
                xvals_fill, yvals_fill = data[fill][oct_string]
                xvals.append(xvals_fill)
                yvals.append(yvals_fill)
                fills_multiple.append(len(xvals_fill)*[fill])

            xvals = np.hstack(xvals)
            yvals = np.hstack(yvals)
            fills_multiple = np.hstack(fills_multiple)
            tosql = pd.DataFrame(np.array([fills_multiple, xvals, yvals]).transpose(), columns=['fill', 'timestamp', 'current'])
            self._store_data(oct_string, tosql, dtype={'fill': 'int', 'timestamp': 'float', 'current': 'float'})

    def _store_beam_intensities(self, data):
        '''
        Store beam intensities to SQL database. The data must be generated via the pytimber_tools.get_intensity_data function.
        '''

        fills = sorted(data.keys())
        xvals, yvals = [], []
        fills_multiple = []
        for fill in fills:
            xvals_fill, yvals_fill = data[fill][self.beam.beam_intensity]
            xvals.append(xvals_fill)
            yvals.append(yvals_fill)
            fills_multiple.append(len(xvals_fill)*[fill])

        xvals = np.hstack(xvals)
        yvals = np.hstack(yvals)
        fills_multiple = np.hstack(fills_multiple)
        tosql = pd.DataFrame(np.array([fills_multiple, xvals, yvals]).transpose(), 
                             columns=['fill', 'timestamp', 'beam_intensity'])
    
        self._store_data(self.beam.beam_intensity, tosql, 
                                 dtype={'fill': 'int', 'timestamp': 'float', 'beam_intensity': 'float'})

    def _store_bunch_intensities(self, data, verbose=True):
        '''
        Store bunch intensities to SQL database. The data must be generated via the pytimber_tools.get_intensity_data function.
        '''

        fills = sorted(data.keys())
        n_fills = len(fills)
        for fill_index in range(n_fills):
            fill = fills[fill_index]
            xvals_fill, yvals_fill = data[fill][self.beam.bunch_intensity]

            if len(yvals_fill) == 0:
                print ('ERROR: No data found for fill {}. Skipping.'.format(fill))
                continue

            n_timestamps = len(yvals_fill)
            timestamps, intensities, slot_indices = [], [], []
            for k in range(n_timestamps):
                timestamp = xvals_fill[k]
                intensities_all_slots = yvals_fill[k]
                n_slots = len(intensities_all_slots)
                timestamps.append([timestamp]*n_slots)
                intensities.append(intensities_all_slots)
                slot_indices.append(range(1, n_slots + 1))

            timestamps = np.hstack(timestamps)
            intensities = np.hstack(intensities)
            slot_indices = np.hstack(slot_indices)

            tosql = pd.DataFrame(np.array([timestamps, slot_indices, intensities]).transpose(), 
                                 columns=['timestamp', 'slot_index', 'intensity'])

            table_name = '{}-{}'.format(self.beam.bunch_intensity, int(fill))
            if verbose:
                print ('\rStoring data of fill {}, {}/{}'.format(fill, fill_index + 1, n_fills), end='')
            self._store_data(table_name, tosql, verbose=False, dtype={'timestamp': 'float', 'slot_index': 'int', 'intensity': 'float'})

    def _load_octupole_currents(self):
        '''
        Load octupole currents from SQL database.
        '''
        octupole_names = self.beam.oct_d + self.beam.oct_f
        octupole_data = self._get_data(["'{}'".format(octupole_name) for octupole_name in octupole_names])
        return {octupole_names[k]: octupole_data[k] for k in range(len(octupole_names))}

    def _load_beam_intensities(self):
        '''
        Load beam intensities from SQL database.
        '''
        return self._get_data("'{}'".format(self.beam.beam_intensity))

    def _load_bunch_intensities(self):
        '''
        Load bunch intensities from SQL database.
        '''

        return self._get_data("'{}'".format(self.beam.bunch_intensity))
            

# A Python routine for fits using confidence bands; many thanks to A. Oeftiger
import statsmodels.api as sm
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from scipy.stats import norm
def fit_with_ci(xdata, ydata, ytobefit, conf_level_sigma=1,
                verbose=False, add_constant=False):
    '''Calculate fit and confidence intervals for an ordinary linear
    regression model.
    Arguments:
        xdata: independent variable data
        ydata: dependent variable data
        ytobefit: tuple with entries for each functional dependency,
            e.g. (xdata, xdata**2, np.sin(xdata))
        conf_level_sigma: number of normal sigma for confidence level
            e.g. 1 corresponds to 68.3%, 2 corresponds to 95.4% c.l.
        verbose_results: prints regression results
        add_constant: adds a variable constant to the fit model
    Return 5-tuple of sorted xdata, fitted ydata and respective
    lower and upper confidence level values, and finally the
    statsmodels model fit results object.
    
    Example:
    --------
    xdata and ydata are two np.arrays of equal length with the data 
    We want to determine a, b, c with a*x^2 + b*sin(x) + c, where we want
    to use 2 sigma, i.e. 95% confidence level:

    xdata_ex, yfit_ex, ylower_ex, yupper_ex, results = fit_with_ci(
        xdata, ydata, (xdata**2, np.sin(xdata)),
        conf_level_sigma=2,
        verbose_results=True, add_constant=True) 
    '''
    
    u, perm = np.unique(xdata, return_index=True)
    # separate tuple entries for each functional dependency:
    X = np.column_stack(ytobefit)
    if add_constant:
        X = sm.add_constant(X)
    model = sm.OLS(ydata, X)
    results = model.fit()

    if verbose:
        print (results.summary())

    # Calculate confidence interval lines
    alpha = (1 - (norm.cdf(conf_level_sigma) -
             norm.cdf(-conf_level_sigma)))
    prstd, iv_l, iv_u = wls_prediction_std(results, alpha=alpha)
    return (xdata[perm], results.fittedvalues[perm],
            iv_l[perm], iv_u[perm], results, perm)


