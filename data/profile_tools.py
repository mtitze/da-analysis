import numpy as np
from misc.general import check_for_nan, get_rms_value, helpsort

from scipy.optimize import curve_fit
from scipy.signal import savgol_filter

# collection of tools to handle profile data

def bi_gaussian(x, a1, sigma1, sigma2, center=0, offset=0, scale=1):
    # n.B. the scaling factor 'scale' is required in order to handle profiles which are not yet normalized.
    a1 = abs(a1)
    sigma1 = abs(sigma1)
    sigma2 = abs(sigma2)
    return scale*a1/np.sqrt(2*np.pi)/sigma1*np.exp(-(x - center)**2/(2*sigma1**2)) + \
           scale*(1 - a1)/np.sqrt(2*np.pi)/sigma2*np.exp(-(x - center)**2/(2*sigma2**2)) + offset

def get_noise_level(data, **kwargs):
    '''
    This function attempts to find the noise level of given data, by first performing a Savitzky-Golay filter on the
    data (**kwargs are propagated to scipy.signal.savgol_filter). Then the mean of non-trivial noise rms is computed.
    '''

    s_data = savgol_filter(data, **kwargs)
    data_near_sf = data - s_data
    rms, cent = get_rms_value(data_near_sf, centered=True)
    data_noise = data_near_sf[abs(data_near_sf) >= rms] # can be used to remove contribution of almost perfectly fitted data.
    large_noise_rms, noise_cent = get_rms_value(data_noise, centered=True)
    return {'noise_rms': rms, 'large_noise_rms': large_noise_rms, 'filtered_data': s_data}
    
def get_offset(data, perc=30, **kwargs):
    '''
    Compute the offset of given data by using the value by which a given number of data points have small derivative.

    perc: percentile of all data having a small derivative. We consider them to reside on the null-line and
    take their median as the offset. 
    '''
    s_data1 = savgol_filter(data, deriv=1, **kwargs)
    q = np.percentile(abs(s_data1), perc)
    null = data[abs(s_data1) < q]
    return np.median(null), s_data1

def get_valid_parameter_range(data, threshold, min_width):
    '''
    This function determines the valid parameter range of given data, based on a given threshold. The largest group
    of data points above the given threshold defines the valid parameters. The parameter range is then given by the borders
    of other valid groups (if they exist).

    Returns
    =======
    index_start, index_stop, boolean

    The boolean variable is True, if there is data above the threshold.
    '''

    index_start, index_stop = 0, len(data) - 1

    signals = np.where(data > threshold)[0]
    if len(signals) == 0:
        # no valid parameter range possible
        return index_start, index_stop, False

    jumps = np.where(np.diff(signals) > 1)[0] # there are len(jumps) + 1 different regions where we have a signal
    groups = [[0]]
    for jump in jumps:
        groups[-1].append(signals[jump])
        groups.append([signals[jump + 1]])
    groups[-1].append(len(data))
    
    group_strengths = np.hstack(np.diff(groups))

    # we do not want to take into account small individual outliers
    valid_group_indices = np.where(group_strengths >= min_width)[0]
    valid_group_strengths = list(group_strengths[valid_group_indices])
    valid_groups = [groups[k] for k in valid_group_indices]

    index_best_group = valid_group_strengths.index(max(valid_group_strengths))
    
    if 0 < index_best_group:
        index_start = valid_groups[index_best_group - 1][-1]

    if index_best_group + 1 < len(valid_groups):
        index_stop = valid_groups[index_best_group + 1][0]

    return index_start, index_stop, True


def noise_check(data, threshold, offset_perc, min_width=1, **kwargs):
    '''
    This function determines the noise level of given data. Then
    it is checked whether the given data has points larger than the threshold times this rms value.
    min_with is an optional parameter to control the minimal width beyond where we consider data to be no noise.
    '''

    # step 1: get the noise of the give data
    noise = get_noise_level(data=data, **kwargs)
    fdata = noise['filtered_data']
    noise_rms = noise['noise_rms']

    # step 2: Sometimes the data can have an offset. We will correct this here.
    offset, fdata1 = get_offset(data, perc=offset_perc, **kwargs)
    fdata -= offset

    # step 3: determine signal start and stop, based on the noise level
    noise_threshold = noise_rms*threshold
    index_start, index_stop, ok = get_valid_parameter_range(abs(fdata), noise_threshold, min_width=min_width)
    
    # step 4: get index of smoothed data as estimate for the maximum
    valid_fdata = fdata[index_start: index_stop]
    index_max = list(valid_fdata).index(max(valid_fdata)) + index_start

    out = {}
    out['offset'] = offset
    out['valid_data'] = ok
    out['index_max'] = index_max
    out['index_start'] = index_start
    out['index_stop'] = index_stop
    out['noise_threshold'] = noise_threshold
    out['filtered_data'] = fdata
    out['filtered_data1'] = fdata1

    return out

def bg_normalize_curve(x, y, p0, verbose=True):
    '''
    Normalize raw data using the offset of a bi-Gaussian fit.
    '''

    if x[0] < x[-1]:
        base = np.linspace(x[0], x[-1], len(y))
    else:
        # x-data seems to be reversed
        base = np.linspace(x[-1], x[0], len(y))
    y = np.interp(base, x, y)

    fpre = curve_fit(bi_gaussian, base, y, p0=p0) # N.B.: bi_gaussian(x, a1, sigma1, sigma2, center=0, offset=0, scale=1)

    offset = fpre[0][4]
    I = fpre[0][5] # I = scipy.integrate.simps(y + offset, base) would give slightly different results because of noise in the data.
    if I > 0:
        return base, (y - offset)/I, True
    else:
        if verbose:
            print ('could not normalize curve')
        return base, y - offset, False

def prepare_shot(abszisse, ordinate, bg_normalize=[], centralize=True, verbose=True, **kwargs):
    '''
    Helper function to prepare profile data. Depending on the options, this function:
    - checks for NaNs
    - checks for noise (see simple_noise_check)
    - corrects input with respect to ascending x-coordinates
    - centralizes profile (default: True)
    - normalizes profile (with respect to double-Gaussian, if initial fit parameters are provided)

    bg_normalize: list of 6 parameters: Attempt to normalize the curves by using a bi_gaussian fit to remove any offset beforehand.
    The list corresponds to the start parameters of the fitting procedure (the last entry is an estimate of the integral of the
    non-normalized integral of the profile). If the list is empty, then this procedure is not executed.
    '''
    
    # check for NaNs
    ok = not check_for_nan(ordinate) # and not check_for_nan(abszisse)
    if not ok:
        error_message = 'ERROR: NaNs in data detected.'
        return abszisse, ordinate, False, error_message

    # do not consider the degenerate case of a single point
    if len(abszisse) == 1:
        error_message = 'ERROR: length of data: 1.'
        return abszisse, ordinate, False, error_message

    # It can happen that some of the data is in the wrong order
    if any(np.diff(abszisse) < 0):
        abszisse, ordinate = helpsort(abszisse, ordinate)

    # check for noise
    noise_dict = noise_check(ordinate, **kwargs)
    ok = noise_dict['valid_data']
    if not ok:
        error_message = 'Noisy data detected.'
        return abszisse, ordinate, False, error_message
    index_start, index_stop = noise_dict['index_start'], noise_dict['index_stop']
    abszisse = abszisse[index_start:index_stop]
    ordinate = ordinate[index_start:index_stop] - noise_dict['offset']
    peak_index = noise_dict['index_max'] - index_start

    if centralize:
        abszisse = abszisse - abszisse[peak_index]
        # also make bounds symmetric
        indices_sym = abs(abszisse) <= min([abs(min(abszisse)), max(abszisse)])
        abszisse = abszisse[indices_sym]
        ordinate = ordinate[indices_sym]

    if len(bg_normalize) > 0:
        try:
            abszisse, ordinate, ok = bg_normalize_curve(abszisse, ordinate, verbose=verbose, p0=bg_normalize)
        except:
            error_message = 'Normalizing curve via bi-Gaussian fit failed.'
            return abszisse, ordinate, False, error_message     
        
    message = 'Profile preparation successful.'
    return abszisse, ordinate, ok, message
