import numpy as np
import pandas as pd
from .database_tools import pytimber_tools, offline_hdf_database, fit_with_ci

def gateDelayToSlotNumber(gateDelay, beamNumber):
    return int(np.floor(gateDelay + 0.5))

def getEmit(sigma, beta, corr, gamma):
    return (sigma**2 - corr**2)*gamma/beta

class pytimber_bsrt_tools(pytimber_tools):
    '''
    Class to handle the LHC BSRT emittances. Some parts are taken/recycled from
      /afs/cern.ch/work/l/lhcim/public/LHCIMOnline/BSRTAnalysis.py
    many thanks to X. Buffat.
    '''

    def __init__(self):
        pytimber_tools.__init__(self)

        self.sigmaVars = {'LHC.BSRT.5R4.B1:FIT_SIGMA_H':'HB1',
                          'LHC.BSRT.5R4.B1:FIT_SIGMA_V':'VB1',
                          'LHC.BSRT.5L4.B2:FIT_SIGMA_H':'HB2',
                          'LHC.BSRT.5L4.B2:FIT_SIGMA_V':'VB2'}
    
        self.gateVars = {1:'LHC.BSRT.5R4.B1:GATE_DELAY',
                         2:'LHC.BSRT.5L4.B2:GATE_DELAY'}
    
        self.betaVars = {'HB1':'LHC.BSRT.5R4.B1:BETA_H',
                         'VB1':'LHC.BSRT.5R4.B1:BETA_V',
                         'HB2':'LHC.BSRT.5L4.B2:BETA_H',
                         'VB2':'LHC.BSRT.5L4.B2:BETA_V'}
    
        self.corrVars = {'HB1':'LHC.BSRT.5R4.B1:LSF_H',
                         'VB1':'LHC.BSRT.5R4.B1:LSF_V',
                         'HB2':'LHC.BSRT.5L4.B2:LSF_H',
                         'VB2':'LHC.BSRT.5L4.B2:LSF_V'}
    
        self.energyVar = 'LHC.BOFSU:OFSU_ENERGY'

    def _loadEmittanceData(self, t0, tEnd):
        setupVars = list(self.betaVars.values()) + list(self.corrVars.values()) + [self.energyVar]
        timberData = self.db.get(setupVars + list(self.sigmaVars.keys()) + list(self.gateVars.values()), t0, tEnd)
        for setupVar in setupVars:
            if len(timberData[setupVar][0]) == 0:
                newData = self.db.get(setupVar, t0, None)
                timberData[setupVar] = newData[setupVar]
        return self._toSlotBySlot(timberData) #, sigmaVars, gateVars, betaVars, corrVars, energyVar)

    def _toSlotBySlot(self, timberData, delay_tol=1e-6): #, sigmaVars, gateVars, betaVars, corrVars, energyVar):
        """
        Reorganise the raw BSRT data from the Logging DB to a new data structure to be used as follows :
        sData['HB1', 'VB1','HB2' or 'VB2'][slot number][0=time,1=varValue][valueNumber].
        
        Returns the normalized rms emittance evolution bunch by bunch,
        obtained from a Gaussian fit on the transverse beam profile.
        It works both at top and injection energies, but not during the ramp.
        
        The slots correspond to the bunch number.

        Attention: Hard-coded variables !!! --> fix required.
        """

        sData = {}
        for sigmaVar in self.sigmaVars.keys():
            key = self.sigmaVars[sigmaVar]
            if "B1" in key:
                beamNumber = 1
            else:
                beamNumber = 2

            if(sigmaVar in timberData and self.gateVars[beamNumber] in timberData):
                if not sigmaVar in sData:
                    sData[key] = {}
                for acqIndex in range(int(np.min([np.shape(timberData[self.gateVars[beamNumber]][0])[0],
                                                  np.shape(timberData[sigmaVar][0])[0]]))):
                    timestamp = timberData[self.gateVars[beamNumber]][0][acqIndex]
                    if timestamp - timberData[sigmaVar][0][acqIndex] > delay_tol:
                        print('WARNING : sigma and gate delays are not synchronised')
                    currentSlot = 0
                    tmpSum = 0
                    nAcq = 0
                    for vectornumericIndex in range(np.shape(timberData[sigmaVar][1][acqIndex])[0]):
                        if len(timberData[self.betaVars[key]][1]) > 0 and len(timberData[self.corrVars[key]][1]) > 0:
                            slot = gateDelayToSlotNumber(timberData[self.gateVars[beamNumber]][1][acqIndex][vectornumericIndex], beamNumber)
                            if not slot in sData[key]:
                                sData[key][slot]=[[],[]]
                            sData[key][slot][0].append(timestamp)
                            if len(timberData[self.energyVar][1]) > 0:
                                sData[key][slot][1].append(getEmit(
                                       timberData[sigmaVar][1][acqIndex][vectornumericIndex],
                                       np.interp(timestamp, timberData[self.betaVars[key]][0], timberData[self.betaVars[key]][1]),
                                       np.interp(timestamp, timberData[self.corrVars[key]][0], timberData[self.corrVars[key]][1])/1000,
                                       np.interp(timestamp, timberData[self.energyVar][0], timberData[self.energyVar][1])/0.938272))
                            else:
                                sData[key][slot][1].append(getEmit(
                                       timberData[sigmaVar][1][acqIndex][vectornumericIndex],
                                       np.interp(timestamp, timberData[self.betaVars[key]][0], timberData[self.betaVars[key]][1]),
                                       np.interp(timestamp, timberData[self.corrVars[key]][0], timberData[self.corrVars[key]][1])/1000,
                                       450.0/0.938272))
        for key in sData.keys():
            for slot in sData[key].keys():
                sData[key][slot] = np.array(sData[key][slot])
        return sData

    def _get_emittances_fill(self, fill_nr, phys_key, verbose=True):
        startend = self.get_fill_timespan(fill_nr, phys_key=phys_key, verbose=verbose)
        if not startend == None:
            start, end = startend
            return self._loadEmittanceData(start, end)

    def load_emittance_data(self, fills, phys_key='INJPHYS', verbose=True):
        '''
        Reads the normalized bunch-by-bunch emittances of specific LHC fills from PyTimber
        and returns a dictionary of Pandas data frames.
        '''

        columns_fill_beam = ['timestamp', 'slot_index', 'enx', 'eny']

        all_data = {}
        count = 0

        n_fills = len(fills)
        for fill_index in range(n_fills):
            fill = int(fills[fill_index])
            if verbose:
                print('\rProgress {0}/{1}'.format(fill_index + 1, n_fills), end='')

            output = self._get_emittances_fill(fill_nr=fill, phys_key=phys_key, verbose=False)
            if output == None:
                continue

            count += 1

            for beam_nr in [1, 2]:

                slot_indices = []
                enx, eny = [], []
                timestamps = []

                filled_slots_beam = sorted(output['HB{}'.format(beam_nr)].keys())
                if len(filled_slots_beam) == 0:
                    continue

                for slot_index in filled_slots_beam:
                    timestamps_index, emittances_x = output['HB{}'.format(beam_nr)][slot_index]
                    timestamps_index, emittances_y = output['VB{}'.format(beam_nr)][slot_index] # n.b. timestamps (H) = timestamps (V)

                    n_timestamps_index = len(timestamps_index)
                    if n_timestamps_index == 0:
                        continue

                    slot_indices.append([slot_index + 1]*n_timestamps_index) # n.b. the slot indices in Xaviers script start at 0.
                    enx.append(emittances_x)
                    eny.append(emittances_y)
                    timestamps.append(timestamps_index)

                slot_indices = np.hstack(slot_indices)
                enx, eny = np.hstack(enx), np.hstack(eny)
                timestamps = np.hstack(timestamps)

                if len(slot_indices) == 0:
                    continue

                all_data[(fill, beam_nr)] = pd.DataFrame(np.array([timestamps, slot_indices, enx, eny]).transpose(), columns=columns_fill_beam)

        if verbose:
            print ('\nFills containing {}: {} out of {}.'.format(phys_key, count, n_fills))

        return all_data


class bsrt_database(offline_hdf_database):
    '''
    This class is taylored around loading the BSRT emittances.
    '''
    def __init__(self, filename):
        offline_hdf_database.__init__(self, filename=filename)

        self._bsrt_emittance_info = 'bsrt_emittances' # Store all table names of the BSRT emittance data
        self._bsrt_table_prefix = 'bsrt_emit_' # Store the individual BSRT emittance data tables

    def store_emittances(self, data, verbose=True):
        '''
        Stores the (LHC) BSRT data to the dataframe. data must be created with the
        pytimber_bsrt_tools.load_emittance_data
        routine.
        '''

        if verbose:
            print ('Creating tables "{}<fill_nr>_B<beam_nr>" ...'.format(self._bsrt_table_prefix))

        final_table_info = []

        n_tables = len(data.keys())
        k = 0
        hdfstore = pd.HDFStore(self.filename, mode='a')
        for key in data.keys():

            fill, beam_nr = key
            data_k = data[key]

            if verbose:
                print ('\rProgress: {}/{}'.format(k + 1, n_tables), end='')
            k += 1

            full_table_name = self._bsrt_table_prefix + '{}_B{}'.format(fill, beam_nr)
            final_table_info.append([fill, beam_nr, full_table_name])
            hdfstore.put(value=data_k.astype({'slot_index': 'int'}), key=full_table_name, format='t', data_columns=True)

        if verbose:
            print ('\nStoring table information in "{}".'.format(self._bsrt_emittance_info))
        tohdf = pd.DataFrame(final_table_info, columns=['fill', 'beam_nr', 'name']).astype({'fill': 'int', 'beam_nr': 'int'})
        hdfstore.put(value=tohdf, key=self._bsrt_emittance_info, format='t', data_columns=True)
        hdfstore.close()

    def _make_bunch_regression_fill(self, fill_nr, order=2, conf_level_sigma=1, minemit=0.5, hdfstore=None, verbose=False):
        '''
        Perform a regression fit on the BSRT emittance data for selected slot indices.

        INPUT
        =====
        order: degree of fitted curve to the given data of emittances.
        minemit: sort out zero values (or negative values (!)) which may occur in the raw data.

        OUTPUT
        ======
        A dataframe containing the processed data and one containing information of the fit.
        '''

        if not hasattr(self, 'beam'):
            print ('ERROR: set_beam first.')
            return

        close_hdf = False
        if hdfstore == None:
            hdfstore = pd.HDFStore(self.filename, mode='r')
            close_hdf = True

        fill_info = hdfstore.select(self._bsrt_emittance_info, where='"fill" == {} and "beam_nr" == {}'.format(fill_nr, self.beam.beam_nr))
        if len(fill_info) != 1:
            print ('ERROR: No unique selection possible for fill {}.'.format(fill_nr))
            return
        table_name = fill_info.iloc[0]['name'] # the table name of the BSRT data for this case

        # get the filling scheme for this fill
        filling_scheme_name = hdfstore.select(self._selected_fills_info, where='"fill" == {}'.format(fill_nr))
        filling_scheme_fill = hdfstore.select(filling_scheme_name.iloc[0]['name'])

        slot_indices = filling_scheme_fill['slot_index'].values
        bunch_indices = filling_scheme_fill['bunch_index'].values
        bunch_counts = filling_scheme_fill['bunch_count'].values

        slot2bunch_index = lambda x: dict(zip(slot_indices, bunch_indices))[x]
        slot2bunch_count = lambda x: dict(zip(slot_indices, bunch_counts))[x]

        uslot_indices = list(np.unique(slot_indices))
        smin, smax = min(uslot_indices), max(uslot_indices)

        # now select the slots of interest out of the BSRT data
        selected_data = hdfstore.select(table_name, where='"slot_index" >= {} and "slot_index" <= {} and "enx" >= {} and "eny" >= {}'.format(smin, smax, minemit, minemit))
        selected_data = selected_data.loc[selected_data['slot_index'].isin(uslot_indices)]
        if close_hdf:
            hdfstore.close()

        data, info = [], []
        columns_data = ['slot_nr', 'bunch_index', 'bunch_count', 'timestamp', 'time', 'enx_fit', 'enx_lower', 'enx_upper', 
                        'eny_fit', 'eny_lower', 'eny_upper'] # intention: one table per fill
        columns_info = ['fill', 'slot_nr', 'bunch_index', 'bunch_count', 'timestamp', 'duration'] + ['enx_fit_{}'.format(k) for k in range(order + 1)] + \
                       ['eny_fit_{}'.format(k) for k in range(order + 1)] # intention: one such table for all fills

        for slot_index in uslot_indices:
            bsrt_emit_slot = selected_data.loc[selected_data['slot_index'] == slot_index]
            timestamps, enx, eny = bsrt_emit_slot['timestamp'].values, bsrt_emit_slot['enx'].values, bsrt_emit_slot['eny'].values

            # check data consistency
            if len(timestamps) == 0:
                if verbose:
                    print ('WARNING: Slot number {}, fill {}, contains insufficient data for emittance fit.'.format(fill_nr, slot_index))
                continue

            time0 = timestamps[0]
            times = timestamps - time0
            times_fit = [times**(j + 1) for j in range(order)]

            xdata_x, yfit_x, ylower_x, yupper_x, results_x, perm = fit_with_ci(times, enx, times_fit,
                                                                               conf_level_sigma=conf_level_sigma,
                                                                               verbose=False, add_constant=True)

            xdata_y, yfit_y, ylower_y, yupper_y, results_y, perm = fit_with_ci(times, eny, times_fit,
                                                                               conf_level_sigma=conf_level_sigma,
                                                                               verbose=False, add_constant=True)


            bunch_index = slot2bunch_index(slot_index)
            bunch_count = slot2bunch_count(slot_index)
            info.append([fill_nr, slot_index, bunch_index, bunch_count, time0, times[-1]] + list(results_x.params) + list(results_y.params))


            data.append([[slot_index]*len(xdata_x), [bunch_index]*len(xdata_x), [bunch_count]*len(xdata_x), timestamps[perm], xdata_x, 
                               yfit_x, ylower_x, yupper_x,
                               yfit_y, ylower_y, yupper_y])

        if len(data) > 0:
            data = np.hstack(data)
            info_db = pd.DataFrame(info, columns=columns_info)
            data_db = pd.DataFrame(data.transpose(), columns=columns_data).astype({'slot_nr': 'int', 'bunch_index': 'int', 'bunch_count': 'int'})
            return info_db, data_db
        else:
            return None


    def make_bunch_regression(self, fills, verbose=True, vverbose=False, **kwargs):
        '''
        Perform self._make_bunch_regression_fill over a set of fills and store the results in the database.

        **kwargs are passed to self._make_bunch_regression_fill.

        Remark: It is assumed that the database contains information on the filling schemes for all considered fills.
        I.e. the tables listen in self._selected_fills_info must be found in the database.
        '''

        info = []
        hdfstore = pd.HDFStore(self.filename, mode='a')
       
        n_fills = len(fills)
        for fill_index in range(n_fills):
            fill = int(fills[fill_index])
            if verbose:
                print ('\rProcessing fill {}, {}/{} ...'.format(fill, fill_index + 1, n_fills), end='')

            results = self._make_bunch_regression_fill(fill_nr=fill, hdfstore=hdfstore, verbose=vverbose, **kwargs)
            if results == None:
                if verbose:
                    print ('ERROR: No data received for fill {}.'.format(fill))
                continue

            info_fill, data_fill = results

            table_name = self._bsrt_regression_table_prefix + str(fill)
            info_fill['name'] = [table_name]*len(info_fill)
            info.append(info_fill)
            hdfstore.put(value=data_fill, key=table_name, format='t', data_columns=True)

        if len(info) > 0:
            if verbose:
                print ('\nStoring BSRT regression information into table:\n "{}"'.format(self._bsrt_regression_info))

            info = pd.concat(info, ignore_index=True)
            hdfstore.put(value=info, key=self._bsrt_regression_info, format='t', data_columns=True)
        hdfstore.close()






