import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from pyarrow import parquet as papq

########################################################
# Plot routines using seaborn violinplot functionality #
########################################################
# N.B. these routines are outdated and kept here only in
# order to be able to modify them in case of alternatives.
# I recommend to use the plotting routines based on numpy 
# histograms, see below.

from misc.general import get_fdr_bin_width, get_perc_data

def plot_chi2_statistics_seaborn(data, ylim=[], title='', filename='', figsize=(14, 4), **kwargs):
    '''
    Plot violin plots of the chi2-distributions of given data.

    perc: percentile used to remove outliers in the chi2- and loss-values.
    '''

    # only the chi2-values of the 'default' mode will be plotted; those of the 'rms' mode usually have
    # very different chi2_1 and chi2_2 values. Hence:
    data = data.loc[~data['chi2'].isna()]

    plt.figure(figsize=figsize)

    sns.violinplot(x='study_case', y='chi2', data=data, **kwargs)
    
    if len(ylim) == 2:
        plt.ylim(ylim)
    if len(title) > 0:
        plt.title(title, loc='right')

    plt.ylabel(r'$\chi^2$ (default mode)')
    plt.xlabel('case')    
    plt.xticks(rotation=45)
    
    if len(filename) > 0:
        plt.savefig(filename + '.png', format='png', dpi=300)
    
    plt.show()


def plot_loss_statistics_seaborn(data, ylim=[], title='', filename='', figsize=(14, 4), **kwargs):
    '''
    Plot violin plots of the chi2-distributions of given data.

    **kwargs are given to sns.violinplot.
    '''

    # prepare data to show measurement and losses in splitted violins
    tmp1 = data[['study_case', 'exp_loss']]
    tmp1 = tmp1.rename(columns={'exp_loss': 'loss'})
    tmp1['status'] = ['measurement']*len(tmp1)

    tmp2 = data[['study_case', 'sim_loss']]
    tmp2 = tmp2.rename(columns={'sim_loss': 'loss'})
    tmp2['status'] = ['simulation']*len(tmp2)

    tovio = pd.concat([tmp1, tmp2])

    plt.figure(figsize=figsize)
    vplot = sns.violinplot(x='study_case', y='loss', hue='status', split=True, data=tovio, **kwargs)
    vplot.legend(loc='center left', bbox_to_anchor=(1.05, 0.5), ncol=1)

    if len(ylim) == 2:
        plt.ylim(ylim)
    if len(title) > 0:
        plt.title(title, loc='right')

    plt.ylabel(r'$\Delta I/I_0$')
    plt.xlabel('case')

    if len(filename) > 0:
        plt.savefig(filename + '.png', format='png', dpi=300)
    
    plt.show()

def plot_chi2_group_stats_seaborn(data, group_identifyer, ylim=[], title='', filename='', **kwargs):
    '''
    Show group statistics of the fitted data.
    '''
    # only the chi2-values of the 'default' mode will be plotted; those of the 'rms' mode usually have
    # very different chi2_1 and chi2_2 values. Hence:
    data = data.loc[~data['chi2'].isna()]

    sns.violinplot(x=group_identifyer, y='chi2', data=data, **kwargs)
    plt.ylabel(r'$\chi^2$ (default mode)')
    if len(ylim) > 0:
        plt.ylim(ylim)
    if len(title) > 0:
        plt.title(title, loc='right')
    plt.xticks(rotation=45)
    
    if len(filename) > 0:
        plt.savefig(filename + '.png', format='png', dpi=300)
    
    plt.show()


def plot_loss_group_stats_seaborn(data, group_identifyer, figsize=(8, 4), ylim=[], title='', filename='',  **kwargs):
    '''
    Show group statistics of the fitted data.
    '''

    # prepare data to show measurement and losses in splitted violins
    tmp1 = data#[['study_case', 'exp_loss']]
    tmp1 = tmp1.rename(columns={'exp_loss': 'loss'}).drop(columns='sim_loss')
    tmp1['status'] = ['measurement']*len(tmp1)

    tmp2 = data#[['study_case', 'sim_loss']]
    tmp2 = tmp2.rename(columns={'sim_loss': 'loss'}).drop(columns='exp_loss')
    tmp2['status'] = ['simulation']*len(tmp2)

    tovio = pd.concat([tmp1, tmp2])

    plt.figure(figsize=figsize)
    vplot = sns.violinplot(x=group_identifyer, y='loss', hue='status', split=True, data=tovio, **kwargs)
    vplot.legend(loc='center left', bbox_to_anchor=(1.05, 0.5), ncol=1)

    plt.ylabel(r'$\Delta I/I_0$')
    if len(ylim) > 0:
        plt.ylim(ylim)
    if len(title) > 0:
        plt.title(title, loc='right')
    plt.xticks(rotation=45)
    
    if len(filename) > 0:
        plt.savefig(filename + '.png', format='png', dpi=300)
    
    plt.show()

########################################
# Plot routines using numpy histograms #
########################################

from misc.general import get_common_binning

def prepare_split_hist(values1, values2, common_binning=True, bins='auto', **kwargs):
    '''
    Function to create the histograms of two data sets, by using an optional common binning and an optional bin number.

    perc: an optional percentile to filter outliers contributing to the individual histograms. In effect if a value
          greater than zero is passed.
    '''
    if common_binning:
        common_bounds, bins = get_common_binning(values1, values2, bins=bins)
        x1, y1 = np.histogram(values1, range=common_bounds, bins=bins, **kwargs)
        x2, y2 = np.histogram(values2, range=common_bounds, bins=bins, **kwargs)
        
    else:
        if 'bins' == 'auto':
            h1 = get_fdr_bin_width(values1)
            h2 = get_fdr_bin_width(values2)
        
            bins1 = int((max(values1) - min(values1))/h1)
            bins2 = int((max(values2) - min(values2))/h2)
        else:
            bins1 = bins
            bins2 = bins
    
        x1, y1 = np.histogram(values1, bins=bins1, **kwargs)
        x2, y2 = np.histogram(values2, bins=bins2, **kwargs)
    return x1, y1, x2, y2

def no_yborders(ax):
    '''
    Small helper function to remove some borders of the given axis.
    '''
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.axes.get_yaxis().set_visible(False)

def create_split_hist_figure(fig, values1, values2, bounds=[0, 1], equal_area=True,
                             show_yleft=True, show_yright=True, second_color_alpha=0.5,
                             xticklabel='', axes_border_percent=0.2, common_binning=True, bins='auto', 
                             color1='C0', color2='C1', alpha=1, xtick_rotation=45, **kwargs):

    '''
    Function used in plot_loss_statistics2 to create splitted violin plots. It requires a figure class
    to be created first.

    equal_area: if True, re-scale the two histograms so that they span the same area.
    bounds: The horizontal bounds of the figure to be created.
    '''
    
    x1, y1, x2, y2 = prepare_split_hist(values1, values2, common_binning=common_binning, bins=bins, **kwargs)

    # if density=True in kwargs, then
    # 1 = sum(x1*np.diff(y1))
    # etc.

    w1 = np.diff(y1)[0]
    w2 = np.diff(y2)[0]
    positions1 = y1[:-1] # lower edge positions of the bins
    positions2 = y2[:-1]

    b1, b2 = bounds
    ax1 = fig.add_axes([b1, axes_border_percent, (b2 - b1)/2, 1 - 2*axes_border_percent])
    ax2 = fig.add_axes([b1 + (b2 - b1)/2, axes_border_percent, (b2 - b1)/2, 1 - 2*axes_border_percent])
        
    bc1 = ax1.barh(y=positions1, width=x1, height=w1, alpha=alpha, color=color1, align='edge')
    bc2 = ax2.barh(y=positions2, width=x2, height=w2, alpha=alpha, color=color2, align='edge')
    
    if equal_area:
        # trick to re-scale both plots
        bc1b = ax1.barh(y=positions2, width=x2, height=w2, alpha=second_color_alpha, color=color1, align='edge')
        bc2b = ax2.barh(y=positions1, width=x1, height=w1, alpha=second_color_alpha, color=color2, align='edge')
    ax1.invert_xaxis()
    
    ax1.set_xticklabels([])
    ax1.set_xticks([])
    ax2.set_xticklabels([xticklabel], rotation=xtick_rotation)
    ax2.set_xticks([0])
    
    # show only horizontal grid (if at all)
    ax1.grid(axis='y')
    ax2.grid(axis='y')
    
    if not show_yleft:
        no_yborders(ax1)
    
    if not show_yright:    
        no_yborders(ax2)
    
    return ax1, ax2

import matplotlib.patches as patches    

def plot_split_statistics(metadata, column1, column2, data_path_column='table_name',
                          bins='auto', title='', filename='', figsize=(12, 4), second_color_alpha=0, 
                          sub_selection=False, group_identifyer='study_case', xlabel=None, ylabel='',
                          label1='', plot_cases=[], label2='', color1='C0', color2='C1', ylim=[], alpha=1, verbose=True,
                          pyarrow_options={}, perc=0, **kwargs):
    '''
    Alternative version of plot_loss_statistics, hereby using a stack of vertical histograms for the purpose of customizing the
    bin width.
    
    metadata is assumed to be a Pandas DataFrame containing information about the location of the data to be plotted in
    a column <data_path>.
    
    The individual data tables must have the columns column1 and column2.

    sub_selection: If boolean and True, then reference data of column2 global (e.g. measuerment data).
                   If boolean and False, then the individual data must also have the columns: fill, bunch_index, wsh_inout and wsv_inout
                   If any other type will be assumed as numpy array. The values in this array will serve as reference for the losses.
    plot_cases: Optional list of cases to be plotted (attention: needs to be present in metadata). This is for the purpose
                of plotting cases in a desired order.
    '''

    if len(plot_cases) == 0:
        plot_cases = metadata[group_identifyer].unique()
        
    n_plot_cases = len(plot_cases)

    fig = plt.figure(figsize=figsize)
    fig.set_tight_layout(False)

    if type(sub_selection) == bool:
        if not sub_selection:
            all_tables = list(metadata[data_path_column].values)
            dataset = papq.ParquetDataset(all_tables, use_legacy_dataset=False, **pyarrow_options)
            columns_ref = ['fill', 'bunch_index', 'wsh_inout', 'wsv_inout']
            df1 = dataset.read(columns=columns_ref + [column2]).to_pandas().drop_duplicates(subset=columns_ref)
            values2_inp = df1[column2].values
    else:
        values2_inp = sub_selection

    axes_vertical_border_percent = 0.15 # percentage of the figure for space to show descriptions and ticks etc.
    axes_horizontal_border_percent = 0.1
    legend_border_percent = 0.1
    borders_histograms = [axes_horizontal_border_percent, 1 - legend_border_percent]
    bounds = np.linspace(borders_histograms[0], borders_histograms[1], n_plot_cases + 1)

    axes = []
    show_yleft, show_yright = True, False
    for k in range(n_plot_cases):
        if verbose:
            print ('Processing {}/{}'.format(k + 1, n_plot_cases), end='\r')
        case = plot_cases[k]
        metadata_k = metadata.loc[metadata[group_identifyer] == case]
        tables_k = list(metadata_k[data_path_column].values)      
        dataset_k = papq.ParquetDataset(tables_k, use_legacy_dataset=False, **pyarrow_options)
        df_k = dataset_k.read(columns=list(np.unique([column1, column2]))).to_pandas()
        values1 = df_k[column1].values
        
        if type(sub_selection) == bool:
            if sub_selection:
                values2_inp = df_k[column2].values
                
        values2 = values2_inp
                
        values1 = values1[~np.isnan(values1)]
        values2 = values2[~np.isnan(values2)]

        if perc > 0:
            values1 = get_perc_data(values1, perc)
            values2 = get_perc_data(values2, perc)

        ax1, ax2 = create_split_hist_figure(fig, values1=values1, values2=values2, 
                                            bounds=[bounds[k], bounds[k + 1]],
                                            show_yleft=show_yleft, show_yright=show_yright,
                                            density=True, bins=bins, xticklabel=case, second_color_alpha=second_color_alpha, 
                                            axes_border_percent=axes_vertical_border_percent, 
                                            color1=color1, color2=color2, alpha=alpha, **kwargs)

        axes.append(ax1)
        axes.append(ax2)

        show_yleft = False
        show_yright = False

    common_ylim = ylim
    if len(ylim) == 0:
        # set a common y-scaling of all histograms
        ylims = [ax.get_ylim() for ax in axes]
        common_ylim = (min([y[0] for y in ylims]), max([y[1] for y in ylims]))
    for ax in axes:
        ax.set_ylim(common_ylim)

    # remove some grids/ticks at borders
    axes[-1].set_yticks([])
    axes[0].grid(False)
    axes[0].spines['right'].set_visible(False)

    # title & description
    if xlabel == None:
        xlabel = group_identifyer
    axes[-1].set_xlabel(xlabel)
    axes[0].set_ylabel(ylabel)   
    axes[-2].set_title(title, loc='right')

    # add some space for a legend
    right_border_space = 0
    if len(label1) == 0 and len(label2) == 0:
        right_border_space = 0.08

    ax = fig.add_axes([1 - legend_border_percent, axes_vertical_border_percent, 
                       legend_border_percent - right_border_space, 1 - 2*axes_vertical_border_percent])
    ax.axes.get_xaxis().set_visible(False)
    ax.axes.get_yaxis().set_visible(False)

    ax.spines['top'].set_visible(False)
    ##ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['right'].set_visible(False)

    # draw a custom legend
    if len(label1) > 0:
        shift = 0
        if second_color_alpha == 0:
            shift = 0.09
        rect1 = patches.Rectangle((0.2, 0.4), 0.15, 0.05, linewidth=0, edgecolor=None, facecolor=color1, alpha=second_color_alpha)
        rect1b = patches.Rectangle((0.35 - shift, 0.4), 0.15, 0.05, linewidth=0, edgecolor=None, facecolor=color1, alpha=alpha)
        ax.add_patch(rect1)
        ax.add_patch(rect1b)
        ax.text(0.25, 0.5, label1, fontsize=16, rotation=90)
    if len(label2) > 0:
        rect2 = patches.Rectangle((0.62 + shift, 0.4), 0.15, 0.05, linewidth=0, edgecolor=None, facecolor=color2, alpha=alpha)
        rect2b = patches.Rectangle((0.77, 0.4), 0.15, 0.05, linewidth=0, edgecolor=None, facecolor=color2, alpha=second_color_alpha)
        ax.add_patch(rect2)
        ax.add_patch(rect2b)
        ax.text(0.7, 0.5, label2, fontsize=16, rotation=90)

    if len(filename) > 0:
        fig.canvas.print_figure(filename + '.png', format='png', dpi=300)

    plt.show()
    plt.close(fig)


def plot_chi2_statistics(metadata, color='C0', ylabel=r'$\chi^2$', **kwargs):

    '''
    Function to create violin-like plots of the chi2-values, using a user-defined binning.
    '''
    
    plot_split_statistics(metadata=metadata, column1='chi2', column2='chi2', sub_selection=True, color1=color, color2=color,
                          second_color_alpha=0, ylabel=ylabel, equal_area=False, **kwargs)

    
def plot_loss_statistics(metadata, ylabel=r'$\Delta I/I_0$', label1='simulation', label2='measurement',
                         sub_selection=False, common_binning=False, second_color_alpha=0, equal_area=True, **kwargs):

    '''
    Function to create violin-like plots of the loss values, using a user-defined binning.
    '''
    
    plot_split_statistics(metadata=metadata, column1='sim_loss', column2='exp_loss', sub_selection=sub_selection, 
                          label1=label1, label2=label2, ylabel=ylabel, common_binning=common_binning,
                          second_color_alpha=second_color_alpha, equal_area=equal_area, **kwargs)


#########################
# Convenience functions #
#########################

def plot_correlation(cor, filename='', figsize=(6, 6), title=''):
    '''
    Takes the output of get_correlation and plots a seaborn heatmap.
    '''

    # show only lower triangle
    # see:
    # https://stackoverflow.com/questions/57414771/how-to-plot-only-the-lower-triangle-of-a-seaborn-heatmap
    mask = np.zeros_like(cor, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    plt.figure(figsize=figsize)
    ax = sns.heatmap(cor, mask=mask, linewidth=0.5, cmap='cividis', vmin=0, vmax=1)#, annot=True)

    # prevent seaborn to show upmost row and last column
    # see
    # https://stackoverflow.com/questions/53227186/seaborn-diagonal-correlation-matrix-skip-first-row-and-last-column
    ax.set_xlim(0, len(cor) - 1)#
    ax.set_ylim(len(cor), 1)

    if len(title) > 0:
        plt.title(title, loc='right')

    if len(filename) > 0:
        plt.savefig(filename)
    plt.show()

def latex_figure_block(filename, width=1, indent='  '):
    return '{0}{0}\\includegraphics[width={1}\\textwidth]{{{2}}}\n'.format(indent, width, filename)

def make_latex_figure(filenames, caption='', label='', **kwargs):
    latex_str = '\\begin{figure}[!htb]\n\\centering\n'
    for filename in filenames:
        latex_str += latex_figure_block(filename, **kwargs)
    if len(caption) > 0:
        latex_str += '\\caption{{{}}}\n'.format(caption)
    if len(label) > 0:
        latex_str += '\\label{{{}}}\n'.format(label)
    latex_str += '\\end{figure}'
    return latex_str

