import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from sixdesk.notebook_tools import six_handler, progress_indicator
from sixdesk.error_analysis import error_instance

import time as timelib
from itertools import product

from misc.general import append_to_parquet_table

'''
Collection of tools related to the calculation of beam losses after the DA has been determined.
These tools make use of those in 'data' and 'sixdesk'.
'''

class loss_instance(error_instance):
    '''
    Class to handle simulation sixtrack SQL database to extrapolate the DA to given turn numbers
    and compute the respective losses.
    '''
    def __init__(self, filename, da_model):
        error_instance.__init__(self, filename=filename, da_model=da_model)

class loss_study(six_handler):
    '''
    Class to extrapolate the DA to user-specified ranges and compare them with measured losses.
    '''

    def __init__(self, filenames, da_model, profile_model):
        self.model = da_model
        self.profile_model = profile_model
        six_handler.__init__(self, filenames=filenames, instance=loss_instance, da_model=da_model)

    def _get_common_sixdb_table(self, with_errors=False):      
        '''
        Obtain the fit parameters for each of the considered SixDB SQL databases.
        '''
        fit_parameters_sqlfiles, fit_parameter_errors_sqlfiles = [], []
        sql_index = 0
        for loss_obj in self.objects:
            fit_parameters_df = loss_obj.get_fit_parameters()
            fit_parameters_df['sql_index'] = sql_index # add sql index to keep track of sql file index
            fit_parameters_sqlfiles.append(fit_parameters_df)
            if with_errors:
                fit_parameter_errors_df = loss_obj.get_fit_parameter_errors(verbose=False)
                fit_parameter_errors_df['sql_index'] = sql_index
                fit_parameter_errors_sqlfiles.append(fit_parameter_errors_df)
            sql_index += 1

        fp = pd.concat(fit_parameters_sqlfiles, ignore_index=True)
        if with_errors: # initialize required variables for error calculation
            fpe = pd.concat(fit_parameter_errors_sqlfiles, ignore_index=True)
        else:
            fpe = []

        return fp, fpe


    def compute_simulated_losses(self, filename, profile_table, modus='default', sigma=0, verbose=True, **kwargs):
        '''
        Compute the simulated losses, based on a given modus.

                profile_table: Pandas DataFrame containing the profile fit parameters, the emittances and turn numbers.

                sigma: Determine the errors for X times the standard deviation.

        modus:
        'default': Individual emittances
            'rms': Re-scale emittances
        '''

        with_errors = sigma > 0

        self._simulated_losses_sigma = sigma
        self.profile_table = profile_table

        # Obtain the fit parameters for each of the considered SQL databases.
        fp, fpe = self._get_common_sixdb_table(with_errors=with_errors)
        self.sixdb_table = fp
        self.sixdb_error_table = fpe

        if verbose:
            print ('           DA model: {}'.format(self.model.description))
            print ('      Profile model: {}'.format(self.profile_model.description))
            print ('compute loss errors: {}'.format(with_errors))
            if with_errors:
                print ('Loss errors calculated at {} sigma.'.format(sigma))

        if modus == 'default':
            self.run_modus_default(filename=filename, with_errors=with_errors, verbose=verbose, **kwargs)
        if modus == 'rms':
            self.run_modus_rms(filename=filename, with_errors=with_errors, verbose=verbose, **kwargs)
        

    def run_modus_default(self, filename, with_errors=False, lookup_key='emit_index', integrity_check=True, verbose=True):
        '''
        Compute simulated beam losses, based on given time lengths.

        Note that all the user-given SQL databases, together with their various seeds and tunes enter the statistics.

        INPUT
        =====
               lookup_key: key by which we identify the emittance indices in the profile tables.
                                   (e.g. 'bunch_count' or 'fill')
               integrity_check: Perform an integrity check if the number of emittances in the SQL database and those given by
                                the column 'lookup_key' in the profile parameter table are identical.
              verbose: Print additional output.
        '''

        sixdb_emit_indices = sorted(self.sixdb_table['emit_index'].unique()) # while there may be a degree of freedom for the index of the profiles, 'emit_index' is always the reference name for the SixDB data.
        profile_emit_indices = sorted(self.profile_table[lookup_key].unique())

        n_emittances = len(sixdb_emit_indices)
        n_profile_emittances = len(profile_emit_indices)
        if n_emittances != n_profile_emittances and integrity_check:
            print ("ERROR: The number of emittances ({}) from SixDB is not equal to the number of unique indices ({}) given by column\n '{}'\n in the profile database.".format(n_emittances, n_profile_emittances, lookup_key))
            return

        fit_parameters_cases = ['sql_index', 'seed', 'tunex', 'tuney']
        output_columns = fit_parameters_cases + ['damodel_chi2'] + ['damodel_{}'.format(c) for c in self.model.fit_parameter_keys] + self.profile_table.columns.tolist() + ['da', 'sim_loss']
        fit_parameters_cases += ['chi2'] + self.model.fit_parameter_keys # names of columns to be taken over from fp table (see above) to the output table.

        if with_errors: # initialize required variables for error calculation
            cdict = self.profile_model._init_cdict(da_model=self.model)

            #simulated_loss_errors_pos, simulated_loss_errors_neg = [], []
            output_columns += ['da_error_pos', 'da_error_neg', 'sim_loss_error_pos', 'sim_loss_error_neg']

        if verbose:
            print ('          lookup_key: {}'.format(lookup_key))
            print ('        # emittances: {}'.format(n_emittances))

            # initialize parameters of indicator on the progress of the calculations in [%]
            step_indicator = 0.2
            progress = np.linspace(0, 100, int(100/step_indicator) + 1)
            n_progress_steps = len(progress)

            start_time = timelib.time()

        count = 0
        writer = None
        for k in range(n_emittances):
            if verbose:
                print('\n{0}/{1}'.format(count + 1, n_emittances))
                k0, step = -1, 0
            count += 1

            fit_parameters = self.sixdb_table.loc[self.sixdb_table['emit_index'] == sixdb_emit_indices[k]]
            if with_errors:
                fpe_index = self.sixdb_error_table.loc[self.sixdb_error_table['emit_index'] == sixdb_emit_indices[k]]
            profile_fit_parameters = self.profile_table.loc[self.profile_table[lookup_key] == profile_emit_indices[k]]

            da_information = fit_parameters[fit_parameters_cases].values.tolist()
            datablock_emit_index = []
            datablock_lengths = []

            n_turns = len(profile_fit_parameters)
            for k in range(n_turns):
                if verbose:
                    step, k0 = progress_indicator(step, k0, iter_list=range(n_turns), progress=progress)

                profile_fit_parameters_turn = profile_fit_parameters.iloc[[k]]
                turn = profile_fit_parameters_turn['turn'].values
                loss_time_k, da_mult, profile_params, da2_series = self.profile_model.compute_loss(da_model=self.model,
                                                                     fit_parameters=fit_parameters,
                                                                     profile_fit_parameters=profile_fit_parameters_turn, 
                                                                     turn=turn)

                # add the information from the individual DA values
                da_information_mult = np.vstack([[e]*len(profile_fit_parameters_turn) for e in da_information])
                profile_data = np.hstack([da_information_mult, profile_params])

                datablock_time_k = [profile_data, np.array([da_mult]).transpose(), np.array([loss_time_k]).transpose()]
                datablock_lengths.append(len(da_mult))

                if with_errors: # In this case we also compute the error in the respective DA
                    err_inp = {}
                    err_inp['error_table'] = fpe_index
                    err_inp['da_model'] = self.model
                    err_inp['fit_parameters'] = fit_parameters
                    err_inp['profile_fit_parameters'] = profile_fit_parameters_turn
                    err_inp['turn'] = turn
                    err_inp['sigma'] = self._simulated_losses_sigma
                    err_inp['da_series'] = np.sqrt(da2_series)
                    err_inp['cdict'] = cdict

                    loss_errors_pos, loss_errors_neg, da2_mult_pos, da2_mult_neg, profile_params = self.profile_model.compute_loss_errors_noinit(**err_inp)

                    datablock_time_k += [np.array([np.sqrt(da2_mult_pos)]).transpose(), np.array([np.sqrt(da2_mult_neg)]).transpose(),
                                       np.array([loss_errors_pos]).transpose(), np.array([loss_errors_neg]).transpose()]

                datablock_emit_index.append(datablock_time_k)

            # assemble & store output datablock
            datablock = np.zeros([sum(datablock_lengths), len(output_columns)])
            position = 0
            for k in range(len(datablock_emit_index)):
                length = datablock_lengths[k]
                position += length
                hpos = 0
                for sblock in datablock_emit_index[k]:
                    datablock[position - length: position, hpos:hpos + sblock.shape[1]] = sblock
                    hpos += sblock.shape[1]

            datablock_db = pd.DataFrame(datablock, columns=output_columns).astype({'fill': 'int', 'emit_index': 'int',
                                                     'slot_index': 'int', 'bunch_index': 'int', 
                                                     'bunch_count': 'int', 'sql_index': 'int', 'seed': 'int', 
                                                     'wsh_inout': 'int', 'wsv_inout': 'int'})

            writer = append_to_parquet_table(dataframe=datablock_db, filepath=filename, writer=writer)

        writer.close()
        if verbose:
            print ('\nTime elapsed: {:.3f} [s]'.format(timelib.time() - start_time))
            print ('Simulated losses stored in file:\n {}'.format(filename))


    @staticmethod
    def emit_matrix(emittances, e0, e1):
        '''
        Computes 2x2 matrices which can provide the fit for a given list of goal emittances,
        based on two reference emittance pairs e0 and e1 (see my notes for details).
        '''
    
        emit1 = np.array([e[0] for e in emittances])
        emit2 = np.array([e[1] for e in emittances])
    
        ai11 = emit1/e0[0]
        ai12 = emit2/e0[1]
    
        ai21 = emit1/e1[0]
        ai22 = emit2/e1[1]
    
        return np.array([[ai22, -ai12], [-ai21, ai11]])/(ai11*ai22 - ai21*ai12)


    def run_modus_rms(self, emittances0, emittances1, filename, with_errors=False, sigma=[], verbose=True):
        '''
        If the DA was computed via the rms definition, we can obtain all fit values by application of a suitable linear map.
        In order that this feature can work, two reference emittances needs to be processed through the DA analysis scripts.

        OUTPUT
        ======
        numpy array containing the simulated losses.
        '''

        if with_errors: # initialize required variables for error calculation
            n_parameters = len(self.model.fit_parameter_keys)
            cdict = self.profile_model._init_cdict(da_model=self.model)
            simulated_loss_errors_pos, simulated_loss_errors_neg = [], []

        # compute the scaling matrices for the emittances
        all_emittances = self.profile_table[['enx', 'eny']].values
        bmats = self.emit_matrix(all_emittances, e0=emittances0, e1=emittances1)

        # load the fit parameters for the first- and second reference emittances (for all considered seeds, tunes and SQL files)
        fit_parameters_1_df = self.sixdb_table.loc[self.sixdb_table['emit_index'] == 1].copy()
        fit_parameters_2_df = self.sixdb_table.loc[self.sixdb_table['emit_index'] == 2].copy()
        # ensure that the two dataframes are sorted according to the individual cases (as their fit values enter the model formula below)
        fit_parameters_cases = ['sql_index', 'seed', 'tunex', 'tuney']
        fit_parameters_1_df = fit_parameters_1_df.sort_values(fit_parameters_cases)
        fit_parameters_2_df = fit_parameters_2_df.sort_values(fit_parameters_cases)
        fit_parameters_information = fit_parameters_1_df[fit_parameters_cases + self.model.fit_parameter_keys].copy() # we store this information in the output dataframe
        fit_parameters_information['damodel_chi2_1'] = fit_parameters_1_df['chi2'].values
        fit_parameters_information['damodel_chi2_2'] = fit_parameters_2_df['chi2'].values
        if with_errors:
            fpe_index_1_df = self.sixdb_error_table.loc[self.sixdb_error_table['emit_index'] == 1]
            fpe_index_2_df = self.sixdb_error_table.loc[self.sixdb_error_table['emit_index'] == 2]
            fpe_index_1_df = fpe_index_1_df.sort_values(fit_parameters_cases)
            fpe_index_2_df = fpe_index_2_df.sort_values(fit_parameters_cases)

        output_columns = fit_parameters_cases + ['damodel_{}'.format(c) for c in self.model.fit_parameter_keys] + \
         ['damodel_chi2_1', 'damodel_chi2_2'] + list(self.profile_table.columns) + ['da', 'sim_loss']
        n_fit_parameter_columns = len(fit_parameters_information.columns)

        if len(fit_parameters_1_df) != len(fit_parameters_2_df):
            print ('ERROR: # cases emit1 != # cases emit2')
            return

        # determine the (unique) durations
        turns =  self.profile_table['turn'].unique()

        if verbose:
            # initialize parameters of indicator on the progress of the calculations in [%]
            step_indicator = 0.1 
            progress = np.linspace(0, 100, int(100/step_indicator) + 1)
            n_progress_steps = len(progress)
            k0, step = -1, 0

            print ('work table length: {}'.format(len(self.profile_table)))
            print ('          # turns: {}'.format(len(turns)))
            start_time = timelib.time()

        writer = None
        n_work_table_columns = len(self.profile_table.columns)
        for turn in turns:
            if verbose:
                step, k0 = progress_indicator(step, k0, iter_list=turns, progress=progress)

            profile_data_time = self.profile_table.loc[self.profile_table['turn'] == turn]
            indices = profile_data_time.index.tolist()

            # get the DA values of the two reference curves at the respective turn, for all considered
            # seeds, tunes and SQL files
            da2_1 = self.model.run(fit_parameters_1_df[self.model.fit_parameter_keys].values.transpose(), turn)**2
            da2_2 = self.model.run(fit_parameters_2_df[self.model.fit_parameter_keys].values.transpose(), turn)**2

            '''
            # Implementing error calculation in this mode requires work ...
            if with_errors:
                grad_dict_1 = self.model.darun(fit_parameters_1, turn)
                grad_dict_2 = self.model.darun(fit_parameters_2, turn)

                grad_1 = np.array([grad_dict_1[p] for p in self.model.fit_parameter_keys])
                grad_2 = np.array([grad_dict_2[p] for p in self.model.fit_parameter_keys])

                E1E1 = np.zeros(len(fpe_index_1))
                E1E2 = np.zeros(len(fpe_index_1)) # fpe_index_1 = fpe_index_2
                E2E2 = np.zeros(len(fpe_index_2))

                for j, w in product(range(n_parameters), range(n_parameters)): # the error is given as grad*fit_errors*grad.transpose() by first-order error propagation
                    E1E1 += grad_1[j, :]*fpe_index[cdict[(j, w)]].values*grad_1[w, :]

                    # we can not use fpe_index_1 or fpe_index_2 here, because they depend on the two parameters a_1 and a_2 and
                    # not on the goal parameters (...)
            '''
            n_times = len(profile_data_time)
            n_fits = len(da2_1) # = (number of seeds * number of turns * number of sql files) etc.

            # We need to store the output in datablocks, otherwise there will be a memory overflow in some scenarios.
            datablock = np.zeros([n_times*n_fits, len(output_columns)])

            for k in range(n_times):
                bmat = bmats[:, :, indices[k]]
                da2 = (bmat[0, 0] + bmat[1, 0])*da2_1 + (bmat[0, 1] + bmat[1, 1])*da2_2 # see my notes for 2019-12
                profile_data_time_k = profile_data_time.iloc[[k]]
                loss_time_k, da2_mult, profile_params = self.profile_model.compute_simulated_losses(profile_data_time_k, da2)
                # loss_time_k is a list containing the losses of n_fits for the specific profile k at the specific time.

                # Example with 60 seeds and 4 sql files:
                #  profile_params.shape
                # (240, 24)
                # loss_time_k.shape
                # (240,)
                # da2_mult.shape
                # (240,)
                # fit_parameters_information.values.shape
                # (240, 6)

                da_mult = np.sqrt(da2_mult)
                datablock[k*n_fits: (k + 1)*n_fits, :n_fit_parameter_columns] = fit_parameters_information.values
                datablock[k*n_fits: (k + 1)*n_fits, n_fit_parameter_columns:n_fit_parameter_columns + n_work_table_columns] = profile_params
                datablock[k*n_fits: (k + 1)*n_fits, -2:-1] = np.array([da_mult]).transpose()
                datablock[k*n_fits: (k + 1)*n_fits, -1:] = np.array([loss_time_k]).transpose()

            datablock_db = pd.DataFrame(datablock, columns=output_columns).astype({'fill': 'int', 'emit_index': 'int',
                                                     'slot_index': 'int', 'bunch_index': 'int', 
                                                     'bunch_count': 'int', 'sql_index': 'int', 'seed': 'int', 
                                                     'wsh_inout': 'int', 'wsv_inout': 'int'})

            writer = append_to_parquet_table(dataframe=datablock_db, filepath=filename, writer=writer)

        writer.close()
        if verbose:
            print ('\nTime elapsed: {:.3f} [s]'.format(timelib.time() - start_time))
            print ('Simulated losses stored in file:\n {}'.format(filename))


    def get_experimental_losses(self, verbose=True):
        '''
        Returns the experimental losses of the current prepared table
        self.profile_model._prepared_table_name
        '''
        if verbose:
            print ('Reading experimental losses from table:\n "{}"'.format(self.profile_model._prepared_table_name))
        hdfstore = pd.HDFStore(self.profile_model.filename, mode='r')
        exp_losses = hdfstore.select(self.profile_model._prepared_table_name)['exp_loss'].values
        hdfstore.close()
        return exp_losses

    @staticmethod
    def plot_losses(losses, ax, **kwargs):
        '''
        Plot the beam losses.
        '''
        ax.hist(losses, histtype='step', **kwargs)

    def plot_errors(self, ax, bounds, bins=100, linewidth=1.5):
        '''
        Plot the errors of the simulated beam losses.
        '''
        # FIXME this function requires rework
        if with_errors:
            if hasattr(self, 'simulated_loss_errors_pos'):
                ax.hist(self.simulated_loss_errors_pos, bins=bins,
                         range=bounds,
                         weights=self.weights_sim,
                         density=True, histtype='step',
                         label=r'sim. errors ($+ {{{}}}\sigma$)'.format(self._simulated_losses_sigma), lw=linewidth)

            if hasattr(self, 'simulated_loss_errors_neg'):
                ax.hist(self.simulated_loss_errors_neg, bins=bins, 
                         range=bounds,
                         weights=self.weights_sim,
                         density=True, histtype='step',
                         label=r'sim. errors ($- {{{}}}\sigma$)'.format(self._simulated_losses_sigma), lw=linewidth)

        if len(title) > 0:
            plt.title(title, loc='right')
        plt.xlabel(r'$\Delta I/I_0$')
        plt.ylabel(r'$n$')
        plt.legend()
        return plt

