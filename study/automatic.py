
# This file contains scripts to perform an automatic analysis on the data

import numpy as np
import pyarrow as pa
from pyarrow import parquet as papq
import os, glob
import sqlite3
import time as timelib
import pandas as pd
from itertools import product

from sixdesk.da import precompute_da, compute_da, pre_da_default, pre_da_interpolate, pre_da_mean, pre_da_maxnorm, pre_da_min, pre_da_original, pre_da_maximum, pre_da_mtunequal, pre_da_spline
from sixdesk.da_models import model1, model2, model2b, lambert, mg_model1, lambert_b, model4b
from sixdesk.fit_routines import make_lmfit, algopy_fit, jax_fit
from sixdesk.fit_routines import perform_fit
from sixdesk.error_analysis import error_jax, error_instance
from .beamloss import loss_study as sd_loss_study
from sixdesk.notebook_tools import six_handler, get_sql_data

from misc.general import append_to_parquet_table, get_perc_data, get_rms_value
from data.ws import get_emittances, ws_dgaussian_link, ws_dgaussian

import json

def cfunc_perc(x, step, perc):
    '''
    step function, but do not set more than 'perc' % of the entire data set to zero.
    '''
    Q = np.percentile(x, perc)
    bound = min([Q, step])
    return (x > bound).astype(int)

class study:
    '''
    Generic study class to collect the input of the various subroutines.
    '''

    def __init__(self, output_folder, output_filename='results', **kwargs):
        self.output_folder = output_folder
        self.output_filename = output_filename

        self._metainfo_filename = '{}/{}.db'.format(self.output_folder, self.output_filename)
        self._metainfo_tablename = 'meta_info'
        self._table_suffix = 'table_' # suffix for the output tables

        self._prepared_table_prefix = 'profile_data_scale' # suffix for profile input tables
        
        # The following initialization routines generate dictionaries by which the script can identify the methods
        # in case the run is to be continued after break.
        self._init_pre_da_methods()
        self._init_da_models()
        self._init_weight_functions()

    def _init_pre_da_methods(self):
        self.pre_da_methods_dict = {'default': pre_da_default, 'interpolate': pre_da_interpolate, 'mean': pre_da_mean, 
                                    'maxnorm': pre_da_maxnorm, 'min': pre_da_min, 'original': pre_da_original, 
                                    'maximum': pre_da_maximum, 'mt-unequal': pre_da_mtunequal, 'spline': pre_da_spline}

        # the input passed to pre_da_method.run
        self.pre_da_methods_inp = {key: {} for key in self.pre_da_methods_dict}
        self.pre_da_methods_inp['interpolate'] = {'min_wrt_turns': True}
        self.pre_da_methods_inp['mt-unequal'] = {'stable': True}
        
    def _init_da_models(self):
        self.da_models_dict = {'model1': model1, 'model2': model2, 'model2b': model2b, 'lambert': lambert,
                               'lambertb': lambert_b, 'mgmodel1': mg_model1, 'model4b': model4b}
        
    def _init_weight_functions(self):   
        self.weight_functions_dict = {'null': lambda x: x, 
                                      'log': lambda x: np.log(x),
                                      'lin': lambda x: x/x[-1],
                                      'cfunc': lambda x: (x > 4e4).astype(int)}
        self.weight_functions_dict['cfunc2'] = lambda x: cfunc_perc(x, 4e4, 40)

    def get_entry(self, number, verbose=True, format='pandas', **kwargs):
        '''
        Obtain the data of a specific entry of the meta-data, given by its index.
        '''

        entry_info = self.meta_info.iloc[number]
        database_name = '{}/{}'.format(self.output_folder, entry_info['table_name'])
        if os.path.isfile(database_name):
            if verbose:
                print ('Opening database\n {}'.format(database_name))
                
            data = papq.read_table(database_name, **kwargs)
            if format == 'pandas':
                data = data.to_pandas()
        else:
            if verbose:
                print ('Database\n {}\ndoes not exist.'.format(database_name))
            data = []
            entry_info = []
            
        return data, entry_info

    def get_meta_info(self, **kwargs):
        '''
        Obtain the meta information of the study.
        '''
        if os.path.exists(self._metainfo_filename):
            meta_info = get_sql_data(filename=self._metainfo_filename, table_names=self._metainfo_tablename)
        else:
            print ('Meta database\n {}\ndoes not yet exists. Attempting to build it ...'.format(self._metainfo_filename))
            meta_info = self.build_meta_info()
            conn = sqlite3.connect(self._metainfo_filename)
            meta_info.to_sql(self._metainfo_tablename, conn, index=False, **kwargs)
            conn.close()
        self.meta_info = meta_info
        return meta_info
        
    def build_meta_info(self):
        '''
        Combine several process-ID files to common meta info database.
        
        Note that the names of these pid-databases are defined in the launcher/submit.py script.
        '''
        
        metainfo_filenames = glob.glob('{}/{}_pid*.db'.format(self.output_folder, self.output_filename))
        common_table = []
        for filename in metainfo_filenames:
            common_table.append(get_sql_data(filename=filename, table_names=self._metainfo_tablename))
        return pd.concat(common_table)
    
    def create_meta_info(self, verbose=True, force=False):
        '''
        Search the output folder for meta data information. In case that several processes were working on the study, there will be
        more than one meta data information file. This function will create a combined table. If only one file was found, then no
        operation will be performed.
        
        force: force regeneration of meta table
        '''
        if os.path.exists(self._metainfo_filename) and not force:
            print ('Meta data file\n {}\nexists.'.format(self._metainfo_filename))
            return

        meta_data_files = sorted(glob.glob('{}/{}_pid*.db'.format(self.output_folder, self.output_filename)))
        if len(meta_data_files) > 0:
            data = []
            for filename in meta_data_files:
                if verbose:
                    print ('Reading file {}'.format(filename))
                data.append(get_sql_data(filename, self._metainfo_tablename))
            data = pd.concat(data)
            conn = sqlite3.connect(self._metainfo_filename)
            data.to_sql(name=self._metainfo_tablename, con=conn, if_exists='replace', index=False)
            conn.close()
            if verbose:
                print ('Meta data table\n {}\ncreated using {} files.'.format(self._metainfo_filename, len(meta_data_files)))
 
 
    def get_data(self, meta_info=[], use_legacy_dataset=False, **kwargs):
        '''
        meta_info: Pandas DataFrame of the data which should be loaded.
        
        Filters can be passed in the form:
            filters = [('seed', '=' 3)] etc.
        see more at 
        https://arrow.apache.org/docs/python/generated/pyarrow.parquet.ParquetDataset.html
        (use_legacy_dataset=False must be enabled for this to work)
        
        **kwargs are passed to ParquetDataset.
        
        Returns a PyArrow.parquet.DataSet object
        '''
        if len(meta_info) == 0:
            meta_info = self.get_meta_info()
        tables = ['{}/{}'.format(self.output_folder, e) for e in list(meta_info['table_name'].values)]
        return papq.ParquetDataset(tables, use_legacy_dataset=use_legacy_dataset, **kwargs)
        
    def set_prepared_table_name(self, scale_to):
        '''
        Returns the table name of the given scaling factor.
        '''
        return '{}_{}'.format(self._prepared_table_prefix, scale_to)

    def get_scaling_factor_from_prepared_table_name(self, filename):
        '''
        Returns the scaling factor of the prepared table name as string.
        '''
        return filename.split('_')[-1].split('.')[0]


    def prepare_profile_data_input(self, path, profile_model, scaling_factors=[], verbose=True, store=False, **kwargs):
        '''
        A study may require at least one table containing the necessary information of the beam profiles (and emittances).
        In order to prevent to use (and copy) the full HDF database when running a study on a cluster, such tables needs to be
        computed and stored beforehand, depending on the scaling factors used.

        INPUT:
        path: The path of the files which will be generated.
        profile_model: An instance of a profile model.
        scaling_factors: A list of objects. E.g. [0.3, 1.1, None]

        **kwargs are given to profile_model.prepare_sim_loss
        '''
        for scale_to in scaling_factors:
            prepared_table_name = self.set_prepared_table_name(scale_to)
            if verbose:
                print ("Computing table '{}' in database:\n {}".format(prepared_table_name,
                                                                       profile_model.filename))                
                  
            data = profile_model.prepare_sim_loss(scale_to=scale_to, verbose=verbose, store=store, **kwargs)

            filename = '{}/{}.parquet'.format(path, prepared_table_name)
            if verbose:
                print ('Storing table to:\n{}'.format(filename))
            append_to_parquet_table(data, filepath=filename)

    def load_profile_data(self, filenames, verbose=True):
        '''
        Load beam profile tables into memory.
        '''
        profile_data = {}
        for filename in filenames:
            if verbose:
                print ('Reading profile table:\n{}'.format(filename))
            table = papq.read_table(filename).to_pandas()
            scale_to = self.get_scaling_factor_from_prepared_table_name(filename=filename)
            profile_data[scale_to] = table
        self.profile_data = profile_data

    def load_profile_emittances(self, lookup_key='emit_index', verbose=False):
        '''
        Determine the emittances from the profile data (if exists).
        
        filenames: PyArrow table names containing the profile data. See self.load_profile_data.
        '''
        if not hasattr(self, 'profile_data'):
            raise ValueError('self.profile_data needs to be generated via self.load_profile_data first.')

        emittances = {}
        for key in self.profile_data.keys():
            emittances[key] = get_emittances(self.profile_data[key], lookup_key=lookup_key, verbose=verbose)
        return emittances

    def get_jobs(self, cont):
        '''
        This function checks if there are already jobs in the output folder (based on the metadata) and calls the job generator if
        necessary. A list of jobs is returned.
        '''
        if cont:
            database_exists = os.path.isfile(self._metainfo_filename)
            if not database_exists:
                raise RuntimeError('Output database\n {}\ndoes not exist, but cont == True.'.format(self._metainfo_filename))
            if verbose:
                print ('Continuing loop from last case in database ...')
            # this option is abusing the fact that the results of the individual steps are performed independently. Thus we do not
            # have to recompute everything, if we assume that the SQL databases have not been touched.
            jobs = self._generate_missing_job_list()
        else:
            jobs = self.cases
        
        return jobs


class sixdesk_study(study):
    
    def __init__(self, **kwargs):
        '''
        errors: A list which can be either 'none', 'x', 'y', 'xy'. This means either no error bars are taking into account,
            only errors in the DA or both the DA and turns.
        '''

        study.__init__(self, **kwargs)
        
        self.required_job_keys = ['errors', 'pre_da_method', 'da_mode', 'int_method', 'da_model', 'weight']
        self.meta_info_columns = self.required_job_keys + ['fit_method', 'fit_opt', 'execution_date',
                               'table_name', 'run_time']

    def run(self, sql_files, jobs, turns, emittances=[], start_index_da=0, stop_index_da=None, verbose=False, save=False,
            error_analysis=False, cont=False, **optargs):
        
        '''
        error_analysis: (boolean) Also perform an error analysis (to compute confidence levels). Warning: This may slow down
                        calculations in the next loops (for a reason yet to be understood.)
        save: If 'True', then store certain intermediate results to SQL file(s). Default: False.
        emittances: List of emittance pairs (optional).
        '''
        jobs = pd.DataFrame(jobs).sort_values(self.required_job_keys) # ensures that the jobs are sorted so that when passing through the jobs, the first keys are changing only if required.
        
        n_jobs = len(jobs)

        print ('Output database:\n {}'.format(self._metainfo_filename))
        print ('         # jobs: {}'.format(n_jobs))
        
        old_job = {key: None for key in self.required_job_keys}
        
        for k in range(n_jobs):
        
            job = jobs.iloc[k]
            if k > 0:
                old_job = jobs.iloc[k - 1]
        
            errors = job['errors']
            pre_da_method = job['pre_da_method']
            da_mode = job['da_mode']
            integration_method = job['int_method']
            da_model = job['da_model']
            weight_function = job['weight']

            if verbose:
                print ('\nrunning case')
                print ('------------')
                print ('            errors: {}'.format(errors))
                print ('     pre_da_method: {}'.format(pre_da_method))
                print ('           da_mode: {}'.format(da_mode))
                print ('integration_method: {}'.format(integration_method))
                print ('          da_model: {}'.format(da_model))
                print ('   weight_function: {}\n'.format(weight_function))
            run_start_time = timelib.time()
            
            check1 = errors != old_job['errors'] or pre_da_method != old_job['pre_da_method']
            if check1:
                with_errors = not errors == 'none'
                
                pda = precompute_da(sql_files, instance=self.pre_da_methods_dict[pre_da_method], 
                                    turns=turns, with_errors=with_errors, verbose=verbose)
                pda.run(verbose=verbose, **self.pre_da_methods_inp[pre_da_method])
                         
            check3 = check1 or da_mode != old_job['da_mode'] or integration_method != old_job['int_method']
            if check3:
                da = compute_da(sql_files, with_errors=with_errors, verbose=verbose)
                da.run(emittances=emittances, 
                       mode=da_mode, 
                       method=integration_method,
                       start_index=start_index_da,
                       stop_index=stop_index_da,
                       save=save,
                       verbose=verbose)

            check4 = check3 or da_model != old_job['da_model'] or weight_function != old_job['weight']
            if check4:
                fit_input = {'errors': errors} # if y in errors, then lmfit is run with y-errors in this first iteration.
                fit_input['use_existing_fit_parameters'] = False
                fit_input['davst'] = da
                if weight_function != 'null':
                    fit_input['weight_function'] = self.weight_functions_dict[weight_function]

                da_model_instance = self.da_models_dict[da_model]()
                fit_routine = make_lmfit(model=da_model_instance, **optargs)
                fit_method_descr = fit_routine.description
                fit_method_method = fit_routine.method
                fit = perform_fit(sql_files, fit_routine=fit_routine, verbose=verbose)
                fit.run(**fit_input)
                    
                if errors in ['x', 'xy']:
                    if verbose:
                        print ('Re-run fit, now with existing fit parameters near the optimum ...')
                    # Initiate class to re-run this case, now using algopy (or JAX), see below:

                    fit_input_err = {'errors': errors}
                    fit_input_err['use_existing_fit_parameters'] = True # use existing fit parameters as start parameters for fits
                    fit_input_err['davst'] = da
                    if weight_function != 'null':
                        fit_input_err['weight_function'] = self.weight_functions_dict[weight_function]

                    da_model_instance_err = self.da_models_dict[da_model]()
                    fit_routine_err = algopy_fit(model=da_model_instance_err, **optargs)
                    #fit_routine_err = jax_fit(model=da_model_instance_err, **optargs)
                    fit_method_descr = fit_routine_err.description # overwrite fit_method_descr for the database information
                    fit_method_method = fit_routine_err.method # overwrite fit_method_method for database information
                    fit_err = perform_fit(sql_files, fit_routine=fit_routine_err, verbose=verbose)
                    fit_err.run(**fit_input_err)
                    
                if with_errors and error_analysis:
                    da_model_instance3 = self.da_models_dict[da_model]()
                    # attention: This may slow down calculations afterwards (need to figure out the reason)
                    ea = error_jax(filenames=sql_files, model=da_model_instance3)
                    ea.run(verbose=verbose)

                # store the results
                tkey = int(timelib.time()) # we use the timestamp as key to store this run to the database. In order to
                # locate the run, we store its meta info in the meta-info table.
                filename = '{}.parquet_{}{}'.format(self.output_filename, self._table_suffix, tkey)
                run_info_list = [errors, pda.objects[0].description, da_mode, integration_method, da_model_instance.suffix[1:],
                                        weight_function, fit_method_descr, fit_method_method, tkey, filename]

                # show progress of the study
                print ('===========================')
                print ('Storing results to database\n {}\n key: {}'.format(self._metainfo_filename, filename))
                print ('Parameters:\n{}\n{}'.format(self.meta_info_columns, run_info_list))     
                print ('Case: {}/{}'.format(k + 1, n_jobs))
                print ('===========================')

                filename_full_path = '{}/{}'.format(self.output_folder, filename)
                
                # get the simulation data
                six_objects = six_handler(filenames=sql_files, instance=error_instance, da_model=da_model_instance)
                # Obtain the fit parameters for each of the considered SQL databases.
                fit_parameters_sqlfiles, fit_parameter_errors_sqlfiles = [], []
                sql_index = 0
                for loss_obj in six_objects.objects:
                    fit_parameters_df = loss_obj.get_fit_parameters()
                    fit_parameters_df['sql_index'] = sql_index # add sql index to keep track of sql file index
                    fit_parameters_sqlfiles.append(fit_parameters_df)
                    if with_errors and error_analysis:
                        fit_parameter_errors_df = loss_obj.get_fit_parameter_errors()
                        fit_parameter_errors_df['sql_index'] = sql_index
                        fit_parameter_errors_sqlfiles.append(fit_parameter_errors_df)
                    sql_index += 1

                fp = pd.concat(fit_parameters_sqlfiles, ignore_index=True)
                #emit_indices = fp['emit_index'].unique()
                #if with_errors: # initialize required variables for error calculation
                #    fpe = pd.concat(fit_parameter_errors_sqlfiles, ignore_index=True)
                
                papq.write_table(pa.Table.from_pandas(fp), filename_full_path)

                run_time = timelib.time() - run_start_time
                if verbose:
                    print ('Total run time: {:.3}'.format(run_time))
                run_info = pd.DataFrame([run_info_list + [run_time]], columns=self.meta_info_columns)
                
                conn = sqlite3.connect(self._metainfo_filename)
                run_info.to_sql(name=self._metainfo_tablename, con=conn, if_exists='append', index=False)
                conn.close()


class loss_study(study):
    
    def __init__(self, profile_model, output_folder, input_folder, output_filename='results', **kwargs):

        '''
        Automatic analysis of the SixTrack data -- as well as the respective beam loss calculation.
        The individual jobs are stored in Apache Parquet files.
        A file containing the meta-data of all jobs are stored seperately in form of an SQLite file.
        '''

        study.__init__(self, output_folder=output_folder, output_filename=output_filename)
        self.input_folder = input_folder # folder in which the profile data has been stored.
        self.profile_model = profile_model # profile model which will be used to compute the beam losses.
        
        # The following initialization routines generate dictionaries by which the script can identify the methods
        # in case the run is to be continued after break.
        self._init_emittances()
        self._init_loss_models(**kwargs)

        self.required_job_keys = ['errors', 'pre_da_method', 'da_mode', 'int_method', 
                                  'emit_case', 'scale_to', 'da_model', 'weight']
        self.meta_info_columns = self.required_job_keys + ['fit_method', 'fit_opt', 'loss_model', 'execution_date',
                               'table_name', 'run_time']
                               
    def _init_loss_models(self, dGaussL_params={}, dGauss_params={}, **kwargs):
        # note that the profile models are used in the loss calculation only by their loss formula
        # TODO dGauss model requires tests
        self.loss_models_dict = {'dGaussL': ws_dgaussian_link(filename='', verbose=False, **dGaussL_params),
                                 'dGauss': ws_dgaussian(filename='', verbose=False, **dGauss_params)}

    def _init_emittances(self, verbose=True):
        # Initialize a pair of default emittances for the rms mode:
        a = np.sqrt(2)
        emittance0 = (a*3, 3)
        emittance1 = (3, a*3)
        self.emittances_rms = [emittance0, emittance1]

    def prepare_profile_data_input(self, scaling_factors, profile_chi2_perc=85, path='', verbose=True, **kwargs):
        '''
        profile_chi2_perc: Percentile to drop badly fitted beam profiles from the considered data.
        scaling_factors: The scaling factors by which the tables should be computed.
        
        More details see study.prepare_profile_data_input.
        '''

        if len(path) == 0:
            path = self.input_folder

        study.prepare_profile_data_input(self, path=path, profile_model=self.profile_model, 
                                         scaling_factors=scaling_factors,
                                         profile_chi2_perc=profile_chi2_perc,
                                         verbose=verbose, **kwargs)


    def run(self, sql_files, jobs, turns, emittances=[], profile_data_filenames=[], lookup_key='emit_index', 
            start_index_da=0, stop_index_da=None, 
            verbose=False, error_analysis=False, use_default_emittances=False, 
            min_wrt_turns_interpolate=True, save=False, **optargs):
        
        '''
        error_analysis: (boolean) Also perform an error analysis (to compute confidence levels). Warning: This may slow down
                        calculations in the next loops (for a reason yet to be understood.)
        save: If 'True', then store certain intermediate results to SQL file(s). Default: False.

        emittances: (Optional) Run with custom emittances.
        '''
      
        jobs = pd.DataFrame(jobs).sort_values(self.required_job_keys) # ensures that the jobs are sorted so that when passing through the jobs, the first keys are changing only if required.
        n_jobs = len(jobs)

        print ('Output database:\n {}'.format(self._metainfo_filename))
        print ('         # jobs: {}'.format(n_jobs))
        
        old_job = {key: None for key in self.required_job_keys}

        self.load_profile_data(filenames=profile_data_filenames, verbose=verbose)
        if len(emittances) == 0 and not use_default_emittances:
            self.emittances = self.load_profile_emittances(lookup_key=lookup_key)
        else:
            self.emittances = {key: emittances for key in self.profile_data.keys()} # work with always the same emittance set for all keys

        # check input consistency:
        sfset = set(jobs['scale_to'].astype({'scale_to': str}).unique())
        pdset = set(self.profile_data.keys())
        if not sfset.issubset(pdset):
            raise ValueError('Requested scaling factors not a subset of given profile data input.' + \
                             '\nScaling factors: {}\nProfile data input: {}'.format(sfset, pdset))

        for k in range(n_jobs):
        
            job = jobs.iloc[k]
            if k > 0:
                old_job = jobs.iloc[k - 1]
        
            errors = job['errors']
            pre_da_method = job['pre_da_method']
            da_mode = job['da_mode']
            integration_method = job['int_method']
            emit_case = job['emit_case']
            scale_to = job['scale_to']
            da_model = job['da_model']
            weight_function = job['weight']

            if verbose:
                print ('\nrunning case')
                print ('------------')
                print ('            errors: {}'.format(errors))
                print ('     pre_da_method: {}'.format(pre_da_method))
                print ('           da_mode: {}'.format(da_mode))
                print ('integration_method: {}'.format(integration_method))
                print ('         emit_case: {}'.format(emit_case))
                print ('          scale_to: {}'.format(scale_to))
                print ('          da_model: {}'.format(da_model))
                print ('   weight_function: {}\n'.format(weight_function))
            run_start_time = timelib.time()
            
            check1 = errors != old_job['errors'] or pre_da_method != old_job['pre_da_method']
            if check1:
                with_errors = not errors == 'none'
                
                pda = precompute_da(sql_files, instance=self.pre_da_methods_dict[pre_da_method], 
                                    turns=turns, with_errors=with_errors, verbose=verbose)
                pda.run(verbose=verbose, **self.pre_da_methods_inp[pre_da_method])
                
            check2 = check1 or da_mode != old_job['da_mode'] or integration_method != old_job['int_method'] or emit_case != old_job['emit_case'] or scale_to != old_job['scale_to']
            if check2:
                if emit_case == 'default':
                    emittances_used = self.emittances[str(scale_to)]
                if emit_case == 'rms':
                    emittances_used = self.emittances_rms

                da = compute_da(sql_files, with_errors=with_errors, verbose=verbose)
                da.run(emittances=emittances_used, 
                       mode=da_mode, 
                       method=integration_method,
                       start_index=start_index_da,
                       stop_index=stop_index_da,
                       save=save,
                       verbose=verbose)

            check3 = check2 or da_model != old_job['da_model'] or weight_function != old_job['weight'] 
            if check3:                
                fit_input = {'errors': errors} # if y in errors, then lmfit is run with y-errors in this first iteration.
                fit_input['davst'] = da
                fit_input['use_existing_fit_parameters'] = False
                if weight_function != 'null':
                    fit_input['weight_function'] = self.weight_functions_dict[weight_function]

                da_model_instance = self.da_models_dict[da_model]()
                fit_routine = make_lmfit(model=da_model_instance, **optargs)
                fit_method_descr = fit_routine.description
                fit_method_method = fit_routine.method
                fit = perform_fit(sql_files, fit_routine=fit_routine, verbose=verbose)
                fit.run(**fit_input)
                    
                if errors in ['x', 'xy']:
                    if verbose:
                        print ('Re-run fit, now with existing fit parameters near the optimum ...')
                    # Initiate class to re-run this case, now using algopy (or JAX), see below:

                    fit_input_err = {'errors': errors}
                    fit_input_err['use_existing_fit_parameters'] = True # use existing fit parameters as start parameters for fits
                    fit_input_err['davst'] = da
                    if weight_function != 'null':
                        fit_input_err['weight_function'] = self.weight_functions_dict[weight_function]

                    da_model_instance_err = self.da_models_dict[da_model]()
                    fit_routine_err = algopy_fit(model=da_model_instance_err, **optargs)
                    #fit_routine_err = jax_fit(model=da_model_instance_err, **optargs)
                    fit_method_descr = fit_routine_err.description # overwrite fit_method_descr for the database information
                    fit_method_method = fit_routine_err.method # overwrite fit_method_method for database information
                    fit_err = perform_fit(sql_files, fit_routine=fit_routine_err, verbose=verbose)
                    fit_err.run(**fit_input_err)
                    
                if with_errors and error_analysis:
                    da_model_instance3 = self.da_models_dict[da_model]()
                    # attention: This may slow down calculations afterwards (TODO need to figure out the reason)
                    ea = error_jax(filenames=sql_files, model=da_model_instance3)
                    ea.run(verbose=verbose)

                # now the sql files have all data, ready to be used to compute the beam losses.
                if verbose:
                    print ('Computing bunch losses ...')
                        
                # initiate the loss study
                ls = sd_loss_study(sql_files, 
                                   da_model=da_model_instance, 
                                   profile_model=self.profile_model)

                # to store the results
                tkey = int(timelib.time()) # we use the timestamp as key to store this run to the database. In order to
                # locate the run, we store its meta info in the meta-info table.
                filename = '{}.parquet_{}{}'.format(self.output_filename, self._table_suffix, tkey)

                loss_input = {}
                if emit_case == 'rms':
                    loss_input['emittances0'] = self.emittances_rms[0]
                    loss_input['emittances1'] = self.emittances_rms[1]
                if emit_case == 'default':
                    loss_input['lookup_key'] = lookup_key

                filename_full_path = '{}/{}'.format(self.output_folder, filename)                

                ls.compute_simulated_losses(filename=filename_full_path, modus=emit_case, 
                                            profile_table=self.profile_data[str(scale_to)], 
                                            verbose=verbose, **loss_input)

                run_info_list = [errors, pda.objects[0].description, da_mode, integration_method, emit_case, str(scale_to), 
                                 da_model_instance.suffix[1:], weight_function, fit_method_descr, fit_method_method, 
                                 self.profile_model.loss_model_description, tkey, filename]

                # show progress of the study
                print ('===========================')
                print ('Storing results to database\n {}\n key: {}'.format(self._metainfo_filename, filename))
                print ('Parameters:\n{}\n{}'.format(self.meta_info_columns, run_info_list))     
                print ('Case: {}/{}'.format(k + 1, n_jobs))
                print ('===========================')

                run_time = timelib.time() - run_start_time
                if verbose:
                    print ('Total run time: {:.3}'.format(run_time))

                run_info = pd.DataFrame([run_info_list + [run_time]], columns=self.meta_info_columns)
                
                conn = sqlite3.connect(self._metainfo_filename)
                run_info.to_sql(name=self._metainfo_tablename, con=conn, if_exists='append', index=False)
                conn.close()


# ------------------------------------------
# Routines which help to analyse the results
# ------------------------------------------
    
def study_case_mapper(target, reference, iterator_name='study_case'):
    '''
    Adds the study cases to a given target DataFrame, based by the columns in a reference DataFrame.
    '''
    target = target.copy()
    
    ref_columns = list(reference.columns.drop(iterator_name))
    target_columns = list(target.columns)
    
    if iterator_name in target_columns:
        print ('Column {} already found in target. Target unchanged.'.format(iterator_name))
        return target
        
    group = target.groupby(ref_columns).groups
    group_ref = reference.groupby(ref_columns).groups

    for key in group_ref.keys():
        index_ref = group_ref[key]
        case = reference.iloc[index_ref][iterator_name].values[0] # the entries of the reference DataFrame are assumed to be unique
        try:
            index = group[key]
        except:
            print ('key\n {}\nnot found in target.'.format(key))
            continue
        target.loc[index, iterator_name] = case
    # bring column iterator_name to the front
    return target.reindex(columns=[iterator_name] + target_columns).astype({iterator_name: int})

def create_study_cases(data, columns, iterator_name='study_case'):
    '''
    Takes a list of Pandas DataFrames, determines a common set of mutually distinguishable cases and
    returns a function which will add to a given DataFrame a new column enlisting these cases, as
    well as a DataFrame of these unique cases.
    
    data: List of Pandas Dataframes
    
    columns: List of column names by which different cases will be identified.
    
    iterator_name: Name of the column which will be used to enumerate the cases.
    '''
    common_df = []
    for df in data:
        grp = df.groupby(columns)
        common_df.append(list(grp.groups.keys()))
    cgrp = pd.DataFrame(np.vstack(common_df), columns=columns).groupby(columns)
    unique_cases =  pd.DataFrame(list(cgrp.groups.keys()), columns=columns).reset_index().rename(columns={'index': iterator_name})
    return unique_cases, lambda x: study_case_mapper(target=x, reference=unique_cases, iterator_name=iterator_name)


def find_case(input_file, pardict):
    '''
    Helper function to find the data of a specific case in a cluster job.
    This can be useful to load the tables if the entire job is still not completed.
    '''
    
    with open(input_file, 'r') as f:
        indict = json.load(f)
    
    jobs = indict['jobs']
        
    all_indices = []
    for parkey in pardict.keys():
        parameter_job = jobs[parkey]
        
        indices_key = set([k for k in parameter_job.keys() if parameter_job[k] == pardict[parkey]])
        all_indices.append(indices_key)
        
    indices_oi = list(set.intersection(*all_indices))
        
    return indices_oi

def load_case(job_directory, pardict, input_filename='beamloss.inp'):
    '''
    Helper function to load the data of a specific case in a cluster job.
    This can be useful to load the tables if the entire job is still not completed.

    Example:
    ========
    pardict = {'errors': 'y', 'pre_da_method': 'mt-unequal', 'da_mode': 'rms', 'int_method': 'simpson',
           'emit_case': 'default', 'scale_to': 0, 'da_model': 'model2b', 'weight': 'null'}
    data = load_case(<job_output_directory>, pardict)
    '''
    
    input_file = '{}/input/{}'.format(job_directory, input_filename)
    
    indices_oi = find_case(input_file=input_file, pardict=pardict)
    
    if len(indices_oi) != 1:
        raise ValueError('Problem with indices of interest: {}'.format(indices_oi))
    
    filenames = glob.glob('{}/output/*pid{}.parquet*'.format(job_directory, indices_oi[0]))
    filename = filenames[0]
    
    if len(filenames) != 1:
        raise RuntimeError('Case\n {}\nseems to be not finished yet.')
    else:
        print ('Loading database\n {}'.format(filename))
        
    database = papq.read_table(filename).to_pandas()
    
    return database


def get_valid_chi2_dataframe_rows(data, perc=95, verbose=True):
    '''
    Takes a Pandas dataframe and returns a dataframe in which only those rows remain, which have chi2-values
    which are:
      1) not NaNs
      2) within a given percentile range 'perc'.
    '''

    rms_chi2_1_name = 'damodel_chi2_1'
    rms_chi2_2_name = 'damodel_chi2_2'

    data_part1 = data.loc[~data['chi2'].isna()]
    if perc >= 0:
        data_out1 = get_perc_data(data_part1['chi2'].values, perc)
    
    if rms_chi2_1_name in data.columns and rms_chi2_2_name in data.columns:
        # in the rms mode, there are chi2_1 and chi2_2 values instead
        data_part2 = data.loc[(~data[rms_chi2_1_name].isna()) & (~data[rms_chi2_2_name].isna())]
        if perc >= 0:
            q2_a = np.percentile(data_part2[rms_chi2_1_name].values, perc)
            q2_b = np.percentile(data_part2[rms_chi2_2_name].values, perc)
            data_out2 = data_part2.loc[(data_part2[rms_chi2_1_name] <= q2_a) & (data_part2[rms_chi2_2_name] <= q2_b)]

        data_out = pd.concat([data_out1, data_out2])
    else:
        data_out = data_out1

    if verbose:
        print ("Dropping rows with respect to chi2's ...")
        print ('a) not NaNs')
        print ('b) percentile: {} %'.format(perc))
        print ('{:.3f} % of initial data dropped.'.format((len(data) - len(data_out))/len(data)*100))

    return data_out


def get_valid_loss_dataframe_rows(data, perc=95, drop_pos=True, verbose=True):
    '''
    Takes a Pandas dataframe and returns a dataframe in which only those rows remain, which
    are:
      1) not NaNs
      2) exp_loss and sim_loss are positive (= negative beam loss in the machine)
      3) within a given percentile range 'perc'.
    '''

    n_datapoints = len(data)
    data = get_valid_chi2_dataframe_rows(data, perc=perc, verbose=verbose)
    if drop_pos:
        data = data.loc[(~data['sim_loss'].isna()) & (data['sim_loss'] >= 0) &
                        (~data['exp_loss'].isna()) & (data['exp_loss'] >= 0)]
    else:
        data = data.loc[(~data['sim_loss'].isna()) & (~data['exp_loss'].isna())]


    if perc >= 0:
        q1 = np.percentile(data['sim_loss'].values, perc)
        q2 = np.percentile(data['exp_loss'].values, perc)
        data = data.loc[(data['sim_loss'] <= q1) & (data['exp_loss'] <= q2)]

    if verbose:
        print ('Dropping rows with respect to columns sim_loss, exp_loss using')
        print ('percentile: {} %'.format(perc))
        print ('Dropping rows with invalid loss raise: {}'.format(drop_pos))
        print ('{:.3f} % of initial data dropped.'.format((n_datapoints - len(data))/n_datapoints*100))

    return data

from misc.general import df_sub_selection, chi2_test

class generic_test:
    def __init__(self, ascending=False, description1='1', description2='2', with_ndatapoints=False, **kwargs):
        self.ascending = ascending # default sorting to get the best cases on the top of the list
        self.description = 'generic'
        self.description1 = description1
        self.description2 = description2
        self.column_description = []
        if with_ndatapoints:
            self.column_description += ['n_datapoints_{}'.format(description1), 'n_datapoints_{}'.format(description2)]

class c_chi2_test(generic_test):
    def __init__(self, bins='auto', **kwargs):
        generic_test.__init__(self, **kwargs)
        self.description = 'chi2'
        self.column_description += ['chi2 ({})'.format(bins)]
        self.bins = bins

    def run(self, dist1, dist2):
        return len(dist1), len(dist2), chi2_test(dist1, dist2, bins=self.bins)

class c_median_diff(generic_test):
    def __init__(self, **kwargs):
        generic_test.__init__(self, **kwargs)
        self.description = 'median_diff'
        self.column_description += ['median_diff']

    def run(self, dist1, dist2):
        return len(dist1), len(dist2), abs(np.median(dist1) - np.median(dist2))
        
class c_median_relquot(generic_test):
    def __init__(self, **kwargs):
        generic_test.__init__(self, **kwargs)
        self.description = 'median_relquot'
        self.column_description += ['median_relquot']

    def run(self, dist1, dist2):
        return len(dist1), len(dist2), abs(np.median(dist1) - np.median(dist2))/np.median(dist1)

class c_differences(generic_test):
    def __init__(self, **kwargs):
        generic_test.__init__(self, **kwargs)
        self.description = 'differences'
        self.column_description += ['differences_cent', 'differences_rms', 'median_{}'.format(self.description1), 'median_{}'.format(self.description2)]
        self.run_options = kwargs

    def run(self, dist1, dist2, **kwargs):
        rms, cent = get_rms_value(dist1 - dist2, **kwargs)
        return len(dist1), len(dist2), cent, rms, np.median(dist1), np.median(dist2)

def test_losses(metadata, method, sub_selection=True, verbose=True, 
                data_path_column='table_name', group_identifyer='study_case', perc=0, 
                pyarrow_options={}, run_options={}, **kwargs):
    '''
    Test the statistics of simulated losses and measured losses, for the given cases specified by meta_data_columns.

    method: The method how we want to test the statistics.
    sub_selection: If boolean and True, then reference data of column2 global (e.g. measuerment data).
                   If boolean and False, then the individual data must also have the columns: fill, bunch_index, wsh_inout and wsv_inout
                   If any other type will be assumed as numpy array. The values in this array will serve as reference for the losses.
    
    metadata is a list containing the tables for each of the individual simulation results
    '''
    supported_methods = {'chi2': c_chi2_test, 'median_diff': c_median_diff, 'differences': c_differences,
                         'relquot': c_median_relquot}

    rms_chi2_1_name = 'damodel_chi2_1'
    rms_chi2_2_name = 'damodel_chi2_2'
    
    selected_method = supported_methods[method](description1='exp', description2='sim', **kwargs)

    cases = metadata[group_identifyer].unique() # all considered cases in the metadata 
    n_cases = len(cases)

    if verbose:
        print ('  method: {}'.format(selected_method.description))
        print ('n_cases: {}'.format(n_cases))

    if type(sub_selection) == bool:
        if not sub_selection:
            all_tables = list(out[data_path_column].values)
            dataset = papq.ParquetDataset(all_tables, use_legacy_dataset=False, **pyarrow_options)
            columns_ref = ['fill', 'bunch_index', 'wsh_inout', 'wsv_inout']
            df1 = dataset.read(columns=columns_ref + ['exp_loss']).to_pandas().drop_duplicates(subset=columns_ref) # exp_loss does not depend on simulation multiples like seeds, sql indices etc.
            exp_losses_inp = df1['exp_loss'].values
    else:
        exp_losses_inp = sub_selection
        
    out = []
    for k in range(n_cases):
        if verbose:
            print ('Processing {}/{}'.format(k + 1, n_cases), end='\r')
        case = cases[k]
        rows = metadata[group_identifyer] == case
        metadata_case = metadata.loc[rows]
        tables_k = list(metadata_case[data_path_column].values)      
        dataset_k = papq.ParquetDataset(tables_k, use_legacy_dataset=False, **pyarrow_options)
        
        # we will also load the chi2-values from the fitting, if they exist in the tables
        if 'chi2' in dataset_k.schema.names:
            df_k = dataset_k.read(columns=['sim_loss', 'exp_loss', 'chi2']).to_pandas()
        elif rms_chi2_1_name in dataset_k.schema.names and rms_chi2_2_name in dataset_k.schema.names:
            # chi2 in the rms mode will be determined as the average of the two reference chi2-values
            df_k = dataset_k.read(columns=['sim_loss', 'exp_loss', rms_chi2_1_name, rms_chi2_2_name]).to_pandas() 
            df_k['chi2'] = (df_k[rms_chi2_1_name].values + df_k[rms_chi2_2_name].values)/2.0
        else:
            df_k = dataset_k.read(columns=['sim_loss', 'exp_loss']).to_pandas()

        sim_losses = df_k['sim_loss'].values
        
        if type(sub_selection) == bool:
            if sub_selection:
                exp_losses_inp = df_k['exp_loss'].values
                
        exp_losses = exp_losses_inp
        
        exp_losses = exp_losses[~np.isnan(exp_losses)]
        sim_losses = sim_losses[~np.isnan(sim_losses)]

        if perc > 0:
            exp_losses = get_perc_data(exp_losses, perc)
            sim_losses = get_perc_data(sim_losses, perc)
            
            
        method_out = selected_method.run(exp_losses, sim_losses, **run_options)
        
        out_case = pd.DataFrame(metadata_case)
        out_case[selected_method.column_description] = pd.DataFrame([method_out], index=metadata_case.index)
     
        if 'chi2' in df_k.columns:
            chi2s = df_k['chi2'].values
            chi2s = chi2s[~np.isnan(chi2s)]
            out_case['chi2'] = [np.median(chi2s)]*len(out_case)
        
        out.append(out_case)

    if verbose:
        print ()

    return pd.concat(out)

