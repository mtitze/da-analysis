import numpy as np
import math

import pyarrow as pa
import pyarrow.parquet as pq

def df_sub_selection(dataframe, conditions):
    '''
    Helper function to get subset of given Pandas dataframe by multiple conditions (as dict).
    '''
    query_str = ''
    for key in conditions.keys():
        value = conditions[key]
        if type(value) == str:
            value = "'{}'".format(value)
        query_str += "{} == {} & ".format(key, value)
    
    return dataframe.query(query_str[:-3])

def append_to_parquet_table(dataframe, filepath=None, writer=None):
    """Method to write/append dataframes in parquet format.

    This method is used to write pandas DataFrame as pyarrow Table in parquet format. If the methods is invoked
    with writer, it appends dataframe to the already written pyarrow table.

    :param dataframe: pd.DataFrame to be written in parquet format.
    :param filepath: target file location for parquet file.
    :param writer: ParquetWriter object to write pyarrow tables in parquet format.
    :return: ParquetWriter object. This can be passed in the subsequenct method calls to append DataFrame
        in the pyarrow Table
    """
    # see: https://stackoverflow.com/questions/47113813/using-pyarrow-how-do-you-append-to-parquet-file
    table = pa.Table.from_pandas(dataframe)
    if writer is None:
        writer = pq.ParquetWriter(filepath, table.schema)
    writer.write_table(table=table)
    return writer

def helpsort(a, b):
    '''sort list b according to list a'''
    # https://stackoverflow.com/questions/6618515/sorting-list-based-on-values-from-another-list
    c = sorted(zip(a, b))
    return np.array([p for p,_ in c]), np.array([n for _,n in c])

def nmoment(x, n, weight=None, c=0.0, normalize=True, stable=False):
    '''Compute the moments of a given (1D) distribution
    with weight function (usually an array of ones)
    c: center of array
    n: power of moment.

    stable: Instead of the arithmetic mean use the median, 
            which is more stable against outliers.
    '''
    xx = x - c # correct input by center
    if weight is None:
        weight = np.ones(len(xx))

    if normalize:
        weight = weight/sum(weight)
    summand = xx**n
    
    if stable:
        # compute weighted median; alternative: import weighted .. then: weighted.median(xx, yy)
        ss, sw = helpsort(summand, weight)
        swc = np.cumsum(sw)
        cutoff = sum(sw)/2
        second_half = np.where(swc > cutoff)[0]
        index = second_half[0]
        if index >= 1:
            median = (ss[index - 1] + ss[index])/2
        else:
            median = ss[index]
        return median
    else:
        return sum(summand*weight)

def get_rms_value(hor_data, dat_wght=None, centered=True, biased=False, stable=False, verbose=False):
    '''Takes two arrays (of the same length), the baseline of the ordinate and returns
    the RMS value and the center of the data.

    biased: If True, do not include Bessel's correction.
    stable: Option propagated to nmoment.

    RETURNS:
    rms, center
    '''
    dat_center = 0.0
    if centered:
        dat_center = nmoment(hor_data, 1, weight=dat_wght, stable=stable)

    result = np.sqrt(nmoment(hor_data, 2, weight=dat_wght, c=dat_center, stable=stable))

    if not biased: # include Bessel's correction
        result = np.sqrt(len(hor_data)/(len(hor_data) - 1))*result

    if verbose:
        print ('centered: {}'.format(centered))
        print ('  center: {}'.format(dat_center))
        print ('  biased: {}'.format(biased))
    
    return result, dat_center

def error_of_average(errors):
    '''
    The error propagated by the average (see the manual for details).
    '''
    return np.sqrt(sum(np.array(errors)**2))/len(errors)

def check_for_nan(data):
    '''
    Small helper function to check for NaNs.
    '''
    return any([math.isnan(d) for d in data])

from scipy.stats import iqr as sc_iqr

def get_fdr_bin_width(values):
    '''
    Get Freedman-Diaconics bin width.
    '''
    iqr = sc_iqr(values)
    return 2*iqr/len(values)**(1/3)

def get_common_binning(values1, values2, bins='auto'):
    '''
    Get a common bound and bin number of two data sets.
    '''           
    common_lower_bound = min([min(values1), min(values2)])
    common_upper_bound = max([max(values1), max(values2)])
    common_bound = [common_lower_bound, common_upper_bound]
        
    if bins == 'auto':
        bin_width_1 = get_fdr_bin_width(values1)
        bin_width_2 = get_fdr_bin_width(values2)
        common_bin_width = max([bin_width_1, bin_width_2])
        bins = int((common_bound[1] - common_bound[0])/common_bin_width)

    return common_bound, bins

from scipy.special import gammainc

def chi2_test_binned(dist1, dist2, tol=1e-8):
    '''
    A chi2 test whether two binned distributions may come from a common distribution, 
    see Ref. [1], Eq. (14.3.2), p. 616.
    
    Returns: Probability that both binned distributions originate from a common distribution.
    
    Attention: It is assumed that both binned distributions used the same bin width, so that the number
    of data points in each bin correspond to the same intervals. This must be guaranteed before applying this
    function.
    
    References:
      [1]: Press, Teukolsky, Vetterling, Flannery: Numerical Recipes in FORTRAN 77, Edition 2, Volume 1 (1997).
    '''
    nbins = len(dist1) # = len(dist2)
    degrees_of_freedom = nbins
    if abs(sum(dist1) - sum(dist2)) < tol:
        degrees_of_freedom += -1
        
    chi2 = 0
    for k in range(nbins):
        d1, d2 = dist1[k], dist2[k]
        if d1 == 0 and d2 == 0:
            degrees_of_freedom += -1
        else:
            chi2 += (d1 - d2)**2/(d1 + d2)
 
    return 1 - gammainc(degrees_of_freedom/2, chi2/2) # = gammq(a, x) in Ref. [1], see p.211.

def chi2_test(values1, values2, bins='auto', tol=1e-8, density=True, **kwargs):
    common_bound, bins = get_common_binning(values1=values1, values2=values2, bins=bins)
    h1, e1 = np.histogram(values1, range=common_bound, bins=bins, density=density, **kwargs)
    h2, e2 = np.histogram(values2, range=common_bound, bins=bins, density=density, **kwargs)
    return chi2_test_binned(h1, h2)

def get_perc_data(data, perc):
    '''
    Return data with respect to a given percentile.
    '''
    Q = np.percentile(data, perc)
    return data[data <= Q]

def get_sorted_correlations(data):
    '''
    Takes a Pandas DataFrame and returns an index of its entries, sorted by the strongest correlated variables.
    '''
    corr_matrix = data.corr().abs()
    # From 
    # https://stackoverflow.com/questions/17778394/list-highest-correlation-pairs-from-a-large-correlation-matrix-in-pandas
    #the matrix is symmetric so we need to extract upper triangle matrix without diagonal (k = 1)
    sol = (corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
                      .stack()
                      .sort_values(ascending=False))
    return sol
