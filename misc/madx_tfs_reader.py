from sys import stdout
import numpy as np
from itertools import product
from scipy import constants as const

# N.B. taken from Oeftiger's libtunespread.py, translated to Python 3

def get_content_length(filename):
    source = open(filename, "r")
    n_content_lines = 0
    for line in source:
        if line.split()[0] not in ["@", "*", "$"]:
            n_content_lines += 1
    if n_content_lines == 0:
        raise IOError("Could not identify twiss data contents in file" +
                        filename + "!")
    source.close()
    return n_content_lines

def evaluate_headers(sources, inputs):
    translation = { "MASS": "mass",
                    "GAMMA": "gamma", "SIGT": "sig_z",
                    "NPART": "n_part", "EX": "emit_geom_x",
                    "EY": "emit_geom_y", "SIGE": "deltaE" ,
                    "Q1": "qx", "Q2": "qy"}
    for src in sources:
        src.seek(0, 0)
        line = src.readline()
        while line[0] == "@":
            fields = line.split()
            if fields[1] == "SEQUENCE":
                inputs["machine"] = fields[3].strip('"')
            for twiss_var, trans in translation.items():
                if fields[1] == twiss_var and not inputs[trans]:
                    inputs[trans] = float(fields[3])
            line = src.readline()
    return inputs

def advance_to_col_def(source):
    source.seek(0, 0)
    last_pos = source.tell()
    while source.readline()[0] is not "*":
        last_pos = source.tell()
    source.seek(last_pos)
    return source

def advance_to_contents(source):
    last_pos = source.tell()
    while source.readline()[0] in ["@", "*", "$"]:
        last_pos = source.tell()
    source.seek(last_pos)
    return source

def parse_cols(col_description_lines):
    columns = dict()
    for line in col_description_lines:
        assert line[0] == "*"
    col_madx_names = { "name": ["NAME"],
                       "s": ["S"],
                       "beta_x": ["BETX", "BETA11"],
                       "beta_y": ["BETY", "BETA22"],
                       "d_x": ["DX", "DISP1"],
                       "d_y": ["DY", "DISP2"] }
    fields = [line.split() for line in col_description_lines]
    for var, synonyms in col_madx_names.items():
        for syn, i_file in product(synonyms, range(len(fields))):
            try:
                # -1 due to additional asterisk field in the line
                col = fields[i_file].index(syn) - 1
                break
            except ValueError:
                col = -1
        else:
            i_file = None
        columns[var] = (i_file, col)
    return columns

def fill_data(lines, columns, data):
    fields = [ l.split() for l in lines ]
    for var, coltuple in columns.items():
        value = fields[ coltuple[0] ][ coltuple[1] ]
        try:
            data[var].append( float(value) )
        except:
            data[var].append(value)
    return data

def check_data_integrity(data, inputs):
    assert (len(data["s"]) == len(data["beta_x"]))
    assert (len(data["beta_x"]) == len(data["beta_y"]))
    assert (len(data["beta_y"]) == len(data["d_x"]))
    assert (len(data["d_x"]) == len(data["d_y"]))
    assert (inputs["gamma"] > 1.0)
    if not inputs.get('coasting', False):
        assert (inputs["sig_z"] != 0.0)

def get_source_program(filename):
    source = open(filename, "r")
    fields = source.readline().split()
    while fields[0] == "@":
        if fields[1] == "NAME":
            if fields[3] == '"PTC_TWISS"':
                return "PTC"
            elif fields[3] == '"TWISS"':
                return "MADX"
            else:
                break
        fields = source.readline().split()
    return "notfound"

def correct_madx_dispersion(data, inputs):
    # correcting for ( dispersion / beta ) output from MAD-X,
    # make sure this is done as long as the gamma is still
    # the one taken from the MADX file (and not yet overwritten by
    # complete() with the prioritized command line argument value)
    inputs["beta"] = np.sqrt(1.0 - 1.0 / inputs["gamma"]**2)
    data["d_x"][:] = [ d * inputs["beta"] for d in data["d_x"] ]
    data["d_y"][:] = [ d * inputs["beta"] for d in data["d_y"] ]
    return data

def complete(data, inputs):
    '''Determine the priorities for overriding parameters w.r.t each
    other and the inputs read from the TWISS file.
    '''
    if "Ekin" in inputs:
        inputs["gamma"] = inputs["Ekin"] / inputs["mass"] + 1.0
    else:
        inputs["Ekin"] = (inputs["gamma"] - 1) * inputs["mass"]
    inputs["beta"] = np.sqrt(1.0 - 1.0 / inputs["gamma"]**2)
    if "deltap" in inputs:
        inputs["deltaE"] = (inputs["beta"] * inputs["beta"] *
                                                    inputs["deltap"] )
    else:
        inputs["deltap"] = ( inputs["deltaE"] /
                                ( inputs["beta"] * inputs["beta"] ) )
    if inputs.get("coasting", False):
        circumference = data["s"][-1]
        inputs["sig_z"] = circumference
        inputs.pop("bunch_length", None)
    if "bunch_length" in inputs:
        inputs["sig_z"] = ( 1.0e-9 * inputs["beta"] * const.c *
                                inputs["bunch_length"] / 4.0 )
    else:
        inputs["bunch_length"] = ( 4.0 * inputs["sig_z"] /
                                ( 1.0e-9 * inputs["beta"] * const.c ) )
    if "emit_norm_tr" in inputs:
        inputs["emit_norm_x"] = inputs["emit_norm_tr"]
        inputs["emit_norm_y"] = inputs["emit_norm_tr"]
        inputs["emit_geom_x"] = ( inputs["emit_norm_x"] /
                                ( inputs["gamma"] * inputs["beta"] ) )
        inputs["emit_geom_y"] = ( inputs["emit_norm_y"] /
                                ( inputs["gamma"] * inputs["beta"] ) )
    else:
        if 'emit_norm_x' in inputs:
            inputs["emit_geom_x"] = (inputs["emit_norm_x"] /
                                     (inputs["gamma"] * inputs["beta"] ))
        else:
            inputs["emit_norm_x"] = ( inputs["emit_geom_x"] *
                                    inputs["gamma"] * inputs["beta"] )
        if 'emit_norm_y' in inputs:
            inputs["emit_geom_y"] = (inputs["emit_norm_y"] /
                                     (inputs["gamma"] * inputs["beta"] ))
        else:
            inputs["emit_norm_y"] = ( inputs["emit_geom_y"] *
                                    inputs["gamma"] * inputs["beta"] )
    return data, inputs

def get_inputs(filenames, params={}, f_verbose=False):
    """obtains parameters and TWISS data from the MAD-X or PTC TWISS files
        <i>filenames</i> (usually ending on .tfs) and returns
        dictionaries / hash tables <i>inputs</i> and <i>data</i>
        containing all required data for calculating the space charge
        tune shift with the function calc_tune_spread. Parameters are
        taken from the files with descending priority. Parameters
        defined by <i>params</i> override parameters read from the
        TWISS files."""
    n_content_lines = get_content_length(filenames[0])
    inputs = {
        "machine": None, "mass": None, "n_part": None,
        "emit_geom_x": None, "emit_geom_y": None,
        "gamma": None, "deltaE": None, "sig_z": None,
        #"n_charges_per_part": None,
        "coasting": params.get('coasting', False),
        "lshape": params.get('lshape', 1.),
        "qx": None, "qy": None
    }
    data = {"name": [], "s": [], "beta_x": [], "beta_y": [], "d_x": [], "d_y": [] }
    sources = [open(fn, "r") for fn in filenames]
    #for fn in filenames:
    #    assert n_content_lines == get_content_length(fn)
    # twiss file header part:
    inputs = evaluate_headers(sources, inputs)
    for src in sources:
        advance_to_col_def(src)
    columns = parse_cols( [ src.readline() for src in sources ] )
    for src in sources:
        advance_to_contents(src)
    # twiss file content part
    current_content_line = 0
    while current_content_line < n_content_lines:
        data = fill_data( [ src.readline() for src in sources ], columns, data)
        current_content_line += 1
        if f_verbose:
            stdout.write("\rread twiss %.2f%% (item %d out of %d)" %
                    (100.0 * current_content_line / n_content_lines,
                            current_content_line, n_content_lines) )
            stdout.flush()
    if f_verbose:
        stdout.write("\n")
    for src in sources: src.close()
    inputs.update(params)
    for key, value in inputs.items():
        # complain if no value is set, ignore sig_z for a coasting beam
        if (value is None) and not (key is 'sig_z' and inputs['coasting']):
            raise NameError(key + " has not been set appropriately!")
    check_data_integrity(data, inputs)
    source_program = get_source_program(filenames[0])
    if source_program is "MADX":
        data = correct_madx_dispersion(data, inputs)
        if f_verbose:
            print ("INFO: corrected for MADX dispersion convention via "
                   "multiplying DX (DISP1) by the relativistic beta.")
    elif source_program is "notfound":
        print ("\nWARNING: source program (@NAME) could not be " +
                "detected! No correction of relativistic beta " +
                "normalized dispersion functions (occuring in " +
                "MADX output) has been performed, the dispersion " +
                "has been used as defined in the first TWISS file. \n")
    data, inputs = complete(data, inputs)
    return data, inputs

