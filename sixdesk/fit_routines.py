import numpy as np
import pandas as pd
from scipy.optimize import minimize as scipy_minimize
from lmfit import Minimizer
from .notebook_tools import six_handler, six_instance
from misc.general import check_for_nan, get_rms_value, nmoment

import time as timelib
from itertools import product
import sqlite3

import matplotlib.pyplot as plt
import matplotlib.ticker as mticker

'''
This file contains a collection of various fit routines as well as the tools to load the SQL databases and perform the fitting procedure themselves.

Note that the chi2-values from routines with x- and y- error currently can not be compared to routines having only y-errors.
'''

class perform_fit(six_handler):
    '''
    Master class to perform the fitting on the given SQL database files.
    '''
    def __init__(self, filenames, fit_routine, verbose=True, **kwargs):
        self.fit_routine = fit_routine
        self.verbose = verbose
        six_handler.__init__(self, filenames=filenames, instance=fit_instance, fit_routine=fit_routine, **kwargs)
        if verbose:
            print ('        Fit routine: {}'.format(self.fit_routine.description))
            print ('          Fit model: {}'.format(self.fit_routine.model.description))
            print ('Optimization method: {}'.format(self.fit_routine.method))

    def run(self, davst=None, **kwargs):
        '''
        Perform a fitting for every database of interest.

        davst: davst object (see sixdesk.da.py). Default: None. If a davst object is given as argument, then the da_emit data
               is not loaded from the SQL file, but instead from davst.objects[k].da_out. 
        '''
        for k in range(len(self.objects)):
            obj = self.objects[k]
            inp = kwargs
            if davst != None:
                inp['da_emit'] = davst.objects[k].da_out
            obj.fit(verbose=self.verbose, **inp)

    def get_merits(self, **kwargs):
        '''
        Returns the values of the fit merit functions for all considered cases.
        '''
        merits = []
        for obj in self.objects:
            chi2s = obj._get_data(table_names=obj._fit_parameters_table_name, column_names=['chi2'])
            chi2s = chi2s.loc[chi2s['chi2'].notna()]
            merits.append(obj.fit_merit(chi2s['chi2'].values, **kwargs))
        return merits


def prepare_fit_data(data_key, weight_function=None, errors=None, start_index=None, stop_index=None):

    xdata = data_key['turn'].values[start_index:stop_index]
    ydata = data_key['da'].values[start_index:stop_index]

    if weight_function != None:
        weights = weight_function(xdata)
    else:
        weights = np.ones(len(xdata))

    # If some weights are zero, then also reduce xdata and ydata accordingly.
    indices_of_interest = np.where(weights)
    weights = weights[indices_of_interest]
    xdata = xdata[indices_of_interest]
    ydata = ydata[indices_of_interest]

    xdata_err, ydata_err = [], []
    if errors == 'xy':
        xdata_err = data_key['turn_error'].values[start_index:stop_index]
        ydata_err = data_key['da_error'].values[start_index:stop_index]
        xdata_err = xdata_err[indices_of_interest]
        ydata_err = ydata_err[indices_of_interest]
    elif errors == 'y':
        ydata_err = data_key['da_error'].values[start_index:stop_index]
        ydata_err = ydata_err[indices_of_interest]
    elif errors == 'x':
        xdata_err = data_key['turn_error'].values[start_index:stop_index]
        xdata_err = xdata_err[indices_of_interest]
    else:
        pass

    return weights, xdata, ydata, xdata_err, ydata_err



class fit_instance(six_instance):
    '''
    Class to collect the general routines to perform the fit on the data of an individual SQL file. The fit itself requires
    a specific class of type fit_routine which defines the exact procedure how the fit is performed.
    '''
    def __init__(self, filename, fit_routine=None):
        six_instance.__init__(self, filename)
        self.fit_routine = fit_routine

        # obtain the number of seeds and the tunes used
        self.conn = sqlite3.connect(self.filename)
        cursor = self.conn.cursor()
        self.seeds = self._get_db_seeds(cursor=cursor)
        self.conn.commit()
        self.tunes = self._get_db_tunes(cursor=cursor)
        self.conn.commit()
        self.conn.close()

        self._fit_parameters_table_name = 'fit_parameters2' # the name of the table where to store and load the fit parameters

    def fit(self, da_emit=[], errors='none', seeds=None, use_existing_fit_parameters=False,
            verbose=True, start_index=1, stop_index=-1, weight_function=None, **kwargs):
        '''
        For every seed of interest perform the fitting of the user-given model.
        This is only carried out if more than self._min_datapoints are available.

        INPUT
        =====

        da_emit: Pandas DataFrame containing a table of DA vs. turn values (the output of the davst class in da.py).
                 If nothing is given, then the script will load the data from its SQL table.

        use_existing_fit_parameters: If True, check if there are fit parameters which can be used as
                                     start values for the fitting procedure. E.g. one can run a linear square fit with
                                     constraints prior to a non-linear chi2 optimization routine.
        '''

        ###########################################################################
        if use_existing_fit_parameters:
            # does the SQL DB already have fit parameters? and if yes, do they have the same length as requested in the current model?
            # if yes, these parameters will be used as start values.
            try:
                fdata = self._get_data(self._fit_parameters_table_name, ['seed', 'tunex', 'tuney', 'emit_index'] 
                                       + self.fit_routine.model.fit_parameter_keys)
            except:
                if verbose:
                    print ('ERROR: obtaining fit_parameters from DB failed.')
                use_existing_fit_parameters = False
                pass
        ###########################################################################

        if verbose:
            print ('start_index: {}'.format(start_index))
            print (' stop_index: {}'.format(stop_index))

        if len(da_emit) == 0:
            # load DA vs. turn data
            da_emit = self._get_data('da_emit')
        emittance_indices = da_emit['emit_index'].unique()

        # initialize fit routine input default objects
        weights = []
        da_err, turns_err = [], []

        # loop over seeds which correspond to the different simulation cases
        start = timelib.time()
        results = []

        n_seeds, n_emittances = len(self.seeds), len(emittance_indices)

        for seed, tune_pair, emit_index in product(self.seeds, self.tunes, emittance_indices):
            tunex, tuney = tune_pair
            data_key = da_emit.loc[(da_emit['seed'] == seed) &
                                   (da_emit['tunex'] == tunex) &
                                   (da_emit['tuney'] == tuney) &
                                   (da_emit['emit_index'] == emit_index)]
            # N.B. data_key still consists of a list of data: the DA's for all considered turns.

            if verbose:
                print('\r  FIT of seed {}/{}, tune ({}, {}) ... emit_index: {}/{}   '.format(seed, n_seeds, 
                       tunex, tuney, emit_index, n_emittances), end='')

            weights, turns, da, turns_err, da_err = prepare_fit_data(data_key, weight_function=weight_function, errors=errors,
                                                                     start_index=start_index, stop_index=stop_index)

            self.fit_routine.model.set_fit_parameter_ranges(xdata=turns, ydata=da)

            # a given fit_routine class must have the following functionality (and a 'columns' field)
            self.fit_routine.set_residual(xdata=turns, ydata=da, xdata_err=turns_err, ydata_err=da_err, weights=weights) # must be set BEFORE fit_routine.set_parameters, because in the case of lmfit, set_parameters requires parameter ranges to be set first.

            if use_existing_fit_parameters:
                # instead of the above heuristic start values we use those from the DB
                fit_parameters_seed = fdata.loc[(fdata['seed'] == seed) &
                                                (fdata['tunex'] == tunex) &
                                                (fdata['tuney'] == tuney) &
                                                (fdata['emit_index'] == emit_index)]
                start_values = fit_parameters_seed[self.fit_routine.model.fit_parameter_keys].values[0]
            else:
                start_values = self.fit_routine.model.get_fit_start_parameters()

            parameters = self.fit_routine.model.set_parameters(start_values)
            fit_results = self.fit_routine.fit(parameters=parameters, **kwargs)
            to_append, popt, chi2 = self.fit_routine.make_fit_output(fit_results) # used to create the Pandas DataFrame below
            results.append([seed, tunex, tuney, emit_index] + to_append)

        end = timelib.time()
        if verbose:
            print('\nTime elapsed: {:.3f} [s]'.format(end - start))

        results = pd.DataFrame(results, columns=['seed', 'tunex', 'tuney', 'emit_index'] + self.fit_routine.columns)
        results = results.reset_index(drop=True)
        self._store_data(table_name=self._fit_parameters_table_name, data=results, verbose=verbose)

    def get_fit_parameters(self, model=None):
        '''
        Obtain fit parameters from SQL database.
        '''
        if model == None:
            if hasattr(self, 'model'):
                model = self.model
            elif hasattr(self.fit_routine, 'model'):
                model = self.fit_routine.model
            else:
                print ('ERROR: get_fit_parameters requires a DA-model.')
                return []

        return self._get_data(table_names=self._fit_parameters_table_name, column_names=['seed', 'tunex', 'tuney', 'emit_index'] + 
                                        model.fit_parameter_keys + ['chi2'])

    def show_extrapolated_da(self, turn, seeds=[], emit_indices=[], title='', figsize=(6, 4), filename='', **kwargs):
        '''
        Show the statistics of the DA, extrapolated to a given number of turns.
        '''
        fit_parameters = self._get_data(self._fit_parameters_table_name)
        if len(seeds) > 0:
            fit_parameters = fit_parameters.loc[fit_parameters['seed'].isin(seeds)]
        if len(emit_indices) > 0:
            fit_parameters = fit_parameters.loc[fit_parameters['emit_index'].isin(emit_indices)]
        fit_parameters = fit_parameters[self.fit_routine.model.fit_parameter_keys].values.T
        das = np.array(self.fit_routine.model.run(fit_parameters, turn))
        das = das[~np.isnan(das)] # drop any NaNs

        plt.figure(figsize=figsize)
        h = plt.hist(das, histtype='step', linewidth=2, **kwargs)

        med = np.median(das)
        plt.vlines([med], ymin=0, ymax=max(h[0]), color='red', linestyle='--', linewidth=2, label='median: {:.3f}'.format(med))
        
        plt.title(title, loc='right')
        plt.xlabel(r'DA [$\sigma$]')
        plt.ylabel(r'$N$')
        plt.legend()
        if len(filename) > 0:
            print ('saving plot to file:\n {}'.format(filename))
            plt.savefig(filename, format='svg')
        plt.show()

    def show_fit(self, seed, tunex, tuney, emit_index, fit_start_index, extrapolate=None, fit_values=[], resolution=300,
                 figsize=(10, 4), ax=None, fit_color='black', label='', weight_function=None,
                 with_errors=False, show_data=True):
        '''
        Shows the fitting result of a specific database and seed, for a given model.

        model: requierd to display model specific parameters and descriptions.
        seed: The seed of which we want to display the data (currently no tune/emittance specification implemented).
        fit_start_index: The start index by which the fit has been done (important to prevent NaNs in case that some DA models have bounds).
        extrapolate: (default: None) The scaling in number of turns by which we want to extrapolate the data. By default we use
                     the maximum given turn number.
        fit_values: (optional) instead of using the fit parameters of the database, use user-given fit parameters.
        ax: optional matplotlib axis to use the plot in a specific context. If None is provided (default),
            then a new figure & axis will be created.
        show_data (boolean): Show the raw data (default: True).
        with_errors: Show error bars on the raw data (default: False).
        weight_function: If the fit was run with a weight function, the turns of the fit are only used where the weights are != 0. This can
                         affect the parameter range of the DA model. So the weight function has to be known.

        Note that the chi2-values from routines with x- and y- error currently can not be compared to routines having only y-errors.
        '''

        if hasattr(self, 'model'):
            model = self.model
        else:
            model = self.fit_routine.model

        data = self._get_data([self._fit_parameters_table_name, 'da_emit'], 
                              ['seed, tunex, tuney, emit_index,' + ', '.join(model.fit_parameter_keys), 'seed, tunex, tuney, emit_index, turn, da, turn_error, da_error'])
        fit_parameters = data[0]
        xy_seeds = data[1]

        if ax == None:
            fig, ax = plt.subplots(figsize=figsize)
            make_descriptions = True
            make_labels = False
        else:
            make_descriptions = False
            make_labels = True

        da_data = xy_seeds.loc[(xy_seeds['seed'] == seed) &
                               (xy_seeds['tunex'] == tunex) &
                               (xy_seeds['tuney'] == tuney) &
                               (xy_seeds['emit_index'] == emit_index)]
        xdata = np.array(da_data['turn'])
        ydata = np.array(da_data['da'])

        if with_errors and (not 'turn_error' in da_data.keys() or not 'da_error' in da_data.keys()):
            print ('ERROR: No data of turn-error or DA-error found.')
            with_errors = False
            
        if show_data:
            if with_errors:
                # plot the error bars of the raw data.
                xdata_errors = np.array(da_data['turn_error'])
                ydata_errors = np.array(da_data['da_error'])
                ax.errorbar(xdata, ydata, yerr=ydata_errors, xerr=xdata_errors, capsize=2, fmt='none')
            else:
                ax.scatter(xdata, ydata, marker='.', s=30)

        if len(fit_values) == 0:
            parameters_case = fit_parameters.loc[(fit_parameters['seed'] == seed) &
                                                 (fit_parameters['tunex'] == tunex) &
                                                 (fit_parameters['tuney'] == tuney) &
                                                 (fit_parameters['emit_index'] == emit_index)]
            fit_values = np.array([parameters_case[par_label].values for par_label in model.fit_parameter_keys])
        

        # we will show the fit at the data points for which weight != 0:
        xdata_oi = xdata
        if weight_function != None:
            weights = weight_function(xdata)
            indices_of_interest = np.where(weights)
            xdata_oi = xdata[indices_of_interest]

        if extrapolate == None:
            extrapolate = xdata_oi[-1]

        xdata_ext = np.linspace(xdata_oi[fit_start_index], extrapolate, resolution)
        ydata_fit = model.run(fit_values, xdata_ext)
        
        print ('final_extrapolated_da:\n {}'.format(ydata_fit[-1]))

        if make_labels:
            labels = []
            k = 0
            for value in fit_values:
                labels.append('{} = {:.3f}'.format(model.fit_parameter_keys_label[k], value[0]))
                k += 1
            ax.plot(xdata_ext, ydata_fit, color=fit_color, zorder=100, label='{} ({})'.format(label, ', '.join(labels)))
            ax.legend()
        else:
            ax.plot(xdata_ext, ydata_fit, color=fit_color, zorder=100, label='{}'.format(label))

        # show errors if this function is called in the context of error analysis
        has_error_info = hasattr(self, 'get_fit_parameter_errors') and hasattr(self, 'construct_covariance_matrix')
        if with_errors and has_error_info:
            cov_df = self.get_fit_parameter_errors()
            cov = self.construct_covariance_matrix(cov_df, seed=seed, tunex=tunex, tuney=tuney, emit_index=emit_index)
            fit_rms_errors = np.sqrt(cov.diagonal()) # note that these errors do not indicate any correlations.

        if make_descriptions:
            title_string = '{}'.format(model.description)
            for k in range(len(fit_values)):
                param = fit_values[k][0]
                if with_errors and has_error_info:
                    param_err = fit_rms_errors[k]
                    title_string += ', {}: {:.3f} ({:.3f})'.format(model.fit_parameter_keys_label[k], param, param_err)
                else:
                    title_string += ', {}: {:.3f}'.format(model.fit_parameter_keys_label[k], param)
             
            # display further information ...
            run_params_seeds = self._get_data(self._fit_parameters_table_name, ['seed, tunex, tuney, emit_index, chi2'])
            run_params = run_params_seeds.loc[(run_params_seeds['seed'] == seed) & (run_params_seeds['emit_index'] == emit_index)]

            chi2 = (run_params['chi2'].values)[0]
            emit_index = (run_params['emit_index'].values)[0]

            # to display data in scientific notation in titles, legends etc., taken from
            # https://stackoverflow.com/questions/51502747/convert-float-notation-to-power-of-10-scientific-notation-in-python
            f = mticker.ScalarFormatter(useOffset=False, useMathText=True)
            g = lambda x, pos : "${}$".format(f._formatSciNotation('%1.3e' % x))
            fmt = mticker.FuncFormatter(g)

            title_string += '\n' + r'seed: {}, $Q_x = {{{}}}$, $Q_y = {{{}}}$, emit_index: {}, '.format(seed, tunex, tuney, emit_index) + \
            r'$\chi^2$: {}'.format(fmt(chi2))

            ax.axes.set_title(title_string, loc='right')
            ax.axes.set_xlabel('Turn')
            ax.axes.set_ylabel(r'DA [$\sigma$]')

        return ax

    def fit_parameter_statistics(self, **kwargs):

        data = self._get_data(table_names=self._fit_parameters_table_name, 
                                            column_names=list(self.fit_routine.model.fit_parameter_keys) + ['chi2'])

        model = self.fit_routine.model
        plot_fit_parameter_statistics(data=data, model=model, **kwargs)


def fit_merit(chi2, stable=True):
    '''
    Function to determine how good the fits are -- over all possible seeds.

    We will return the median of the data by default.

    Note that the chi2-values from routines with x- and y- error currently can not be compared to routines having only y-errors.

    INPUT
    =====
    chi2's of all seeds.
    '''
    return nmoment(chi2, n=1, stable=stable) # stable=True: np.median(chi2)



from misc.general import df_sub_selection

def plot_fit_parameter_statistics(data, model, by=[], labels=[], description='', bins='auto', stable=True, perc=90, filename='',
                                 with_bounds=False, file_format='pdf', show_chi2=True, legend=False, figsize=(7, 4)):

    '''
    Display statistics of the fit parameters of the given database.

    Data: Pandas Dataframe containing the necessary fit parameters. If nothing is provided, the routine will load the
              data from the current SQL database.

    by: A list of columns of data, by which we split the data into individual subgroups.
    with_bounds: Show the fit parameter ranges.
    stable: True: show median. False: show mean.
    perc: percentile to remove outliers in chi2.
    filename: Store files to disk using the given filename.
    file_format: Store files to disk, using the given file format (default: 'pdf').
    show_chi2: Also show the chi2-statistics of the fit (default: True).
    '''

    data = data[data['chi2'].notna()]

    parameters = list(model.fit_parameter_keys)
    labels = list(model.fit_parameter_keys_label)

    iterators = [range(len(parameters))]
    if len(by) > 0:
        group = data.groupby(by=by)
        iterators.append(sorted(group.groups.keys()))

    figures = {}
    for el in product(*iterators):

        k = el[0]
        if len(by) > 0:
            conditions = {by[j]: el[1][j] for j in range(len(by))}        
            data_oi = df_sub_selection(data, conditions)
        else:
            data_oi = data


        fig = plt.figure(num=k, figsize=figsize)
        fig.set_tight_layout(False)
        figures[k] = fig

        if len(fig.axes) == 0:
            ax = fig.add_axes([0.15, 0.15, 0.50, 0.6])
        else:
            ax = fig.axes[0]

        parameter_values = data_oi[parameters[k]].values
        median = np.median(parameter_values)
        if len(by) > 0:
            hist_label = '{}\nmed.: {:.3f}'.format(str(conditions)[1:-1], median)
        else:
            hist_label = 'med.: {:.3f}'.format(median)
        uu = ax.hist(parameter_values, bins=bins, histtype='step', linewidth=2, label=hist_label)
        b, a = uu[0], uu[1]


        if len(by) == 0:
            ax.vlines([median], ymin=0, ymax=max(b), linestyle='--', linewidth=2)

        r0, r1 = model.fit_parameter_ranges[k]
        if abs(r0) != np.inf and with_bounds:
            ax.vlines([r0], ymin=0, ymax=max(b), color='red')
        if abs(r1) != np.inf and with_bounds:
            ax.vlines([r1], ymin=0, ymax=max(b), color='red')

    filenames = []
    for k in figures.keys():
        ax = figures[k].axes[0]
        ax.set_ylabel(r'$N$')
        ax.set_xlabel(r'{}'.format(labels[k]))

        if legend:
            # add some space for a legend; we abuse the fact that the figure objects in the above dictionary are modified
            ax1 = figures[k].add_axes([0.65, 0.15, 0.3, 0.6])

            # put the legend of axis 'ax' on axis 'ax1':
            h, l = ax.get_legend_handles_labels()
            ax1.legend(h, l, bbox_to_anchor=(0.5, 0.5), loc='center')

            ax1.axes.get_xaxis().set_visible(False)
            ax1.axes.get_yaxis().set_visible(False)

            ax1.spines['top'].set_visible(False)
            ax1.spines['left'].set_visible(False)
            ax1.spines['bottom'].set_visible(False)
            ax1.spines['right'].set_visible(False)

            ax1.set_title('{}'.format(description), loc='right')
        else:
            ax.set_title('{}'.format(description), loc='right')


        if len(filename) > 0:
            filename_pic = '{}_{}.{}'.format(filename, parameters[k], file_format)
            figures[k].canvas.print_figure(filename_pic, format=file_format, dpi=300)
            filenames.append(filename_pic)

    if show_chi2:
        # also show chi2
        chi2s = data['chi2'].values
        if perc > 0:
            # remove outliers
            p = np.percentile(chi2s, perc)
            chi2s = chi2s[chi2s <= p]

        plt.figure(figsize=figsize)
        vv = plt.hist(chi2s, bins=bins, alpha=0.5, color='green', histtype='step', linewidth=2)
        yy, xx = vv[0], vv[1]


        # show fit merit of chi2 distribution
        merit = fit_merit(chi2s, stable=stable)
        if stable:
            descr = 'median'
        else:
            descr = ''
        plt.vlines([merit], ymin=0, ymax=max(yy), color='red', linestyle='--', 
                   label=r'$\langle \chi^2 \rangle_{{{}}}$ = '.format(descr) + '{:.6f}'.format(merit), linewidth=2)
        plt.ylabel(r'$N$')
        plt.xlabel(r'$\chi^2$')
   
        plt.title('{}'.format(description), loc='right')

        plt.legend()
        if len(filename) > 0:
            filename_pic = '{}_chi2.{}'.format(filename, file_format)
            plt.savefig(filename_pic, format=file_format)
            filenames.append(filename_pic)
    plt.show()
    return filenames


class fit_routine:
    '''
    Class to collect the functionality of how the fit is performed (for example, by using only linear algebra or by including automatic differentiation).

    Note: We divide chi2 by the number of data points minus the number of parameters to avoid that the quantity becomes large;
          in the absence of errors in x-direction, the expectation value of chi2 is just this number (see e.g. my notes regarding
          error analysis).
    '''
    def __init__(self, model, **kwargs):
        self.model = model
        error_keys = ['{}_err'.format(fkey) for fkey in self.model.fit_parameter_keys]
        self.columns = self.model.fit_parameter_keys + error_keys + ['chi2']

    def set_method(self, **kwargs):
        '''
        Sets the method for scipy.optimize.minimize.
        '''
        if 'method' in kwargs.keys():
            self.method = kwargs['method']
        else: 
            self.method = self.model.default_fit_method

    def fit(self, parameters):
        '''
        Perform the fit by calling scipy.optimize.minimize, depending on self.method.
        Note that not all methods are currently implemented. More methods can be supported by adding them
        to the lists below, according to their requirements.

        Supported methods
        -----------------
         Newton-CG
         SLSQP
         L-BFGS-B
         trust-constr
        '''
        inp = {'fun': self.residual_xy, 'x0': parameters, 'method': self.method}

        # Bounds
        if self.method in ['SLSQP', 'L-BFGS-B', 'trust-constr']:
            inp['bounds'] = self.model.fit_parameter_ranges

        # Constraints
        if self.method in ['SLSQP', 'trust-constr'] and hasattr(self.model, '_trust_constraints'):
            inp['constraints'] = self.model._trust_constraints

        # Jacobian
        if self.method in ['Newton-CG', 'L-BFGS-B', 'trust-constr']:
            inp['jac'] = self.residual_xy_gradient

        # Hessian
        if self.method in ['Newton-CG', 'trust-constr']:
            inp['hess'] = self.residual_xy_hessian

        return scipy_minimize(**inp)


class make_lmfit(fit_routine):
    '''
    Fit a given DA-model by using the lmfit Python module.
    N.B. if using errors: Avoid too small errors. Otherwise the routine may break down! (in particular if using sigmas).
    All errors must be positive.

    Optional weights will be included in the fit by the convention that they are applied to chi^2.
    '''
    
    def __init__(self, model, **kwargs):
        fit_routine.__init__(self, model, **kwargs)
        self.model.init_lmfit()
        self.description = 'lmfit'
        self.set_method(**kwargs)
    
    def set_residual(self, xdata, ydata, weights, xdata_err=[], ydata_err=[]):

        # the lmfit routines are based on the assumption of a bilinear log-likelihood with weighted chi2 distribution.
        # xdata_err is not implemented for this method.
        if len(ydata_err) > 0: # len(ydata_err) = len(ydata) will be assumed
            chi2_weights = 1/np.array(ydata_err) # the **2 is avoided because the residual enter lmfit without square
        else:
            chi2_weights = np.ones(len(ydata))

        chi2_weights *= np.sqrt(weights) # square root here, because weights are supposed to be applied to chi^2
        m_n = len(xdata) - len(self.model.fit_parameter_keys) 
        sqrt_m_n = np.sqrt(m_n) # we have to take the square root here, because the residual has no square in this method.

        def residual_xy(parameters):
            parameters_list = [parameters[key].value for key in self.model.fit_parameter_keys] # self.model.fit_parameter_keys is a list, so the order is properly taken into account.
            r = self.model.run(parameters_list, xdata) - ydata
            return r*chi2_weights/sqrt_m_n

        self.residual_xy = residual_xy

    def fit(self, parameters, **kwargs):
        # The default option of nan_policy is to raise a ValueError.
        # Using the option 'propagate' seems to prevent this error.
        mini = Minimizer(self.residual_xy, parameters, nan_policy='propagate')
        return mini.minimize(method=self.method)

    def make_fit_output(self, fit_results):
        ''' 
        create colum names and data to be stored to the SQL database of this fit routine
        output optval used to determine better start values for succeeding fits.
        '''

        p_opt, p_opt_err = [], []
        for parameter_key in self.model.fit_parameter_keys: # fit_parameter_keys is a list, so the order will be correct
            p_opt.append(fit_results.params[parameter_key].value)
            p_opt_err.append(fit_results.params[parameter_key].stderr)

        chi2 = fit_results.chisqr # = sum(self.fit_results.residual**2)
        result = p_opt + p_opt_err + [chi2]

        return result, p_opt, chi2


from algopy import UTPM # UTPM: Univariate Taylor Polynomial of Matrices

class algopy_fit(fit_routine):
    '''
    Class to handle optimization of non-linear chi2 function to include errors in both x and y components.
    
    Method: Algopy using forward mode differentiation.
    Optional weights will be included in the fit by the convention that they are applied to chi^2.
    '''    
    def __init__(self, model, **kwargs):
        fit_routine.__init__(self, model, **kwargs)
        self.model.init_algopy()
        self.description = 'algopy'
        self.set_method(**kwargs)

    def set_residual(self, xdata, ydata, weights, xdata_err=[], ydata_err=[]):
        # attention: no check if both errors are zero!
        if len(xdata_err) == 0:
            xdata_err = np.zeros(len(ydata))
        if len(ydata_err) == 0:
            ydata_err = np.zeros(len(ydata))

        m_n = len(xdata) - len(self.model.fit_parameter_keys)

        #if self._use_dx == 0:
        #    algopy_xdata = UTPM.init_jacobian(xdata)
        #    def non_linear_chi2(parameters):
        #        fxa_ap = self.model.run(parameters, algopy_xdata)
        #        dfdx = UTPM.extract_jacobian(fxa_ap).diagonal()
        #        # convert function evaluation to numpy array as well:
        #        fxa = fxa_ap.data[0, 0]
        #        return sum((fxa - ydata)**2/((xdata_err*dfdx)**2 + ydata_err**2)*weights)/m_n
        #elif self._use_dx == 1:
        #    def non_linear_chi2(parameters):
        #        fxa = self.model.run(parameters, xdata)
        #        dfdx = self.model.dxrun(parameters, xdata)
        #        return sum((fxa - ydata)**2/((xdata_err*dfdx)**2 + ydata_err**2)*weights)/m_n
        #else:
        def non_linear_chi2(parameters):
            fxa = self.model.run(parameters, xdata)
            dfdx = self.model.dxrun_efficient(fxa, parameters, xdata)
            return sum((fxa - ydata)**2/((xdata_err*dfdx)**2 + ydata_err**2)*weights)/m_n
           
        self.residual_xy = non_linear_chi2

    def residual_xy_gradient(self, parameters):
        '''The gradient of self.residual_xy for given parameters.'''
        q1 = UTPM.init_jacobian(parameters)
        y1 = self.residual_xy(q1)
        return UTPM.extract_jacobian(y1)

    def residual_xy_hessian(self, parameters):
        '''The Hessian of self.residual_xy for given parameters.'''
        q2 = UTPM.init_hessian(parameters)
        y2 = self.residual_xy(q2) 
        return UTPM.extract_hessian(len(parameters), y2)
    
    def make_fit_output(self, fit_results):
        ''' 
        create colum names and data to be stored to the SQL database of this fit routine
        output optval used to determine better start values for succeeding fits.
        '''
        
        p_opt = list(fit_results.x)
        optval = fit_results.fun
        p_opt_err = [0]*len(p_opt)

        result = p_opt + p_opt_err + [optval]
        
        return result, p_opt, optval


from jax.config import config
config.update("jax_enable_x64", True)
from jax import jit
from jax import grad as jax_grad
from jax import hessian as jax_hessian
from jax import numpy as jax_numpy

class jax_fit(fit_routine):
    '''
    Class to handle optimization of non-linear chi2 function to include errors in both x and y components.
    
    Method: JAX
    Optional weights will be included in the fit by the convention that they are applied to chi^2.
    '''    
    def __init__(self, model, use_dx=2, **kwargs):
        fit_routine.__init__(self, model, **kwargs)
        self.model.init_jax()
        self._use_dx = use_dx
        self.description = 'JAX'
        self.set_method(**kwargs)

        if use_dx == 0:
            # precompute x-derivative of the model function
            dr = jax_grad(self.model.run, argnums=1) # we assume that the first array corresponds 
            # to the parameters, while the second array to the turns.
            #self._dr_jit = vmap(jit(dr), in_axes=(0, None)) # seems to not work atm with gradient
            self._dr_jit = jit(dr)
            def fit(**kwargs):
                self.fit_results = scipy_minimize(self.residual_xy, self.model.jax_parameters, method=self.method)
        elif use_dx == 1:
            def chi2(parameters, xdata, ydata, xdata_err, ydata_err, weights=[]):
                fxa = self.model.run(parameters, xdata)
                dfdx = self.model.dxrun(parameters, xdata)
                return sum((fxa - ydata)**2/((xdata_err*dfdx)**2 + ydata_err**2)*weights)
        else:
            def chi2(parameters, xdata, ydata, xdata_err, ydata_err, weights=[]):
                fxa = self.model.run(parameters, xdata)
                dfdx = self.model.dxrun_efficient(fxa, parameters, xdata)
                return sum((fxa - ydata)**2/((xdata_err*dfdx)**2 + ydata_err**2)*weights)


        if use_dx != 0:
            self._chi2 = jit(chi2)
            # precompute a-derivative and a-Hessian of chi2
            self._dchi2 = jit(jax_grad(chi2, argnums=0)) # derivative wrt. a (argnum=0)
            self._hchi2 = jit(jax_hessian(chi2, argnums=0))

        
    def set_residual(self, xdata, ydata, weights, xdata_err=[], ydata_err=[]):
        # attention: no check if both errors are zero!        
        if len(xdata_err) == 0:
            xdata_err = jax_numpy.zeros(len(ydata))
        if len(ydata_err) == 0:
            ydata_err = jax_numpy.zeros(len(ydata))

        m_n = len(xdata) - len(self.model.fit_parameter_keys)
            
        if self._use_dx == 0:
            def non_linear_chi2(parameters):
                # compute derivative of f at given parameters
                #dfdx = self._dr_jit(parameters, xdata)
                dfdx = jax_numpy.array([self._dr_jit(parameters, xe) for xe in xdata])
                # convert function evaluation to numpy array as well:
                fxa = self.model.run(parameters, xdata)
                return jax_numpy.sum((fxa - ydata)**2/((xdata_err*dfdx)**2 + ydata_err**2)*weights)/m_n
        elif self._use_dx == -1: # for testing purposes to apply jax to scipy.optimize (custom version) itself
            def non_linear_chi2(parameters):
                return self._chi2(parameters, xdata, ydata, xdata_err, ydata_err, weights=weights)/m_n

            self.residual_xy_gradient = lambda parameters: self._dchi2(parameters, xdata, ydata, xdata_err, ydata_err, weights=weights)/m_n
            self.residual_xy_hessian = lambda parameters: self._hchi2(parameters, xdata, ydata, xdata_err, ydata_err, weights=weights)/m_n
        else:
            def non_linear_chi2(parameters):
                return np.array(self._chi2(parameters, xdata, ydata, xdata_err, ydata_err, weights=weights))/m_n

            self.residual_xy_gradient = lambda parameters: np.array(self._dchi2(parameters, xdata, ydata, xdata_err, ydata_err, weights=weights))/m_n
            self.residual_xy_hessian = lambda parameters: np.array(self._hchi2(parameters, xdata, ydata, xdata_err, ydata_err, weights=weights))/m_n

        self.residual_xy = non_linear_chi2

    def make_fit_output(self, fit_results):
        ''' 
        create colum names and data to be stored to the SQL database of this fit routine
        output optval used to determine better start values for succeeding fits.
        '''
        
        p_opt = list(fit_results.x)
        optval = float(fit_results.fun)
        p_opt_err = [0]*len(p_opt)
        result = p_opt + p_opt_err + [optval]
        return result, p_opt, optval


