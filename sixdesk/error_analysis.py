import numpy as np
import pandas as pd

from jax.config import config
config.update("jax_enable_x64", True)
import jax
import mpmath
import time as timelib

from itertools import product
import matplotlib.pyplot as plt
from matplotlib import cm

import sqlite3

from .notebook_tools import six_handler, is_pos_def
from .fit_routines import fit_instance, prepare_fit_data

def get_cov_dict(n_parameters):
    # determine shape of confidence data to be loaded (same code as in error_analysis.error_jax)
    labels = np.array([['cov_{}_{}'.format(i + 1, j + 1) for j in range(n_parameters)] for i in range(n_parameters)])
    indices = np.triu_indices(n_parameters)
    dataframe_column_labels = list(labels[indices])

    # because we are going to fill the matrices symmetrically, we also need the indices of the strict lower triangular part
    indices_l = np.tril_indices(n_parameters, -1)
    indices_l_transposed = (indices_l[1], indices_l[0])

    out = {}
    out['n_parameters'] = n_parameters
    out['labels'] = labels
    out['indices'] = indices
    out['dataframe_column_labels'] = dataframe_column_labels
    out['indices_l'] = indices_l
    out['indices_l_transposed'] = indices_l_transposed
    return out


class error_instance(fit_instance):
    '''
    Class containing operational functions related to the
    error propagation data in an individual SQL file.
    '''
    def __init__(self, filename, da_model):
        fit_instance.__init__(self, filename=filename)
        self.model = da_model
        self._fit_parameters_cov_table_name = self._fit_parameters_table_name + '_cov'

    def get_fit_parameter_errors(self, verbose=True):
        '''
        Obtain the covariance matrices of the SQL database.
        '''
        cov_dict = get_cov_dict(len(self.model.fit_parameter_keys))
        cov = []

        try:
            if verbose:
                print ('Reading table\n "{}"'.format(self._fit_parameters_cov_table_name))
            cov = self._get_data(table_names=self._fit_parameters_cov_table_name, column_names=['seed', 'tunex', 'tuney', 'emit_index'] + cov_dict['dataframe_column_labels'])
        except:
            print ("ERROR: Error reading table '{}' of file {}".format(self._fit_parameters_cov_table_name, self.filename))

        return cov

    def construct_covariance_matrix(self, cov, seed, tunex, tuney, emit_index):
        '''
        Construct the covariance matrix from a given Pandas dataframe for a specific case.
        '''
        n_parameters = len(self.model.fit_parameter_keys)
        cov_dict = get_cov_dict(n_parameters)

        cov_labels = cov_dict['labels']
        cov_indices = cov_dict['indices']
        cov_indices_l = cov_dict['indices_l']
        cov_indices_l_transposed = cov_dict['indices_l_transposed']
     
        cov_matrix = np.zeros([n_parameters, n_parameters])
        cov_matrix[cov_indices] = cov[cov_labels[cov_indices]].loc[(cov['seed'] == seed) & 
                                                                   (cov['tunex'] == tunex) &
                                                                   (cov['tuney'] == tuney) &
                                                                   (cov['emit_index'] == emit_index)].values
        cov_matrix[cov_indices_l] = cov[cov_labels[cov_indices_l_transposed]].loc[(cov['seed'] == seed) & 
                                                                                  (cov['tunex'] == tunex) &
                                                                                  (cov['tuney'] == tuney) &
                                                                                  (cov['emit_index'] == emit_index)].values

        return cov_matrix

    def show_confidence_levels(self, seed, tunex, tuney, emit_index, sigma_levels=[1, 2], embedding=[], ax=None,
                               figsize=(6.5, 5.0), dps=32, resolution=50, 
                               show_description=True, show_xylabels=True, verbose=False):
        '''
        Shows the confidence region in the fit parameters. Note that we compute only the linear part and therefore large sigma
        levels might not be accurate. This method computes the sigma level contours using mpmath with respect to the given embedding
        of the 2-D plot space into the N-D parameter space.

        INPUT
        =====
        seed: seed number (int)
        sigma_levels: a list (float) of levels to show confidence levels. Default is [1, 2, 3].
        embedding: a numpy array of shape (dim, 2) which defines the embedding with respect to which we consider the data.
                   This is meant to be used in case of dim > 2. The default will consider a projection onto the first two parameters.
        figsize: figure size of plot
        xlim, ylim: optional plot ranges
        
        OUTPUT
        ======
        ax: The axes of the current plot
        data: The data of the current plot in form of a dictionary. The slope is hereby given as y1/x1 if (x1, y1) are the coordinates of the
              major axes with respect to the current embedding. If the data is highly correlated, then this can serve as an 
              approximation of the slope of the underlying curve.
        '''

        mpmath.mp.dps = dps

        dim = len(self.model.fit_parameter_keys)
        if len(embedding) == 0:
            # construct default projection operator (required to project N-dim data).
            # Note that any change of this default projection operator should come along with a respective change
            # in the default labeling of the x and y axis (see code below). 
            embedding = np.zeros([dim, 2])
            embedding[0, 0] = 1
            embedding[1, 1] = 1
            make_xylabels = False  # in order to display the labels on the plot according to the subspace we are considering (see below)
        else:
            make_xylabels = True

        cov_df = self.get_fit_parameter_errors(verbose=verbose)
        cov = self.construct_covariance_matrix(cov_df, seed=seed, tunex=tunex, tuney=tuney, emit_index=emit_index)

        sigma_levels = sorted(sigma_levels)
        contour_dict = get_contours(cov=cov, sigma_levels=sigma_levels, embedding=embedding, resolution=resolution)

        fit_parameters = self.get_fit_parameters()
        parameters_case = fit_parameters.loc[(fit_parameters['seed'] == seed) &
                                             (fit_parameters['tunex'] == tunex) &
                                             (fit_parameters['tuney'] == tuney) &
                                             (fit_parameters['emit_index'] == emit_index)]
        mu = np.array([parameters_case[par_label].values for par_label in self.model.fit_parameter_keys])
        mu_tr = mu.transpose()

        mu_prox = get_proximum(embedding, mu)

        if verbose:
            print ('-- Verbose mode --')
            print ('mpmath precision: {}'.format(dps))
            print ('Parameter order:\n{}'.format(self.model.fit_parameter_keys))
            print ('Optimum:\n {}'.format(mu))
            print ('Covariance matrix:\n{}'.format(cov))
            print ('Diagonal (rms):\n{}'.format(np.sqrt(cov.diagonal())))
            print ('Embedding:\n{}'.format(embedding))

        # now we can initiate the plot
        if ax == None:
            fig, ax = plt.subplots(figsize=figsize)

        data_out = {}
        for ls in sigma_levels:
            xvals = [contour_dict[ls][k][0] + mu_prox[0] for k in range(contour_dict['resolution'])]
            yvals = [contour_dict[ls][k][1] + mu_prox[1] for k in range(contour_dict['resolution'])]
            ax.plot(xvals, yvals, label=r'{:.2f} $\sigma$'.format(ls))
            
            data_out['x'] = mu_prox[0][0]
            data_out['y'] = mu_prox[1][0]
            
            data_out['major_axis_x'] = float(contour_dict['major_axis'][0])
            data_out['major_axis_y'] = float(contour_dict['major_axis'][1])

            data_out['minor_axis_x'] = float(contour_dict['minor_axis'][0])
            data_out['minor_axis_y'] = float(contour_dict['minor_axis'][1])
            
            data_out['slope'] = float(contour_dict['major_axis'][1]/contour_dict['major_axis'][0])

        # show the x and y labels according to the given embedding
        labels_plain = self.model.fit_parameter_keys_label
        if make_xylabels:
            coordinate_pullback = get_proximum(embedding, np.eye(dim)) # pullback of the coordinate system to the 2D embedding space
            xlabel = ''
            for k in range(dim - 1):
                xlabel += '{:.2f} {} + '.format(coordinate_pullback[0, k], labels_plain[k])
            xlabel += '{:.2f} {}'.format(coordinate_pullback[0, dim - 1], labels_plain[dim - 1])

            ylabel = ''
            for k in range(dim - 1):
                ylabel += '{:.2f} {} + '.format(coordinate_pullback[1, k], labels_plain[k])
            ylabel += '{:.2f} {}'.format(coordinate_pullback[1, dim - 1], labels_plain[dim - 1])
        else: # default axis labeling
            xlabel = labels_plain[0]
            ylabel = labels_plain[1]

        if show_xylabels:
            ax.axes.set_xlabel('{}'.format(xlabel))
            ax.axes.set_ylabel('{}'.format(ylabel))

        # construct the title string
        opt_str = 'optimum: '
        for k in range(dim - 1):
            opt_str += '{} = {:.3f}, '.format(labels_plain[k], mu[k][0])
        opt_str += '{} = {:.3f}'.format(labels_plain[-1], mu[-1][0])

        if show_description:
            ax.axes.set_title('{}, seed: {}\n {}\n'.format(self.model.description, seed, opt_str), loc='right')
            ax.legend()

        return ax, data_out

    def show_confidence_image(self, seed, tunex, tuney, emit_index, sigma_levels=[1, 2], embedding=[],
                              figsize=(8.0, 5.0), gridx=64, gridy=64, verbose=False):
        '''
        Shows the confidence region in the fit parameters. Note that we compute only the linear part and therefore large sigma
        levels might not be accurate. This method attempts to plot an image of the situation according to a given grid resolution.
        Note that for highly correlated data the internal precision and the grid resolution might be too rough.

        INPUT
        =====
        seed: seed number (int)
        sigma_levels: a list (float) of levels to show confidence levels. Default is [1, 2, 3].
        embedding: a numpy array of shape (dim, 2) which defines the embedding with respect to which we consider the data.
                   This is meant to be used in case of dim > 2. The default will consider a projection onto the first two parameters.
        figsize: figure size of plot
        gridx, gridy: grid resolution of plot
        '''

        dim = len(self.model.fit_parameter_keys)
        if len(embedding) == 0:
            # construct default projection operator (required to project N-dim data).
            # Note that any change of this default projection operator should come along with a respective change
            # in the default labeling of the x and y axis (see code below). 
            embedding = np.zeros([dim, 2])
            embedding[0, 0] = 1
            embedding[1, 1] = 1
            make_xylabels = False  # in order to display the labels on the plot according to the subspace we are considering (see below)
        else:
            make_xylabels = True

        cov_df = self.get_fit_parameter_errors(verbose=verbose)
        cov = self.construct_covariance_matrix(cov_df, seed=seed, tunex=tunex, tuney=tuney, emit_index=emit_index)

        evalues, evectors = np.linalg.eigh(cov)

        if any(evalues <= 0):
            print ('WARNING: np.linalg.eigh returned at least one negative eigenvalue:')
            print ('{}'.format(evalues))
            print ('Checking for positive definiteness of covariance matrix:')
            pdcheck = is_pos_def(cov)
            print ('{}'.format(pdcheck))
            if pdcheck:
                print ('Attempt to use np.linalg.eig ...')
                evalues, evectors = np.linalg.eig(cov)
            if any(evalues <= 0):
                print ('Unsuccessful. Check matrix generation procedure. Aborting.')
                return
            else:
                print ('Success. Eigenvalues:')
                print ('{}'.format(evalues))

        sigma = np.sqrt(np.array(evalues))

        fit_parameters = self.get_fit_parameters()
        parameters_case = fit_parameters.loc[(fit_parameters['seed'] == seed) &
                                             (fit_parameters['tunex'] == tunex) &
                                             (fit_parameters['tuney'] == tuney) &
                                             (fit_parameters['emit_index'] == emit_index)]
        mu_tr = np.array([parameters_case[par_label].values for par_label in self.model.fit_parameter_keys])
        mu = mu_tr.transpose()

        if verbose:
            print ('-- Verbose mode --')
            print ('Parameter order:\n{}'.format(self.model.fit_parameter_keys))
            print ('Optimum:\n{}'.format(mu))
            print ('Covariance matrix:\n{}'.format(cov))
            print ('Diagonal (rms):\n{}'.format(np.sqrt(cov.diagonal())))
            print ('Eigenvalues:\n{}'.format(evalues))
            print ('Eigenvectors:\n{}'.format(evectors))
            print ('Embedding:\n{}'.format(embedding))

        # calculate plot region
        scale = max(sigma_levels)
        evectors_region = np.append(evectors*sigma*scale, 
                                    evectors*-sigma*scale, axis=1) + mu_tr
        # evectors_region span a certain region in the parameter space which will define the borders of the plot. This region
        # is N-D, so we have to consider its extensions on the 2D-subspace of interest.
        evectors2 = get_proximum(embedding, evectors_region) # project all those eigenvectors to the 2D plane of interest

        # in the 2D space (which is mapped to the N-D space, we now have the range of parameters to be considered ...
        xlims = np.array([min(evectors2[0]), max(evectors2[0])])
        ylims = np.array([min(evectors2[1]), max(evectors2[1])])
        # ... and according to these ranges and the given grid size we set the grid.
        x2 = np.linspace(xlims[0], xlims[1], gridx)
        y2 = np.linspace(ylims[0], ylims[1], gridy)
        # Now we want to map these vectors to the N-D space, which will then be inserted into the density function.
        x_origin = np.zeros([2, gridx])
        x_origin[0, :] = x2
        y_origin = np.zeros([2, gridy])
        y_origin[1, :] = y2
        x = np.matmul(embedding, x_origin)
        y = np.matmul(embedding, y_origin)
        # the vectors x and y define the 2D grid we must insert into the density function.

        index_pairs = [(pair[0], pair[1]) for pair in product(range(gridx), range(gridy))] # to reconstruct 2D array afterwards
        vectors = np.zeros([dim, len(index_pairs)])

        for k in range(len(index_pairs)):
            i, j = index_pairs[k]
            vectors[:,k] = (x[:,i] + y[:,j]).transpose()

        values = gauss_mod(vectors, mu=mu, sigma=sigma, Q=evectors)

        # now we can initiate the plot
        plt.figure(figsize=figsize)

        # construct 2D meshgrids for plotting
        X = np.zeros([gridx, gridy])
        Y = np.zeros([gridx, gridy])
        Z = np.zeros([gridx, gridy])
        for k in range(len(index_pairs)):
            i, j = index_pairs[k]
            Z[i, j] = values[k]
            X[i, j] = x2[i]
            Y[i, j] = y2[j]

        # we compute the contour levels. For this purpose we pass the eigenvalues (multiplied with
        # the respective sigma's times the contour scale) to the density.
        n_contours = len(sigma_levels)
        contour_inp = evectors*sigma*sigma_levels[0] + mu_tr
        for sl in sigma_levels[1:]:
            contour_inp = np.append(contour_inp, evectors*sigma*sl + mu_tr, axis=1)
        contour_levels_ev = gauss_mod(contour_inp, mu=mu, sigma=sigma, Q=evectors)
        contour_levels = sorted([contour_levels_ev[2*k] for k in range(n_contours)])
        cmap_contours = cm.get_cmap('magma', n_contours*2)
        contours = plt.contour(X, Y, Z, contour_levels, cmap=cmap_contours) #colors='black')
        plt.clabel(contours, inline=True, fontsize=10)

        # fill the graph with some colors to show density better
        image_lims = [xlims[0], xlims[1], 
                      ylims[0], ylims[1]]
        cmap_image = cm.get_cmap('PuBu', 32)
        plt.imshow(Z, extent=image_lims, origin='lower',
                   cmap=cmap_image, aspect='auto')

        # plot color bar on side of plot
        cent_val = max(values)
        color_ticks = np.linspace(0, cent_val, 6, endpoint=True)*0.999 # 0.999 trick to show largest tick at upper border.
        cbar = plt.colorbar(ticks=color_ticks);
        cbar.ax.set_yticklabels(['{:.2f}'.format(ct) for ct in color_ticks])
        cbar.set_label(r'$n$')#, labelpad=-10, y=1.07, rotation=0)

        # show the x and y labels according to the given embedding
        labels_plain = self.model.fit_parameter_keys_label
        if make_xylabels:
            coordinate_pullback = get_proximum(embedding, np.eye(dim)) # pullback of the coordinate system to the 2D embedding space
            xlabel = ''
            for k in range(dim - 1):
                xlabel += '{:.2f} {} + '.format(coordinate_pullback[0, k], labels_plain[k])
            xlabel += '{:.2f} {}'.format(coordinate_pullback[0, dim - 1], labels_plain[dim - 1])

            ylabel = ''
            for k in range(dim - 1):
                ylabel += '{:.2f} {} + '.format(coordinate_pullback[1, k], labels_plain[k])
            ylabel += '{:.2f} {}'.format(coordinate_pullback[1, dim - 1], labels_plain[dim - 1])
        else: # default axis labeling
            xlabel = labels_plain[0]
            ylabel = labels_plain[1]
        plt.xlabel('{}'.format(xlabel))
        plt.ylabel('{}'.format(ylabel))

        # construct the title string
        opt_str = 'optimum: '
        for k in range(dim - 1):
            opt_str += '{} = {:.3f}, '.format(labels_plain[k], mu[0][k])
        opt_str += '{} = {:.3f}'.format(labels_plain[-1], mu[0][-1])
        plt.title('{}, seed: {}\n {}\n$\\sigma$-levels: {}'.format(self.model.description, seed, 
                  opt_str, sigma_levels), loc='right')
        return plt


class error_study(six_handler):
    '''
    Master class to perform error analysis on a set of SixTrack SQL files.

    Here we will assume that the underlying errors in the x and y data are uncorrelated, i.e.
    that the matrix B (see my notes) is *diagonal* and of the form
    B = diag(1/err_x**2, 1/err_y**2).
    Further details can be found in my notes.
    '''
    def __init__(self, filenames, da_model):
        self.model = da_model
        six_handler.__init__(self, filenames=filenames, instance=error_instance, da_model=da_model)

        cov_dict = get_cov_dict(len(self.model.fit_parameter_keys))
        self._cov_labels = cov_dict['labels']
        self._cov_indices = cov_dict['indices']
        self._cov_column_labels = cov_dict['dataframe_column_labels']

    # The next functions are found in my notes regarding error propagation for the general chi2 method.
    # The arguments include the errors bixx and biyy themselves rather than accessing these variables from
    # the parameter space of the class, because in the end we want to create derivatives in which these
    # errors (and the fit parameters) can vary, thereby the derivatives can be applied to all possible seeds fast.

    def e_half(self, a, x, bixx, biyy):
        cxx = bixx*self.model.dxrun(a, x)**2
        return cxx + biyy # n.b. cyy = biyy and cxy = 0 = cyx.
        
    def omega(self, a, x, bixx, biyy, weights):
        return weights/self.e_half(a, x, bixx, biyy)
        
    def da_omega(self, a, x, bixx, biyy, weights):     
        return {parameter: -2*bixx*(self.model.dxrun(a, x)*self.omega(a, x, bixx, biyy, weights))**2*self.model.dadxrun(a, x)[parameter]
                for parameter in self.model.fit_parameter_keys}
        
    def F(self, a, x):
        return [self.model.darun(a, x)[parameter] for parameter in self.model.fit_parameter_keys]


class error_numpy(error_study):

    def __init__(self, filenames, da_model):
        error_study.__init__(self, filenames=filenames, da_model=da_model)
        self.model.init_numpy()

    def prepare(self, xerr, yerr):
        '''
        Generates the error-input for the tools, depending on the chosen method.
        '''
        bixx = np.array(xerr)**2
        biyy = np.array(yerr)**2
        return bixx, biyy 

    def F(self, a, x):
        return np.array(error_study.F(self, a, x)).transpose()

    def delta_a(self, a, x, y, bixx, biyy, weights):
        fmat = self.F(a, x)
        fo = fmat.transpose()*self.omega(a, x, bixx, biyy, weights) # numpy multiplication corresponds to multiplication of diagonal matrix
        delta_y = self.model.run(a, x) - y
        daomega = self.da_omega(a, x, bixx, biyy, weights)
            
        fof = np.matmul(fo, fmat)
        second_summand_L = np.zeros(fof.shape) # belonging to L in my notes:
        second_summand_v = np.zeros(len(self.model.fit_parameter_keys)) # belonging to the vector in my notes (call it v)
            
        ss_index = 0
        for parameter in self.model.fit_parameter_keys:
            delta_y_daOmega = delta_y*daomega[parameter]
            second_summand_L_row = np.matmul(delta_y_daOmega, fmat) # this is a row vector, which will be multiplied from the left by a standard basis vector ek in the formula for L. The result is a matrix, but due to the fact that the basis vector has zeros besides of entry k, the expression is just the k-th row of the second summand for L.
            second_summand_v_row = np.matmul(delta_y_daOmega, delta_y) # actually, it is just one entry since v is a vector
            second_summand_L[ss_index] = second_summand_L_row
            second_summand_v[ss_index] = second_summand_v_row
            ss_index += 1
                
        L = fof + second_summand_L
        v = np.matmul(fo, delta_y) + second_summand_v/2

        return np.linalg.solve(L, v)

    def v(self, a, x, y, bixx, biyy, weights):
        fmat = self.F(a, x)
        fo = fmat.transpose()*self.omega(a, x, bixx, biyy, weights)
        delta_y = self.model.run(a, x) - y
        daomega = self.da_omega(a, x, bixx, biyy, weights)
            
        second_summand_v = np.zeros(len(self.model.fit_parameter_keys))
        ss_index = 0
        for parameter in self.model.fit_parameter_keys:
            delta_y_daOmega = delta_y*daomega[parameter]
            second_summand_v_row = np.matmul(delta_y_daOmega, delta_y)
            second_summand_v[ss_index] = second_summand_v_row
            ss_index += 1

        v = jax.numpy.matmul(fo, delta_y) + second_summand_v/2
        return v

    def L(self, a, x, y, bixx, biyy, weights):
        fmat = self.F(a, x)
        fo = fmat.transpose()*self.omega(a, x, bixx, biyy, weights) # numpy multiplication corresponds to multiplication of diagonal matrix
        delta_y = self.model.run(a, x) - y
        daomega = self.da_omega(a, x, bixx, biyy, weights)
            
        fof = np.matmul(fo, fmat)
            
        second_summand_L = np.zeros(fof.shape) # belonging to L in my notes:
        ss_index = 0
        for parameter in self.model.fit_parameter_keys:
            delta_y_daOmega = delta_y*daomega[parameter]
            second_summand_L_row = np.matmul(delta_y_daOmega, fmat)
            second_summand_L[ss_index] = second_summand_L_row
            ss_index += 1
                
        L = fof + second_summand_L
        return L


class error_mpmath(error_study):

    def __init__(self, filenames, da_model, dps=16):
        error_study.__init__(self, filenames=filenames, da_model=da_model)
        self.model.init_mpmath(dps=dps)

    def prepare(self, xerr, yerr):
        '''
        Generates the error-input for the tools, depending on the chosen method.
        '''
        bixx = np.array(xerr)**2
        biyy = np.array(yerr)**2
        return bixx, biyy 

    def F(self, a, x):
        return np.array(error_study.F(self, a, x)).transpose()

    def v(self, a, x, y, bixx, biyy, weights):
        fmat = self.F(a, x)
        fo = fmat.transpose()*self.omega(a, x, bixx, biyy, weights) 
        delta_y = self.model.run(a, x) - y
        daomega = self.da_omega(a, x, bixx, biyy, weights)

        delta_y_mat = mpmath.matrix(delta_y)
            
        second_summand_v = np.zeros(len(self.model.fit_parameter_keys))
        ss_index = 0
        for parameter in self.model.fit_parameter_keys:
            delta_y_daOmega = delta_y*daomega[parameter]
            second_summand_v_row = mpmath.matrix(delta_y_daOmega).transpose()*delta_y_mat
            second_summand_v[ss_index] = second_summand_v_row[0]
            ss_index += 1
                
        L = mpmath.matrix(fo)*delta_y_mat + mpmath.matrix(second_summand_v)/2
        return L
        
    def L(self, a, x, y, bixx, biyy, weights):
        fmat = self.F(a, x)
        fo = fmat.transpose()*self.omega(a, x, bixx, biyy, weights) # numpy multiplication corresponds to multiplication of diagonal matrix
        delta_y = self.model.run(a, x) - y
        daomega = self.da_omega(a, x, bixx, biyy, weights)
            
        fmat_mp = mpmath.matrix(fmat)
        fof = mpmath.matrix(fo)*fmat_mp
            
        second_summand_L = np.zeros((fof.rows, fof.cols)) # belonging to L in my notes:
        ss_index = 0
        for parameter in self.model.fit_parameter_keys:
            delta_y_daOmega = delta_y*daomega[parameter]
            second_summand_L_row = mpmath.matrix(delta_y_daOmega).transpose()*fmat_mp
            second_summand_L[ss_index] = second_summand_L_row
            ss_index += 1
                
        L = fof + mpmath.matrix(second_summand_L)
        return L

    def delta_a(self, a, x, y, bixx, biyy):
        # Not speed optimized, but the purpose of mpmath here is primarily for analyzing poor conditioned matrices.
        L = self._mpmath_L(a, x, y, bixx, biyy)
        v = self._mpmath_v(a, x, y, bixx, biyy)
        return mpmath.inverse(L)*v


class error_jax(error_study):

    def __init__(self, filenames, da_model):
        error_study.__init__(self, filenames=filenames, da_model=da_model)
        self.model.init_jax()
        self.jac_delta_a = jax.jit(jax.jacobian(self._to_jac_delta_a, argnums=1))
        self.jac_v = jax.jit(jax.jacobian(self._to_jac_v, argnums=1))

    def prepare(self, xerr, yerr):
        '''
        Generates the error-input for the tools, depending on the chosen method.
        '''
        bixx = jax.numpy.array(xerr)**2
        biyy = jax.numpy.array(yerr)**2
        return bixx, biyy 

    def F(self, a, x):
        return jax.numpy.array(error_study.F(self, a, x)).transpose()
        
    def delta_a(self, a, x, y, bixx, biyy, weights):
        fmat = self.F(a, x)
            
        fo = fmat.transpose()*self.omega(a, x, bixx, biyy, weights) # numpy multiplication corresponds to multiplication of diagonal matrix
        delta_y = self.model.run(a, x) - y
        daomega = self.da_omega(a, x, bixx, biyy, weights)
            
        fof = jax.numpy.matmul(fo, fmat)
        second_summand_L = jax.numpy.zeros(fof.shape) # belonging to L in my notes:
        second_summand_v = jax.numpy.zeros(len(self.model.fit_parameter_keys)) # belonging to the vector in my notes (call it v)
            
        ss_index = 0
        for parameter in self.model.fit_parameter_keys:
            delta_y_daOmega = delta_y*daomega[parameter]
            second_summand_L_row = jax.numpy.matmul(delta_y_daOmega, fmat)
            second_summand_v_row = jax.numpy.matmul(delta_y_daOmega, delta_y)
            second_summand_L = jax.ops.index_update(second_summand_L, ss_index, second_summand_L_row)
            second_summand_v = jax.ops.index_update(second_summand_v, ss_index, second_summand_v_row)
            ss_index += 1
                
        L = fof + second_summand_L
        v = jax.numpy.matmul(fo, delta_y) + second_summand_v/2

        return jax.numpy.linalg.solve(L, v)
        
    def v(self, a, x, y, bixx, biyy, weights):
        fmat = self.F(a, x)
        fo = fmat.transpose()*self.omega(a, x, bixx, biyy, weights) # numpy multiplication corresponds to multiplication of diagonal matrix
        delta_y = self.model.run(a, x) - y
        daomega = self.da_omega(a, x, bixx, biyy, weights)
            
        second_summand_v = jax.numpy.zeros(len(self.model.fit_parameter_keys)) # belonging to the vector in my notes (call it v)
        ss_index = 0
        for parameter in self.model.fit_parameter_keys:
            delta_y_daOmega = delta_y*daomega[parameter]
            second_summand_v_row = jax.numpy.matmul(delta_y_daOmega, delta_y) # actually, it is just one entry since v is a vector
            second_summand_v = jax.ops.index_update(second_summand_v, ss_index, second_summand_v_row)
            ss_index += 1

        v = jax.numpy.matmul(fo, delta_y) + second_summand_v/2
        return v
        
    def L(self, a, x, y, bixx, biyy, weights):
        fmat = self.F(a, x)
        fo = fmat.transpose()*self.omega(a, x, bixx, biyy, weights)
        delta_y = self.model.run(a, x) - y
        daomega = self.da_omega(a, x, bixx, biyy, weights)
            
        fof = jax.numpy.matmul(fo, fmat)
            
        second_summand_L = jax.numpy.zeros(fof.shape)
        ss_index = 0
        for parameter in self.model.fit_parameter_keys:
            delta_y_daOmega = delta_y*daomega[parameter]
            second_summand_L_row = jax.numpy.matmul(delta_y_daOmega, fmat)
            second_summand_L = jax.ops.index_update(second_summand_L, ss_index, second_summand_L_row)
            ss_index += 1
                
        L = fof + second_summand_L
        return L

    # The next two functions construct the jacobians which can be used on the covariance-matrices of the 
    # x- and y- data in order to obtain the respective covariance matrices of the model parameters 
    # (at the location of the optimum). See my notes for details or the functions below.

    def _to_jac_delta_a(self, parameters, xyinput, bixx, biyy, weights):
        m = len(bixx)
        return self.delta_a(parameters, xyinput[:m], xyinput[m:], bixx, biyy, weights)

    def _to_jac_v(self, parameters, xyinput, bixx, biyy, weights):
        m = len(bixx)
        return self.v(parameters, xyinput[:m], xyinput[m:], bixx, biyy, weights)

    ### Tools to obtain the correlation matrix (two options) ###

    # METHOD 1
    def cov_delta_a(self, parameters, xdata, ydata, xdata_errors, ydata_errors, weights=[]):
        '''
        Obtain the parameter covariance matrix based on the derivative of L^{-1}M (see my notes).
        '''
        if len(weights) == 0:
            weights = jax.numpy.ones(len(xdata))
        weights = jax.numpy.array(weights)

        if len(xdata_errors) == 0:
            xdata_errors = jax.numpy.zeros(len(xdata))
        if len(ydata_errors) == 0:
            ydata_errors = jax.numpy.zeros(len(ydata))

        bixx, biyy = self.prepare(xdata_errors, ydata_errors)
        xyinput = jax.numpy.array(np.hstack([xdata, ydata]))
        da = self.jac_delta_a(parameters, xyinput, bixx, biyy, weights)
        sigma = jax.numpy.diag(np.hstack([bixx, biyy]))
        s_dat = jax.numpy.matmul(sigma, da.transpose())
        return jax.numpy.matmul(da, s_dat)
        
    # METHOD 2 
    # N.b. this method does not involve the inverse of the problematic matrix L during the AD, only in the final step,
    # where it may be analyzed and processed further.
    def cov_v(self, parameters, xdata, ydata, xdata_errors, ydata_errors, weights=[],
              method_L='jax', Li=None, check=False):
        '''
        Obtain the parameter covariance matrix based on the derivative of M alone (see my notes).

        method_L: one of
          numpy
          mpmath
          jax (default)

        The method of how to use the matrix L. If something else than 'jax' is given,
        then the inverse of L must be provided by the respective external routine.
        '''
        if len(weights) == 0:
            weights = jax.numpy.ones(len(xdata))
        weights = jax.numpy.array(weights)

        if len(xdata_errors) == 0:
            xdata_errors = jax.numpy.zeros(len(xdata))
        if len(ydata_errors) == 0:
            ydata_errors = jax.numpy.zeros(len(ydata))

        bixx, biyy = self.prepare(xdata_errors, ydata_errors)
        xyinput = jax.numpy.array(np.hstack([xdata, ydata]))
        dv = self.jac_v(parameters, xyinput, bixx, biyy, weights)

        if method_L == 'numpy':
            #L = self._numpy_L(parameters, xdata, ydata, bixx, biyy)
            #Li = np.linalg.inv(L)
            sigma = np.diag(np.hstack([bixx, biyy])) # converts bixx and biyy back to numpy
            dv_np = np.array(dv, dtype=np.float64)

            f1 = np.matmul(Li, dv_np)
            f2 = np.matmul(f1, sigma)
            f3 = np.matmul(f2, dv_np.transpose())
            cov = np.matmul(f3, Li.transpose())

        if method_L == 'mpmath':
            #L = self._mpmath_L(parameters, xdata, ydata, bixx, biyy)
            #Li = mpmath.inverse(L)
            sigma = mpmath.diag(np.hstack([bixx, biyy]))
            dv_mpmath = mpmath.matrix(np.array(dv, dtype=np.float64))
            cov = Li*dv_mpmath*sigma*dv_mpmath.transpose()*Li.transpose()

        if method_L == 'jax':
            L = self.L(parameters, xdata, ydata, bixx, biyy, weights)
            Li = jax.numpy.linalg.inv(L)
            sigma = jax.numpy.diag(np.hstack([bixx, biyy]))
            
            f1 = np.matmul(Li, dv)
            f2 = np.matmul(f1, sigma)
            f3 = np.matmul(f2, dv.transpose())
            cov = np.matmul(f3, Li.transpose())

        if check:
            v_opt = self.v(parameters, xdata, ydata, bixx, biyy, weights)
            print ('v(parameters): {}\n'.format(v_opt)) # this should be very close to zero (zero at the optimum). Test this if entering parameters a bit away from the optimum or with different fitting routines.

            pos_def = is_pos_def(cov)
            print ('cov pos. definite: {}'.format(pos_def))

        return cov


    def run(self, method='delta_a', verbose=True, save=True, weight_function=None, fit_start_index=1, fit_stop_index=-1, 
            errors='xy', **kwargs):
        '''
        Perform an error analysis by computing the covariance matrix at the optimum value.

        start_index, stop_index: The indices by which the fitting has been done. Enter the same values here, otherwise one might
                                 produce NaNs at the borders of the parameter ranges.

        errors: The type of errors to be used. Either: 'x', 'y' or 'xy'.
        '''

        if method == 'delta_a':
            cov_method = self.cov_delta_a
        if method == 'v':
            cov_method = self.cov_v

        if verbose:
            print ('Performing error analysis')
            print ('Analysis method: {}'.format(method))
            print ('          Model: {}'.format(self.model.description))

        for obj in self.objects:
            data = obj._get_data([obj._fit_parameters_table_name, 'da_emit'], 
                        ['seed, tunex, tuney, emit_index,' + ', '.join(self.model.fit_parameter_keys), 
                         'seed, tunex, tuney, emit_index, turn, da, turn_error, da_error'])
            fit_parameters = data[0]
            emittance_indices = data[0]['emit_index'].unique()
            xy_seeds = data[1]

            n_seeds = len(obj.seeds)
            n_emittances = len(emittance_indices)

            start_time = timelib.time()
            dataframe_columns = []
            for seed, tune_pair, emit_index in product(obj.seeds, obj.tunes, emittance_indices): # n.b. seed == k + 1

                tunex, tuney = tune_pair

                if verbose: # show the calculation progress
                    print('\r  Analysis of seed {}/{}, tune ({}, {}), emit_index: {}/{} ...'.format(
                               seed, n_seeds, tunex, tuney, emit_index, n_emittances), end='')

                parameters_case = fit_parameters.loc[(fit_parameters['seed'] == seed) &
                                                     (fit_parameters['tunex'] == tunex) &
                                                     (fit_parameters['tuney'] == tuney) &
                                                     (fit_parameters['emit_index'] == emit_index)]

                parameters = jax.numpy.array([parameters_case[par_label].values for par_label in self.model.fit_parameter_keys])

                xy = xy_seeds.loc[(xy_seeds['seed'] == seed) &
                                  (xy_seeds['tunex'] == tunex) &
                                  (xy_seeds['tuney'] == tuney) &
                                  (xy_seeds['emit_index'] == emit_index)]

                weights, turn, da, turn_error, da_error = prepare_fit_data(xy, weight_function=weight_function, errors=errors,
                                                                         start_index=fit_start_index, stop_index=fit_stop_index)

                cov_column = np.array(cov_method(parameters, turn, da, turn_error, da_error, 
                                                 weights=weights, **kwargs))[self._cov_indices]
                dataframe_columns.append([seed, tunex, tuney, emit_index] + list(cov_column))
            results = pd.DataFrame(dataframe_columns, columns=['seed', 'tunex', 'tuney', 'emit_index'] + self._cov_column_labels)

            end_time = timelib.time()
            if verbose:
                print('\nTime elapsed: {:.3f} [s]'.format(end_time - start_time))

            if save:
                obj._store_data(table_name=obj._fit_parameters_cov_table_name, data=results, verbose=verbose)



def ndgauss(x, mu, sigma):
    '''
    Returns the value of the n-dimensional Gaussian function
    with mu's as center and sigma's as rms size.
    '''
    vec = np.exp(-(x - mu)**2/(2*sigma**2))/np.sqrt(2*np.pi*sigma**2)
    
    # works for x.shape in (dim, k)
    result = vec[0]
    for k in range(len(vec) - 1):
        result *= vec[k + 1]
    return result


def gauss_mod(x, sigma, mu=[], Q=[]):
    
    '''
    Modifies n-d gauss using linear map Q (convenience class)
    '''
    sigma = np.array([sigma]).transpose() # required to recognize x structure during multiplication
    
    dim = x.shape[0]
    n_vec = x.shape[1]
    
    if len(mu) == 0:
        mu_inp = np.zeros([dim, n_vec])
    else:
        mu_inp = np.zeros([dim, n_vec])
        for k in range(n_vec):
            mu_inp[:, k] = mu
    
    # modify input according to linear map Q
    if len(Q) == 0:
        Q = np.eye(dim)
    x = np.matmul(Q, x)
    mu_inp = np.matmul(Q, mu_inp)
    return ndgauss(x=x, mu=mu_inp, sigma=sigma)


def get_proximum(embedding, vectors):
    '''
    A small realization of the least squares problem.
    '''
    ata = np.matmul(embedding.transpose(), embedding)
    atx = np.matmul(embedding.transpose(), vectors)
    return np.linalg.solve(ata, atx)


def get_contours(cov, embedding, sigma_levels, resolution=64):
    '''
    Function to calculate the contour lines of a given list of sigma's.
    '''

    cov = mpmath.matrix(cov)
    if mpmath.det(cov) == 0:
        print ('ERROR: Singular covariance matrix encountered. Aborting.')
        return
    covi = mpmath.inverse(cov)  # defines the Mahalanobis distance.

    embedding = mpmath.matrix(embedding)
    n_contours = len(sigma_levels)
    dim = cov.rows # number of involved parameters
    
    # now we consider the pullback of the quadratic form defined by the covariance matrix onto the given embedding
    pullback_covi = embedding.T*covi*embedding
    
    eigenvalues, eigenvectors = mpmath.eig(pullback_covi) # e.g. pullback_covi*eigenvectors[:, 0] - eigenvalues[0]*eigenvectors[:, 0] = 0
    L = mpmath.cholesky(pullback_covi) # pullback_covi = L*L.T

    minor_axis_index = eigenvalues.index(max(eigenvalues)) # the two eigenvalues are related to 1/sigma_j**2
    major_axis_index = eigenvalues.index(min(eigenvalues)) # the eigenvectors are normed and orthogonal

    minor_axis = eigenvectors[:, minor_axis_index]/mpmath.sqrt(eigenvalues[minor_axis_index])
    major_axis = eigenvectors[:, major_axis_index]/mpmath.sqrt(eigenvalues[major_axis_index]) 
    # Background behind the definition of major_axis and minor_axis:
    # A multivariate Gaussian has the form
    # ~ exp(-x.T*M**(-1)*x/2), where M**(-1) = pullback_covi defines the Mahalanobis distance.
    # The defining equation for the confidence levels with respect to level k is therefore
    # x.T*M**(-1)*x/2/k**2 = 1/2
    # If v is an eigenvector of M**(-1) with respect to eigenvalue e, then the above equation can be satisfied by:
    # u := v/sqrt(e)*k
    # The levels k are set in the code below.

    base = mpmath.linspace(0, 2*mpmath.pi, resolution)

    op = {}
    for k in sigma_levels:
        op[k] = [mpmath.cos(phi)*major_axis*k + mpmath.sin(phi)*minor_axis*k for phi in base]
   
    op['resolution'] = resolution
    op['L'] = L
    op['eigenvalues'] = eigenvalues
    op['eigenvectors'] = eigenvectors
    op['major_axis'] = major_axis
    op['minor_axis'] = minor_axis
    
    return op


