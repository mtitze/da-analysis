import pandas as pd
import numpy as np
import sqlite3, sys

import matplotlib.pyplot as plt

'''
Collection of general notebook tools.
'''

def progress_indicator(step, k0, iter_list, progress):
    '''
    show progress of computation; initialize this function before the loop with, e.g.

    step_indicator = 0.1  # small indicator on the progress of the calculations in [%]
    progress = np.linspace(0, 100, int(100/step_indicator) + 1)
    n_progress_steps = len(progress)
    k0, step = -1, 0

    then inside the loop:
    for e in my_loop_list:
        step, k0 = progress_indicator(step, k0, iter_list=my_loop_list, progress=progress)
      
        <do something else>
    '''

    n_elements = len(iter_list)
    n_progress_steps = len(progress)
    
    current_progress = step/n_elements*100

    k1 = min([k for k in range(n_progress_steps - 1) if progress[k] <= current_progress 
         and current_progress < progress[k + 1]])
    
    if k0 < k1:
        sys.stdout.write("\r" + str(np.floor(progress[k1]*1000)/1000) + ' %')
        sys.stdout.flush()
        k0 = k1
    if step == n_elements - 1:
        sys.stdout.write("\r" + str(np.floor(progress[-1]*1000)/1000) + ' %')
        sys.stdout.flush()
    
    step += 1
    
    return step, k0

def is_pos_def(A, code='numpy', verbose=False, tol=1e-12):
    '''For a real matrix $A$, we have $x^TAx=\frac{1}{2}(x^T(A+A^T)x)$, 
    and $A+A^T$ is a symmetric real matrix. So $A$ is positive definite 
    iff $A+A^T$ is positive definite, iff all the eigenvalues of $A+A^T$ are positive.'''
    
    # note: use cholesky decomposition
    if code == 'mpmath':
        result = np.all([mpmath.eig(A + A.transpose())[0][k] > tol for k in range(len(A))])
    else: # code == 'numpy':
        check = np.linalg.eigvals(A + A.transpose())
        if verbose:
            print (check)
        result = np.all(check > tol)
    
    return result

########################################################
# minimalistic interface to sixdesk/sixtrack SQL files #
########################################################

def get_sql_data(filename, table_names, column_names='*'):
    '''Get specific data from the SixDesk database.'''
    conn = sqlite3.connect(filename)
    if isinstance(table_names, str):
        if isinstance(column_names, str):
            column_names = [column_names]
        query = "SELECT {} FROM {};".format(', '.join(column_names), table_names)
        data = pd.read_sql_query(query, conn)
    else: # assume a list of tables and colum names is given
        n_tables = len(table_names)
        if column_names == '*':
            column_names = ['*']*n_tables
        data = []
        for k in range(n_tables):
            query = "SELECT {} FROM {};".format(column_names[k], table_names[k])
            data.append(pd.read_sql_query(query, conn))
    conn.close()
    return data

class six_instance:
    def __init__(self, filename):
        self.filename = filename

    def _sql_execute(self, sql_command, cursor=None):
        # derived from SixDeskDB
        close = False
        if cursor == None:
            conn = sqlite3.connect(self.filename)
            cursor = conn.cursor()
            close = True
        cursor.execute(sql_command)
        if close:
            conn.commit()
            conn.close()
        return list(cursor)
 
    # N.B. _get_db_seeds and _get_db_tunes were taken from original SixDeskDB
    def _get_db_seeds(self, cursor):
        ''' get seeds from DB'''
        sql = 'SELECT DISTINCT seed FROM six_input ORDER BY seed'
        return list(zip(*self._sql_execute(sql, cursor=cursor)))[0]

    def _get_db_tunes(self, cursor):
        ''' get tunes from DB'''
        sql = 'SELECT DISTINCT tunex, tuney FROM six_input ORDER BY tunex, tuney'
        return self._sql_execute(sql, cursor=cursor)

    def _get_data(self, table_names, column_names='*'):
        '''Get specific data from the SixDesk database.'''
        return get_sql_data(self.filename, table_names=table_names, column_names=column_names)

    def _drop_data(self, remove, verbose=True):
        '''
        Remove table(s) from a sixdesk database
        in order to force/ensure that the values are re-computed if there is a change.
        '''
        if verbose:
            print ('Dropping {} from SQL database\n {}'.format(remove, self.filename))
    
        conn = sqlite3.connect(self.filename)
        cursor = conn.cursor()
        for table_name in remove:
            try:
                cursor.execute("DROP TABLE {}".format(table_name))
            except:
                print ('Problem with table {}'.format(table_name))
                print ("error: {}".format(sys.exc_info()[0]))
                continue
        conn.close()

    def _store_data(self, table_name, data, verbose=True, if_exists='replace', **kwargs):
        '''
        Store data to SQL file. Data is assumed to be a Pandas dataframe.
        '''
        if verbose:
            print ('storing {} to file\n {}'.format(table_name, self.filename))
        conn = sqlite3.connect(self.filename)
        data.to_sql(table_name, conn, if_exists=if_exists, index=False, **kwargs)
        conn.close()

    def _exists_in_db(self, query):
        '''
        Check if given data exists in DB.
        '''
        conn = sqlite3.connect(self.filename)
        try:
            pd.read_sql_query(query, conn)
            exists = True
        except:
            exists = False
        conn.close()
        return exists


class six_handler:
    '''
    Class to handle several sixdesk database files in a self-contained fashion. 
    '''

    def __init__(self, filenames, instance=six_instance, **kwargs):
        self.instance = instance
        self.filenames = filenames

        if 'verbose' in kwargs.keys():
            print ('Initializing SQL database objects ...')

        self.objects = []
        for filename in self.filenames:
            if 'verbose' in kwargs.keys():
                print (filename)
            self.objects.append(self.instance(filename=filename, **kwargs))










