import numpy as np

'''
References:
[1] 'Advances on the application of Nekhoroshev stability-time estimate
     for modelling the dynamic aperture time scaling' by A. Bazzani et. al.
[2] 'Some considerations on Nekhoroshev theorem' by M. Giovannozzi, March 12 2019, 
     CERN internal communication report.

Note:
Higher-order derivatives for each of the DA models are used in the non-linear and non-bilinear
chi2-routines and their respective error estimates.
'''

from lmfit import Parameters as lmfit_parameters_class

class da_model:
    '''
    Generic da model containing common tools.
    '''

    def __init__(self):
        self._epsilon = 1e-8 # generic 'small' number to set in domains to prevent division by zero
        self.default_fit_method = 'Newton-CG' # recommended scipy.optimize.minimize method

    def init_numpy(self):
        self.log = np.log
        self.exp = np.exp
        def set_parameters(parameters):
            return parameters
        self.set_parameters = set_parameters

    def init_mpmath(self, dps=16): # dps: decimal precision to be used
        import mpmath
        mpmath.mp.dps = dps
        self.log = np.frompyfunc(lambda x: mpmath.log(x), 1, 1)
        self.exp = np.frompyfunc(lambda x: mpmath.exp(x), 1, 1)
        def set_parameters(parameters):
            return parameters
        self.set_parameters = set_parameters

    def init_lmfit(self):
        self.log = np.log
        self.exp = np.exp
        self.default_fit_method = 'leastsq'
        def set_parameters(parameters):
            return self.set_lmfit_parameters(parameters=parameters)
        self.set_parameters = set_parameters

    def init_algopy(self):
        from algopy import log as algopy_log
        from algopy import exp as algopy_exp
        self.log = algopy_log
        self.exp = algopy_exp
        def set_parameters(parameters):
            return self.set_algopy_parameters(parameters=parameters)
        self.set_parameters = set_parameters

    def init_jax(self):
        from jax.config import config
        config.update("jax_enable_x64", True)
        from jax.numpy import log as jax_log
        from jax.numpy import exp as jax_exp
        self.log = jax_log
        self.exp = jax_exp
        def set_parameters(parameters):
            return self.set_jax_parameters(parameters=parameters)
        self.set_parameters = set_parameters

    def set_fit_parameter_ranges(self, xdata, ydata):
        # sometimes the fit parameters depend on the given data point ranges
        self.turn_min = min(xdata)

    def set_lmfit_parameters(self, parameters):
        '''
        Function to set parameter values for lmfit, while at the same time guarantee that
        constraints are satisfied. The order of the values must agree with those set in
        self.fit_parameter_keys.
        '''
        lmfit_parameters = lmfit_parameters_class()
        for k in range(len(parameters)):
            lmfit_parameters.add(self.fit_parameter_keys[k], value=parameters[k], 
                                      min=self.fit_parameter_ranges[k][0], max=self.fit_parameter_ranges[k][1], 
                                      vary=True)
        return lmfit_parameters

    def set_algopy_parameters(self, parameters):
        return parameters

    def set_jax_parameters(self, parameters):
        return parameters


class model1(da_model):
    '''Model 1 in Ref. [1].'''

    def __init__(self):
        da_model.__init__(self)
        self.fit_parameter_keys = ['d', 'b', 'kappa']
        self.fit_parameter_keys_label = [r'$D_\infty [\sigma]$', r'$b$', r'$\kappa$']
        self.description = 'model 1'
        self.suffix = '_model1'

    def set_fit_parameter_ranges(self, **kwargs):
        da_model.set_fit_parameter_ranges(self, **kwargs)
        self.fit_parameter_ranges = [[0, np.inf], [0, np.inf], [0, np.inf]]

    def get_fit_start_parameters(self):
        return np.array([4.0, 40.0, 2.0])

    def run(self, params, turn):
        d, b, kappa = params
        return d + b/self.log(turn)**kappa

    ### d/dx of function ###
    def dxrun(self, params, turn):
        '''Partial derivative of self.run with respect to the turns.'''
        d, b, kappa = params
        return -kappa*b/self.log(turn)**(kappa + 1)/turn

    def dxrun_efficient(self, run, params, turn):
        '''Partial derivative of self.run which can utilize an already known function evaluation.''' 
        return self.dxrun(params, turn) # I can't see any significant improvement for this case.  

    ### d/da of function ###
    # Hereby 'a' denotes the vector of parameters.
    # The 'logturn' variable here is an internal input for efficiency. It means self.log(turn), see e.g. darun function below.
    def _dd_run(self, params, turn):
        d, b, kappa = params
        return 0.5*(turn**0 + d**0) # should return 1 (depending on the shape of 'd' or 'turn')

    def _db_run(self, params, logturn):
        d, b, kappa = params
        return 1/logturn**kappa
        
    def _dkappa_run(self, params, turn, logturn):
        d, b, kappa = params
        return -b*self.log(logturn)/logturn**kappa

    def darun(self, params, turn):
        logturn = self.log(turn)
        
        return {'d': self._dd_run(params, turn),
                'b': self._db_run(params, logturn),
                'kappa': self._dkappa_run(params, turn, logturn)}
             
    ###  d/da d/dx of function ###
    def _dd_dxrun(self, logturn):
        return 0*logturn

    def _db_dxrun(self, params, turn, logturn):
        d, b, kappa = params
        return -kappa/(turn*logturn**(kappa + 1))

    def _dkappa_dxrun(self, params, turn, logturn):
        d, b, kappa = params
        return -b/turn/logturn**(kappa + 1)*(1 - kappa*self.log(logturn))

    def dadxrun(self, params, turn):
        logturn = self.log(turn)

        return {'d': self._dd_dxrun(logturn),
                'b': self._db_dxrun(params, turn, logturn),
                'kappa': self._dkappa_dxrun(params, turn, logturn)}

class model2(da_model):
    def __init__(self):
        da_model.__init__(self)
        self.fit_parameter_keys = ['rho', 'kappa', 'n0']
        self.fit_parameter_keys_label = [r'$\rho_*$', r'$\kappa$', r'$N_0$'] # for plotting etc.
        self.description = 'model 2'
        self.suffix = '_model2'

    def init_algopy(self):
        # the fit parameters are sometimes close to its borders; we want
        # to ensure that they remain inside
        da_model.init_algopy(self)
        self.default_fit_method = 'SLSQP' #'L-BFGS-B'

    def init_jax(self):
        # the fit parameters are sometimes close to its borders; we want
        # to ensure that they remain inside
        da_model.init_jax(self)
        self.default_fit_method = 'SLSQP' #'L-BFGS-B'

    def set_fit_parameter_ranges(self, **kwargs):
        da_model.set_fit_parameter_ranges(self, **kwargs)
        self.fit_parameter_ranges = [[0, np.inf], [self._epsilon, 4], [0.1, self.turn_min - 0.1]]
        # note: self._epsilon for minimum kappa, to prevent 0**0 in algopy, which is causing errors.

    def get_fit_start_parameters(self):
        n0a, n0b = self.fit_parameter_ranges[2]
        # note that this model does not seem to change n0 if its start value is set around turn_min/2 = 1000 (in our standard examples).
        return np.array([22.0, 0.5, n0a + (n0b - n0a)/np.pi/4])

    def run(self, params, turn):
        '''Model 2 in Ref. [1]'''
        rho, kappa, n0 = params
        return rho*(kappa/2/self.exp(1)/self.log(1/n0*turn))**kappa # 1/n0*turn instead of turn/n0 due to algopy UTMP instance

    ### d/dx of function ###
    def dxrun(self, params, turn):
        '''Partial derivative of self.run with respect to the turns.'''
        rho, kappa, n0 = params
        return -kappa*rho*(kappa/2/self.exp(1))**kappa/(self.log(1/n0*turn))**(kappa + 1)/turn

    def dxrun_efficient(self, run, params, turn):
        '''Partial derivative of self.run which can utilize an already known function evaluation.'''
        rho, kappa, n0 = params
        return -kappa*run/turn/self.log(1/n0*turn)

    ### d/da of function ###
    def _drho_run(self, params, logturn):
        rho, kappa, n0 = params
        return (kappa/2/self.exp(1)/logturn)**kappa

    def _dkappa_run(self, params, logturn, drho):
        rho, kappa, n0 = params
        return rho*drho*(self.log(kappa) + 1 - self.log(2*self.exp(1)*logturn))

    def _dn0_run(self, params, turn, logturn, drho):
        rho, kappa, n0 = params
        return rho*kappa*drho/logturn/n0

    def darun(self, params, turn):
        rho, kappa, n0 = params
        logturn = self.log(1/n0*turn)
        drho = self._drho_run(params, logturn)
        return {'rho': drho,
                'kappa': self._dkappa_run(params, logturn, drho),
                'n0': self._dn0_run(params, turn, logturn, drho)}

    ###  d/da d/dx of function ###
    def _drho_dxrun(self, params, tt):
        rho, kappa, n0 = params
        return kappa*tt

    def _dkappa_dxrun(self, params, turn):
        rho, kappa, n0 = params
        return (kappa**kappa*rho*self.log(turn/n0)**(-1 - kappa)*(-1 - kappa*self.log(kappa) + 
                kappa*self.log(2*self.log(turn/n0))))/((2*self.exp(1))**kappa*turn)

    def _dn0_dxrun(self, params, turn, tt):
        rho, kappa, n0 = params
        return rho*kappa*(kappa + 1)*tt/n0/self.log(1/n0*turn)

    def dadxrun(self, params, turn):
        rho, kappa, n0 = params
        tt = -(kappa/2/self.exp(1))**kappa/self.log(1/n0*turn)**(kappa + 1)/turn
        return {'rho': self._drho_dxrun(params, tt),
                'kappa': self._dkappa_dxrun(params, turn),
                'n0': self._dn0_dxrun(params, turn, tt)}


class model2b(da_model):
    def __init__(self):
        da_model.__init__(self)
        self.fit_parameter_keys = ['rho', 'kappa']
        self.fit_parameter_keys_label = [r'$\rho_*$', r'$\kappa$'] # for plotting etc.
        self.description = r'model 2 ($N_0 \equiv 1$)'
        self.suffix = '_model2b'

    def set_fit_parameter_ranges(self, **kwargs):
        da_model.set_fit_parameter_ranges(self, **kwargs)
        self.fit_parameter_ranges = [[0, np.inf], [self._epsilon, 10]]

    def get_fit_start_parameters(self):
        return np.array([150.0, 0.75])

    def run(self, params, turn):
        '''Model 2 with N_0 = 1.'''
        rho, kappa = params
        return rho*(kappa/2/self.exp(1))**kappa/self.log(turn)**kappa

    ### d/dx of function ###
    def dxrun(self, params, turn):
        '''Partial derivative of self.run with respect to the turns.'''
        rho, kappa = params
        return rho*(kappa/2/self.exp(1))**kappa*(-kappa)/self.log(turn)**(kappa + 1)/turn

    def dxrun_efficient(self, run, params, turn):
        '''Partial derivative of self.run which can utilize an already known function evaluation.'''  
        rho, kappa = params
        return -kappa/turn*run/self.log(turn)

    ### d/da of function ###
    def _drho_run(self, params, logturn):
        rho, kappa = params
        return (kappa/2/self.exp(1)/logturn)**kappa

    def _dkappa_run(self, params, logturn, drho):
        # drho for computation efficiency (see next function)
        rho, kappa = params
        return rho*drho*(self.log(kappa) + 1 - self.log(2*self.exp(1)*logturn))

    def darun(self, params, turn):
        logturn = self.log(turn)
        drho = self._drho_run(params, logturn)
        return {'rho': drho,
                'kappa': self._dkappa_run(params, logturn, drho)}

    ###  d/da d/dx of function ###
    def _drho_dxrun(self, params, tt):
        rho, kappa = params
        return kappa*tt

    def _dkappa_dxrun(self, params, turn, tt):
        rho, kappa = params
        return -rho*tt*(1 + kappa*(self.log(kappa) - self.log(2*self.log(turn)) ))

    def dadxrun(self, params, turn):
        rho, kappa = params
        tt = (kappa/2/self.exp(1))**kappa/self.log(turn)**(kappa + 1)/turn
        return {'rho': self._drho_dxrun(params, tt),
                'kappa': self._dkappa_dxrun(params, turn, tt)}


class lambert_preprint(da_model):
    def __init__(self):
        da_model.__init__(self)
        self.small_lambda = 0.5
        self.fit_parameter_keys = ['r', 'kappa']
        self.fit_parameter_keys_label = [r'$r_*$', r'$\kappa$'] # for plotting etc.
        self.description = 'lambert'
        self.suffix = '_lambert'
        self.n0_factors = 7*np.sqrt(6)/48 # See Eq. (4c) in Ref. [1]

    def set_fit_parameter_ranges(self, **kwargs):
        da_model.set_fit_parameter_ranges(self, **kwargs)
        self.fit_parameter_ranges = [[(self.turn_min/self.n0_factors)**(-self.small_lambda), np.inf], 
                                     [1/10*self.small_lambda, 3]]
        # kappa should not be made too small and too large due to the exponentiations.
        # r can not be zero, because then n0 = 0 and n0 appears in denominators. In fact,
        # n0 should be larger than the self.turn_min at least.

    def get_fit_start_parameters(self):
        return np.array([max([self.fit_parameter_ranges[0][0], 50.0]), 1.0])

    def _extract_params(self, params):
        # function to compute N0 and other parameters, since they appear in many places in this class
        r, kappa = params
        n0 = self.n0_factors*r**self.small_lambda
        capital_lambda = -1/self.small_lambda/kappa
        return r, kappa, n0, capital_lambda

    def constraint(self, kappa):
        '''
        According to Tab. 1 in Ref. [1] it must hold
           turn/n0 >= (2/3/kappa)**(small_lambda*kappa)*exp(3/2*kappa)                (1)

           i.e.
           n0_factors*r**small_lambda = n0 <= turn_min*(2/3/kappa)**(-small_lambda*kappa)/exp(3/2*kappa)
           so that


           r <= (turn_min*(2/3/kappa)**(-small_lambda*kappa)/exp(3/2*kappa)/n0_factors)**(1/small_lambda)
        '''
        return (2/3/kappa)**(-kappa)*(self.turn_min/np.exp(3/2*kappa)/self.n0_factors)**(1/self.small_lambda)

    def init_lmfit(self):
        da_model.init_lmfit(self)
        from scipy.special import lambertw
        self.lambertw = lambda x: np.real(lambertw(x, k=-1))

    # initialize lmfit constraint
    def set_lmfit_parameters(self, parameters):
        lmfit_parameters = da_model.set_lmfit_parameters(self, parameters=parameters)
        lmfit_parameters._asteval.symtable['constraint'] = self.constraint
        return lmfit_parameters

    '''
    # the constraint is usually well satisfied; the range on r is very large which not only slows down
    # lmfit, but also usually does not converge. That's why we will use the default routine.
    #
    # include constraint in set_lmfit_parameters 
    def set_lmfit_parameters(self, parameters):
        r, kappa = parameters
        self.lmfit_parameters.add('kappa', 
                                  value=kappa, 
                                  min=self.fit_parameter_ranges[1][0],
                                  max=self.fit_parameter_ranges[1][1],
                                  vary=True)
        self.lmfit_parameters.add('delta', value=self.constraint(kappa) - r, 
                                  min=0, 
                                  max=self.constraint(self.fit_parameter_ranges[1][0]) - self.fit_parameter_ranges[0][0],
                                  vary=True)
        self.lmfit_parameters.add('r', expr='constraint(kappa) - delta',
                                  min=self.fit_parameter_ranges[0][0],
                                  max=self.fit_parameter_ranges[0][1])
    '''

    def init_numpy(self):
        # original code using the built-in scipy function
        da_model.init_numpy(self)
        from scipy.special import lambertw
        self.lambertw = lambda x: np.real(lambertw(x, k=-1))

    '''
    def init_numpy_alternative(self):
        # using the custom function below (agrees with the scipy version, only slightly slower)
        # can be used as replacement of init_numpy.
        da_model.init_numpy(self)
        ll = my_lambert()
        ll.branch = -1
        self.lambertw = lambda x: ll.lambert(x)
    '''

    def init_algopy(self):
        da_model.init_algopy(self)
        ll = my_lambert()
        ll.branch = -1
        ll.init_algopy()
        self.lambertw = lambda x: ll.lambert(x)

    def init_jax(self):
        da_model.init_jax(self)
        ll = my_lambert()
        ll.branch = -1
        ll.init_jax()
        self.lambertw = lambda x: ll.lambert(x)   

    def run(self, params, turn):
        '''Lambert function with fixed lambda parameter, Ref. [1].'''
        r, kappa, n0, capital_lambda = self._extract_params(params)
        return r*(1/capital_lambda*self.lambertw(capital_lambda*(1/n0*turn)**capital_lambda))**(-kappa)

    ### d/dx of function ###
    def dxrun(self, params, turn):
        '''Partial derivative of self.run with respect to the turns.'''        
        r, kappa, n0, capital_lambda = self._extract_params(params)
        return self.run(params, turn)/turn/(1 + self.lambertw(capital_lambda*(1/n0*turn)**capital_lambda))/self.small_lambda

    def dxrun_efficient(self, run, params, turn):
        '''Partial derivative of self.run which can utilize an already known function evaluation.'''        
        r, kappa, n0, capital_lambda = self._extract_params(params)
        return run/turn/(1 + capital_lambda*(run/r)**(-1/kappa))/self.small_lambda

    ### d/da of function ###
    def _dr_run(self, wz, wzk):
        return wzk*(1 - 1/(1 + wz))

    def _dkappa_run(self, r, capital_lambda, n0, wz, wzk, turn):
        return -r*wzk*(self.log(1/capital_lambda*wz) + 1 - (1 + capital_lambda*self.log(1/n0*turn))/(1 + wz))

    def darun(self, params, turn):
        r, kappa, n0, capital_lambda = self._extract_params(params)

        z = capital_lambda*(1/n0*turn)**capital_lambda
        wz = self.lambertw(z)
        wzk = (1/capital_lambda*wz)**(-kappa)
        return {'r': self._dr_run(wz, wzk),
                'kappa': self._dkappa_run(r, capital_lambda, n0, wz, wzk, turn)}
                
    ###  d/da d/dx of function ###
    def _dr_dxrun(self, kappa, capital_lambda, wz, wzk, turn):
        return capital_lambda/turn*wzk*wz/(1 + wz)**2*(1/(1 + wz) - kappa)

    def _dkappa_dxrun(self, r, kappa, n0, capital_lambda, wz, turn):
        log1 = self.log(1/capital_lambda*wz)
        return -(r*(kappa*self.small_lambda*wz*(-1 + kappa + kappa*wz) + kappa**2*self.small_lambda*(1 + wz)**2*log1 + 
               (kappa + wz + kappa*wz)*self.log(1/n0*turn)))/(kappa**2*self.small_lambda**2*(wz/capital_lambda)**kappa*(1 + wz)**3*turn)
        
    def dadxrun(self, params, turn):
        r, kappa, n0, capital_lambda = self._extract_params(params)
        
        z = capital_lambda*(1/n0*turn)**capital_lambda
        wz = self.lambertw(z)
        wzk = (1/capital_lambda*wz)**(-kappa)
        return {'r': self._dr_dxrun(kappa, capital_lambda, wz, wzk, turn),
                'kappa': self._dkappa_dxrun(r, kappa, n0, capital_lambda, wz, turn)}       


class lambert(lambert_preprint):
    '''
    In comparison to lambert_preprint there is a factor 1/2.0 in the run routine.
    '''
    def __init__(self):
        lambert_preprint.__init__(self)
        self._cf = 1/2.0 # FIXME check 1/2.0

    def run(self, params, turn):
        '''Lambert function with fixed lambda parameter, Ref. [1]. Corrected by a factor 2 here.'''
        return lambert_preprint.run(self, params, turn)*self._cf 

    ### d/dx of function ###
    def dxrun(self, params, turn):
        '''Partial derivative of self.run with respect to the turns.'''
        return lambert_preprint.dxrun(self, params, turn)*self._cf

    def dxrun_efficient(self, run, params, turn):
        '''Partial derivative of self.run which can utilize an already known function evaluation.'''
        return lambert_preprint.dxrun_efficient(self, run/self._cf, params, turn)*self._cf

    ### d/da of function ###
    def darun(self, params, turn):
        dadict = lambert_preprint.darun(self, params, turn)
        return {'r': dadict['r']*self._cf,
                'kappa': dadict['kappa']*self._cf}

    ###  d/da d/dx of function ###
    def dadxrun(self, params, turn):
        dadxdict = lambert_preprint.dadxrun(self, params, turn)
        return {'r': dadxdict['r']*self._cf,
                'kappa': dadxdict['kappa']*self._cf}



from scipy.optimize import NonlinearConstraint

class lambert_b(da_model):
    '''
    New implementation of the lambert model at the request of M. Giovannozzi, relaxing the relation between N_0 and rho.
    '''
    def __init__(self):
        da_model.__init__(self)
        self.fit_parameter_keys = ['rho', 'kappa', 'n0']
        self.fit_parameter_keys_label = [r'$\rho_*$', r'$\kappa$', r'$N_0$'] # for plotting etc.
        self.description = 'lambert-b'
        self.suffix = '_lambertb'
        self.small_lambda = 0.5

    def set_fit_parameter_ranges(self, **kwargs):
        da_model.set_fit_parameter_ranges(self, **kwargs)
        kappa_min = 0.04 # kappa can not be made too small; check self._lambert_eval with reasonable large turn numbers.
        # We determine the above value by inserting a reasonable large turn number into:
        # turn = 1e8
        # n0 = 1500
        # capital_lambda = -1/0.5/0.04
        # self.lambertw(capital_lambda*(1/n0*turn)**capital_lambda)
        # and check whether python still can handle these numbers.
        kappa_max = 6
        # n0 is bounded by the constraint imposed below: n0 <= self.constraint(kappa). Since this constraint
        # takes its maximal values close to zero, n0 should at least be bounded above by constraint(kappa_min).
        # We also choose the minimum value of n0 not to be smaller than 1% of the given minimum turn number, because
        # if the value is too small, then lmfit+leastsq with y-errors can not find a proper optimum.
        self.fit_parameter_ranges = [[0, np.inf], [kappa_min, kappa_max], [self.turn_min*0.01, self.constraint(kappa_min)]]

        nonlinear_con = lambda params: self.constraint(kappa=params[1]) - params[2]
        # used for e.g. 'trust-constr' method. 'ineq' means "expr >= 0"
        #self._trust_constraints = [{'type': 'ineq', 'fun': nonlinear_con}]
        self._trust_constraints = NonlinearConstraint(nonlinear_con, 0, np.inf)

    def get_fit_start_parameters(self):
        kappa_start = 0.098
        return np.array([20.0, kappa_start, 0.5*self.constraint(kappa_start)])

    def _extract_params(self, params):
        rho, kappa, n0 = params
        capital_lambda = -1/self.small_lambda/kappa
        return rho, kappa, n0, capital_lambda   

    def constraint(self, kappa):
        '''
        According to Tab. 1 in Ref. [1] it must hold
           turn/n0 >= (2/3/kappa)**(small_lambda*kappa)*exp(3/2*kappa)                (1)

           i.e.
           n0 <= turn_min*(2/3/kappa)**(-small_lambda*kappa)/exp(3/2*kappa)

        '''
        return self.turn_min*(2/3/kappa)**(-self.small_lambda*kappa)*np.exp(-3/2*kappa)

    def init_lmfit(self):
        da_model.init_lmfit(self)
        from scipy.special import lambertw
        self.lambertw = lambda x: np.real(lambertw(x, k=-1))

    # include constraint in set_lmfit_parameters 
    def set_lmfit_parameters(self, parameters):
        lmfit_parameters = da_model.set_lmfit_parameters(self, parameters=parameters)
        rho, kappa, n0 = parameters

        lmfit_parameters._asteval.symtable['constraint'] = self.constraint
        #self.default_fit_method = 'basinhopping'  # slow, but converges when lmfit does not.
        # alternatives are: 'tnc', 'ampgo', but they seem to give fits of lesser quality.

        lmfit_parameters.add('rho', 
                                  value=rho, 
                                  min=self.fit_parameter_ranges[0][0],
                                  max=self.fit_parameter_ranges[0][1],
                                  vary=True)
        lmfit_parameters.add('kappa', 
                                  value=kappa, 
                                  min=self.fit_parameter_ranges[1][0],
                                  max=self.fit_parameter_ranges[1][1],
                                  vary=True)
        # the parameter delta is the positive difference from n0 up to self.constraint(kappa)
        lmfit_parameters.add('delta', value=self.constraint(kappa) - n0, 
                                  min=0,
                                  max=self.constraint(kappa) - self.fit_parameter_ranges[2][0],
                                  vary=True)
        lmfit_parameters.add('n0', expr='constraint(kappa) - delta',
                                  min=self.fit_parameter_ranges[2][0],
                                  max=self.constraint(kappa))
        return lmfit_parameters

    def init_numpy(self):
        # original code using the built-in scipy function
        da_model.init_numpy(self)
        from scipy.special import lambertw
        self.lambertw = lambda x: np.real(lambertw(x, k=-1))

    '''
    def init_numpy_alternative(self):
        # using the custom function below (agrees with the scipy version, only slightly slower)
        # can be used as replacement of init_numpy.
        da_model.init_numpy(self)
        ll = my_lambert()
        ll.branch = -1
        self.lambertw = lambda x: ll.lambert(x)
    '''

    def init_algopy(self):
        da_model.init_algopy(self)
        ll = my_lambert()
        ll.branch = -1
        ll.init_algopy()
        self.lambertw = lambda x: ll.lambert(x)
        self.default_fit_method = 'SLSQP' # recommended scipy.optimize.minimize method

    def init_jax(self):
        da_model.init_jax(self)
        ll = my_lambert()
        ll.branch = -1
        ll.init_jax()
        self.lambertw = lambda x: ll.lambert(x)
        self.default_fit_method = 'SLSQP' # recommended scipy.optimize.minimize method

    def _lambert_eval(self, capital_lambda, n0, turn):
        # helper function to evaluate the lambert function with its arguments
        return self.lambertw(capital_lambda*(1/n0*turn)**capital_lambda)

    def run(self, params, turn):
        '''Lambert function see, Ref. [1]. Here using N_0 as additional fit parameter.'''
        rho, kappa, n0, capital_lambda = self._extract_params(params)
        wz = self._lambert_eval(capital_lambda, n0, turn)
        return rho*(kappa/2/self.exp(1))**kappa*(1/capital_lambda*wz)**(-kappa)

    ### d/dx of function ###
    def dxrun(self, params, turn):
        '''Partial derivative of self.run with respect to the turns.'''        
        rho, kappa, n0, capital_lambda = self._extract_params(params)
        wz = self._lambert_eval(capital_lambda, n0, turn)
        wzk = (1/capital_lambda*wz)**(-kappa)
        return rho*(kappa/2/self.exp(1))**kappa*wzk/(1 + wz)/self.small_lambda/turn
        
    def dxrun_efficient(self, run, params, turn):
        '''
        Identical to dxrun here.
        '''
        rho, kappa, n0, capital_lambda = self._extract_params(params)
        wz = self._lambert_eval(capital_lambda, n0, turn)
        wzk = (1/capital_lambda*wz)**(-kappa)
        return rho*(kappa/2/self.exp(1))**kappa*wzk/(1 + wz)/self.small_lambda/turn

    ### d/da of function ###
    def _dkappa_run(self, params, wz, ewzk, turn):
        rho, kappa, n0 = params
        return rho*ewzk*wz/(-wz*self.small_lambda)/kappa/(1 + wz)*(self.log(1/n0*turn) + kappa*self.small_lambda*(wz + 
                            (1 + wz)*self.log(-2*wz*self.small_lambda)))

    def _dn0_run(self, params, wz, ewzk):
        rho, kappa, n0 = params
        return -rho*ewzk/n0/self.small_lambda/(1 + wz)

    def darun(self, params, turn):
        rho, kappa, n0, capital_lambda = self._extract_params(params)
        wz = self._lambert_eval(capital_lambda, n0, turn)
        ewzk = (-2*self.exp(1)*self.small_lambda*wz)**(-kappa)
        return {'rho': ewzk,
                'kappa': self._dkappa_run(params, wz, ewzk, turn),
                'n0': self._dn0_run(params, wz, ewzk)}
                
    ###  d/da d/dx of function ###
    def _drho_dxrun(self, kk, wz, wzk, turn):
        return kk*wzk/self.small_lambda/(1 + wz)/turn

    def _dkappa_dxrun(self, params, wz, ewzk, turn):
        rho, kappa, n0 = params
        return ewzk*(rho*(kappa*self.log(n0) + (1 + kappa)*wz*self.log(n0/turn) - 
             kappa*(self.log(turn) + kappa*(1 + wz**2)*self.small_lambda*self.log(-2*wz*self.small_lambda) + 
                wz*self.small_lambda*(-1 + kappa*(1 + wz + self.log(4)) + 
         2*kappa*self.log(-(wz*self.small_lambda))))))/(kappa**2*(1 + wz)**3*turn*self.small_lambda**2)

    def _dn0_dxrun(self, params, wz, ewzk, turn):
        rho, kappa, n0 = params
        return -ewzk*(rho*(kappa + wz + kappa*wz))/(kappa*n0*(1 + wz)**3*turn*self.small_lambda**2)
        
    def dadxrun(self, params, turn):
        rho, kappa, n0, capital_lambda = self._extract_params(params)
        wz = self._lambert_eval(capital_lambda, n0, turn)
        wzk = (1/capital_lambda*wz)**(-kappa)
        kk = (kappa/2/self.exp(1))**kappa

        ewzk = (-2*self.exp(1)*self.small_lambda*wz)**(-kappa)

        return {'rho': self._drho_dxrun(kk, wz, wzk, turn),
                'kappa': self._dkappa_dxrun(params, wz, ewzk, turn),
                'n0': self._dn0_dxrun(params, wz, ewzk, turn)}


class model4b(da_model):
    '''
    Similar as lambert-b, but here N_0 = 1.
    '''

    '''
    New implementeation of the lambert model at the request of M. Giovannozzi, relaxing the relation between N_0 and rho.
    '''
    def __init__(self):
        da_model.__init__(self)
        self.fit_parameter_keys = ['rho', 'kappa']
        self.fit_parameter_keys_label = [r'$\rho_*$', r'$\kappa$'] # for plotting etc.
        self.description = 'model 4b'
        self.suffix = '_model4b'
        self.small_lambda = 0.5

    def set_fit_parameter_ranges(self, **kwargs):
        da_model.set_fit_parameter_ranges(self, **kwargs)
        self.fit_parameter_ranges = [[0, np.inf], [0.1, 6]]

    def get_fit_start_parameters(self):
        return np.array([300.0, 0.9])

    def _extract_params(self, params):
        rho, kappa = params
        capital_lambda = -1/self.small_lambda/kappa
        return rho, kappa, capital_lambda   

    def constraint(self, kappa):
        '''
        According to Tab. 1 in Ref. [1] it must hold
           turn/n0 >= (2/3/kappa)**(small_lambda*kappa)*exp(3/2*kappa)                (1)

           here with n0 = 1, so that
           1 <= turn_min*(2/3/kappa)**(-small_lambda*kappa)/exp(3/2*kappa)

           This seems to be satisfied; the function takes its minimum between 4.5 and 5.
           Only here for counter-checks.
        '''
        return self.turn_min*(2/3/kappa)**(-self.small_lambda*kappa)/np.exp(3/2*kappa)

    def init_lmfit(self):
        da_model.init_lmfit(self)
        from scipy.special import lambertw
        self.lambertw = lambda x: np.real(lambertw(x, k=-1))
        # self.lmfit_parameters._asteval.symtable['constraint'] = self.constraint

    def init_numpy(self):
        # original code using the built-in scipy function
        da_model.init_numpy(self)
        from scipy.special import lambertw
        self.lambertw = lambda x: np.real(lambertw(x, k=-1))

    '''
    def init_numpy_alternative(self):
        # using the custom function below (agrees with the scipy version, only slightly slower)
        # can be used as replacement of init_numpy.
        da_model.init_numpy(self)
        ll = my_lambert()
        ll.branch = -1
        self.lambertw = lambda x: ll.lambert(x)
    '''

    def init_algopy(self):
        da_model.init_algopy(self)
        ll = my_lambert()
        ll.branch = -1
        ll.init_algopy()
        self.lambertw = lambda x: ll.lambert(x)

    def init_jax(self):
        da_model.init_jax(self)
        ll = my_lambert()
        ll.branch = -1
        ll.init_jax()
        self.lambertw = lambda x: ll.lambert(x)   

    def _lambert_eval(self, capital_lambda, turn):
        # helper function to evaluate the lambert function with its arguments
        return self.lambertw(capital_lambda*(turn)**capital_lambda)

    def run(self, params, turn):
        '''Lambert function see, Ref. [1]. Here using N_0 as additional fit parameter.'''
        rho, kappa, capital_lambda = self._extract_params(params)
        wz = self._lambert_eval(capital_lambda, turn)
        return rho*(kappa/2/self.exp(1))**kappa*(1/capital_lambda*wz)**(-kappa)

    ### d/dx of function ###
    def dxrun(self, params, turn):
        '''Partial derivative of self.run with respect to the turns.'''        
        rho, kappa, capital_lambda = self._extract_params(params)
        wz = self._lambert_eval(capital_lambda, turn)
        wzk = (1/capital_lambda*wz)**(-kappa)
        return rho*(kappa/2/self.exp(1))**kappa*wzk/(1 + wz)/self.small_lambda/turn
        
    def dxrun_efficient(self, run, params, turn):
        '''
        Identical to dxrun here.
        '''
        rho, kappa, capital_lambda = self._extract_params(params)
        wz = self._lambert_eval(capital_lambda, turn)
        wzk = (1/capital_lambda*wz)**(-kappa)
        return rho*(kappa/2/self.exp(1))**kappa*wzk/(1 + wz)/self.small_lambda/turn

    ### d/da of function ###
    def _drho_run(self, kappa, wzk):
        return (kappa/2/self.exp(1))**kappa*wzk

    def _dkappa_run(self, params, wz, turn):
        rho, kappa, capital_lambda = self._extract_params(params)
        return (capital_lambda*rho*(self.log(turn) + kappa*self.small_lambda*(self.log(-2*wz*self.small_lambda) + 
                wz*self.log(-2*self.exp(1)*wz*self.small_lambda))))/((2*self.exp(1))**kappa*(1 + wz)*(-(wz*self.small_lambda))**kappa)

    def darun(self, params, turn):
        rho, kappa, capital_lambda = self._extract_params(params)
        wz = self._lambert_eval(capital_lambda, turn)
        wzk = (1/capital_lambda*wz)**(-kappa)
        return {'rho': self._drho_run(kappa, wzk),
                'kappa': self._dkappa_run(params, wz, turn)}
                
    ###  d/da d/dx of function ###
    def _drho_dxrun(self, kk, wz, wzk, turn):
        return kk*wzk/self.small_lambda/(1 + wz)/turn

    def _dkappa_dxrun(self, rho, kappa, wz, turn):   
        return -((rho*((kappa + wz + kappa*wz)*self.log(turn) + 
               kappa*self.small_lambda*(kappa*(1 + wz**2)*self.log(-2*wz*self.small_lambda) + 
                  wz*(-1 + kappa*(1 + wz + self.log(4)) + 2*kappa*self.log(-(wz*self.small_lambda))))))/
           ((2*self.exp(1))**kappa*kappa**2*(1 + wz)**3*turn*self.small_lambda**2*(-(wz*self.small_lambda))**kappa))
        
    def dadxrun(self, params, turn):
        rho, kappa, capital_lambda = self._extract_params(params)
        wz = self._lambert_eval(capital_lambda, turn)
        wzk = (1/capital_lambda*wz)**(-kappa)
        kk = (kappa/2/self.exp(1))**kappa
        return {'rho': self._drho_dxrun(kk, wz, wzk, turn),
                'kappa': self._dkappa_dxrun(rho, kappa, wz, turn)}


class mg_model1(da_model):
    def __init__(self):
        da_model.__init__(self)
        self.fit_parameter_keys = ['s', 'mu']
        self.fit_parameter_keys_label = [r'$s$', r'$\hat \mu(\sigma)$']
        self.description = 'mg-model-1'
        self.suffix = '_mgmodel1'

    def set_fit_parameter_ranges(self, **kwargs):
        da_model.set_fit_parameter_ranges(self, **kwargs)
        self.fit_parameter_ranges = [[self._epsilon, self.turn_min - 1], [0.1, np.inf]]
        # mu should not be too small to avoid overflow errors; we also remove 10% of the maximal
        # range of s, because the logarithm must not be zero.

    def init_algopy(self):
        da_model.init_algopy(self)
        self.default_fit_method = 'SLSQP' # 'L-BFGS-B'

    def init_jax(self):
        da_model.init_jax(self)
        self.default_fit_method = 'SLSQP' # 'L-BFGS-B'

    def get_fit_start_parameters(self):
        return np.array([0.25*self.fit_parameter_ranges[0][-1], 3.0])

    def run(self, params, turn):
        '''According to model 1 in Ref. [2].'''
        s, mu = params
        return (s/self.log(1/s*turn))**(1/mu)

    ### d/dx of function ###
    def dxrun(self, params, turn):
        '''Partial derivative of self.run with respect to the turns.'''
        s, mu = params
        return -s**(1/mu)/mu/turn*(1/self.log(1/s*turn))**(1/mu + 1)

    def dxrun_efficient(self, run, params, turn):
        '''Partial derivative of self.run which can utilize an already known function evaluation.'''
        s, mu = params
        return -run**(mu + 1)/mu/turn/s

    ### d/da of function ###
    def _ds_run(self, params, logxsi):
        s, mu = params
        return (logxsi*s)**(1/mu)/s/mu*(1 + logxsi)

    def _dmu_run(self, params, logxsi):
        s, mu = params
        return -1/mu**2*self.log(logxsi*s)*(logxsi*s)**(1/mu)

    def darun(self, params, turn):
        s, mu = params
        logxsi = 1/self.log(1/s*turn)
        return {'s': self._ds_run(params, logxsi),
                'mu': self._dmu_run(params, logxsi)}

    ###  d/da d/dx of function ###
    def _ds_dxrun(self, params, logxsi, ll):
        s, mu = params
        return -1/s**2*ll*(1 + (1 + mu)*logxsi)

    def _dmu_dxrun(self, params, logxsi, ll):
        s, mu = params
        return 1/s*ll*(1 + 1/mu*self.log(logxsi*s))

    def dadxrun(self, params, turn):
        s, mu = params
        logxsi = 1/self.log(1/s*turn)
        ll = 1/mu**2/turn*(logxsi*s)**(1/mu + 1)
        return {'s': self._ds_dxrun(params, logxsi, ll),
                'mu': self._dmu_dxrun(params, logxsi, ll)}


class mg_model3(da_model):
    '''
    CLASS NOT FINISHED!
    '''

    def __init__(self):
        da_model.__init__(self)
        da_model.init_mpmath(self, dps=16)
        self.relambertw_mp = np.frompyfunc(lambda p, k: mpmath.re(mpmath.lambertw(p, k)), 2, 1)
        self.fit_parameter_keys = ['s', 'tau', 'mu']
        self.fit_parameter_keys_label = [r'$s$', r'$\tau$', r'$\hat \mu(\sigma)$']
        self.description = 'mg-model-3'
        self.suffix = '_mgmodel3'

    def set_fit_parameter_ranges(self, **kwargs):
        da_model.set_fit_parameter_ranges(self, **kwargs)
        self.fit_parameter_ranges = [[self._epsilon, np.inf], [self._epsilon, np.inf], [0.1, np.inf]]

    def get_fit_start_parameters(self):
        return np.array([100.0, 1.0, 0.8])

    def run(self, params, turn):
        '''According to model 3 in Ref. [2].'''
        s, tau, mu = params

        with np.errstate(over='raise'):
            try:
                # the numpy function is faster, but can lead to a overflow issue
                result = ((s/tau)/np.real(lambertw(s/tau*(turn/s)**(1/tau), k=0)))**(1/mu)
            except FloatingPointError:
                # in this case we use the slower but precise mpmath function.
                mps, mptau, mpmu = mpmath.mpf(1)*s, mpmath.mpf(1)*tau, mpmath.mpf(1)*mu
                lambert_result = self.relambertw_mp(mps/mptau*(turn/mps)**(1/mptau), 0)
                result_mp = (mps/mptau/lambert_result)**(1/mpmu)
                result = np.array([float(r) for r in result_mp])
        return result

    # ToDo: include dxrun and dxrun_effective


class mg_model1_c(da_model):
    '''
    CLASS NOT FINISHED!
    '''

    def __init__(self):
        da_model.__init__(self)
        self.fit_parameter_keys = ['s', 'mu', 'd']
        self.fit_parameter_keys_label = [r'$s$', r'$\hat \mu(\sigma)$', r'$D_\infty$']
        self.description = 'mg-model-1c'
        self.suffix = '_mgmodel1c'

        '''
        turn/s >= 1
        '''

    def set_fit_parameter_ranges(self, **kwargs):
        da_model.set_fit_parameter_ranges(self, **kwargs)
        self.fit_parameter_ranges = [[self._epsilon, np.inf], [0.1, np.inf], [0, np.inf]]

    def get_fit_start_parameters(self):
        return np.array([225.0, 2.0, 6.0])

    def run(self, params, turn):
        '''According to model 1 in Ref. [2], with an additional constant.'''
        s, mu, d = params
        return d + (s/self.log(turn/s))**(1/mu)

    ### d/dx of function ###
    def dxrun(self, params, turn):
        '''Partial derivative of self.run with respect to the turns.'''
        s, mu, d = params
        return -s**(1/mu)/mu/turn*(1/self.log(1/s*turn))**(1/mu - 3)

    def dxrun_efficient(self, run, params, turn):
        s, mu, d = params
        return -run/mu/turn*(s*run**(-mu))**3


class mg_model3_c(da_model):
    '''
    CLASS NOT FINISHED!
    '''

    def __init__(self):
        da_model.__init__(self)
        da_model.init_mpmath(self, dps=16)
        self.relambertw_mp = np.frompyfunc(lambda p, k: mpmath.re(mpmath.lambertw(p, k)), 2, 1)
        self.fit_parameter_keys = ['s', 'tau', 'mu', 'd']
        self.fit_parameter_keys_label = [r'$s$', r'$\tau$', r'$\hat \mu(\sigma)$', r'$D_\infty$']
        self.description = 'mg-model-3c'
        self.suffix = '_mgmodel3c'

        '''
        turn/s >= 1
        '''

    def set_fit_parameter_ranges(self, **kwargs):
        da_model.set_fit_parameter_ranges(self, **kwargs)
        self.fit_parameter_ranges = [[self._epsilon, np.inf], [self._epsilon, np.inf], [self._epsilon, np.inf], [0, np.inf]]

    def get_fit_start_parameters(self):
        return np.array([100.0, 1.0, 1.0, 6.0])

    def run(self, params, turn):
        '''According to model 3 in Ref. [3], with an additional constant.'''
        s, tau, mu, d = params

        with np.errstate(over='raise'):
            try:
                # the numpy function is faster, but can lead to a overflow issue
                result = ((s/tau)/np.real(lambertw(s/tau*(turn/s)**(1/tau), k=0)))**(1/mu)
            except FloatingPointError:
                # in this case we use the slower but precise mpmath function.
                mps, mptau, mpmu = mpmath.mpf(1)*s, mpmath.mpf(1)*tau, mpmath.mpf(1)*mu
                lambert_result = self.relambertw_mp(mps/mptau*(turn/mps)**(1/mptau), 0)
                result_mp = (mps/mptau/lambert_result)**(1/mpmu)
                result = np.array([float(r) for r in result_mp])


        return d + result

    # ToDo: include dxrun and dxrun_effective


#######################
# Auxiliary functions #
#######################


class my_lambert:
    '''
    The built-in routine of the Lambert-W function in scipy is often not supported by the DA modules. We therefore
    have created a direct evaluation. Based on our tests, lambert_iteration_1 seems to provide more stable
    results for the -1 branch.
    '''
    
    def __init__(self):
        self.branch = 0
        self.lambert_method = self.lambert_iteration_1
        self.init_numpy()
        
    def init_numpy(self):
        import numpy as np
        self.fl = np # function library for other numpy operations
        self.log_func = np.log
        self.exp_func = np.exp
        
    def init_algopy(self):
        import numpy as np
        from algopy import log as algopy_log
        from algopy import exp as algopy_exp
        self.fl = np
        self.log_func = algopy_log
        self.exp_func = algopy_exp
        
    def init_jax(self):
        import jax.numpy as jnp
        self.fl = jnp
        self.log_func = jnp.log
        self.exp_func = jnp.exp

    def get_lambert_start_value(self, x):
        # estimate start value
        # there are two branches if -1/e <= x < 0. These branches produce two roots of x*e**x = y.
        # if the -1 branch is requested, the initial guess value for w should be smaller than -1
        if self.branch == -1:
            start_value = self.log_func(-x) - self.log_func(-self.log_func(-x))
        if self.branch == 0:
            # this function is taylored by hand in order to avoid any IF-statements or comparisons
            # which break the DA
            start_value = self.log_func(x + 2/self.exp_func(1)) - \
            self.exp_func(-1/(x + 1.6))*self.log_func(self.log_func(x + 1.6))
            
            '''
            start_value = x
            if not isinstance(x, float): #isinstance(x, list) or isinstance(x, np.ndarray):
                nn = np.where(x > 2)[0]
                if len(nn) > 0:
                    x1 = x[nn]
                    x0 = x[[k for k in range(len(x)) if k not in nn]]
                    start_value = self.fl.hstack([x0, self.log_func(x1) - self.log_func(self.log_func(x1))])
            else:
                if x > 2:
                    start_value = self.log_func(x) - self.log_func(self.log_func(x))
            '''
                    
        return start_value
    
    def lambert(self, x, start_value=None, n_iter=6, verbose=False):
 
        if start_value == None:
            start_value = self.get_lambert_start_value(x)
        self.start_value = start_value
            
        if verbose:
            print ('start_value: {}'.format(start_value))
            t0 = time.time()
        w = self.lambert_method(x=x, n_iter=n_iter)
        if verbose:
            print ('evaluation time: {}'.format(time.time() - t0))
            
        return w

    def lambert_iteration_1(self, x, n_iter):
        '''
        See
        "On the Lambert W Function" by Corless, Gonnet, Hare, Jeffrey and Knuth. (1996) Advances in Computational
        Mathematics Vol. 5.
        https://web.archive.org/web/20101214110615/http://www.apmaths.uwo.ca/~djeffrey/Offprints/W-adv-cm.pdf
        original paper quoted inside, i.e.
        https://www.jstor.org/stable/pdf/2321760.pdf
        '''
        w = self.start_value
        for k in range(n_iter):
            w = w - (w*self.exp_func(w) - x)/(self.exp_func(w)*(w + 1) - (w + 2)*(w*self.exp_func(w) - x)/(2*w + 2))
        return w 

    def lambert_iteration_2(self, x, n_iter=10):
        '''
        See "Precise and fast computation of Lambert W-functions without transcendental function evaluations"
        T. Fukushima (2013) J. of Comput. and Appl. Math.
        '''
        w = self.start_value
        for k in range(n_iter):
            y = x*self.exp_func(-w)
            f0 = w - y
            f1 = 1 + y
        
            f2 = y # fk = (-1)**k*x for k >= 2
            f3 = -y
            f4 = y
            
            w = w - 4*f0*(6*f1**3 - 6*f0*f1*f2 + f0**2*f3)/\
                    (24*f1**4 - 36*f0*f1**2*f2 + 6*f0**2*f2**2 + 8*f0**2*f1*f3 - f0**3*f4)
        return w
    

