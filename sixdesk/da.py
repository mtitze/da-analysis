import pandas as pd
import numpy as np
import sqlite3
import os

from .notebook_tools import six_instance, six_handler
from misc.general import helpsort, get_rms_value, error_of_average

import matplotlib.pyplot as plt
import time as timelib
   
#        Compile search.f90 via
#        f2py -c -m search search.f90
#        for details see
#        https://stackoverflow.com/questions/7632963/numpy-find-first-index-of-value-fast
from . import search

'''
 Scripts to compute the amplitudes of the least stable particles (for each of the given angles)
 and the DA itself.

 References mentioned below:
 [1]: Press, Teukolsky, Vetterling, Flannery: Numerical Recipes in FORTRAN 77, Edition 2, Volume 1 (1997).
 [2]: Todesco and Giovannozzi: Dynamic aperture estimates and phase-space distortions in nonlinear betatron motion.
      Physical Review E, Vol. 53, no. 4, April 1996.

!!! Attention: Some methods are not finished (for example, they may not have the error calculation implemented).
'''

from itertools import product

############################################################################
### Methods to prepare computation of DA
############################################################################

class precompute_da(six_handler):
    '''
    Master class to pre-compute stable amplitudes of the SQL databases, based on a precompute_da_method.
    See the precompute_da_method classes and the DA templates for further information.
    '''

    def __init__(self, filenames, instance, verbose=True, **kwargs):
        six_handler.__init__(self, filenames=filenames, instance=instance, **kwargs)
        if verbose:
            if 'with_errors' in kwargs.keys():
                print ('with errors: {}'.format(kwargs['with_errors']))

    def run(self, reset=False, verbose=True, **kwargs):
        '''
        INPUT
        =====
        reset:
            If 'True', the table name in the SQL database is removed before new data is written.

        **kwargs:
            Further arguments passed to self.pre_da_method.run
        '''

        if verbose:
            print ('Computing stable/unstable amplitudes ...')
            print ('\nreset = {}'.format(reset))

        for obj in self.objects:
            if reset:
                obj._drop_data(remove=[obj._stable_unstable_table_name], verbose=verbose)
            obj.run(verbose=verbose, **kwargs)


class da_instance(six_instance):
    '''
    Generic class for all DA operations.
    '''

    def __init__(self, filename, with_errors=False):
        self.with_errors = with_errors  # This is required if error calculations should be performed, as it will produce the initial error bars on the amplitudes.
        six_instance.__init__(self, filename=filename)

        self.conn = sqlite3.connect(self.filename)
        cursor = self.conn.cursor()

        # obtain general SixTrack run parameters
        sql_command = "SELECT keyname, value FROM env"
        self.env_var = dict(self._sql_execute(sql_command, cursor=cursor))
        self.conn.commit()

        # obtain the number of seeds and the tunes used
        self.seeds = self._get_db_seeds(cursor=cursor)
        self.conn.commit()
        self.tunes = self._get_db_tunes(cursor=cursor)
        self.conn.commit()
        self.conn.close()

        self.gamma = self.env_var['gamma']
        self.beta = np.sqrt(1 - 1/self.gamma**2)

        # from get_angles routine in original SixDeskDB
        kmaxl = int(self.env_var['kmaxl'])
        kinil = int(self.env_var['kinil'])
        kendl = int(self.env_var['kendl'])
        kstep = int(self.env_var['kstep'])
        s = 90./(kmaxl + 1)
        self.angles = np.arange(kinil, kendl + 1, kstep)*s
        self.n_angles = len(self.angles)

        ###############################################
        # Initialize table column format
        ftype = [('seed', int), 
                 ('tunex', float), 
                 ('tuney', float), 
                 ('turn', int)]

        ftype = ftype + [('surv_qx_theta{}'.format(k), float) for k in range(self.n_angles)] + \
                        [('surv_qy_theta{}'.format(k), float) for k in range(self.n_angles)]

        if self.with_errors:
            ftype = ftype + [('surv_qx_theta{}_error'.format(k), float) for k in range(self.n_angles)] + \
                            [('surv_qy_theta{}_error'.format(k), float) for k in range(self.n_angles)] + \
                            [('turn_theta{}_error'.format(k), float) for k in range(self.n_angles)]
            ftype = ftype + [('av_turn_error', float)]

        self._ftype = ftype
        self._ftype_str = dict([(k[0], {int: 'int', float: 'float'}[k[1]]) for k in ftype]) # required type declaration for pandas.to_sql

        self._stable_unstable_table_name = 'da_vst2' # name of table to store information about stable/unstable particles

    def plot_survivors(self, smin, s=1, xlim=[], ylim=[], figsize=(5, 5)):
        '''
        Plot the start amplitudes of those particles which survived at least up to turn 'smin'.
        '''
        data = self._get_data('results', ['emitx', 'emity', 'sturns1', 'sturns2'])
        d1 = data.loc[(data['sturns1'] > smin) & (data['sturns2'] > smin)]
    
        fig, ax = plt.subplots(figsize=figsize)
        ax.scatter(np.sqrt(d1['emitx'].values), np.sqrt(d1['emity'].values), s=s)
        ax.set_aspect('equal')

        if len(xlim) > 0:
            ax.set_xlim(xlim)
        if len(ylim) > 0:
            ax.set_ylim(ylim)

        plt.xlabel(r'$\sqrt{2 J_x}$')
        plt.ylabel(r'$\sqrt{2 J_y}$')
        #plt.legend(markerscale=6)
        plt.title('> {} turns'.format(smin), loc='right')
        plt.show()

    def plot_lost_particles(self, angle_indices, seeds=[], s=4, figsize=(12, 4)):
        '''
        Plot the amplitudes of lost particles versus the turn number, for given angles and seeds.
        If no seeds are specified, plot the data for all seeds.
        '''
        data = self._get_data('results', ['emitx', 'emity', 'sturns1', 'sturns2', 'angle', 'seed'])
        relevant_data = data.loc[(data['emitx'] > 0) & (data['emity'] > 0)]

        fig, ax = plt.subplots(figsize=figsize)

        for angle_index in angle_indices:
            angle = self.angles[angle_index]
            if len(seeds) > 0:
                data_angle = relevant_data.loc[(relevant_data['angle'] == angle) & relevant_data['seed'].isin(seeds)]
            else:
                data_angle = relevant_data.loc[(relevant_data['angle'] == angle)]
            radii = np.sqrt(data_angle['emitx'].values + data_angle['emity'].values)
            turns = np.min(np.array([data_angle['sturns1'].values, data_angle['sturns2'].values]), axis=0)

            ax.scatter(turns, radii, s=s, label=r'$\theta_{{{}}} = {{{:.3f}}}$'.format(angle_index, angle))

        ax.set_xlabel('Turn')
        ax.set_ylabel(r'$\sqrt{2 J_x + 2 J_y}$')
        ax.set_title('Lost particles; all tunes', loc='right')

        plt.legend(markerscale=3)
        plt.show()


def get_hole_indices(amplitudes, larger_than=1.2, turns=[], tmax=0, nodwalls=True):
    '''
    Small helper function to obtain the indices of a sequence of amplitudes which constitute a hole.
    The parameter larger_than controls how we determine the connected part. Namely we will be looking at
    the median of the differences of the amplitudes and check when the differences are smaller than larger_than*median.
    
    Returns the indices and the amplitude step.

    !!! it is assumed that input amplitudes are sorted !!!

    occurrences[0] means that a point having this index is at the border of the first hole, but not yet on the other side.

    nodwalls: If True, tmax > 0, and len(turns) > 0, then also remove double walls where there is one particle missing in between
          two particles which survived up to tmax (default: True).
    '''

    differences = np.diff(amplitudes)
    med_difference = np.median(differences[differences > 0])
    occurrences = np.where(differences > med_difference*larger_than)[0]
    occurrences = np.append(occurrences, len(differences)) # we also define the largest amplitude value to be at the lower border of an hole
    n_dwalls, lower_indices = 0, []
    if tmax > 0 and len(turns) > 0: # TODO check if replacement tmax = max(turns) works in all methods
        
        # check for double-walls with respect to given turn tmax
        occurrences_nodwall = []
        for e in occurrences[:-1]:
            if e - 1 >= 0 and e + 1 <= len(turns) - 1:
                if turns[e - 1] == tmax and turns[e + 1] == tmax:
                    continue
                occurrences_nodwall.append(e)
            else:
                occurrences_nodwall.append(e)
        n_dwalls = len(occurrences) - 1 - len(occurrences_nodwall)
        if nodwalls: # remove these double-walls
            occurrences = occurrences_nodwall + [len(differences)]

        # also get the lower indices, i.e. the indices of those amplitudes were there are particles lost
        lower_indices = np.where(turns[:occurrences[0] + 1] < tmax)[0]

    return occurrences, med_difference, n_dwalls, lower_indices


def detect_holes(data, **kwargs):
    '''
    Workaround method to detect the initial holes in the amplitudes: The
    initial amplitudes are currently not stored in the SQL files. Particles
    outside of stable regions are marked with survival turn -1.

    Requires fix in SixTrack to avoid using this routine.
    **kwargs are given to function get_hole_indices.
    '''
    # FIXME: need fix in SixTrack to avoid using this routine

    dwalls = []
    for iang in range(len(data)):
        data_angle = data[iang]
        x_coords = data_angle['rx']
        y_coords = data_angle['ry']
        turns_angle = data_angle['sturn']
        
        radii = np.sqrt(x_coords**2 + y_coords**2)
        indices_oi = np.where(radii > 0) # particles of radii 0 are not of interest to us (attention: In SixTrack emitx and emity are set to zero if a particle is lost within the first writebin turns)
        radii_oi = radii[indices_oi]
        occurrences, ampstep, n_dwalls_angle, lower_indices = get_hole_indices(radii_oi, turns=turns_angle[indices_oi], **kwargs)
        dwalls.append(n_dwalls_angle)        

        first_index_oi = indices_oi[0][0] # _get_surv is ordering the entries by their radii, so we can safely take the first index here
        occurrences += first_index_oi # correct indices by the shift above
        data_angle['sturn'][occurrences[0] + 1:] = 0 # the survival times from first_occurence (+1 because occurrence[0] index is not on the other side of the hole) onwards is set to 0
        if len(lower_indices) > 1: # to filter amplitudes smaller than the largest one, we also set the survival turns to tmax + 1 here
            data_angle['sturn'][:max([0, lower_indices[0] - 1])] = kwargs['tmax'] + 1 # n.b. if len(lower_indices) > 1, then 'tmax' in kwargs.keys()
    return data, dwalls


class pre_da(da_instance):
    '''
    Generic class to compute amplitudes of least stable particles.

    N.B. the notation is:
      xu = x_coords = rx = qx = sqrt(emitx) = sqrt(2*J_x) 
    '''

    def __init__(self, filename, turns, with_errors=False, **kwargs):
        self.turns_user = np.array(turns)
        da_instance.__init__(self, filename=filename, with_errors=with_errors, **kwargs)
        self.description = 'generic'

    def prepare(self, survival_data, **kwargs):
        '''
        This function prepares the SQL survival data of a specific seed and tune pair, so that it can
        be used by one of the pre-da methods.
        '''

        tmax = np.max(survival_data['sturn'][np.logical_or(survival_data['rx'] > 0, survival_data['ry'] > 0)]) # maximum number of turns; np.logical_ works with nested numpy arrays

        survival_data, dwalls = detect_holes(survival_data, tmax=tmax, **kwargs) # fix to cope with SixTrack output
        survival_turns = survival_data['sturn']
        qx = survival_data['rx']
        qy = survival_data['ry']
        survival_turns[np.logical_and(qx == 0, qy == 0)] = tmax + 1 # it can happen that there are entries with qx = 0 and qy = 0 which have 0 turns as survival turns (SixTrack writebinl issue; FIXME).

        self.qx, self.qy = qx, qy
        self.tmax = tmax
        self.survival_turns = survival_turns
        self.turns = self.turns_user[self.turns_user <= tmax]

        return dwalls

    def _get_survival(self, seed, tune_pair):
        '''get survival turns from DB calculated from emitI and emitII.'''
        # N.B. the function _get_survival
        # was derived (and modified) from original SixDeskDB

        (tunex, tuney) = tune_pair

        turnsl = self.env_var['turnsl']
        cmd = """SELECT angle, emitx, emity,
                 CASE WHEN sturns1 < sturns2 THEN sturns1 ELSE sturns2 END
                 FROM results WHERE seed={} AND tunex={} AND tuney={} AND turn_max={}
                 ORDER BY angle, emitx + emity""".format(seed, tunex, tuney, turnsl)

        #conn = sqlite3.connect(self.filename)
        cur = self.conn.cursor().execute(cmd)
        ftype = [('angle', float), ('emitx', float), ('emity', float), ('sturn', float)]
        data = np.fromiter(cur, dtype=ftype)

        out = np.ndarray(len(data), dtype=[('angle', float), ('rx', float), ('ry', float), ('sturn', float)])
        out['angle'] = data['angle']
        out['sturn'] = data['sturn']
        twoJx, twoJy = data['emitx'], data['emity']

        out['rx'] = np.sqrt(twoJx)  # The angles are taken wrt. rx and ry, the coordinates in Floquet-space.
        out['ry'] = np.sqrt(twoJy)
        # see also p. 33 in SixTrack Version 4.2.16: "Single Particle Tracking Code Treating Transverse Motion with
        # Synchrotron Oscillations in a Symplectic Manner" User's Reference Manual 2012 by F. Schmidt.

        try:
            out_rs = out.reshape(self.n_angles, -1)
            return out_rs 
        except ValueError:
            print(('Cannot reshape array of size {} into '.format(len(out))+
                   'shape ({}, newaxis). Skip this seed {}!'.format(self.n_angles, seed)))
            return None
        
    def run(self, nodwalls=True, verbose=True, **kwargs):
        '''
        nodwalls: see function get_hole_indices (default: True)
        '''
        out = []
        if verbose:
            start_time = timelib.time()

        self.conn = sqlite3.connect(self.filename)
        n_dwalls_total = 0 # to count number of double-walls (SixTrack writebinl feature)
        for seed in self.seeds:
            seed = int(seed)

            for tune_pair in self.tunes:
                if verbose:
                    print('\ranalyzing seed {}/{}, tune {} ...'.format(seed, len(self.seeds), tune_pair), end='')
                survival_data = self._get_survival(seed, tune_pair)

                if survival_data is None:
                    if verbose:
                        print("ERROR: survival data could not be retrieved due to "+
                              "an error in the database or tracking data. Skipping "
                              "this case: (seed, tune) = ({}, {})".format(seed, tune_pair))
                    continue

                dwalls = self.prepare(survival_data, nodwalls=nodwalls)
                n_dwalls_total += sum(dwalls)
                out.append(self.calc(ID=[seed, tune_pair[0], tune_pair[1]], **kwargs))
        self.conn.close()

        if verbose:
            print ('\nTime elapsed: {:.3f} [s]'.format(timelib.time() - start_time))
            print ('Double walls: {}'.format(n_dwalls_total))
            print ('flattening {} cases & storing results to SQL database ...'.format(len(out)))

        # store results in SQL database
        out = pd.DataFrame(np.concatenate(out, axis=0), columns=[fkey[0] for fkey in self._ftype])
        self._store_data(table_name=self._stable_unstable_table_name, data=out, verbose=verbose, dtype=self._ftype_str)


class pre_da_stable_unstable(pre_da):
    '''
    This class groups together certain similar methods to obtain stable and unstable particles for a given
    turn number.
    '''

    def __init__(self, **kwargs):
        pre_da.__init__(self, **kwargs)
        self._su_type = [('index_stable', int), ('index_unstable', int)]
        self.description = 'generic_su'

    def find_stable_unstable(self, turn, min_wrt_turns=False, smallest_stable_turn0=1e8):
        """
        Determine the indices of the particles which are stable and unstable for a given turn number.

        If min_wrt_turns = True, then the stable particle for the given turn is determined as those
        particle which will become next unstable. 

        Example:
        ========
                   amplitude
                   ^
                5  |---------*
                4  |---* <~unstable for turn t0
                3  |----------------*
                2  |------------* <~stable for turn t0 (next unstable)
                1  |--------------------------*
        index:  0  |-------------------------------* 
                   |       
                   +------|---------------------------> turn
                          t0

        For turn t0 therefore:
        index_stable = 2
        index_unstable = 4

        Be aware if there are already initial holes in the amplitudes, as this function is not catching such cases.

        If min_wrt_turns = False, then the stable and unstable particles are computed the 'classic' way, which means
        in the above example for the given turn t0:
        index_stable = 3
        index_unstable = 4
        """

        for iang in range(self.n_angles):
            t_ang = self.survival_turns[iang] # survival turns belonging to this angle, all amplitudes (sorted).

            if min_wrt_turns:
                smallest_stable_turn = smallest_stable_turn0

                index_stable, index_unstable = 0, -1
                running_index = 0
                for survival_turn in t_ang:
                    if survival_turn < turn:
                        index_unstable = running_index
                        break
                    else: # survival_turn >= turn
                        if survival_turn <= smallest_stable_turn:
                            index_stable = running_index # the index of the data point which will become the next unstable
                            smallest_stable_turn = survival_turn   
                    running_index += 1

            else: # classic way
                boolean_turn = t_ang >= turn # if qx_ang or qy_ang are small, t_ang is expected to be large, so the start of this array is usually filled with 'True'
                index_unstable = search.find_first(False, boolean_turn) # first loss as criteria, i.e lost after smaller or equal the given turn.
                if index_unstable == -1: # we reached the end of the list without finding any unstable point.
                    index_stable = -1
                else:
                    index_stable = max([0, index_unstable - 1]) # the index must not drop below zero.

            # the coordinates of the largest stable particle (note that the data must be sorted by their
            # amplitude qx + qy (or qx**2 + qy**2)
            self.stable_unstable_indices_at_turn[iang] = (index_stable, index_unstable)


    def get_current_coordinates(self, index_str='index_stable'):
        su_indices = self.stable_unstable_indices_at_turn

        qx, qy = [], []
        for k in range(self.n_angles):
            index_k = su_indices[index_str][k]

            if self.survival_turns[k][index_k] == -1:
                # FIXME
                # this is a special case in which index_k appeared from a hole, see function detect_holes.
                # in this case we reduce the index by one. This circumvents an issue in SixTrack related to the variable writebin.
                # Namely, to set the emittances to zero if a particle is lost below writebin. 
                # If this has been fixed in SixTrack, one can remove this index shift.
                index_k = index_k - 1

            qx.append(self.qx[k][index_k])
            qy.append(self.qy[k][index_k])

        return np.array(qx), np.array(qy)


    def get_current_turns(self, index_str='index_stable'):
        su_indices = self.stable_unstable_indices_at_turn

        turns = []
        for k in range(self.n_angles):
            index_k = su_indices[index_str][k]
            turns.append(self.survival_turns[k][index_k])
        return np.array(turns)

    def compute_default_errors(self, turn):
        '''
        This method computes some default errors for the given turn number and is used in some of the methods.
        '''
        xu, yu = self.get_current_coordinates('index_unstable')
        self.x_turn_error = abs(xu - self.x_turn)
        self.y_turn_error = abs(yu - self.y_turn)

        turns_stable = self.get_current_turns('index_stable')
        turns_unstable = self.get_current_turns('index_unstable')
        # we define the error in the turn number as the minimum of the two differences |turn - turn_stable| and
        # |turn - turn_unstable|
        self.turns_error = np.minimum(abs(turns_stable - turn), abs(turn - turns_unstable))
        self.turn_ref_error = error_of_average(self.turns_error)

    def calc(self, ID=[], **kwargs):
        '''
        Perform the pre-computation. ID will be inserted into the first columns.
        '''
        self.stable_unstable_indices_at_turn = np.zeros(self.n_angles, dtype=self._su_type) # a running list used in function find_stable_unstable.
        ll = len(self.turns)
        out = np.zeros([ll, len(self._ftype)])

        count = 0
        for turn in self.turns:
            self.find_stable_unstable(turn, **kwargs)
            self.method(turn)

            if not self.with_errors:
                out[count] = np.array([ID + [self.turn_ref] + \
                                      list(self.x_turn) + list(self.y_turn)])
            else:
                out[count] = np.array([ID + [self.turn_ref] + \
                                   list(self.x_turn) + list(self.y_turn) + \
                                   list(self.x_turn_error) + list(self.y_turn_error) + \
                                   list(self.turns_error) + [self.turn_ref_error]])
            count += 1

        return out[:count]


class pre_da_interpolate(pre_da_stable_unstable):
    '''
    Linear interpolate the DA between the unstable and the stable particle, if the given turn number 
    is within the survival turns of the current unstable and stable particle.
    '''
    def __init__(self, **kwargs):
        pre_da_stable_unstable.__init__(self, **kwargs)
        self.description = 'interpolate'

    def method(self, turn):
        qxs, qys = self.get_current_coordinates('index_stable')
        qxu, qyu = self.get_current_coordinates('index_unstable')

        # linear interpolate DA by using the survival turn numbers as estimate
        turns_stable = self.get_current_turns('index_stable')
        turns_unstable = self.get_current_turns('index_unstable')

        delta_turns = turns_stable - turns_unstable
        inverse_delta_turns = np.array([1/d if d > 0 else 0 for d in delta_turns])
        
        self.x_turn = qxs + (qxu - qxs)*inverse_delta_turns*(turns_stable - turn)
        self.y_turn = qys + (qyu - qys)*inverse_delta_turns*(turns_stable - turn)
        self.turn_ref = turn

        if self.with_errors:
            #self.x_turn_error = (np.absolute(self.x_turn - qxs) + np.absolute(qxu - self.x_turn))/2.0
            #self.y_turn_error = (np.absolute(self.y_turn - qys) + np.absolute(qyu - self.y_turn))/2.0
            #self.turns_error = (abs(turns_stable - turn) + abs(turn - turns_unstable))/2.0

            self.x_turn_error = np.minimum(np.absolute(self.x_turn - qxs), np.absolute(qxu - self.x_turn))
            self.y_turn_error = np.minimum(np.absolute(self.y_turn - qys), np.absolute(qyu - self.y_turn))
            self.turns_error = np.minimum(abs(turns_stable - turn), abs(turn - turns_unstable))

            self.turn_ref_error = error_of_average(self.turns_error) # using the formula of the rms of the average.
            #self.turn_ref_error = min(self.turns_error) # requested by MG as test.


class pre_da_default(pre_da_stable_unstable):
    '''
    The default method to compute the DA is by computing the stable amplitudes for a given turn number.
    '''
    def __init__(self, **kwargs):
        pre_da_stable_unstable.__init__(self, **kwargs)
        self.description = 'default'

    def method(self, turn):
        self.x_turn, self.y_turn = self.get_current_coordinates('index_stable')
        self.turn_ref = turn
        if self.with_errors:
            self.compute_default_errors(turn)

class pre_da_mean(pre_da_stable_unstable):
    def __init__(self, **kwargs):
        pre_da_stable_unstable.__init__(self, **kwargs)
        self.description = 'mean'

    def method(self, turn):
        self.x_turn, self.y_turn = self.get_current_coordinates('index_stable')
        turns_stable_turn = self.get_current_turns('index_stable')
        self.turn_ref = np.mean(turns_stable_turn)
        if self.with_errors:
            self.compute_default_errors(turn)

class pre_da_min(pre_da_stable_unstable):
    # FIXME: This method can yield different amplitudes to the same turn number.
    def __init__(self, **kwargs):
        pre_da_stable_unstable.__init__(self, **kwargs)
        self.description = 'min'

    def method(self, turn):
        self.x_turn, self.y_turn = self.get_current_coordinates('index_stable')
        turns_stable_turn = self.get_current_turns('index_stable')
        self.turn_ref = min(turns_stable_turn)
        if self.with_errors:
            self.compute_default_errors(turn)

class pre_da_maxnorm(pre_da_stable_unstable):
    def __init__(self, **kwargs):
        pre_da_stable_unstable.__init__(self, **kwargs)
        self.description = 'maxnorm'

    def method(self, turn):
        su_indices = self.stable_unstable_indices_at_turn
        radii_oo = np.array([max(self.qx[k][su_indices['index_stable'][k]], self.qy[k][su_indices['index_stable'][k]]) for k in range(self.n_angles)])
        kk = radii_oo.argmin()
        self.x_turn = np.array([self.qx[kk][su_indices['index_stable'][kk]]]*self.n_angles)
        self.y_turn = np.array([self.qy[kk][su_indices['index_stable'][kk]]]*self.n_angles)
        self.turn_ref = self.survival_turns[kk][su_indices['index_stable'][kk]]
        if self.with_errors:
            # temp. solution
            self.compute_default_errors(turn)


class pre_da_maximum(pre_da):
    def __init__(self, **kwargs):
        pre_da.__init__(self, **kwargs)
        self.description = 'maximum'

    def calc(self, ID=[]):
        '''
        Compute the stable amplitudes vs. turn number as maxima of the amplitudes of those cases which are lost
        at smaller the given turn number. 'prepare' must be run beforehand. 
        '''
        ll = len(self.turns)
        daout = np.ndarray(ll, dtype=self._ftype)

        mta = []
        for iang in range(self.n_angles): # corrsponds in looping over the different angles
            t_ang = self.survival_turns[iang] # survival turns belonging to this angle, all amplitudes.
            indices = range(len(t_ang))
            t_ang_s, indices_s = helpsort(t_ang, indices) 
            qx_ang_s = self.qx[iang][indices_s]
            qy_ang_s = self.qy[iang][indices_s]
            mta.append([t_ang_s, qx_ang_s, qy_ang_s])

        count = 0
        for turn in self.turns:
            qxout, qyout = [], []
            for iang in range(self.n_angles):
                sturns, qxs, qys = mta[iang]
                ww = np.where(sturns <= turn)
                if len(ww[0]) > 0:
                    qxout.append(min(qxs[ww]))
                    qyout.append(min(qys[ww]))

            # FIXME (to be changed later)
            turn_error = [1000]*self.n_angles
            av_turn_error = 1000
            qxout_error = qxout
            qyout_error = qyout

            if len(qxout) == self.n_angles and len(qyout) == self.n_angles:
                if not self.with_errors:
                    daout[count] = tuple(ID + [turn] + qxout + qyout)
                else:
                    daout[count] = tuple(ID + [turn] + \
                                      qxout + qyout + qxout_error + qyout_error + turn_error + [av_turn_error])
                count += 1

        return daout[:count]


class pre_da_unequal(da_instance):
    '''
    Generic class for routines to obtain data (turn, amplitude) of surviving particles in a SixTrack simulation, using the
    dynamics. I.e. in general an *unequal* number of surviving data points per angle.
    '''

    def __init__(self, filename, with_errors=False, **kwargs):
        da_instance.__init__(self, filename=filename, with_errors=with_errors)
        self.description = 'generic-unequal'

    def prepare_angle(self, data, nodwalls=True):
        '''
        Prepare a Pandas DataFrame to take into account SixTrack 'writebinl' issue.

        INPUT
        =====
        data: Pandas DataFrame having columns 'emit_sum', 'sturn' and where the maximal number of turns should not exceed tmax.

        OUTPUT
        ======
        Pandas DataFrame in which no holes appear in the amplitudes given by 'emit_sum'.
        '''
        # Remove all data having larger amplitudes when a given hole has been detected (which occured after removal of null-emittances).
        # (this step requires that also the data for sturn = tmax remain in the database)
        amplitudes_angle = np.sqrt(data['emit_sum'].values)
        sindices = np.argsort(amplitudes_angle) # routine get_hole_indices requires amplitudes to be sorted
        occurrences, ampstep, n_dwalls, lower_indices = get_hole_indices(amplitudes_angle[sindices], 
                                                                         turns=data['sturn'].values[sindices], 
                                                                         tmax=self.tmax, 
                                                                         nodwalls=nodwalls)

        # n.b. amplitudes[sindices[0]], amplitudes[sindices[1]], ... sorted ascending.
        # occurences[0] = sindices[k] for a certain index k. This index k will yield the appropriate entry in the DataFrame 
        # (which has originally been sorted by turns). We only keep the indices up to the first hole < tmax
        data_noholes = data.iloc[sindices[lower_indices]] # n.b. iloc refers to the position of the df and not its index

        return {'data': data_noholes, 'n_dwalls': n_dwalls, 'sindices': sindices, 
                'occurrences': occurrences, 'lower_indices': lower_indices}
        

    def get_decreasing_amplitudes(self, data, seed, tunex, tuney, angles, **kwargs):
        '''
        Takes a Pandas DataFrame with columns
          seed, tunex, tuney, angle, emitx, emity, sturn, emit_sum
        and determines, for every given angle, a decreasing sequence of (turn, amplitude) pairs which denote
        those particles which are lost in the course of the simulation.
    
        Keyworded arguments are given to function 'prepare'.

        Returns list of sequences of these amplitudes in x, y and their respective turns. Furthermore the number of observed double-walls.
        '''
        n_dwalls_total = 0
        amplitudes_x_all_angles, amplitudes_y_all_angles, stable_turns_all_angles = [], [], []
        for angle in angles:

            data_angle = data.loc[(data['seed'] == seed) &
                                  (data['tunex'] == tunex) & 
                                  (data['tuney'] == tuney) &
                                  (data['angle'] == angle)]

            # Prepare data according to holes (SixTrack writebinl issue)
            prepared_data_dict = self.prepare_angle(data_angle, **kwargs)
            data_angle_turn_sorted = prepared_data_dict['data'].sort_values(by=['sturn'])
            n_dwalls_total += prepared_data_dict['n_dwalls']

            # Now the data is prepared to determine the sequence of stable amplitudes and their turns 
            # for this specific angle
            stable_region = data_angle_turn_sorted.loc[(data_angle_turn_sorted.emit_sum <= data_angle_turn_sorted.emit_sum.cummin())].drop_duplicates(subset=['sturn'], keep='last')

            # TODO check if .groupby is faster; see
            # https://stackoverflow.com/questions/32093829/remove-duplicates-from-dataframe-based-on-two-columns-a-b-keeping-row-with-max

            amplitudes_x_all_angles.append(np.sqrt(stable_region['emitx'].values))
            amplitudes_y_all_angles.append(np.sqrt(stable_region['emity'].values))
            stable_turns_all_angles.append(stable_region['sturn'].values)

        return amplitudes_x_all_angles, amplitudes_y_all_angles, stable_turns_all_angles, n_dwalls_total

    def _get_border_index(self, lower_indices, sindices, occurrences):
        '''
        From the output of prepare_angle, determine the index of the particle having the largest amplitude and which survived up to tmax.
        '''
        if len(lower_indices) > 0:
            border_index = sindices[max([0, lower_indices[0] - 1])] # n.b. if lower_indices[0] = 0, the data is bad enough that likely an error will occur later on when we remove duplicates.
        else:
            border_index = sindices[occurrences[0]]
        return border_index

    def prepare(self):
        '''
        Loads the 'results' table of the SQL database and prepare a Pandas DataFrame containing essential information.
        Sets self.tmax and self.ampstep.
        '''
        survival_data = self._get_data('results', ['six_input_id', 'seed', 'tunex', 'tuney', 'emitx', 'emity', 'sturns1', 'sturns2', 'angle'])

        # for criteria to find particles with largest amplitude:
        survival_data['emit_sum'] = survival_data['emitx'] + survival_data['emity'] # = qx**2 + qy**2
        s = np.sqrt(survival_data['emit_sum'].values)
        self.ampstep = np.abs((s[s > 0][1]) - (s[s > 0][0])) # the amplitude step used in the simulation; taken from original SixDeskDB
        # self.ampstep = np.median(np.diff(s[s > 0])) # alternative

        survival_data['sturn'] = survival_data[['sturns1', 'sturns2']].min(axis=1)
        self.tmax = survival_data['sturn'].max()
        
        # we do not consider exotic cases with amplitude 0 and also do not consider cases surviving up to self.tmax
        #survival_data = survival_data.loc[(survival_data['emit_sum'] > 0) & (survival_data['sturn'] < self.tmax)] 
        #survival_data = survival_data.sort_values(by=['sturn'])
        survival_data = survival_data.loc[survival_data['emit_sum'] > 0] 

        return survival_data

    def run(self, nodwalls=True, verbose=True, **kwargs):
        '''
        **kwargs: additional arguments transported to self.method
        '''
        survival_data = self.prepare()

        if verbose:
            start_time = timelib.time()

        out = []
        n_dwalls_total = 0 # to count number of double-walls (SixTrack writebinl feature)
        for tune_pair, seed in product(self.tunes, self.seeds):
            seed = int(seed)
            tunex, tuney = tune_pair
            if verbose:
                print('\ranalyzing data of seed {}/{}, tune {} ...'.format(seed, len(self.seeds), tune_pair), end='')

            amplitudes_x_all_angles, amplitudes_y_all_angles, stable_turns_all_angles, n_dwalls = self.get_decreasing_amplitudes(data=survival_data, seed=seed, tunex=tunex, tuney=tuney, angles=self.angles, nodwalls=nodwalls)
            n_dwalls_total += n_dwalls

            ax, ay, turns, ax_err, ay_err, individual_turn_errors, turn_errors = self.method(amplitudes_x_all_angles, amplitudes_y_all_angles, stable_turns_all_angles, **kwargs)

            if self.with_errors:
                datablock = np.concatenate([[[seed]*len(turns), [tunex]*len(turns), [tuney]*len(turns), turns], 
                                            ax, ay, ax_err, ay_err, individual_turn_errors, turn_errors])
            else:
                datablock = np.concatenate([[[seed]*len(turns), [tunex]*len(turns), [tuney]*len(turns), turns], 
                                            ax, ay])

            out.append(pd.DataFrame(datablock.transpose(), columns=[k[0] for k in self._ftype]))

        if verbose:
            print ('\nTime elapsed: {:.3f} [s]'.format(timelib.time() - start_time))
            print ('Double walls: {}'.format(n_dwalls_total))
            print ('Storing results to SQL database ...')

        # store results
        out = pd.DataFrame(np.concatenate(out, axis=0), columns=[fkey[0] for fkey in self._ftype])
        self._store_data(table_name=self._stable_unstable_table_name, data=out, verbose=verbose, dtype=self._ftype_str)


class pre_da_mtunequal(pre_da_unequal):
    '''
    Compute the stable/unstable particles while the resulting turn numbers may be unequally spaced.
    '''

    def __init__(self, filename, with_errors=False, **kwargs):
        pre_da_unequal.__init__(self, filename=filename, with_errors=with_errors)
        self.description = 'mt-unequal'

    @staticmethod
    def _get_opt(turns, **kwargs):
        '''
        This routine takes a set of turns and computes their rms value (depending on the given arguments in **kwargs). 
        This rms value is then used as an error estimate for
        each individual turn. Then, based on these individual error estimates, the error of the average of all of these
        turns is computed, using the propagation law of the average.
        '''
        opt_rms, opt = get_rms_value(turns, **kwargs)
        # we will assume that opt_rms is the typical error of an individual turn. Hence: 
        turns_err = np.array([opt_rms]*len(turns))
        opt_err = error_of_average(turns_err)
        return opt, opt_err, turns_err

    def get_common_turns(self, turn_collection, **kwargs):
        '''
        Input:
        turn_collection: A list of arrays, in general of unequal lengths. Each array is assumed to represent an ordered sequence of turns.
        **kwargs: arguments transported to get_rms_value
        Output:
        1) An array of unique turns so that each turn T is the median of a group of turns in the collection. Hereby the group contains one
        turn per given turn list, and so that the group is close together.
        2) The overall errors as the rms spread.
        3) The individual turns used in the calculation
        4) The individual differences to each T (i.e. the individual errors)
        5) The indices of the turns used to determine the average turns from the turn_collection
        '''
        # Step 1: get borders defining regions where we expect unique common turns.
        borders = []
        k = 0
        for turns in turn_collection:
            m = len(turns) - 1
            borders += list(zip((turns[:-1] + turns[1:])/2, [k]*m, range(m)))
            k += 1
        borders = sorted(borders)
        # 'borders' is a list of 3-tuples of the form (F, G, H). The meaning is as follows:
        # F denotes a border where a given turn T would be closest to the turn turn_collection[G][H]
    
        # Step 2: Obtain common turns
        get_turns_group = lambda indices: np.array([turn_collection[k][indices[k]] for k in range(len(turn_collection))])

        point_indices = np.zeros(len(turn_collection), dtype='int')
        point_indices_all = [np.copy(point_indices)]
    
        selected_turns = get_turns_group(point_indices)
        opt, opt_err, selected_turns_err = self._get_opt(selected_turns, **kwargs)
    
        individual_turns, individual_errors = [selected_turns], [selected_turns_err]
        turns, errors = [opt], [opt_err]
        for triple in borders:
            point_indices[triple[1]] = triple[2] + 1
            point_indices_all.append(np.copy(point_indices))
            selected_turns = get_turns_group(point_indices)
            opt, opt_err, selected_turns_err = self._get_opt(selected_turns, **kwargs)
            individual_turns.append(selected_turns)
            individual_errors.append(selected_turns_err)
            turns.append(opt)
            errors.append(opt_err)

        return np.array(turns), np.array(errors), np.array(individual_turns), np.array(individual_errors).T, np.array(point_indices_all)

    def prepare_angle(self, data, nodwalls=True):
        '''
        Sort given data to be by their survival turns.
        '''
        prepared_data_dict = pre_da_unequal.prepare_angle(self, data=data, nodwalls=nodwalls)
        data_noholes = prepared_data_dict['data']
        sindices = prepared_data_dict['sindices']
        occurrences = prepared_data_dict['occurrences']
        lower_indices = prepared_data_dict['lower_indices']

        border_index = self._get_border_index(lower_indices, sindices, occurrences)
        # ! note that the next operations may affect any further use of 'sindices' and other index entries of prepared_data_dict

        # Check for insufficient data. Note that by construction there are no particles with tmax in data.
        if len(data_noholes) == 0:
            # In this case we load the border point and set its survival turn to tmax/2, as well as increase its amplitude
            # by half of one amplitude step
            data_noholes = data.iloc[[border_index]].copy()
            data_noholes['sturn'] = data_noholes['sturn'].map(lambda x: int(x/2)) 
            angle = data_noholes['angle'].values
            shift_x = np.cos(angle/180*np.pi)**2*self.ampstep*0.5
            shift_y = np.sin(angle/180*np.pi)**2*self.ampstep*0.5
            data_noholes['emitx'] += shift_x
            data_noholes['emity'] += shift_y
            data_noholes['emit_sum'] += self.ampstep*0.5

        prepared_data_dict['data'] = data_noholes
        return prepared_data_dict

    def method(self, amplitudes_x_all_angles, amplitudes_y_all_angles, stable_turns_all_angles, **kwargs):
        # Apply the algorithm to combine the data points of the different angles
        turns, turn_errors, individual_turns, individual_turn_errors, indices = self.get_common_turns(stable_turns_all_angles, **kwargs)

        ax = np.array([amplitudes_x_all_angles[k][indices[:,k]] for k in range(self.n_angles)]) # n.b. ax.shape = (self.n_angles, len(turns))
        ay = np.array([amplitudes_y_all_angles[k][indices[:,k]] for k in range(self.n_angles)])

        ax_err = np.array([[0.5*self.ampstep]*len(turns)]*self.n_angles)
        ay_err = ax_err # = np.array([[0.5*self.ampstep]*len(turns)]*self.n_angles)

        return ax, ay, turns, ax_err, ay_err, individual_turn_errors, np.array([turn_errors])


from scipy.interpolate import pchip, CubicSpline

class pre_da_spline(pre_da_unequal):
    '''
    Interpolate the DA between the unstable and the stable particle, if the given turn number 
    is within the survival turns of the current unstable and stable particle. The interpolation is
    using monotonic splines from scipy.interpolate.pchip.
    '''
    def __init__(self, turns, **kwargs):
        pre_da_unequal.__init__(self, **kwargs)
        self.turns_user = np.array(turns)
        self.description = 'spline'

    def prepare(self, **kwargs):
        survival_data = pre_da_unequal.prepare(self, **kwargs)
        self.turns = self.turns_user[self.turns_user <= self.tmax]
        return survival_data

    def prepare_angle(self, data, nodwalls=True):
        '''
        Sort given data to be by their survival turns. Here we keep the turns with tmax.
        '''
        prepared_data_dict = pre_da_unequal.prepare_angle(self, data=data, nodwalls=nodwalls)
        data_noholes = prepared_data_dict['data']
        sindices = prepared_data_dict['sindices']
        lower_indices = prepared_data_dict['lower_indices'] 
        occurrences = prepared_data_dict['occurrences']   

        border_index = self._get_border_index(lower_indices, sindices, occurrences)
        # ! note that the next operations may affect any further use of 'sindices' and other index entries of prepared_data_dict

        # Always add the border point at tmax, because, in any case, spline interpolation should not extrapolate by itself to tmax
        data_end = data.iloc[[border_index]].copy()
        data_noholes = data_noholes.append(data_end)

        # Check for insufficient data. Here this means that we have only one data point.
        if len(data_noholes) == 1:
            # In this case we load the border point and set its survival turn to 0, 
            # as well as increase its amplitude by one amplitude step
            data_start = data.iloc[[border_index]].copy()
            data_start['sturn'] = data_start['sturn'].map(lambda x: 0) 
            angle = data_start['angle'].values
            shift_x = np.cos(angle/180*np.pi)**2*self.ampstep
            shift_y = np.sin(angle/180*np.pi)**2*self.ampstep
            data_start['emitx'] += shift_x
            data_start['emity'] += shift_y
            data_start['emit_sum'] += self.ampstep
            data_noholes = data_noholes.append(data_start)

        prepared_data_dict['data'] = data_noholes
        return prepared_data_dict

    def method(self, amplitudes_x_all_angles, amplitudes_y_all_angles, stable_turns_all_angles, kmin=2):

        ax, ay = [], []
        for angle_index in range(self.n_angles):
            a_x = amplitudes_x_all_angles[angle_index]
            a_y = amplitudes_y_all_angles[angle_index]
            a_turns = stable_turns_all_angles[angle_index]

            # we can not go below a minimum turn number otherwise issue with extrapolation in pchip
            turns_pchip = [t for t in self.turns if t >= min(a_turns)]
            turns_ext = [t for t in self.turns if t < min(a_turns)]

            k = min([kmin, len(a_turns)]) # number of data points taken into account to extrapolate data before min(a_turns)
            interp_x_ext = CubicSpline(a_turns[:k], a_x[:k])
            interp_y_ext = CubicSpline(a_turns[:k], a_y[:k])

            interp_x = pchip(a_turns, a_x)
            interp_y = pchip(a_turns, a_y)

            ax.append(np.concatenate([interp_x_ext(turns_ext), interp_x(turns_pchip)]))
            ay.append(np.concatenate([interp_y_ext(turns_ext), interp_y(turns_pchip)]))

        ax = np.array(ax)
        ay = np.array(ay)
        turns = self.turns

        ax_err = np.zeros(ax.shape)
        ay_err = np.zeros(ay.shape)
        turn_errors = np.zeros((1, len(turns)))
        individual_turn_errors = np.zeros(ax.shape)

        return ax, ay, turns, ax_err, ay_err, individual_turn_errors, turn_errors


class pre_da_original(pre_da):
    '''
    Use original SixDeskDB code snippets.
    Constructs a 'sigma' value based on the sum: emitx + emity.

    Child of pre_da to get self.run.
    self.calc, self.prepare and self._get_survival are overwritten according to original SixDeskDB routines.
    '''
    def __init__(self, filename, turns, with_errors=False, **kwargs):
        self.turns_user = np.array(turns)
        pre_da.__init__(self, filename=filename, turns=turns, with_errors=with_errors, **kwargs)
        self.description = 'original (DA vs Turns)'

    def _get_survival(self, seed, tune_pair):
        '''get survival turns from DB calculated from emitI and emitII'''

        (tunex, tuney) = tune_pair
        # emit = float(self.env_var['emit'])
        # gamma = float(self.env_var['gamma'])
        turnsl = self.env_var['turnsl']
        cmd="""SELECT angle, emitx + emity,
            CASE WHEN sturns1 < sturns2 THEN sturns1 ELSE sturns2 END
            FROM results WHERE seed=%s AND tunex=%s AND tuney=%s AND turn_max=%s
            ORDER BY angle, emitx + emity"""
        cur = self.conn.cursor().execute(cmd%(seed, tunex, tuney, turnsl))
        ftype = [('angle', float), ('sigma', float), ('sturn', float)]
        data = np.fromiter(cur, dtype=ftype)
        data['sigma'] = np.sqrt(data['sigma']) #/(emit/gamma)) # emit & gamma will be treated in the DA calculation;
        angles = len(set(data['angle']))
        try:
            return data.reshape(angles, -1)
        except ValueError:
            print("Cannot reshape array of size %s into "%(len(data)) +
                  "shape (%s,newaxis). Skip this seed %s!"%(angles, seed))
            return None

    def prepare(self, survival_data, **kwargs):
        s, a, t = survival_data['sigma'], survival_data['angle'], survival_data['sturn']
        tmax = np.max(t[s > 0]) # maximum number of turns
        t[s == 0] = tmax*100

        self.s = s
        self.a = a
        self.survival_turns = t
        self.turns = self.turns_user[self.turns_user <= tmax]
        self.n_angles = len(a)
        self.ampstep = np.abs((s[s > 0][1]) - (s[s > 0][0])) # the amplitude step (in terms of sigma's) used in the simulation.
        
        return [-1] # the original method does not check for double-walls. We set a default value here.

    def find_stable_unstable(self, turn): # from code davsturns.get_min_turn_ang
        """
        returns array with (angle,minimum sigma,sturn) of particles with lost turn number < turn.

        check if there is a particle with angle ang with lost turn number < turn
        if true: lost turn number and amplitude of the last stable particle is saved = particle "before" the particle with the smallest amplitude with nturns<it
        if false: the smallest lost turn number and the largest amplitude is saved
        """
        # s, t, a are ordered by angle, amplitude
        angles, sigmas = self.survival_turns.shape # angles = number of angles, sigmas = number of amplitudes
        ftype = [('angle',float), ('sigma',float), ('sturn',float)]
        mta = np.zeros(angles, dtype=ftype)
        # enumerate(a[:,0]) returns (0, a[0]), (1, a[1]), (2, a[2]), ... = iang, ang where iang = index of the array (0, 1, 2, ...) for ang = angle (e.g. [1.5, ... , 1.5] , [3.0, ... ,3.0])
        for iang, ang in enumerate(self.a[:,0]):
            tang = self.survival_turns[iang]
            sang = self.s[iang]
            iturn = tang < turn # select lost turn number < it
            if any(tang[iturn]):
                sangit = sang[iturn].min()
                argminit = sang.searchsorted(sangit) # get index of smallest amplitude with sturn < turn - amplitudes are ordered ascending
                mta[iang] = (ang, sang[argminit - 1], tang[argminit - 1]) # last stable amplitude -> index argminit - 1
            else:
                mta[iang] = (ang, sang.max(), tang.min())
        return mta

    def calc(self, ID=[], **kwargs):
        '''
        Perform the pre-computation according to original code. This has been checked using mode=original, method=simpson and
        comparing to original DA data: (nturnavg, dawsimp)
        '''
        n_turns = len(self.turns)
        out = np.zeros([n_turns, len(self._ftype)])

        turnstep = 0
        if len(self.turns) > 0:
            turnstep = self.turns[1] - self.turns[0]

        count = 0
        for k in range(n_turns):
            turn = self.turns[k]
            mta = self.find_stable_unstable(turn)
            mta_angle = mta['angle']*np.pi/180 # convert to rad
            mta_sigma = mta['sigma'] # n.b. sigma = sqrt(2*J_x + 2*J_y) = sqrt(qx**2 + qy**2)

            qx = mta_sigma*np.cos(mta_angle)
            qy = mta_sigma*np.sin(mta_angle)

            tlossmin = np.min(mta['sturn'])

            if 0 < k and k < n_turns - 1:
                if all(qx == old_qx) and all(qy == old_qy) or old_tlossmin == tlossmin:
                    continue
            old_tlossmin = tlossmin
            old_qx = qx
            old_qy = qy

            nturnavg = (turn - turnstep + tlossmin)/2.0 # behavior in original code

            if not self.with_errors:
                out[count] = np.array([ID + [nturnavg] + \
                                      list(qx) + list(qy)])
            else:
                # The following equations for sigma_x and sigma_y follow from the formula of error-propagation of the 'radius'
                # f(qx, qy) := sqrt(qx**2 + qy**2) as follows: From the definition we have
                # sigma_f = sqrt((sigma_x**2*qx**2 + sigma_y**2*qy**2)/(qx**2 + qy**2)).
                # Now if qx = r*cos(theta), y = r*sin(theta), then
                # sigma_f = sqrt(sigma_x**2*cos(theta)**2 + sigma_y**2*sin(theta)**2)), so that we obtain
                # sigma_f = 0.5*self.ampstep -- which has been used as the assumed error in the original SixDeskDB code -- 
                # via:
                sigma_x = [0.5*self.ampstep]*self.n_angles
                sigma_y = sigma_x # = [0.5*self.ampstep]*self.n_angles

                # The errors in the turn(s):
                span = np.median(mta['sturn'] - nturnavg)
                span = min([span, nturnavg]) # if span > nturnavg, then span = nturnavg (gurantees that error bar does not reach negative values)
                turns_errors = [span]*self.n_angles
                turn_ref_error = error_of_average(turns_errors)

                out[count] = np.array([ID + [nturnavg] + \
                                   list(qx) + list(qy) + \
                                   sigma_x + sigma_y + \
                                   turns_errors + [turn_ref_error]])

            count += 1

        return out[:count]

############################################################################
### Methods to compute the DA
############################################################################

class compute_da(six_handler):
    def __init__(self, filenames, verbose=True, **kwargs):
        six_handler.__init__(self, filenames=filenames, instance=davst, **kwargs)
        if verbose:
            if 'with_errors' in kwargs.keys():
                print ('with errors: {}'.format(kwargs['with_errors']))


    def run(self, emittances=[],  verbose=True, **kwargs):
        '''
        Computes the DA of the SQL databases of interest.

        INPUT
        =====
        sql_filenames: 
            list of SQL database filenames of interest.

        emittances: 
            An optional list of emittance pairs to compute the DA.

        **kwargs:
            Further arguments passed to davst.da method.
        '''
        for obj in self.objects:
            obj.da(emittances=emittances, verbose=verbose, **kwargs)


class davst(da_instance):
    '''
    Methods to compute dynamic aperture vs. turn from SixTrack SQL database.
    '''

    def __init__(self, filename, with_errors=False):
        da_instance.__init__(self, filename=filename, with_errors=with_errors)
        self._min_datapoints = 8 # min. number of data points required for fit
        self._da_table_name = 'da_emit'

    def da(self, emittances=[], method='simpson', save=True,
           verbose=True, start_index=None, stop_index=None, mode='classic', external=False):
        '''
        For every seed and tune-pair of interest compute the DA. This is only carried out if more than 
        self._min_datapoints are available.

        start_index, stop_index: indices from which to take the data points.
        emittances: A list of emittance pairs to be considered. If no pair is provided, the emittances found in self.env_var['emit_beam'] are used.

        An index 'emit_index' is created in order to keep track of the number of emittance pairs (for example,
        if connecting an emittance pair to a specific bunch number).
        
        external: If True, then store the results to external .txt files (only if save=True)
        '''

        if len(emittances) == 0:
            if 'emit_beam' in self.env_var.keys():
                emit_default = self.env_var['emit_beam']
                emittances = [(emit_default, emit_default)]
                if verbose:
                    print ('Using default emittance: {}'.format(emit_default))
            else:
                print ('ERROR: No emittances provided and no default emittances found.')
                return


        if mode == 'classic':
            selected_da_formula = self.da_formula
        if mode == 'rms':
            selected_da_formula = self.darms_formula
        if mode == 'original':
            selected_da_formula = self.da4d_formula

        aj, h = get_integration_scheme(N=self.n_angles, method=method)
        dtheta = np.pi/2.0*h

        data = self._get_data(table_names=self._stable_unstable_table_name)

        # loop over seeds which correspond to the different simulation cases
        if verbose:
            print('    gamma = {}'.format(self.gamma))
            print('     da mode: {}'.format(mode))
            print(' int. method: {}'.format(method))
            print(' start_index: {}'.format(start_index))
            print('  stop_index: {}'.format(stop_index))
            start = timelib.time()

        da_emit = []
        for tune_pair, seed in product(self.tunes, self.seeds):
            tunex, tuney = tune_pair
            data_case = data.loc[(data['seed'] == seed) &
                                 (data['tunex'] == tunex) &
                                 (data['tuney'] == tuney)]

            if len(data_case) < self._min_datapoints:
                print("ERROR: At least {0} data points required.".format(self._min_datapoints))
                continue
            turns = data_case['turn'][start_index:stop_index].values
            n_turns = len(turns)
            if n_turns == 0:
                print ('ERROR: len(turns) = 0. Check start_index & stop_index.')          

            emit_index = 0
            for emit_pair in emittances:
                emitnx, emitny = emit_pair
                emitx, emity = emitnx/self.gamma/self.beta, emitny/self.gamma/self.beta

                if verbose:
                    print('\r  DA calculation of seed {}/{}, tune ({}, {}) ... emittances: ({}, {})'.format(seed, 
                          len(self.seeds), tunex, tuney, emitnx, emitny), end='')

                x2, y2 = [], []
                for iang in range(self.n_angles):
                    qxdat2 = data_case['surv_qx_theta{}'.format(iang)].values[start_index:stop_index]**2 # vector of qx**2 data with respect to the given turns
                    qydat2 = data_case['surv_qy_theta{}'.format(iang)].values[start_index:stop_index]**2

                    x2.append(qxdat2)
                    y2.append(qydat2)

                if self.with_errors:
                    x2_error, y2_error = [], []
                    for iang in range(self.n_angles):
                        qxdat2_error = data_case['surv_qx_theta{}_error'.format(iang)].values[start_index:stop_index]**2 # vector of qx**2 data with respect to the given turns
                        qydat2_error = data_case['surv_qy_theta{}_error'.format(iang)].values[start_index:stop_index]**2
 
                        x2_error.append(qxdat2_error)
                        y2_error.append(qydat2_error)
                    x2_error, y2_error = np.array(x2_error), np.array(y2_error)
                    turns_error = data_case['av_turn_error'].values[start_index:stop_index]
                else:
                    x2_error, y2_error = np.zeros(len(x2)), np.zeros(len(y2))
                    turns_error = np.zeros(n_turns)

                # By using the transposition in combination with numpy arrays, we can compute all DA's for all turns
                # at once, for all angles.
                da, da_err = selected_da_formula(x2=np.array(x2).T, y2=np.array(y2).T, ex=emitx, ey=emity, aj=aj, dtheta=dtheta, 
                                        sigmax2=np.array(x2_error).T, sigmay2=np.array(y2_error).T)

                emit_index += 1

                da_emit.append([[seed]*n_turns, [tunex]*n_turns, [tuney]*n_turns, 
                                         [emit_index]*n_turns, [emitnx]*n_turns, [emitny]*n_turns, 
                                          turns, turns_error, da, da_err])

        da_emit = np.hstack(da_emit).T

        if verbose:
            end = timelib.time()
            print ('\nTime elapsed: {:.3f} [s]'.format(end - start))

        da_columns = ['seed', 'tunex', 'tuney', 'emit_index', 'emitnx', 'emitny', 'turn', 'turn_error', 'da', 'da_error']
        self.da_out = pd.DataFrame(da_emit, columns=da_columns).astype({'seed': 'int', 'emit_index': 'int'})
        if save:
            if external:
                np.savetxt('{}/{}.txt'.format(os.path.dirname(self.filename), self._da_table_name), self.da_out.values)
            else:
                self._store_data(table_name=self._da_table_name, data=self.da_out, verbose=verbose, if_exists='replace', chunksize=10000)

    @staticmethod
    def da_formula(x2, y2, ex, ey, aj, dtheta, sigmax2=0, sigmay2=0):
        '''
        x2 and y2 are expected to be lists of numpy arrays of coordinates (squared!) with respect to equally spaced angles of
        dtheta. If x and y have errors sigmax and sigmay, then the routine also computes
        the resulting error in the DA, based on the input sigmax2 := sigmax**2 and sigmay2 := sigmay**2.
        '''
        integrand2 = x2/ex + y2/ey
        da = 2/np.pi*sum((aj*np.sqrt(integrand2)*dtheta).T) # = D_{sigma_x, sigma_y} in Eq. (8) in the notes
        if len(sigmax2) > 0 or len(sigmay2) > 0: # in this case we compute the error in the DA (assuming no cross-correlations)
            inverse_integrand2 = np.reciprocal(integrand2, out=np.zeros(integrand2.shape), where=integrand2 > 0)
            da_err_int2 = (x2/ex**2*sigmax2 + y2/ey**2*sigmay2)*inverse_integrand2
            da_err = 2/np.pi*np.sqrt(sum((aj**2*da_err_int2*dtheta**2).T)) # the rms error of the da for all turns
        else:
            da_err = 0
        return da, da_err

    @staticmethod
    def darms_formula(x2, y2, ex, ey, aj, dtheta, sigmax2=0, sigmay2=0):
        '''
        x2 and y2 are expected to be lists of numpy arrays of coordinates (squared!) with respect to equally spaced angles of
        dtheta. If x and y have errors sigmax and sigmay, then the routine also computes
        the resulting error in the DA (rms version), based on the input sigmax2 := sigmax**2 and sigmay2 := sigmay**2.
        '''
        integrand2 = x2/ex + y2/ey
        sumterm = sum((aj*integrand2*dtheta).T)
        darms = np.sqrt(2/np.pi*sumterm) # = np.sqrt(D_{sigma_x, sigma_y; rms}^2) in Eq. (8) in the notes
        if len(sigmax2) > 0 or len(sigmay2) > 0: # in this case we compute the error in the DA-rms (assuming no cross-correlations)
            darms_err_all2 = 2/np.pi*sum((aj**2*dtheta**2*(sigmax2*x2/ex**2 + sigmay2*y2/ey**2)).T)/sumterm # this is the error**2 of the integrand for all angles and all turns.
            darms_err = np.sqrt(darms_err_all2) # the rms error of darms for all turns
        else:
            darms_err = 0
        return darms, darms_err

    @staticmethod
    def da4d_formula(x2, y2, ex, ey, aj, dtheta, sigmax2=0, sigmay2=0):
        '''
        DA calculation via Eq. (13) in Ref. [2], using a 4D integration (the integrand is considered to be independent on
        theta_1 and theta_2). This version corresponds to the one implemented in the original SixDeskDB code.
        '''
        integrand4 = (x2/ex + y2/ey)**2
        theta = np.linspace(dtheta, np.pi/2 - dtheta, len(integrand4.T))
        sumterm = sum((aj*integrand4*np.sin(2*theta)*dtheta).T)
        da = sumterm**(1/4)
        if len(sigmax2) > 0 or len(sigmay2) > 0: # in this case we compute the error in the DA (assuming no cross-correlations)
            da_err2 = sumterm**(-3/2)*sum((aj**2*np.sin(2*theta)**2*dtheta**2*integrand4*(sigmax2*x2/ex**2 + sigmay2*y2/ey**2)).T)
            da_err = np.sqrt(da_err2)
        else:
            da_err = 0
        return da, da_err


def get_integration_scheme(N, method):
    '''
    Function to obtain the step sizes of a given integration scheme for a given number N of data points.
    '''

    if method == 'simpson':
        # Eq. (4.1.18) in Ref. [1], p.129
        if N > 6:
            # define coefficients for simpson rule (simp)
            aj_simp_s = np.array([55/24., -1/6., 11/8.]) # Simpson rule
            aj_simp_e = np.array([11/8., -1/6., 55/24.])
            aj = np.concatenate((aj_simp_s, np.ones(N - 6), aj_simp_e))
            h = 1/(N + 1) # the integration step size in Ref. [1] p.125. It holds: sum(aj) = N + 1.
        else:
            print('ERROR with integration scheme - at least 7 data points required for the simpson rule!')
            sys.exit(0)
    if method == 'trapezoid':
        # Eq. (4.1.15) in Ref. [1], p.129
        if N > 2:
            # define coefficients for trapezoid rule
            aj_trap_s = np.array([3/2.])
            aj_trap_e = np.array([3/2.])
            aj = np.concatenate((aj_trap_s, np.ones(N - 2), aj_trap_e))
            h = 1/(N + 1) # the integration step size in Ref. [1] p.125. It holds: sum(aj) = N + 1.
        else:
            print('ERROR with integration scheme - at least 3 data points required for the trapezoid rule!')
            sys.exit(0)
    if method == 'uniform':
        aj = np.ones(N)
        h = 1/N
    return aj, h

