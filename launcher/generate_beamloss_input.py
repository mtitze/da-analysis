import glob, json
import numpy as np
from itertools import product
import pandas as pd

'''
This is an example of how to generate the input dictionary required for a beam loss study.

(!) Do not forget to adjust the HTCondor parameters in submit.py
'''

year = 2017
ioct = 40
beam_nr = 1


def generate_input_dict(beam_nr, year, ioct):

    print('beam_nr: {} year: {} ioct: {}'.format(beam_nr, year, ioct))

    target_filename = '/afs/cern.ch/work/m/mtitze/sim/da/beamloss/beam{}/{}-ioct{}/beamloss.inp'.format(beam_nr, year, ioct)
    print('target_filename:\n {}'.format(target_filename))

    job_input = {}
    job_input['description'] = 'beamloss'

    job_input['sql_reference_files'] = glob.glob('/afs/cern.ch/user/m/mtitze/eos/da/first_12/sql_files_1/b{}_{}_ioct{}/*.db'.format(beam_nr, year, ioct))
    #job_input['sql_reference_files'] = glob.glob('/eos/user/m/mtitze/from_afswork/hl-lhc/sixtrack_data_beamloss/{}/beam{}/ioct{}/*.db'.format(year, beam_nr, ioct))

    pre_da_methods = ['default', 'interpolate', 'mt-unequal']
    da_modes = ['rms', 'original']
    integration_methods = ['simpson']
    da_models = ['model2b', 'lambert', 'lambertb', 'model2']
    weight_functions = ['null', 'cfunc2']
    errors = ['y', 'xy']

    stepsize = 1000
    #stepsize = 10000
    job_input['turns'] = np.arange(stepsize, 1e5 + stepsize, stepsize).tolist()



    job_input['hdf_database'] = '/eos/user/m/mtitze/da/hdf_cluster/first_12/{0}/{0}_ioct{1}_beam{2}.h5'.format(year, ioct, beam_nr) # LHC data
    lhc_revolution_frequency = 11245.47506686197 # alternative: set up a beam using database_tools (& MAD-X tfs file)
    # e.g. '/afs/cern.ch/work/m/mtitze2/public/da_h5/first_12/{0}/{0}_inj450_twiss_ir4b{1}.tfs'.format(self.year, self.beam_nr) # LHC lattice

    job_input['revolution_frequency'] = lhc_revolution_frequency

    emit_cases = ['default']
    scale_to = [0, 1]

    #########################################
    
    jobs = [e for e in product(*[errors, pre_da_methods, da_modes, 
                             integration_methods, emit_cases, scale_to,
                      da_models, weight_functions])]
    job_columns = ['errors', 'pre_da_method', 'da_mode', 'int_method', 'emit_case', 'scale_to', 'da_model', 'weight']
    job_input['jobs'] = pd.DataFrame(jobs, columns=job_columns).to_dict()

    #job_input['lookup_key'] = 'bunch_count' # use e.g. 12 average emittances
    job_input['lookup_key'] = 'emit_index' # use all individual emittances (if run in conventional mode)

    job_input['emittances'] = [] # if empty, and use_default_emittances=True, then use emittances found in the individual SQL files


    with open(target_filename, 'w') as f:
        json.dump(job_input, f)
        
    return target_filename
    
if __name__ == "__main__":
    generate_input_dict(beam_nr=beam_nr, year=year, ioct=ioct)
