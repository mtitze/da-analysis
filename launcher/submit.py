import time as timelib
import os, sys, glob, argparse
import numpy as np
from shutil import copyfile
import json

'''
General-purpose launcher to run automatic jobs, either locally or on a cluster.

Usage:
0) Adjust the two script paths 'da_script_path' and 'python3_path' (see below) to your copy of the DA analysis suite.
1) Copy this script to a <directory> where you want to run the job. 
2) Create an input dictionary for the study of interest.
3) Launch the job(s).

Example:
python submit.py da.inp -v

Help about the commands can be displayed with "python submit.py -h".

Created by: malte.titze@cern.ch
'''


####################
# INPUT PATHS
####################
# the path to the location of the DA scripts:
da_script_path = "/eos/user/m/mtitze/gitlab/da_analysis"
# the path to your Python 3 binaries (needs 'activate' script to source, see (*) below)
python3_path = "/afs/cern.ch/work/m/mtitze/anaconda3/bin"


sys.path.append(da_script_path)


def load_input_file(filename):
    '''
    Load an input dictionary
    '''
    with open(filename) as f:
        job_input = json.load(f)
    return job_input

def store_input_file(filename, input_dict):
    '''
    Store a given dictionary as input file
    '''
    with open(filename, 'w') as f:
        json.dump(input_dict, f)


class job_handler:
    '''
    Generic class to collect and manage the job input.
    '''
    
    def __init__(self, filename, job_path, sql_files, verbose=True, **kwargs):
        '''
        filename:
          The name of the file which contains the input of the job in form of a dictionary (e.g. created with JSON).
        
        description:
          A description of the job to be performed.

        job_path:
          The directory where to write the files
        '''
        self._default_input_subfolder_name = 'input'
        self._default_output_subfolder_name = 'output'
        self._reference_sql_files_key = 'sql_reference_files'
        self._name = os.path.basename(__file__)
        self.set_required_keys()

        self.filename = filename
        self.job_input = load_input_file(self.filename)
        self.check_input_consistency(self.job_input)

        self.create_job_paths(job_path=job_path)
        self.sql_files = self.get_sql_files(sql_files)

        if verbose:
            print ('input dictionary:\n {}'.format(self.filename))
            print ('\n.db files:\n {}\n'.format('\n '.join(self.sql_files)))
            
    def set_required_keys(self):
        '''
        The required keys which must be inside the input file (dictionary).
        '''
        self._required_keys = ['description', 'jobs', 'emittances', 'turns']

    def create_inpath(self, job_path):
        inpath = '{}/{}'.format(job_path, self._default_input_subfolder_name)
        if not os.path.exists(inpath):
            os.mkdir(inpath)
        self.inpath = inpath
        
    def create_outpath(self, job_path):
        outpath = '{}/{}'.format(job_path, self._default_output_subfolder_name)
        if not os.path.exists(outpath):
            os.mkdir(outpath)
        self.outpath = outpath

    def create_job_paths(self, job_path=''):
        '''
        Set up the folder structure of the study. If nothing specified, create a subfolder in the current working directory of
        the form job_<current timestamp>. The output tables are then written to the subfolder 
          job_<current_timestamp>/self._default_output_subfolder_name
        '''
        if len(job_path) == 0:
            timestamp = int(timelib.time())
            job_subfolder = 'job_{}'.format(timestamp)
            # use current directory
            output_folder = os.getcwd()
            job_path = '{}/{}'.format(output_folder, job_subfolder)
        if not os.path.exists(job_path):
            os.mkdir(job_path)
        self.job_path = job_path
        self.create_outpath(job_path=self.job_path)
        self.create_inpath(job_path=self.job_path)

    def get_sql_files(self, sql_files):
        if len(sql_files) == 0:
            # the user has not specified any SQL files.
            # We try the sql files specified in this script, if present. 
            # Note that the cluster will always run with sql_files_str = './*.db',
            # so on a cluster we will not drop in this case at all.
            if self._reference_sql_files_key in self.job_input.keys():
                sql_files = self.job_input[self._reference_sql_files_key]
            else:
                print ('Attention: No reference .db files found.')
                # last try: we use those in the local folder.
                sql_files = glob.glob('./*.db')
        if len(sql_files) == 0:
            raise RuntimeError('No .db files found.')
        else:
            return sql_files
            
    def init_run(self, store_db=False, **kwargs):
        '''
        Copy the input files to the local job dir.
        '''
        # copy this script for the record into the job path
        script_local_copy = '{}/{}'.format(os.getcwd(), self._name)
        script_destination = '{}/{}'.format(self.job_path, self._name)
        if not os.path.exists(script_destination):
            if os.path.exists(script_local_copy):
                copyfile(script_local_copy, script_destination)
            else:
                copyfile(__file__, script_destination)
                 
        # copy the input if it does not exist in inpath
        new_filename = '{}/{}'.format(self.inpath, os.path.basename(self.filename))
        if not os.path.exists(new_filename):
            copyfile(self.filename, new_filename)
        self.filename = new_filename
                 
        if store_db:
            # note that store_db will not be True if this script is run on the cluster
            new_sql_files = []
            for sql_file in self.sql_files:
                target_sql_file = '{}/{}'.format(self.inpath, os.path.basename(sql_file))
                copyfile(sql_file, target_sql_file)
                new_sql_files.append(target_sql_file)
            self.sql_files = new_sql_files
            
        self.job_input['output_folder'] = self.outpath
        self.job_input['sql_files'] = self.sql_files
            
    def check_input_consistency(self, job_input={}):
        if len(job_input) == 0:
            job_input = self.job_input
        input_keys = job_input.keys()
        for key in self._required_keys:
            if not key in input_keys:
                raise NameError('key {} in input missing.'.format(key))


class job_handler_beamloss(job_handler):
    '''
    Class to manage beamloss jobs. The underlying profile model will be 'ws_dgaussian_link'.
    '''            
    def set_required_keys(self):
        job_handler.set_required_keys(self)
        self._required_keys += ['hdf_database', 'lookup_key', 'revolution_frequency']
            
    def prepare_profile_input(self, scaling_factors, revolution_frequency):
        from study.automatic import loss_study
        study = loss_study(**self.job_input)
        study.prepare_profile_data_input(path=self.inpath, scaling_factors=scaling_factors, freq=revolution_frequency)

    def init_run(self, **kwargs):
        '''
        Define the input parameters for the beamloss job.
        '''   
        job_handler.init_run(self, **kwargs)
        
        from data.ws import ws_dgaussian_link
        self.profile_model = ws_dgaussian_link(filename=self.job_input['hdf_database'], verbose=False)
        self.job_input['profile_model'] = self.profile_model # note that the profile model is used in the loss calculation only to use the loss formula
        self.job_input['input_folder'] = self.inpath

        # generate profile input files -- if required
        input_files = glob.glob('{}/*.parquet'.format(self.inpath))
        if len(input_files) == 0:
            import pandas as pd
            scaling_factors = pd.DataFrame(self.job_input['jobs'])['scale_to'].unique()
            self.prepare_profile_input(scaling_factors=scaling_factors, revolution_frequency=self.job_input['revolution_frequency'])
            input_files = glob.glob('{}/*.parquet'.format(self.inpath))
        self.job_input['profile_data_filenames'] = input_files


def interprete_sql_inp(sql_files):
    sql_files_fin = []
    for e in sql_files:
        if '*' not in e:
            sql_files_fin.append(e)
        else:
            sql_files_fin += glob.glob(e)
    return sql_files_fin
    

class htcondor_op:
    '''
    This class controls the creation and submission of HTCondor jobs, 
    depending on the nature of the study.
    '''

    def __init__(self, job_path, filename):
        '''
        job_path: path of the job to store input and output data
        
        filename: name of the file containing the input data
        '''
    
        self.universe = 'vanilla'

        #self.max_runtime = 100000
        self.max_runtime = 800000
        #self.max_runtime = 300
        
        self.job_flavour = '"nextweek"'
        #self.job_flavour = '"testmatch"'
        #self.job_flavour = '"espresso"'
        
        self.notify = 'Error'
        #self.notify = 'Always'
        self.accounting_group = '"group_u_BE.ABP.SLAP"'
        self.n_cpus = 1
        self.memory_mb = 3000 # reserved memory in MB
        self.da_script_path = da_script_path
        self.python3_path = python3_path
        
        self._name = os.path.basename(__file__)
        self.job_path = job_path
        self.filename = filename
        
        self.htcondor_submission_filename = 'cluster_submission_file.sub'
        self.htcondor_executable_filename = 'htc_run.sh'

    def create_submission_str(self, sql_files, n_jobs=1, verbose=False, store_db=False, **kwargs):
        if len(sql_files) == 0:
            raise IndexError('At least one SQL file required.')

        inp = {}
        inp['universe'] = self.universe
        inp['executable'] = self.htcondor_executable_filename
        
        inp['notify'] = self.notify

        inp['n_cpus'] = self.n_cpus
        inp['memory'] = self.memory_mb
        inp['acc_group'] = self.accounting_group
        inp['job_flavour'] = self.job_flavour
        inp['max_runtime'] = self.max_runtime
        
        inp['job_path'] = self.job_path
        inp['n_jobs'] = n_jobs
        
        filename_base = os.path.basename(self.filename)
        if n_jobs > 1:
            inp['filename_base'] = '$(Process)_{}'.format(filename_base)
            inp['process_str'] = '--process $(Process) '
            
        else:
            inp['filename_base'] = filename_base
 
            inp['process_str'] = ''
            
        inp['local_db_files'] = './*.db'

        inp['inpath'] = os.path.dirname(self.filename)
        inp['verbose'] = {False: '', True: '-v '}[verbose]
        
        # set the input files to be transfered to the cluster
        inp['input_files'] = '{}/{}, '.format(self.job_path, self._name) # this script
        inp['input_files'] += ', '.join(sql_files)
        
        if store_db:
            inp['transfer_output'] = ''
        else:
            inp['transfer_output'] = 'transfer_output_files = /dev/null'
        
        inp_str = '''
universe = {universe}
executable = {executable}
arguments = {filename_base} {verbose}--path {job_path} {process_str}--files {local_db_files}

output = {job_path}/da_analysis.$(ClusterId).$(Process).out
error = {job_path}/da_analysis.$(ClusterId).$(Process).err
log = {job_path}/da_analysis.$(ClusterId).log

should_transfer_files = YES
when_to_transfer_output = ON_EXIT_OR_EVICT

transfer_input_files = {inpath}/{filename_base}, {input_files}
{transfer_output}

notification = {notify}
RequestCpus = {n_cpus}
Request_Memory = {memory} Mb

+AccountingGroup = {acc_group}
+JobFlavour = {job_flavour}
+MaxRuntime = {max_runtime}
+SpoolOnEvict = False

# Send the job to Held state on failure. 
on_exit_hold = (ExitBySignal == True) || (ExitCode != 0)

# Periodically retry the jobs every 10 minutes, up to a maximum of 5 retries.
periodic_release =  (NumJobStarts < 5) && ((CurrentTime - EnteredCurrentStatus) > 600)

queue {n_jobs}
'''
        inp_str = inp_str.format(**inp)
        return inp_str
        
    def create_executable_str(self):
        '''
        Create executable bash script which is executed on the remote machine. This executable calls this python file (again). 
        (*) needs 'activate' bash in python3_path to set python paths.
        
        Note: Within conda activate script, the current arguments "$@" are looked at. Here the command line arguments are
        dedicated to our python script and not to be used in the activate script, so we have to create at temp variable.
        '''
        exe_str = '''#!/bin/bash -x

STUDY_CMDARGS="$@"
set --

source {python3_path}/activate

{python3_path}/python {executable} $STUDY_CMDARGS
'''

        exe_str = exe_str.format(**{'python3_path': self.python3_path, 'executable': self._name})
        return exe_str
        
    def submit(self, n_jobs=1, verbose=False, submit=True, store_db=False, **kwargs):
    
        executable_job_path = '{}/{}'.format(self.job_path, self.htcondor_executable_filename)
        htc_executable_str = self.create_executable_str()
        with open(executable_job_path, 'w') as f:
            f.write(htc_executable_str)
                 
        os.chmod(executable_job_path, 0o755)
            
        submission_job_path = '{}/{}'.format(self.job_path, self.htcondor_submission_filename)
        submission_str = self.create_submission_str(n_jobs=n_jobs, verbose=verbose, store_db=store_db, **kwargs)
        with open(submission_job_path, 'w') as f:
            f.write(submission_str)
                
        # submit the job
        print ('job directory:\n{}'.format(self.job_path))
        if submit:
            base_dir = os.getcwd()
            os.chdir(self.job_path)
            os.system('condor_submit {}'.format(self.htcondor_submission_filename))
            os.chdir(base_dir)
            
    def split_jobs(self, length, verbose=True):
        '''
        Takes an input dictionary and split its job key into lists having a length of at most 'maxlength'.
        '''
        filename_base = os.path.basename(self.filename)
        filename_dir = os.path.dirname(self.filename)
        
        import pandas as pd
        current_input_dict = load_input_file(self.filename)
        jobs = pd.DataFrame(current_input_dict['jobs'])
        if verbose:
            print ('lenght of job input: {}'.format(len(jobs)))
            print ('  number of subjobs: {}'.format(length))
        if length < len(jobs):
            if verbose:
                print ('Attention: Length of job input smaller than requested number of processes.')
                print ('Reducing number of processes to {}'.format(len(jobs)))
            length = len(jobs) # ensure that we do not split more than given length of jobs
        splitted_jobs = np.array_split(jobs, length)
        subjob_filenames = []
        for k in range(len(splitted_jobs)):
            joblist = splitted_jobs[k]
            subjob_dict = current_input_dict.copy()
            subjob_dict['jobs'] = joblist.to_dict()
            filename_subjob = '{}/{}_{}'.format(filename_dir, k, filename_base)
            subjob_filenames.append(filename_subjob)
            store_input_file(filename_subjob, subjob_dict)
            
        if verbose:
            print ('{} subjob input dictionaries created.'.format(len(subjob_filenames)))
        return subjob_filenames

def launch_job(filename, job_path='', process=None, sql_files=[], store_db=False, cluster=False, verbose=False, notest=True):
    '''
    Launche a job according to the given study description.
    '''
    
    # obtain study description in order to decide which class to instantiate and which study to run
    study_dict = load_input_file(filename)
    description = study_dict['description']
    print ('*** Initializing {} study ***'.format(description))
    
    sql_files = interprete_sql_inp(sql_files)

    # Guarantee that we have unique output and input files...
    output_table_filename = 'results'
    if not cluster and process != None and len(sql_files) > 0:
        new_sql_files = []
        # we assume that the job runs locally on a cluster with a given process ID. Therefore we have to change 
        # the local .db files so that the individual processes do not overwrite each other's .db files.
        for sql_file in sql_files:
            sql_basename = os.path.basename(sql_file) # should be = sql_file
            new_sql_filename = '{}_{}'.format(process, sql_basename)
            copyfile(sql_file, new_sql_filename)
            new_sql_files.append(new_sql_filename)
        sql_files = new_sql_files
        # ensures that the output tables are denoted differently, in the rare case that two results happen at the same second.
        output_table_filename = 'results_pid{}'.format(process)

    if description == 'DA':
        job_inp = job_handler(filename=filename, sql_files=sql_files, job_path=job_path)
    if description == 'beamloss':
        job_inp = job_handler_beamloss(filename=filename, sql_files=sql_files, job_path=job_path)
        
    job_inp.init_run(store_db=store_db)

    if cluster:
        # initialize & submit job(s) for htcondor
        htc = htcondor_op(filename=job_inp.filename, job_path=job_inp.job_path)
        
        n_jobs = 1
        if process != None:
            # split the job list in smaller parts
            subjob_filenames = htc.split_jobs(length=process, verbose=verbose)
            n_jobs = len(subjob_filenames)
            
        htc.submit(sql_files=job_inp.sql_files, n_jobs=n_jobs, submit=notest, store_db=store_db, verbose=verbose)
    elif notest:
        # run the job(s)
        import warnings
        warnings.filterwarnings("default", category=DeprecationWarning)

        # launch the study
        if description == 'DA':
            from study.automatic import sixdesk_study
            study = sixdesk_study(output_filename=output_table_filename, **job_inp.job_input)
            study.run(sql_files=job_inp.sql_files, jobs=job_inp.job_input['jobs'], turns=job_inp.job_input['turns'],
                      emittances=job_inp.job_input['emittances'], verbose=verbose)
        
        if description == 'beamloss':
            from study.automatic import loss_study
            study = loss_study(output_filename=output_table_filename, **job_inp.job_input)    
            study.run(sql_files=job_inp.sql_files, jobs=job_inp.job_input['jobs'], turns=job_inp.job_input['turns'],
                      emittances=job_inp.job_input['emittances'],
                      profile_data_filenames=job_inp.job_input['profile_data_filenames'], 
                      lookup_key=job_inp.job_input['lookup_key'], verbose=verbose)
    else:
        pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='DA analysis job launcher.', epilog="v2020.11.03 - malte.titze@cern.ch")
    parser.add_argument('study', type=str, help="study input reference file") 
    parser.add_argument('-c', action='store_true', dest='cluster', help='run on HTCondor')
    parser.add_argument('-prepare', action='store_false', dest='prepare', help='only prepare job files; do not run')
    parser.add_argument('-v', action='store_true', dest='verbose', help='verbose mode')
    parser.add_argument('-store_db', action='store_true', dest='store_db', help='store .db files in job input path')
    parser.add_argument('--path', type=str, dest='job_path', default='', help='folder to store input and output job data')
    parser.add_argument('--files', type=str, dest='sql_files', nargs='*', default=[], help=".db file(s) to be used. If not specified, use reference_sql_files in input. If input has no reference_sql_files, use local .db file(s).")
    parser.add_argument('--process', type=int, dest='N', default=None, help='If job is sent to cluster via -c, split input into N processes so they can be run in parallel. For demanding studies, this number should be set to the number of cases within the job. If -c is not given, this option will assume that we are on a cluster machine with process ID N and therefore will make local copies of the .db file(s) before starting the study.')
    parser_namespace = parser.parse_args()

    launch_job(filename=parser_namespace.study, job_path=parser_namespace.job_path, 
               sql_files=parser_namespace.sql_files, store_db=parser_namespace.store_db, process=parser_namespace.N,
               cluster=parser_namespace.cluster, verbose=parser_namespace.verbose, notest=parser_namespace.prepare) 

