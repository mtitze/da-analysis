import glob, json
import numpy as np
from itertools import product
import pandas as pd

'''
This is an example of how to generate the input dictionary required for a DA study.

(!) Do not forget to adjust the HTCondor parameters in submit.py
'''

year = 2017
ioct = 40
beam_nr = 1


def generate_input_dict(beam_nr, year, ioct):
    job_input = {}
    job_input['description'] = 'DA'


    job_input['sql_reference_files'] = glob.glob('/eos/user/m/mtitze/from_afswork/hl-lhc/sixtrack_data_beamloss/{}/beam{}/ioct{}/*.db'.format(year, beam_nr, ioct))
    #job_input['sql_reference_files'] = glob.glob('/afs/cern.ch/work/m/mtitze/sim/da/beamloss/test4/*.db')

    pre_da_methods = ['default']
    da_modes = ['rms', 'original']
    integration_methods = ['simpson', 'trapezoid']
    da_models = ['model2b']
    weight_functions = ['null']
    errors = ['none']

    stepsize = 1000
    #stepsize = 10000
    job_input['turns'] = np.arange(stepsize, 1e5 + stepsize, stepsize).tolist()

    target_filename = '/afs/cern.ch/work/m/mtitze/sim/da/beamloss/beam{}/{}-ioct{}/da.inp'.format(beam_nr, year, ioct)

    ########################################

    jobs = [e for e in product(*[errors, pre_da_methods, da_modes, 
                             integration_methods,
                      da_models, weight_functions])]
    job_columns = ['errors', 'pre_da_method', 'da_mode', 'int_method', 'da_model', 'weight']
    job_input['jobs'] = pd.DataFrame(jobs, columns=job_columns).to_dict()

    job_input['emittances'] = [] # if empty, and use_default_emittances=True, then use emittances found in the individual SQL files


    with open(target_filename, 'w') as f:
        json.dump(job_input, f)
        
    return target_filename
    
if __name__ == "__main__":
    generate_input_dict(beam_nr=beam_nr, year=year, ioct=ioct)
